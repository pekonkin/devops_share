#!/usr/bin/env bash

# предполагается, что этот скрипт будет выполняться на bamboo

set -vx
myhost=$1
password=${2:-12345678}
check_type=${3:-flygres}

SPO_DIR=$(mktemp -p /opt/ --dry-run spo_check.XXXXXX)

trap "rm -rf $SPO_DIR" 0

sudo git clone --quiet git@git.swemel.grp:test/spo-deploy.git $SPO_DIR
sudo chown -R bamboo:bamboo $SPO_DIR
python3 $SPO_DIR/spo_deploy_checker.py -s $check_type -d ${myhost} -p ${password} || exit 1
