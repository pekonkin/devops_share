#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh


sudo DomainControl l1c1.pu                     stop
sudo DomainControl l1c1.repo.pu                     stop
sudo DomainControl l1c1.gimsr.pu                     stop
sudo DomainControl l1c1.gimspd.pu                     stop
sudo DomainControl l1c1.gimsps.pu                     stop
sudo DomainControl l1c1.int1.pu                     stop
sudo DomainControl l1c1.int2.pu                     stop
sudo DomainControl l1c1.kuf1.pu                     stop
sudo DomainControl l1c1.kuf2.pu                     stop
sudo DomainControl l1c1.sgio1.pu                     stop
sudo DomainControl l1c1.sgio2.pu                     stop
sudo DomainControl l1c1.psql1.pu                     stop
sudo DomainControl l1c1.psql2.pu                     stop
sudo DomainControl l1c1.briz.pu                     stop
sudo DomainControl l1c1.sbor.pu                     stop
sudo DomainControl l1c1.iusm1.pu                     stop
sudo DomainControl l1c1.iusm2.pu                     stop
sudo DomainControl l1c1.esimo1.pu                     stop
sudo DomainControl l1c1.esimo2.pu                     stop


$NAR l1c1.pu               em1-labeled 192.168.134.0/24
$NAR l1c1.repo.pu           em1-labeled 192.168.134.0/24
$NAR l1c1.gimsr.pu           em1-labeled 192.168.134.0/24
$NAR l1c1.gimspd.pu           em1-labeled 192.168.134.0/24
$NAR l1c1.gimsps.pu           em1-labeled 192.168.134.0/24
$NAR l1c1.int1.pu           em1-labeled 192.168.134.0/24
$NAR l1c1.int2.pu           em1-labeled 192.168.134.0/24
$NAR l1c1.kuf1.pu           em1-labeled 192.168.134.0/24
$NAR l1c1.kuf2.pu           em1-labeled 192.168.134.0/24
$NAR l1c1.sgio1.pu           em1-labeled 192.168.134.0/24
$NA2 l1c1.sgio2.pu           em1-labeled 192.168.134.0/24
$NA2 l1c1.psql1.pu           em1-labeled 192.168.134.0/24
$NA2 l1c1.psql2.pu           em1-labeled 192.168.134.0/24
$NAR l1c1.briz.pu           em1-labeled 192.168.134.0/24
$NAR l1c1.sbor.pu           em1-labeled 192.168.134.0/24
$NAR l1c1.iusm1.pu           em1-labeled 192.168.134.0/24 192.168.172.254
$NAR l1c1.iusm2.pu           em1-labeled 192.168.134.0/24 192.168.172.254
$NAR l1c1.esimo1.pu           em1-labeled 192.168.134.0/24 192.168.172.254
$NAR l1c1.esimo2.pu           em1-labeled 192.168.134.0/24 192.168.172.254
$NAR l1c1.rcod.pu           em1-labeled 192.168.134.0/24 192.168.172.254
