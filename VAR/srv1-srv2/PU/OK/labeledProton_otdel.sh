#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh

VRAM=1000 VDISKSIZE=40000000000 $DC ADMINLOW em1-labeled 192.168.170.1/24 sl-data
# параметры ВМ и интерфейс, смотрящий в сеть данных (приложений)

VCPUS=2 VRAM=4000 VDISKSIZE=70000000000 KICKSTARTFILE=term_wf.ks  $DC l1c1.pu         em1-labeled 192.168.172.1/24     sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=40000000000   KICKSTARTFILE=repoz.ks    $DC  l1c1.repo.pu   em1-labeled 192.168.172.2/24  sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=100000000000  KICKSTARTFILE=astk.ks     $DC  l1c1.gimsr.pu   em1-labeled 192.168.172.5/24    sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=100000000000  KICKSTARTFILE=astk.ks     $DC  l1c1.gimspd.pu   em1-labeled 192.168.172.8/24    sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=100000000000  KICKSTARTFILE=astk.ks     $DC l1c1.gimsps.pu   em1-labeled 192.168.172.11/24    sl-data
VCPUS=2 VRAM=2000 VDISKSIZE=40000000000  KICKSTARTFILE=wf_int.ks    $DC  l1c1.int.pu   em1-labeled 10.60.60.11/24  sl-data
VCPUS=2 VRAM=2000 VDISKSIZE=40000000000  KICKSTARTFILE=wf_int.ks    $DC  l1c1.int.pu2   em1-labeled 10.60.60.10/24  sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=40000000000  KICKSTARTFILE=wf.ks       $DC l1c1.kuf1.pu    em1-labeled 192.168.172.20/24    sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=40000000000  KICKSTARTFILE=wf.ks       $DC l1c1.kuf2.pu    em1-labeled 192.168.172.21/24    sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=100000000000  KICKSTARTFILE=gis.ks      $DC l1c1.sgio1.pu    em1-labeled 192.168.172.26/24    sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=100000000000  KICKSTARTFILE=gis.ks      $DC l1c1.sgio2.pu    em1-labeled 192.168.172.27/24    sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=350000000000  VDISKTYPE=unenc KICKSTARTFILE=psql.ks $DC l1c1.psql1.pu em1-labeled 192.168.172.28/24 sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=350000000000  VDISKTYPE=unenc KICKSTARTFILE=psql.ks $DC l1c1.psql2.pu em1-labeled 192.168.172.29/24 sl-data
VCPUS=1 VRAM=4000 VDISKSIZE=40000000000  KICKSTARTFILE=astk.ks     $DC l1c1.briz.pu   em1-labeled 192.168.172.37/24   sl-data
VCPUS=1 VRAM=4000 VDISKSIZE=40000000000  KICKSTARTFILE=astk.ks     $DC l1c1.sbor.pu   em1-labeled 192.168.172.39/24    sl-data
VCPUS=1 VRAM=4000 VDISKSIZE=40000000000  KICKSTARTFILE=astk.ks     $DC l1c1.iusm1.pu   em1-labeled 192.168.172.42/24    sl-data
VCPUS=1 VRAM=4000 VDISKSIZE=40000000000  KICKSTARTFILE=astk.ks     $DC l1c1.iusm2.pu   em1-labeled 192.168.172.43/24    sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=40000000000  KICKSTARTFILE=astk.ks     $DC l1c1.esimo1.pu   em1-labeled 192.168.172.51/24    sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=40000000000  KICKSTARTFILE=astk.ks     $DC l1c1.esimo2.pu   em1-labeled 192.168.172.52/24    sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=40000000000  KICKSTARTFILE=astk.ks     $DC l1c1.rcod.pu   em1-labeled 192.168.172.70/24    sl-data
sleep 60

sudo DomainControl l1c1.pu                     stop
sudo DomainControl l1c1.repo.pu                     stop
sudo DomainControl l1c1.gimsr.pu                     stop
sudo DomainControl l1c1.gimspd.pu                     stop
sudo DomainControl l1c1.gimsps.pu                     stop
sudo DomainControl l1c1.int1.pu                     stop
sudo DomainControl l1c1.int2.pu                     stop
sudo DomainControl l1c1.kuf1.pu                     stop
sudo DomainControl l1c1.kuf2.pu                     stop
sudo DomainControl l1c1.sgio1.pu                     stop
sudo DomainControl l1c1.sgio2.pu                     stop
sudo DomainControl l1c1.psql1.pu                     stop
sudo DomainControl l1c1.psql2.pu                     stop
sudo DomainControl l1c1.briz.pu                     stop
sudo DomainControl l1c1.sbor.pu                     stop
sudo DomainControl l1c1.iusm1.pu                     stop
sudo DomainControl l1c1.iusm2.pu                     stop
sudo DomainControl l1c1.esimo1.pu                     stop
sudo DomainControl l1c1.esimo2.pu                     stop
sudo DomainControl l1c1.rcod.pu                     stop

# добавление IP-адреса сети общей зоны в файл описания ВМ (xml) и gateway

$NAR2 l1c1.pu               em1-labeled 192.168.134.0/24 192.168.172.254
$NAR2 l1c1.repo.pu           em1-labeled 192.168.134.0/24 192.168.172.254
$NAR2 l1c1.gimsr.pu           em1-labeled 192.168.134.0/24 192.168.172.254
$NAR2 l1c1.gimspd.pu           em1-labeled 192.168.134.0/24 192.168.172.254
$NAR2 l1c1.gimsps.pu           em1-labeled 192.168.134.0/24 192.168.172.254
$NAR2 l1c1.int1.pu           em1-labeled 192.168.134.0/24 192.168.172.254
$NAR2 l1c1.int2.pu           em1-labeled 192.168.134.0/24 192.168.172.254
$NAR2 l1c1.kuf1.pu           em1-labeled 192.168.134.0/24 192.168.172.254
$NAR2 l1c1.kuf2.pu           em1-labeled 192.168.134.0/24 192.168.172.254
$NAR2 l1c1.sgio1.pu           em1-labeled 192.168.134.0/24 192.168.172.254
$NAR2 l1c1.sgio2.pu           em1-labeled 192.168.134.0/24 192.168.172.254
$NAR2 l1c1.psql1.pu           em1-labeled 192.168.134.0/24 192.168.172.254
$NAR2 l1c1.psql2.pu           em1-labeled 192.168.134.0/24 192.168.172.254
$NAR2 l1c1.briz.pu           em1-labeled 192.168.134.0/24 192.168.172.254
$NAR2 l1c1.sbor.pu           em1-labeled 192.168.134.0/24 192.168.172.254
$NAR2 l1c1.iusm1.pu           em1-labeled 192.168.134.0/24 192.168.172.254
$NAR2 l1c1.iusm2.pu           em1-labeled 192.168.134.0/24 192.168.172.254
$NAR2 l1c1.esimo1.pu           em1-labeled 192.168.134.0/24 192.168.172.254
$NAR2 l1c1.esimo2.pu           em1-labeled 192.168.134.0/24 192.168.172.254
$NAR2 l1c1.rcod.pu           em1-labeled 192.168.134.0/24 192.168.172.254

# добавляем ещё один интерфейс для терминальной сети (например, em2-labeled)

$BA l1c1.pu     em2-labeled 172.16.152.1/24  sl-term

sudo DomainControl l1c1.pu                     start
sudo DomainControl l1c1.repo.pu                     start
sudo DomainControl l1c1.gimsr.pu                     start
sudo DomainControl l1c1.gimspd.pu                     start
sudo DomainControl l1c1.gimsps.pu                     start
sudo DomainControl l1c1.int1.pu                     start
sudo DomainControl l1c1.int2.pu                     start
sudo DomainControl l1c1.kuf1.pu                     start
sudo DomainControl l1c1.kuf2.pu                     start
sudo DomainControl l1c1.sgio1.pu                     start
sudo DomainControl l1c1.sgio2.pu                     start
sudo DomainControl l1c1.psql1.pu                     start
sudo DomainControl l1c1.psql2.pu                     start
sudo DomainControl l1c1.briz.pu                     start
sudo DomainControl l1c1.sbor.pu                     start
sudo DomainControl l1c1.iusm1.pu                     start
sudo DomainControl l1c1.iusm2.pu                     start
sudo DomainControl l1c1.esimo1.pu                     start
sudo DomainControl l1c1.esimo2.pu                     start
sudo DomainControl l1c1.rcod.pu                     start
