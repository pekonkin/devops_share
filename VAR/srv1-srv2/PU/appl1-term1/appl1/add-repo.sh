#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh


sudo DomainControl l2c2.wf.pu1                          stop
sudo DomainControl l2c4.wf.pu1                          stop
sudo DomainControl l2c8.wf.pu1                          stop
sudo DomainControl l2c32.wf.pu1                         stop
sudo DomainControl l2c64.wf.pu1                         stop
sudo DomainControl l2c256.analit.pu1                    stop
sudo DomainControl l0c1.ob.pu1                          stop
sudo DomainControl l2c2.wf.pu2			                stop
sudo DomainControl l2c4.wf.pu2			                stop
sudo DomainControl l2c8.wf.pu2			                stop
sudo DomainControl l2c32.wf.pu2			                stop
sudo DomainControl l2c64.wf.pu2			                stop
sudo DomainControl l0c1.ob.pu2                          stop
sudo DomainControl l2c128.wf.pu2                        stop
sudo DomainControl l2c256.analit.pu2                    stop


$NAR2 l2c2.wf.pu2 			em1-labeled 10.1.53.205/32
$NAR2 l2c4.wf.pu2 			em1-labeled 10.1.53.205/32
$NAR2 l2c8.wf.pu2 			em1-labeled 10.1.53.205/32
$NAR2 l2c32.wf.pu2      	em1-labeled 10.1.53.205/32
$NAR2 l2c64.wf.pu2      	em1-labeled 10.1.53.205/32
$NAR2 l2c128.wf.pu2  	   	em1-labeled 10.1.53.205/32
$NAR2 l2c256.analit.pu2     em1-labeled 10.1.53.205/32
$NAR2 l0c1.ob.pu2			em1-labeled 10.1.53.205/32
$NAR2 l2c2.wf.pu1 			em1-labeled 10.1.53.205/32
$NAR2 l2c4.wf.pu1 			em1-labeled 10.1.53.205/32
$NAR2 l2c8.wf.pu1 			em1-labeled 10.1.53.205/32
$NAR2 l2c32.wf.pu1      	em1-labeled 10.1.53.205/32
$NAR2 l2c64.wf.pu1      	em1-labeled 10.1.53.205/32
$NAR2 l2c256.analit.pu1     em1-labeled 10.1.53.205/32
$NAR2 l0c1.ob.pu1			em1-labeled 10.1.53.205/32



sudo DomainControl l2c2.wf.pu1                          start
sudo DomainControl l2c4.wf.pu1                          start
sudo DomainControl l2c8.wf.pu1                          start
sudo DomainControl l2c32.wf.pu1                         start
sudo DomainControl l2c64.wf.pu1                         start
sudo DomainControl l2c256.analit.pu1                    start
sudo DomainControl l0c1.ob.pu1                          start
sudo DomainControl l2c2.wf.pu2			                start
sudo DomainControl l2c4.wf.pu2			                start
sudo DomainControl l2c8.wf.pu2			                start
sudo DomainControl l2c32.wf.pu2			                start
sudo DomainControl l2c64.wf.pu2			                start
sudo DomainControl l0c1.ob.pu2                          start
sudo DomainControl l2c128.wf.pu2                        start
sudo DomainControl l2c256.analit.pu2                    start