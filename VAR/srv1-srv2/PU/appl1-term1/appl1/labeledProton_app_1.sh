#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh


# параметры ВМ и интерфейс, смотрящий в сеть данных (приложений)

VRAM=1000 VDISKSIZE=40000000000 $DC ADMINLOW em1-labeled  192.168.120.1/24 sl-data


VCPUS=2 VRAM=9000 VDISKSIZE=40000000000  KICKSTARTFILE=wf.ks      $DC l2c1.wf.pu1           em1-labeled 192.168.126.6/24   sl-data
VCPUS=2 VRAM=8000 VDISKSIZE=40000000000  KICKSTARTFILE=wf.ks      $DC l2c2.wf.pu1           em1-labeled 192.168.123.6/24   sl-data
VCPUS=2 VRAM=6000  VDISKSIZE=40000000000  KICKSTARTFILE=wf.ks      $DC l2c4.wf.pu1           em1-labeled 192.168.124.6/24   sl-data
VCPUS=2 VRAM=6000  VDISKSIZE=40000000000  KICKSTARTFILE=wf.ks      $DC l2c8.wf.pu1           em1-labeled 192.168.125.6/24   sl-data
VCPUS=2 VRAM=6000  VDISKSIZE=40000000000  KICKSTARTFILE=wf.ks      $DC l2c32.wf.pu1          em1-labeled 192.168.127.6/24   sl-data
VCPUS=2 VRAM=6000  VDISKSIZE=40000000000  KICKSTARTFILE=wf.ks      $DC l2c64.wf.pu1          em1-labeled 192.168.128.6/24   sl-data
VCPUS=2 VRAM=6000  VDISKSIZE=40000000000  KICKSTARTFILE=wf.ks      $DC l2c128.wf.pu1         em1-labeled 192.168.129.6/24   sl-data
VCPUS=2 VRAM=15000 VDISKSIZE=40000000000  KICKSTARTFILE=wf.ks  $DC l2c256.analit.pu1     em1-labeled 192.168.130.7/24   sl-data
VCPUS=2 VRAM=6000  VDISKSIZE=40000000000  KICKSTARTFILE=wf.ks      $DC l0c1.pu1           em1-labeled 192.168.132.1/24   sl-data
VCPUS=2 VRAM=9000 VDISKSIZE=40000000000  KICKSTARTFILE=wf.ks      $DC l2c8388607.wf.pu1     em1-labeled 10.50.50.1/28  sl-data
VCPUS=2 VRAM=9000 VDISKSIZE=40000000000  KICKSTARTFILE=gis.ks     $DC l2c8388607.gis.pu1    em1-labeled 192.168.134.21/24  sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=350000000000 VDISKTYPE=unenc KICKSTARTFILE=psql.ks $DC l2c8388607.psql.pu1   em1-labeled 192.168.134.13/24  sl-data
VCPUS=2 VRAM=12000  VDISKSIZE=350000000000  KICKSTARTFILE=psql.ks    $DC l2c8388607.kuf.pu1    em1-labeled 192.168.134.24/24  sl-data
VCPUS=2 VRAM=9000 VDISKSIZE=40000000000  KICKSTARTFILE=wf.ks    $DC l2c8388607.sbor.pu1   em1-labeled 192.168.134.26/28  sl-data
VCPUS=2 VRAM=1000 VDISKSIZE=40000000000   KICKSTARTFILE=repoz.ks    $DC  l2C8388607.repo.pu1   em1-labeled 192.168.134.29/24  sl-data

sleep 60

sudo DomainControl ADMINLOW	                            stop
sudo DomainControl l2c1.wf.pu1                          stop
sudo DomainControl l2c2.wf.pu1                          stop
sudo DomainControl l2c4.wf.pu1                          stop
sudo DomainControl l2c8.wf.pu1                          stop
sudo DomainControl l2c32.wf.pu1                         stop
sudo DomainControl l2c64.wf.pu1                         stop
sudo DomainControl l2c128.wf.pu1                        stop
sudo DomainControl l2c256.analit.pu1                    stop
sudo DomainControl l0c1.pu1                             stop
sudo DomainControl l2c8388607.wf.pu1                    stop
sudo DomainControl l2c8388607.gis.pu1                   stop
sudo DomainControl l2c8388607.psql.pu1                  stop
sudo DomainControl l2c8388607.kuf.pu1                   stop
sudo DomainControl l2c8388607.sbor.pu1                  stop
sudo DomainControl l2C8388607.repo.pu1                  stop

# добавление IP-адреса сети общей зоны в файл описания ВМ (xml) и gateway

$NAR2 l2c1.wf.pu1       	em1-labeled 192.168.134.0/24 192.168.126.254
$NAR2 l2c2.wf.pu1 			em1-labeled 192.168.134.0/24 192.168.123.254
$NAR2 l2c4.wf.pu1 			em1-labeled 192.168.134.0/24 192.168.124.254
$NAR2 l2c8.wf.pu1 			em1-labeled 192.168.134.0/24 192.168.125.254
$NAR2 l2c32.wf.pu1      	em1-labeled 192.168.134.0/24 192.168.127.254
$NAR2 l2c64.wf.pu1      	em1-labeled 192.168.134.0/24 192.168.128.254
$NAR2 l2c128.wf.pu1     	em1-labeled 192.168.134.0/24 192.168.129.254
$NAR2 l2c256.analit.pu1     em1-labeled 192.168.134.0/24 192.168.130.254
$NAR2 l0c1.pu1              em1-labeled 192.168.134.0/24 192.168.132.254


# доступ до adminlow
$NAR2 l2c8388607.wf.pu1 	em1-labeled 192.168.126.0/24 192.168.134.254
$NAR2 l2c8388607.gis.pu1 	em1-labeled 192.168.126.0/24 192.168.134.254
$NAR2 l2c8388607.psql.pu1   em1-labeled 192.168.126.0/24 192.168.134.254
$NAR2 l2c8388607.kuf.pu1    em1-labeled 192.168.126.0/24 192.168.134.254
$NAR2 l2c8388607.sbor.pu1	em1-labeled 192.168.126.0/24 192.168.134.254
$NAR2 l2C8388607.repo.pu1	 em1-labeled 192.168.126.0/24 192.168.134.254
$NAR l2c1.wf.pu1       	    em1-labeled 192.168.120.0/24
$NAR l2c2.wf.pu1 			em1-labeled 192.168.120.0/24
$NAR l2c4.wf.pu1 			em1-labeled 192.168.120.0/24
$NAR l2c8.wf.pu1 			em1-labeled 192.168.120.0/24
$NAR l2c32.wf.pu1      	    em1-labeled 192.168.120.0/24
$NAR l2c64.wf.pu1      	    em1-labeled 192.168.120.0/24
$NAR l2c128.wf.pu1     	    em1-labeled 192.168.120.0/24
$NAR l2c256.analit.pu1      em1-labeled 192.168.120.0/24
$NAR l0c1.pu1               em1-labeled 192.168.120.0/24


# фильтр с какой сети разрешить обмен (192.168.134.0/24 - общая зона, 192.168.122.0/24 - пользовательские зоны)


$NAR l2c8388607.wf.pu1 		em1-labeled 192.168.120.0/24 192.168.123.0/24 192.168.124.0/24 192.168.125.0/24 192.168.127.0/24 192.168.128.0/24 192.168.129.0/24 192.168.130.0/24
$NAR l2c8388607.gis.pu1 	em1-labeled 192.168.120.0/24 192.168.123.0/24 192.168.124.0/24 192.168.125.0/24 192.168.127.0/24 192.168.128.0/24 192.168.129.0/24 192.168.130.0/24
$NAR l2c8388607.psql.pu1 	em1-labeled 192.168.120.0/24 192.168.123.0/24 192.168.124.0/24 192.168.125.0/24 192.168.127.0/24 192.168.128.0/24 192.168.129.0/24 192.168.130.0/24
$NAR l2c8388607.kuf.pu1 	em1-labeled 192.168.120.0/24 192.168.123.0/24 192.168.124.0/24 192.168.125.0/24 192.168.127.0/24 192.168.128.0/24 192.168.129.0/24 192.168.130.0/24
$NAR l2c8388607.sbor.pu1	em1-labeled 192.168.120.0/24 192.168.123.0/24 192.168.124.0/24 192.168.125.0/24 192.168.127.0/24 192.168.128.0/24 192.168.129.0/24 192.168.130.0/24





# добавляем ещё один интерфейс для терминальной сети (например, em2-labeled)

$BA ADMINLOW em2-labeled 172.16.120.3/24 sl-ipxe

sudo DomainControl ADMINLOW                          start
sudo DomainControl l2c1.wf.pu1 				start
sudo DomainControl l2c2.wf.pu1 				start
sudo DomainControl l2c4.wf.pu1 				start
sudo DomainControl l2c8.wf.pu1 				start
sudo DomainControl l2c32.wf.pu1 				start
sudo DomainControl l2c64.wf.pu1 				start
sudo DomainControl l2c128.wf.pu1 				start
sudo DomainControl l2c256.analit.pu1 				start
sudo DomainControl l0c1.pu1 				start
sudo DomainControl l2c8388607.wf.pu1 				start
sudo DomainControl l2c8388607.gis.pu1 				start
sudo DomainControl l2c8388607.psql.pu1 				start
sudo DomainControl l2c8388607.kuf.pu1 				start
sudo DomainControl l2c8388607.sbor.pu1 				start
sudo DomainControl l2C8388607.repo.pu1 				start
