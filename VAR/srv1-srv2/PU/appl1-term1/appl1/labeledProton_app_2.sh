#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh


# параметры ВМ и интерфейс, смотрящий в сеть данных (приложений)

VCPUS=2 VRAM=9000 VDISKSIZE=40000000000  KICKSTARTFILE=wf.ks      $DC l2c1.wf.pu2           em1-labeled 192.168.126.7/24   sl-data
VCPUS=2 VRAM=8000 VDISKSIZE=40000000000  KICKSTARTFILE=wf.ks      $DC l2c2.wf.pu2           em1-labeled 192.168.123.7/24   sl-data
VCPUS=2 VRAM=6000  VDISKSIZE=40000000000  KICKSTARTFILE=wf.ks      $DC l2c4.wf.pu2           em1-labeled 192.168.124.7/24   sl-data
VCPUS=2 VRAM=6000  VDISKSIZE=40000000000  KICKSTARTFILE=wf.ks      $DC l2c8.wf.pu2           em1-labeled 192.168.125.7/24   sl-data
VCPUS=2 VRAM=6000  VDISKSIZE=40000000000  KICKSTARTFILE=wf.ks      $DC l2c32.wf.pu2          em1-labeled 192.168.127.7/24   sl-data
VCPUS=2 VRAM=6000  VDISKSIZE=40000000000  KICKSTARTFILE=wf.ks      $DC l2c64.wf.pu2          em1-labeled 192.168.128.7/24   sl-data
VCPUS=2 VRAM=6000  VDISKSIZE=40000000000  KICKSTARTFILE=wf.ks      $DC l2c128.wf.pu2         em1-labeled 192.168.129.7/24   sl-data
VCPUS=2 VRAM=15000 VDISKSIZE=40000000000  KICKSTARTFILE=wf.ks      $DC l2c256.analit.pu2     em1-labeled 192.168.130.8/24   sl-data
VCPUS=2 VRAM=14000 VDISKSIZE=40000000000  KICKSTARTFILE=wf.ks      $DC l2c8388607.wf.pu2     em1-labeled 10.50.50.2/28  sl-data
VCPUS=2 VRAM=10000 VDISKSIZE=40000000000  KICKSTARTFILE=gis.ks     $DC l2c8388607.gis.pu2    em1-labeled 192.168.134.22/24  sl-data
VCPUS=2 VRAM=13000 VDISKSIZE=350000000000  VDISKTYPE=unenc KICKSTARTFILE=psql.ks   $DC l2c8388607.psql.pu2   em1-labeled 192.168.134.14/24  sl-data
VCPUS=2 VRAM=6000  VDISKSIZE=350000000000  KICKSTARTFILE=psql.ks   $DC l2c8388607.kuf.pu2    em1-labeled 192.168.134.25/24  sl-data
VCPUS=2 VRAM=6000  VDISKSIZE=40000000000  KICKSTARTFILE=wf.ks      $DC l0c1.ob.pu2           em1-labeled 192.168.132.3/24   sl-data

sleep 60

sudo DomainControl l2c1.wf.pu2			        stop
sudo DomainControl l2c2.wf.pu2			        stop
sudo DomainControl l2c4.wf.pu2			        stop
sudo DomainControl l2c8.wf.pu2			        stop
sudo DomainControl l2c32.wf.pu2			        stop
sudo DomainControl l2c64.wf.pu2			        stop
sudo DomainControl l2c128.wf.pu2                stop
sudo DomainControl l2c256.analit.pu2			stop
sudo DomainControl l2c8388607.wf.pu2			stop
sudo DomainControl l2c8388607.gis.pu2			stop
sudo DomainControl l2c8388607.psql.pu2			stop
sudo DomainControl l2c8388607.kuf.pu2			stop
sudo DomainControl l0c1.ob.pu2                  stop

# добавление IP-адреса сети общей зоны в файл описания ВМ (xml) и gateway

$NAR2 l2c1.wf.pu2       	em1-labeled 192.168.134.0/24 192.168.126.254
$NAR2 l2c2.wf.pu2 			em1-labeled 192.168.134.0/24 192.168.123.254
$NAR2 l2c4.wf.pu2 			em1-labeled 192.168.134.0/24 192.168.124.254
$NAR2 l2c8.wf.pu2 			em1-labeled 192.168.134.0/24 192.168.125.254
$NAR2 l2c32.wf.pu2      	em1-labeled 192.168.134.0/24 192.168.127.254
$NAR2 l2c64.wf.pu2      	em1-labeled 192.168.134.0/24 192.168.128.254
$NAR2 l2c128.wf.pu2     	em1-labeled 192.168.134.0/24 192.168.129.254
$NAR2 l2c256.analit.pu2     em1-labeled 192.168.134.0/24 192.168.134.254
$NAR2 l0c1.ob.pu2			em1-labeled 192.168.134.0/24 192.168.132.254
$NAR2 l2c8388607.wf.pu2 	em1-labeled 192.168.120.0/24 192.168.134.254
$NAR2 l2c8388607.gis.pu2	em1-labeled 192.168.120.0/24 192.168.134.254
$NAR2 l2c8388607.psql.pu2   em1-labeled 192.168.120.0/24 192.168.134.254
$NAR2 l2c8388607.kuf.pu2    em1-labeled 192.168.120.0/24 192.168.134.254

# фильтр с какой сети разрешить обмен (192.168.134.0/24 - общая зона, 192.168.122.0/24 - пользовательские зоны)

$NAR l2c1.wf.pu2  	     	em1-labeled 192.168.120.0/24
$NAR l2c2.wf.pu2 			em1-labeled 192.168.120.0/24
$NAR l2c4.wf.pu2 			em1-labeled 192.168.120.0/24
$NAR l2c8.wf.pu2 			em1-labeled 192.168.120.0/24
$NAR l2c32.wf.pu2      	    em1-labeled 192.168.120.0/24
$NAR l2c64.wf.pu2      	    em1-labeled 192.168.120.0/24
$NAR l2c128.wf.pu2  	   	em1-labeled 192.168.120.0/24
$NAR l2c256.analit.pu2      em1-labeled 192.168.120.0/24
$NAR l0c1.ob.pu2			em1-labeled 192.168.120.0/24
$NAR l2c8388607.wf.pu2 		em1-labeled 192.168.126.0/24 192.168.123.0/24 192.168.124.0/24 192.168.125.0/24 192.168.127.0/24 192.168.128.0/24 192.168.129.0/24 192.168.130.0/24
$NAR l2c8388607.gis.pu2 	em1-labeled 192.168.126.0/24 192.168.123.0/24 192.168.124.0/24 192.168.125.0/24 192.168.127.0/24 192.168.128.0/24 192.168.129.0/24 192.168.130.0/24
$NAR l2c8388607.psql.pu2 	em1-labeled 192.168.126.0/24 192.168.123.0/24 192.168.124.0/24 192.168.125.0/24 192.168.127.0/24 192.168.128.0/24 192.168.129.0/24 192.168.130.0/24
$NAR l2c8388607.kuf.pu2 	em1-labeled 192.168.126.0/24 192.168.123.0/24 192.168.124.0/24 192.168.125.0/24 192.168.127.0/24 192.168.128.0/24 192.168.129.0/24 192.168.130.0/24
# добавляем ещё один интерфейс для терминальной сети (например, em2-labeled)

sudo DomainControl l2c1.wf.pu2                  start
sudo DomainControl l2c2.wf.pu2                  start
sudo DomainControl l2c4.wf.pu2                  start
sudo DomainControl l2c8.wf.pu2                  start
sudo DomainControl l2c32.wf.pu2                 start
sudo DomainControl l2c64.wf.pu2                 start
sudo DomainControl l2c128.wf.pu2                start
sudo DomainControl l2c256.analit.pu2			start
sudo DomainControl l2c8388607.wf.pu2			start
sudo DomainControl l2c8388607.gis.pu2			start
sudo DomainControl l2c8388607.psql.pu2			start
sudo DomainControl l2c8388607.kuf.pu2			start
sudo DomainControl l0c1.ob.pu2                  start
