#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh

# параметры ВМ и интерфейс, смотрящий в сеть данных (приложений)
VCPUS=2 VRAM=8000 VDISKSIZE=70000000000 KICKSTARTFILE=term.ks $DC l2c1.pu1 em1-labeled 192.168.126.1/24 sl-data
VCPUS=2 VRAM=8000 VDISKSIZE=70000000000 KICKSTARTFILE=term.ks $DC l2c1.pu2 em1-labeled 192.168.126.2/24 sl-data
VCPUS=2 VRAM=8000 VDISKSIZE=70000000000 KICKSTARTFILE=term.ks $DC l0c1.pu2 em1-labeled 192.168.132.1/24  sl-data


sleep 60


sudo DomainControl l2c1.pu1 stop
sudo DomainControl l2c1.pu2 stop
sudo DomainControl l0c1.pu2 stop


# доступ к ADMINLOW (LDAP) через маршрутизатор зоны (например, 192.168.122.254)
$NAR2 l2c1.pu1   em1-labeled  192.168.120.0/24 192.168.126.254
$NAR2 l2c2.pu2   em1-labeled  192.168.120.0/24 192.168.126.254
$NAR2 l0c1.pu2   em1-labeled  192.168.120.0/24 192.168.132.254

# доступ в общую зону
$NAR2 l2c1.pu1   em1-labeled 192.168.134.0/24
$NAR2 l2c1.pu1   em1-labeled 192.168.134.0/24
$NAR2 l0c1.pu2   em1-labeled 192.168.134.0/24

# доступ VSM в теминальную сеть
#$BA l0c1.pu2   em2-labeled 10.1.51.65/26 sl-term
$BA l2c1.pu1   em2-labeled 172.16.126.3/24 sl-term
$BA l2c1.pu2   em2-labeled 172.16.126.4/24 sl-term

sudo DomainControl l2c1.pu1 start
sudo DomainControl l2c1.pu2 start
sudo DomainControl l0c1.pu2 start
