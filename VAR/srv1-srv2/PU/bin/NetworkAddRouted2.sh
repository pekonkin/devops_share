#!/bin/bash

ZONE=$1
INTERFACE=$2
NETWORK=$3
GATEWAY=$4

execscript() {
sudo NetworkAddRouted $ZONE << EOF
$INTERFACE
$NETWORK
$GATEWAY
none
EOF

}

execscript
shift


