#!/bin/bash

ZONE=$1
INTERFACE=$2
GATEWAY=${!#}
let NETWORKCOUNT=$#-1

execscript() {
local ZONE=$1
local INTERFACE=$2
local NETWORK=$3
local GATEWAY=$4
sudo NetworkAddRouted $ZONE << EOF
$INTERFACE
$NETWORK
$GATEWAY
none
EOF
}

for ((i=3 ; i <= $NETWORKCOUNT ; i++)); do
	NETWORK=$3
	if [[ $i -gt 3 ]]; then
		GATEWAY=""
	fi
	execscript $ZONE $INTERFACE $NETWORK $GATEWAY
	shift
done
