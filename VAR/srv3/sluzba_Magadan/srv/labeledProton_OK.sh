#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh


# параметры ВМ и интерфейс, смотрящий в сеть данных (приложений)

VCPUS=2 VRAM=2000 VDISKSIZE=30000000000  KICKSTARTFILE=wf_int.ks    $DC  l1c1.int.mg   em1-labeled 10.1.57.130/28  sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=200000000000  KICKSTARTFILE=gis.ks      $DC l1c1.sgio.mg    em1-labeled 10.1.57.131/28    sl-data
VCPUS=2 VRAM=8000 VDISKSIZE=350000000000  VDISKTYPE=unenc KICKSTARTFILE=psql.ks $DC l1c1.psql.mg em1-labeled 10.1.57.132/28 sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=250000000000  KICKSTARTFILE=astk.ks     $DC l1c1.sbor.mg   em1-labeled 10.1.57.133/28    sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=80000000000   KICKSTARTFILE=repoz.ks    $DC  l1c1.repo.mg   em1-labeled 10.1.57.129/28  sl-data

sleep 60

sudo DomainControl l1c1.int.mg                     stop
sudo DomainControl l1c1.sgio.mg                     stop
sudo DomainControl l1c1.psql.mg                     stop
sudo DomainControl l1c1.sbor.mg                     stop
sudo DomainControl l1c1.repo.mg                     stop

# добавление IP-адреса сети общей зоны в файл описания ВМ (xml) и gateway
$NAR2 l1c1.int.mg           em1-labeled 10.1.52.192/28 10.1.57.142
$NAR2 l1c1.sgio.mg           em1-labeled 10.1.52.192/28 10.1.57.142
$NAR2 l1c1.psql.mg           em1-labeled 10.1.52.192/28 10.1.57.142
$NAR2 l1c1.sbor.mg           em1-labeled 10.1.52.192/28 10.1.57.142
$NAR2 l1c1.repo.mg           em1-labeled 10.1.52.192/28 10.1.57.142

# добавляем ещё один интерфейс для терминальной сети (например, em2-labeled)

sudo DomainControl l1c1.int.mg                     start
sudo DomainControl l1c1.sgio.mg                     start
sudo DomainControl l1c1.psql.mg                     start
sudo DomainControl l1c1.sbor.mg                     start
sudo DomainControl l1c1.repo.mg                     start
