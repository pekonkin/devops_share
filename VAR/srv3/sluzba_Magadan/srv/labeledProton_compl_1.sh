#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh

# параметры ВМ и интерфейс, смотрящий в сеть данных (приложений)
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c1024.mg1         em1-labeled 10.1.56.241/28   sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c2048.mg1         em1-labeled 10.1.57.17/28   sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c4096.mg1         em1-labeled 10.1.57.33/28   sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c8192.mg1      em1-labeled 10.1.57.49/28   sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c16384.mg1     em1-labeled 10.1.57.65/28  sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c65536.mg1        em1-labeled 10.1.57.81/28   sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=wf.ks       $DC l2c8388607.wf.mg1  em1-labeled 10.50.50.20/24     sl-data
VCPUS=2 VRAM=8000  VDISKSIZE=150000000000  KICKSTARTFILE=gis.ks      $DC l2c8388607.gis.mg1  em1-labeled 10.1.58.8/26  sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=100000000000  VDISKTYPE=unenc KICKSTARTFILE=psql.ks $DC l2c8388607.psql.mg1 em1-labeled 10.1.58.2/26 sl-data

sleep 60

sudo DomainControl l2c1024.mg1 				stop
sudo DomainControl l2c2048.mg1				stop
sudo DomainControl l2c4096.mg1 				stop
sudo DomainControl l2c8192.mg1 				stop
sudo DomainControl l2c16384.mg1 			stop
sudo DomainControl l2c65536.mg1 			stop
sudo DomainControl l2c8388607.psql.mg1 		stop
sudo DomainControl l2c8388607.wf.mg1 		stop
sudo DomainControl l2c8388607.gis.mg1 		stop

# добавление IP-адреса сети общей зоны в файл описания ВМ (xml) и gateway
$NAR2 l2c1024.mg1       		em1-labeled 10.1.58.0/26 10.1.56.254
$NAR2 l2c2048.mg1 			em1-labeled 10.1.58.0/26 10.1.57.30
$NAR2 l2c4096.mg1 			em1-labeled 10.1.58.0/26 10.1.57.46
$NAR2 l2c8192.mg1 			em1-labeled 10.1.58.0/26 10.1.57.62
$NAR2 l2c16384.mg1 			em1-labeled 10.1.58.0/26 10.1.57.78
$NAR2 l2c65536.mg1 			em1-labeled 10.1.58.0/26 10.1.57.94

$NAR2 l2c8388607.wf.mg1     em1-labeled 10.1.52.0/28 10.1.58.62
$NAR2 l2c8388607.gis.mg1    em1-labeled 10.1.52.0/28 10.1.58.62
$NAR2 l2c8388607.psql.mg1   em1-labeled 10.1.52.0/28 10.1.58.62

# фильтр с какой сети разрешить обмен (192.168.34.0/24 - общая зона, 192.168.22.0/24 - пользовательские зоны)
$NAR l2c1024.mg1  	     	em1-labeled 10.1.52.0/28
$NAR l2c2048.mg1 			em1-labeled 10.1.52.0/28
$NAR l2c4096.mg1 			em1-labeled 10.1.52.0/28
$NAR l2c8192.mg1 			em1-labeled 10.1.52.0/28
$NAR l2c16384.mg1 			em1-labeled 10.1.52.0/28
$NAR l2c65536.mg1 			em1-labeled 10.1.52.0/28

$NAR l2c8388607.psql.mg1 	em1-labeled 10.1.56.240/28 10.1.57.16/28 10.1.57.32/28 10.1.57.48/28 10.1.57.64/28 10.1.57.80/28
$NAR l2c8388607.gis.mg1 	em1-labeled 10.1.56.240/28 10.1.57.16/28 10.1.57.32/28 10.1.57.48/28 10.1.57.64/28 10.1.57.80/28
$NAR l2c8388607.wf.mg1 	    em1-labeled 10.1.56.240/28 10.1.57.16/28 10.1.57.32/28 10.1.57.48/28 10.1.57.64/28 10.1.57.80/28

sudo DomainControl l2c1024.mg1 				start
sudo DomainControl l2c2048.mg1				start
sudo DomainControl l2c4096.mg1 				start
sudo DomainControl l2c8192.mg1 				start
sudo DomainControl l2c16384.mg1 			start
sudo DomainControl l2c65536.mg1 			start
sudo DomainControl l2c8388607.psql.mg1 		start
sudo DomainControl l2c8388607.gis.mg1 		start
sudo DomainControl l2c8388607.wf.mg1 		start
