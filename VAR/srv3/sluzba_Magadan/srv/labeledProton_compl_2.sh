#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh

VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c1024.mg2         em1-labeled 10.1.56.242/28   sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c2048.mg2         em1-labeled 10.1.57.18/28   sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c4096.mg2         em1-labeled 10.1.57.34/28   sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c8192.mg2      em1-labeled 10.1.57.50/28   sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c16384.mg2     em1-labeled 10.1.57.66/28  sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c65536.mg2        em1-labeled 10.1.57.82/28   sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=wf.ks       $DC l2c8388607.wf.mg2   em1-labeled 10.50.50.21/24     sl-data
VCPUS=2 VRAM=8000  VDISKSIZE=150000000000  KICKSTARTFILE=gis.ks      $DC l2c8388607.gis.mg2  em1-labeled 10.1.58.9/26  sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=100000000000  VDISKTYPE=unenc KICKSTARTFILE=psql.ks $DC l2c8388607.psql.mg2 em1-labeled 10.1.58.3/26 sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=200000000000  KICKSTARTFILE=astk.ks     $DC l2c8388607.astk.mg2 em1-labeled 10.1.58.10/26  sl-data

sleep 60

sudo DomainControl l2c1024.mg2 				stop
sudo DomainControl l2c2048.mg2				stop
sudo DomainControl l2c4096.mg2 				stop
sudo DomainControl l2c8192.mg2 				stop
sudo DomainControl l2c16384.mg2 			stop
sudo DomainControl l2c65536.mg2 			stop
sudo DomainControl l2c8388607.psql.mg2 		stop
sudo DomainControl l2c8388607.wf.mg2 		stop
sudo DomainControl l2c8388607.gis.mg2 		stop
sudo DomainControl l2c8388607.astk.mg2 		stop

$NAR2 l2c1024.mg2       		em1-labeled 10.1.58.0/26 10.1.56.254
$NAR2 l2c2048.mg2 			em1-labeled 10.1.58.0/26 10.1.57.30
$NAR2 l2c4096.mg2 			em1-labeled 10.1.58.0/26 10.1.57.46
$NAR2 l2c8192.mg2 			em1-labeled 10.1.58.0/26 10.1.57.62
$NAR2 l2c16384.mg2 			em1-labeled 10.1.58.0/26 10.1.57.78
$NAR2 l2c65536.mg2 			em1-labeled 10.1.58.0/26 10.1.57.94
$NAR2 l2c8388607.wf.mg2     em1-labeled 10.1.52.0/28 10.1.58.62
$NAR2 l2c8388607.gis.mg2    em1-labeled 10.1.52.0/28 10.1.58.62
$NAR2 l2c8388607.psql.mg2   em1-labeled 10.1.52.0/28 10.1.58.62
$NAR2 l2c8388607.astk   em1-labeled 10.1.52.0/28 10.1.58.62


$NAR l2c1024.mg2  	     	em1-labeled 10.1.52.0/28
$NAR l2c2048.mg2 			em1-labeled 10.1.52.0/28
$NAR l2c4096.mg2 			em1-labeled 10.1.52.0/28
$NAR l2c8192.mg2 			em1-labeled 10.1.52.0/28
$NAR l2c16384.mg2 			em1-labeled 10.1.52.0/28
$NAR l2c65536.mg2 			em1-labeled 10.1.52.0/28
$NAR l2c8388607.psql.mg2 	em1-labeled 10.1.56.240/28 10.1.57.16/28 10.1.57.32/28 10.1.57.48/28 10.1.57.64/28 10.1.57.80/28
$NAR l2c8388607.gis.mg2 	em1-labeled 10.1.56.240/28 10.1.57.16/28 10.1.57.32/28 10.1.57.48/28 10.1.57.64/28 10.1.57.80/28
$NAR l2c8388607.wf.mg2 	    em1-labeled 10.1.56.240/28 10.1.57.16/28 10.1.57.32/28 10.1.57.48/28 10.1.57.64/28 10.1.57.80/28
$NAR l2c8388607.astk    em1-labeled 10.1.56.240/28

sudo DomainControl l2c1024.mg2 				start
sudo DomainControl l2c2048.mg2				start
sudo DomainControl l2c4096.mg2 				start
sudo DomainControl l2c8192.mg2 				start
sudo DomainControl l2c16384.mg2 			start
sudo DomainControl l2c65536.mg2 			start
sudo DomainControl l2c8388607.psql.mg2 		start
sudo DomainControl l2c8388607.gis.mg2 		start
sudo DomainControl l2c8388607.wf.mg2 		start
sudo DomainControl l2c8388607.astk.mg2 		start
