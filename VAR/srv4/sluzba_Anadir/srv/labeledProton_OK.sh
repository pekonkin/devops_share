#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh


# параметры ВМ и интерфейс, смотрящий в сеть данных (приложений)

VCPUS=2 VRAM=4000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l1c1.ad         em1-labeled 10.1.59.33/28     sl-data
VCPUS=2 VRAM=2000 VDISKSIZE=30000000000   KICKSTARTFILE=wf_int.ks    $DC  l1c1.int.ad   em1-labeled 10.1.59.34/28  sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=200000000000  KICKSTARTFILE=gis.ks      $DC l1c1.sgio.ad    em1-labeled 10.1.59.35/28    sl-data
VCPUS=2 VRAM=8000 VDISKSIZE=350000000000  VDISKTYPE=unenc KICKSTARTFILE=psql.ks $DC l1c1.psql.ad em1-labeled 10.1.59.36/28 sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=250000000000  KICKSTARTFILE=astk.ks     $DC l1c1.sbor.ad   em1-labeled 10.1.59.37/28    sl-data

sleep 60

sudo DomainControl l1c1.ad                          stop
sudo DomainControl l1c1.int.ad                      stop
sudo DomainControl l1c1.sgio.ad                     stop
sudo DomainControl l1c1.psql.ad                     stop
sudo DomainControl l1c1.sbor.ad                     stop

# добавление IP-адреса сети общей зоны в файл описания ВМ (xml) и gateway

$NAR2 l1c1.ad           em1-labeled 10.1.52.192/28 10.1.59.46
$NAR2 l1c1.int.ad           em1-labeled 10.1.52.192/28 10.1.59.46
$NAR2 l1c1.sgio.ad           em1-labeled 10.1.52.192/28 10.1.59.46
$NAR2 l1c1.psql.ad           em1-labeled 10.1.52.192/28 10.1.59.46
$NAR2 l1c1.sbor.ad           em1-labeled 10.1.52.192/28 10.1.59.46

# добавляем ещё один интерфейс для терминальной сети (например, em2-labeled)

sudo DomainControl l1c1.ad                          start
sudo DomainControl l1c1.int.ad                      start
sudo DomainControl l1c1.sgio.ad                     start
sudo DomainControl l1c1.psql.ad                     start
sudo DomainControl l1c1.sbor.ad                     start
