#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh


# параметры ВМ и интерфейс, смотрящий в сеть данных (приложений)


VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c1024.ad1         em1-labeled 10.1.57.161/28   sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c2048.ad1         em1-labeled 10.1.57.177/28   sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c4096.ad1         em1-labeled 10.1.57.193/28   sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c8192.ad1         em1-labeled 10.1.57.209/28   sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c16384.ad1        em1-labeled 10.1.57.225/28   sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c65536.ad1        em1-labeled 10.1.57.241/28   sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=wf.ks       $DC l2c8388607.wf.ad1   em1-labeled 10.50.50.1/28     sl-data
VCPUS=2 VRAM=8000  VDISKSIZE=150000000000  KICKSTARTFILE=gis.ks      $DC l2c8388607.gis.ad1  em1-labeled 10.1.58.71/28  sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=100000000000  VDISKTYPE=unenc KICKSTARTFILE=psql.ks $DC l2c8388607.psql.ad1 em1-labeled 10.1.58.66/26 sl-data





sleep 60


sudo DomainControl l2c1024.ad1 				stop
sudo DomainControl l2c2048.ad1				stop
sudo DomainControl l2c4096.ad1 				stop
sudo DomainControl l2c8192.ad1 				stop
sudo DomainControl l2c16384.ad1 			stop
sudo DomainControl l2c65536.ad1 			stop
sudo DomainControl l2c8388607.psql.ad1 		stop
sudo DomainControl l2c8388607.wf.ad1 		stop
sudo DomainControl l2c8388607.gis.ad1 		stop




# добавление IP-адреса сети общей зоны в файл описания ВМ (xml) и gateway

$NAR2 l2c1024.ad1       	em1-labeled  10.1.59.32/28 10.1.57.174
$NAR2 l2c2048.ad1 		em1-labeled  10.1.59.32/28 10.1.57.190
$NAR2 l2c4096.ad1 		em1-labeled  10.1.59.32/28 10.1.57.206
$NAR2 l2c8192.ad1 		em1-labeled  10.1.59.32/28 10.1.57.222
$NAR2 l2c16384.ad1 		em1-labeled  10.1.59.32/28 10.1.57.238
$NAR2 l2c65536.ad1 		em1-labeled  10.1.59.32/28 10.1.57.254
$NAR2 l2c8388607.wf.ad1     em1-labeled 10.1.52.0/28 10.1.59.46
$NAR2 l2c8388607.gis.ad1    em1-labeled 10.1.52.0/28 10.1.59.46
$NAR2 l2c8388607.psql.ad1   em1-labeled 10.1.52.0/28 10.1.59.46




# фильтр с какой сети разрешить обмен (192.168.34.0/24 - общая зона, 192.168.22.0/24 - пользовательские зоны)

$NAR l2c1024.ad1  	     	em1-labeled 10.1.52.0/28
$NAR l2c2048.ad1 			em1-labeled 10.1.52.0/28
$NAR l2c4096.ad1 			em1-labeled 10.1.52.0/28
$NAR l2c8192.ad1 			em1-labeled 10.1.52.0/28
$NAR l2c16384.ad1 			em1-labeled 10.1.52.0/28
$NAR l2c65536.ad1 			em1-labeled 10.1.52.0/28
$NAR l2c8388607.psql.ad1 	em1-labeled 10.1.57.160/28 10.1.57.176/28 10.1.57.192/28 10.1.57.208/28 10.1.57.224/28 10.1.57.240/28 10.1.58.64/26
$NAR l2c8388607.gis.ad1 	em1-labeled 10.1.57.160/28 10.1.57.176/28 10.1.57.192/28 10.1.57.208/28 10.1.57.224/28 10.1.57.240/28 10.1.58.64/26
$NAR l2c8388607.wf.ad1 	    em1-labeled 10.1.57.160/28 10.1.57.176/28 10.1.57.192/28 10.1.57.208/28 10.1.57.224/28 10.1.57.240/28 10.1.58.64/26

# добавляем ещё один интерфейс для терминальной сети (например, em2-labeled)


sudo DomainControl l2c1024.ad1 				start
sudo DomainControl l2c2048.ad1				start
sudo DomainControl l2c4096.ad1 				start
sudo DomainControl l2c8192.ad1 				start
sudo DomainControl l2c16384.ad1 			start
sudo DomainControl l2c65536.ad1 			start
sudo DomainControl l2c8388607.psql.ad1 		start
sudo DomainControl l2c8388607.gis.ad1 		start
sudo DomainControl l2c8388607.wf.ad1 		start
