#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh


# ��������� �� � ���������, ��������� � ���� ������ (����������)

VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c1024.ad2         em1-labeled 10.1.57.162/28   sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c2048.ad2         em1-labeled 10.1.57.178/28   sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c4096.ad2         em1-labeled 10.1.57.194/28   sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c8192.ad2         em1-labeled 10.1.57.210/28   sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c16384.ad2        em1-labeled 10.1.57.226/28   sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c65536.ad2        em1-labeled 10.1.57.242/28   sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=30000000000  KICKSTARTFILE=wf.ks       $DC l2c8388607.wf.ad2   em1-labeled 10.50.50.31/24     sl-data
VCPUS=2 VRAM=8000  VDISKSIZE=150000000000  KICKSTARTFILE=gis.ks      $DC l2c8388607.gis.ad2  em1-labeled 10.1.58.72/26  sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=200000000000  KICKSTARTFILE=astk.ks     $DC l2c8388607.astk.ad2 em1-labeled 10.1.58.73/26  sl-data
VCPUS=2 VRAM=12000 VDISKSIZE=100000000000  VDISKTYPE=unenc KICKSTARTFILE=psql.ks $DC l2c8388607.psql.ad2 em1-labeled 10.1.58.67/26 sl-data


sleep 60


sudo DomainControl l2c1024.ad2 				stop
sudo DomainControl l2c2048.ad2				stop
sudo DomainControl l2c4096.ad2 				stop
sudo DomainControl l2c8192.ad2 				stop
sudo DomainControl l2c16384.ad2 			stop
sudo DomainControl l2c65536.ad2 			stop
sudo DomainControl l2c8388607.psql.ad2 		stop
sudo DomainControl l2c8388607.astk.ad2 		stop
sudo DomainControl l2c8388607.wf.ad2 		stop
sudo DomainControl l2c8388607.gis.ad2 		stop



# ���������� IP-������ ���� ����� ���� � ���� �������� �� (xml) � gateway

$NAR2 l2c1024.ad2       em1-labeled  10.1.59.32/28 10.1.57.174
$NAR2 l2c2048.ad2 		em1-labeled  10.1.59.32/28 10.1.57.190
$NAR2 l2c4096.ad2 		em1-labeled  10.1.59.32/28 10.1.57.206
$NAR2 l2c8192.ad2 		em1-labeled  10.1.59.32/28 10.1.57.222
$NAR2 l2c16384.ad2 		em1-labeled  10.1.59.32/28 10.1.57.238
$NAR2 l2c65536.ad2 		em1-labeled  10.1.59.32/28 10.1.57.254
$NAR2 l2c8388607.wf.ad2     em1-labeled 10.1.52.0/28 10.1.59.46
$NAR2 l2c8388607.astk.ad2    em1-labeled 10.1.52.0/28 10.1.59.46
$NAR2 l2c8388607.gis.ad2    em1-labeled 10.1.52.0/28 10.1.59.46
$NAR2 l2c8388607.psql.ad2   em1-labeled 10.1.52.0/28 10.1.59.46




# ������ � ����� ���� ��������� ����� (192.168.34.0/24 - ����� ����, 192.168.22.0/24 - ���������������� ����)

$NAR l2c1024.ad2  	     	em1-labeled 10.1.52.0/28
$NAR l2c2048.ad2 			em1-labeled 10.1.52.0/28
$NAR l2c4096.ad2 			em1-labeled 10.1.52.0/28
$NAR l2c8192.ad2 			em1-labeled 10.1.52.0/28
$NAR l2c16384.ad2 			em1-labeled 10.1.52.0/28
$NAR l2c65536.ad2 			em1-labeled 10.1.52.0/28
$NAR l2c8388607.psql.ad2 	em1-labeled 10.1.57.160/28 10.1.57.176/28 10.1.57.192/28 10.1.57.208/28 10.1.57.224/28 10.1.57.240/28 10.1.58.64/26
$NAR l2c8388607.gis.ad2 	em1-labeled 10.1.57.160/28 10.1.57.176/28 10.1.57.192/28 10.1.57.208/28 10.1.57.224/28 10.1.57.240/28 10.1.58.64/26
$NAR l2c8388607.wf.ad2 	    em1-labeled 10.1.57.160/28 10.1.57.176/28 10.1.57.192/28 10.1.57.208/28 10.1.57.224/28 10.1.57.240/28 10.1.58.64/26
$NAR l2c8388607.astk.ad2    em1-labeled 10.1.57.160/28 10.1.57.176/28 10.1.57.192/28 10.1.57.208/28 10.1.57.224/28 10.1.57.240/28 10.1.58.64/26

# ��������� ��� ���� ��������� ��� ������������ ���� (��������, em2-labeled)



sudo DomainControl l2c1024.ad2 				start
sudo DomainControl l2c2048.ad2				start
sudo DomainControl l2c4096.ad2 				start
sudo DomainControl l2c8192.ad2 				start
sudo DomainControl l2c16384.ad2 			start
sudo DomainControl l2c65536.ad2 			start
sudo DomainControl l2c8388607.psql.ad2 		start
sudo DomainControl l2c8388607.gis.ad2 		start
sudo DomainControl l2c8388607.wf.ad2 		start
sudo DomainControl l2c8388607.astk.ad2 		start
