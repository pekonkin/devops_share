#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh


# параметры ВМ и интерфейс, смотрящий в сеть данных (приложений)

VCPUS=2 VRAM=12000 VDISKSIZE=160000000000   KICKSTARTFILE=repoz.ks    $DC  arm.pv   em1-labeled 10.1.56.193/28  sl-data

sleep 60


sudo DomainControl arm.pv 		stop


# добавление IP-адреса сети общей зоны в файл описания ВМ (xml) и gateway

$NAR2 arm.pv      	em1-labeled 10.1.52.192/28 10.1.56.206

# добавляем ещё один интерфейс для терминальной сети (например, em2-labeled)

sudo DomainControl arm.pv 		start
