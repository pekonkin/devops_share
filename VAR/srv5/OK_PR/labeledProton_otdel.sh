#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh


# параметры ВМ и интерфейс, смотрящий в сеть данных (приложений)

VCPUS=2 VRAM=12000 VDISKSIZE=160000000000   KICKSTARTFILE=repoz.ks    $DC  arm.pr   em1-labeled 10.1.51.129/27  sl-data

sleep 60

sudo DomainControl arm.pr 		stop

# добавление IP-адреса сети общей зоны в файл описания ВМ (xml) и gateway

$NAR2 arm.pr      	em1-labeled 10.1.52.192/28 10.1.51.158

# добавляем ещё один интерфейс для терминальной сети (например, em2-labeled)

sudo DomainControl arm.pr 		start
