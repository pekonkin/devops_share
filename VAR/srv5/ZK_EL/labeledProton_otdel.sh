#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh


# параметры ВМ и интерфейс, смотрящий в сеть данных (приложений)

VCPUS=2 VRAM=4000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c262144.el         em1-labeled 10.1.59.65/28     sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c524288.el         em1-labeled 10.1.59.81/28     sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c1048576.el        em1-labeled 10.1.59.97/28    sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c2097152.el        em1-labeled 10.1.59.113/28    sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=30000000000  KICKSTARTFILE=wf.ks       $DC l2c8388607.wf.el     em1-labeled 10.1.58.129/26    sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=150000000000  KICKSTARTFILE=gis.ks      $DC l2c8388607.gis.el    em1-labeled 10.1.58.131/26    sl-data
VCPUS=2 VRAM=8000 VDISKSIZE=150000000000  VDISKTYPE=unenc KICKSTARTFILE=psql.ks $DC l2c8388607.psql.el em1-labeled 10.1.58.130/26 sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=200000000000  KICKSTARTFILE=astk.ks     $DC l2c8388607.astk.el   em1-labeled 10.1.58.132/26    sl-data

sleep 60

sudo DomainControl l2c262144.el 			stop
sudo DomainControl l2c524288.el 			stop
sudo DomainControl l2c1048576.el 			stop
sudo DomainControl l2c2097152.el			stop
sudo DomainControl l2c8388607.psql.el 		stop
sudo DomainControl l2c8388607.wf.el 		stop
sudo DomainControl l2c8388607.gis.el 		stop
sudo DomainControl l2c8388607.astk.el 		stop


# добавление IP-адреса сети общей зоны в файл описания ВМ (xml) и gateway

$NAR2 l2c262144.el       	em1-labeled 10.1.58.128/26 10.1.59.78
$NAR2 l2c524288.el       	em1-labeled 10.1.58.128/26 10.1.59.94
$NAR2 l2c1048576.el       	em1-labeled 10.1.58.128/26 10.1.59.110
$NAR2 l2c2097152.el 		em1-labeled 10.1.58.128/26 10.1.59.126
$NAR2 l2c8388607.wf.el     em1-labeled 10.1.52.0/28 10.1.58.190
$NAR2 l2c8388607.gis.el    em1-labeled 10.1.52.0/28 10.1.58.190
$NAR2 l2c8388607.psql.el   em1-labeled 10.1.52.0/28 10.1.58.190
$NAR2 l2c8388607.astk.el   em1-labeled 10.1.52.0/28 10.1.58.190

# фильтр с какой сети разрешить обмен (192.168.34.0/24 - общая зона, 192.168.22.0/24 - пользовательские зоны)

$NAR l2c262144.el  	    em1-labeled 10.1.52.0/28
$NAR l2c524288.el  	    em1-labeled 10.1.52.0/28
$NAR l2c1048576.el  	    em1-labeled 10.1.52.0/28 
$NAR l2c2097152.el 		em1-labeled 10.1.52.0/28 
$NAR l2c8388607.psql.el 	em1-labeled 10.1.59.64/28 10.1.59.80/28 10.1.59.96/28 10.1.59.112/28
$NAR l2c8388607.gis.el 	em1-labeled 10.1.59.64/28 10.1.59.80/28 10.1.59.96/28 10.1.59.112/28
$NAR l2c8388607.wf.el 	    em1-labeled 10.1.59.64/28 10.1.59.80/28 10.1.59.96/28 10.1.59.112/28
$NAR l2c8388607.astk.el 	em1-labeled 10.1.59.64/28 10.1.59.80/28 10.1.59.96/28 10.1.59.112/28

# добавляем ещё один интерфейс для терминальной сети (например, em2-labeled)

sudo DomainControl l2c262144.el 			start
sudo DomainControl l2c524288.el 			start
sudo DomainControl l2c1048576.el 			start
sudo DomainControl l2c2097152.el			start
sudo DomainControl l2c8388607.psql.el 		start
sudo DomainControl l2c8388607.gis.el 		start
sudo DomainControl l2c8388607.wf.el 		start
sudo DomainControl l2c8388607.astk.el 		start
