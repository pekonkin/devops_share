#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh


# параметры ВМ и интерфейс, смотрящий в сеть данных (приложений)

VCPUS=2 VRAM=4000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c262144.lav         em1-labeled 10.1.59.161/28     sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c524288.lav         em1-labeled 10.1.56.225/28        sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c1048576.lav        em1-labeled 10.1.59.177/28        sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c2097152.lav        em1-labeled 10.1.59.193/28    sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=30000000000  KICKSTARTFILE=wf.ks       $DC l2c8388607.wf.lav     em1-labeled 10.1.58.193/28     sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=150000000000  KICKSTARTFILE=gis.ks      $DC l2c8388607.gis.lav    em1-labeled 10.1.58.195/28     sl-data
VCPUS=2 VRAM=8000 VDISKSIZE=150000000000  VDISKTYPE=unenc KICKSTARTFILE=psql.ks $DC l2c8388607.psql.lav em1-labeled 10.1.58.194/28 sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=200000000000  KICKSTARTFILE=astk.ks     $DC l2c8388607.astk.lav   em1-labeled 10.1.58.196/26     sl-data



sleep 60


sudo DomainControl l2c262144.lav 			stop
sudo DomainControl l2c524288.lav 			stop
sudo DomainControl l2c1048576.lav 			stop
sudo DomainControl l2c2097152.lav			stop
sudo DomainControl l2c8388607.psql.lav 		stop
sudo DomainControl l2c8388607.wf.lav 		stop
sudo DomainControl l2c8388607.gis.lav 		stop
sudo DomainControl l2c8388607.astk.lav 		stop



# добавление IP-адреса сети общей зоны в файл описания ВМ (xml) и gateway

$NAR2 l2c262144.lav       	em1-labeled 10.1.58.128/26 10.1.59.174
$NAR2 l2c524288.lav       	em1-labeled 10.1.58.128/26 10.1.56.238
$NAR2 l2c1048576.lav       	em1-labeled 10.1.58.128/26 10.1.59.190
$NAR2 l2c2097152.lav 		em1-labeled 10.1.58.128/26 10.1.59.206
$NAR2 l2c8388607.wf.lav     em1-labeled 10.1.52.0/28 10.1.58.254
$NAR2 l2c8388607.gis.lav    em1-labeled 10.1.52.0/28 10.1.58.254
$NAR2 l2c8388607.psql.lav   em1-labeled 10.1.52.0/28 10.1.58.254
$NAR2 l2c8388607.astk.lav   em1-labeled 10.1.52.0/28 10.1.58.254






# фильтр с какой сети разрешить обмен (192.168.34.0/24 - общая зона, 192.168.22.0/24 - пользовательские зоны)

$NAR l2c262144.lav  	    em1-labeled 10.1.52.0/28
$NAR l2c524288.lav  	    em1-labeled 10.1.52.0/28
$NAR l2c1048576.lav  	    em1-labeled 10.1.52.0/28
$NAR l2c2097152.lav 		em1-labeled 10.1.52.0/28
$NAR l2c8388607.psql.lav 	em1-labeled 10.1.59.160/28 10.1.56.224/28 10.1.59.176/28 10.1.59.192/28 10.1.58.192/26
$NAR l2c8388607.gis.lav 	em1-labeled 10.1.59.160/28 10.1.56.224/28 10.1.59.176/28 10.1.59.192/28 10.1.58.192/26
$NAR l2c8388607.wf.lav 	    em1-labeled 10.1.59.160/28 10.1.56.224/28 10.1.59.176/28 10.1.59.192/28 10.1.58.192/26
$NAR l2c8388607.astk.lav 	em1-labeled 10.1.59.160/28 10.1.56.224/28 10.1.59.176/28 10.1.59.192/28 10.1.58.192/26

# добавляем ещё один интерфейс для терминальной сети (например, em2-labeled)




sudo DomainControl l2c262144.lav 			start
sudo DomainControl l2c524288.lav 			start
sudo DomainControl l2c1048576.lav 			start
sudo DomainControl l2c2097152.lav			start
sudo DomainControl l2c8388607.psql.lav 		start
sudo DomainControl l2c8388607.gis.lav 		start
sudo DomainControl l2c8388607.wf.lav 		start
sudo DomainControl l2c8388607.astk.lav 		start
