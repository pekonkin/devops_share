#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh


# параметры ВМ и интерфейс, смотрящий в сеть данных (приложений)

VCPUS=2 VRAM=4000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c131072.ok         em1-labeled 10.1.56.129/28     sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=100000000000  KICKSTARTFILE=wf.ks       $DC l2c8388607.wf.ok     em1-labeled 10.1.53.65/26    sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=150000000000  KICKSTARTFILE=gis.ks      $DC l2c8388607.gis.ok    em1-labeled 10.1.53.67/26    sl-data
VCPUS=2 VRAM=8000 VDISKSIZE=150000000000  VDISKTYPE=unenc KICKSTARTFILE=psql.ks $DC l2c8388607.psql.ok em1-labeled 10.1.53.66/26 sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=200000000000  KICKSTARTFILE=astk.ks     $DC l2c8388607.astk.ok   em1-labeled 10.1.53.68/26    sl-data

sleep 60

sudo DomainControl l2c131072.ok 		stop
sudo DomainControl l2c8388607.psql.ok 		stop
sudo DomainControl l2c8388607.wf.ok 		stop
sudo DomainControl l2c8388607.gis.ok 		stop
sudo DomainControl l2c8388607.astk.ok 		stop

# добавление IP-адреса сети общей зоны в файл описания ВМ (xml) и gateway

$NAR2 l2c131072.ok       	em1-labeled 10.1.53.64/26 10.1.56.142
$NAR2 l2c8388607.wf.ok     em1-labeled 10.1.52.0/28 10.1.53.126
$NAR2 l2c8388607.gis.ok    em1-labeled 10.1.52.0/28 10.1.53.126
$NAR2 l2c8388607.psql.ok   em1-labeled 10.1.52.0/28 10.1.53.126
$NAR2 l2c8388607.astk.ok   em1-labeled 10.1.52.0/28 10.1.53.126

# фильтр с какой сети разрешить обмен (192.168.34.0/24 - общая зона, 192.168.22.0/24 - пользовательские зоны)

$NAR l2c131072.ok  	    em1-labeled 10.1.52.0/28
$NAR l2c8388607.psql.ok 	em1-labeled 10.1.56.128/28
$NAR l2c8388607.gis.ok 	em1-labeled 10.1.56.128/28
$NAR l2c8388607.wf.ok 	    em1-labeled 10.1.56.128/28
$NAR l2c8388607.astk.ok 	em1-labeled 10.1.56.128/28

sudo DomainControl l2c131072.ok 			start
sudo DomainControl l2c8388607.psql.ok 		start
sudo DomainControl l2c8388607.gis.ok 		start
sudo DomainControl l2c8388607.wf.ok 		start
sudo DomainControl l2c8388607.astk.ok 		start
