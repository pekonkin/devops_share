#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh

# параметры ВМ и интерфейс, смотрящий в сеть данных (приложений)

VCPUS=2 VRAM=4000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c262144.pr         em1-labeled 10.1.56.17/28     sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c524288.pr         em1-labeled 10.1.56.33/28     sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c1048576.pr        em1-labeled 10.1.56.49/28     sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=30000000000  KICKSTARTFILE=term_wf.ks  $DC l2c2097152.pr        em1-labeled 10.1.56.65/28     sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=30000000000  KICKSTARTFILE=wf.ks       $DC l2c8388607.wf.pr     em1-labeled 10.1.53.1/26     sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=150000000000  KICKSTARTFILE=gis.ks      $DC l2c8388607.gis.pr    em1-labeled 10.1.53.3/26     sl-data
VCPUS=2 VRAM=8000 VDISKSIZE=150000000000  VDISKTYPE=unenc KICKSTARTFILE=psql.ks $DC l2c8388607.psql.pr em1-labeled 10.1.53.2/26 sl-data
VCPUS=2 VRAM=4000 VDISKSIZE=200000000000  KICKSTARTFILE=astk.ks     $DC l2c8388607.astk.pr   em1-labeled 10.1.53.4/26     sl-data

sleep 60

sudo DomainControl l2c262144.pr 			stop
sudo DomainControl l2c524288.pr 			stop
sudo DomainControl l2c1048576.pr 			stop
sudo DomainControl l2c2097152.pr			stop
sudo DomainControl l2c8388607.psql.pr 		stop
sudo DomainControl l2c8388607.wf.pr 		stop
sudo DomainControl l2c8388607.gis.pr 		stop
sudo DomainControl l2c8388607.astk.pr 		stop

# добавление IP-адреса сети общей зоны в файл описания ВМ (xml) и gateway

$NAR2 l2c262144.pr       	em1-labeled 10.1.53.0/26 10.1.56.30
$NAR2 l2c524288.pr       	em1-labeled 10.1.53.0/26 10.1.56.46
$NAR2 l2c1048576.pr       	em1-labeled 10.1.53.0/26 10.1.56.62 
$NAR2 l2c2097152.pr 		em1-labeled 10.1.53.0/26 10.1.56.78
$NAR2 l2c8388607.wf.pr     em1-labeled 10.1.52.0/28 10.1.53.62
$NAR2 l2c8388607.gis.pr    em1-labeled 10.1.52.0/28 10.1.53.62
$NAR2 l2c8388607.psql.pr   em1-labeled 10.1.52.0/28 10.1.53.62
$NAR2 l2c8388607.astk.pr   em1-labeled 10.1.52.0/28 10.1.53.62

# фильтр с какой сети разрешить обмен (192.168.34.0/24 - общая зона, 192.168.22.0/24 - пользовательские зоны)

$NAR l2c262144.pr  	    em1-labeled 10.1.52.0/28
$NAR l2c524288.pr  	    em1-labeled 10.1.52.0/28
$NAR l2c1048576.pr  	    em1-labeled 10.1.52.0/28 
$NAR l2c2097152.pr 		em1-labeled 10.1.52.0/28 
$NAR l2c8388607.psql.pr 	em1-labeled 10.1.56.16/28 10.1.56.32/28 10.1.56.48/28 10.1.56.64/28
$NAR l2c8388607.gis.pr 	em1-labeled 10.1.56.16/28 10.1.56.32/28 10.1.56.48/28 10.1.56.64/28
$NAR l2c8388607.wf.pr 	    em1-labeled 10.1.56.16/28 10.1.56.32/28 10.1.56.48/28 10.1.56.64/28
$NAR l2c8388607.astk.pr 	em1-labeled 10.1.56.16/28 10.1.56.32/28 10.1.56.48/28 10.1.56.64/28

# добавляем ещё один интерфейс для терминальной сети (например, em2-labeled)

sudo DomainControl l2c262144.pr 			start
sudo DomainControl l2c524288.pr 			start
sudo DomainControl l2c1048576.pr 			start
sudo DomainControl l2c2097152.pr			start
sudo DomainControl l2c8388607.psql.pr 		start
sudo DomainControl l2c8388607.gis.pr 		start
sudo DomainControl l2c8388607.wf.pr 		start
sudo DomainControl l2c8388607.astk.pr 		start
