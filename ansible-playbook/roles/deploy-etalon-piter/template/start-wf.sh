#!/usr/bin/env bash
STATIC_DIR='/opt/spo/nginx'
WILDFLY_DIR='/opt/wildfly/standalone/deployments'
WILDFLY_LOG='/opt/wildfly/standalone/log/server.log'
STARTUP_WAIT=20

service wildfly start

count=0
started=false

until [ $count -gt $STARTUP_WAIT ]
do
	grep 'JBAS015874:' $WILDFLY_LOG > /dev/null
	if [ $? -eq 0 ] ; then
        started=true
        break
	fi
	sleep 5
	let count=$count+1;
done

if [[ $started == false ]]; then
	echo 'we have some problems my masta'
	exit 1
else
	echo 'wildfly started my masta'
	rm -rf /tmp/*frontier*
	rm -rf /tmp/*classifier*
	exit 0
fi