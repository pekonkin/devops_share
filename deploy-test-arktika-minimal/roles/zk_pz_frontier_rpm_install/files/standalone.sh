#!/usr/bin/env bash
let first=`cat -n /opt/wildfly/standalone/configuration/standalone.xml | grep '<system-properties>' | awk '{print $1}'`
let last=`cat -n /opt/wildfly/standalone/configuration/standalone.xml | grep '</system-properties>' | awk '{print $1}'`
sed -i "$first,${last}d" /opt/wildfly/standalone/configuration/standalone.xml