#!/bin/bash

echo "===================== START $i =========================="
#echo "Add repo"
echo -e '[z36c-spo-proton]
name=Zircon36C - SPO - proton
baseurl=http://10.1.39.13/proton/Packages
enabled=1
gpgcheck=0
[z36c-spo-media-dvd]
name=Zircon36C - SPO - Media -dvd
baseurl=http://10.1.39.13/repo-new
enabled=1
gpgcheck=0
' > /etc/yum.repos.d/Zircon36C-Media.repo

echo "disable selinux"
sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
setenforce 0
echo "install libselinux-python"
yum -y install libselinux-python at rsync
echo "===================== END $i =========================="

