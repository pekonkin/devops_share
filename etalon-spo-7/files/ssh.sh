#!/bin/bash

server_list=$(egrep -ro "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)" ../host* | awk -F\: {'print$2'} | sort -n | uniq )

for i in $server_list
do
  SSHPASS='12345678' sshpass -e scp -o StrictHostKeyChecking=no repo.sh root@${i}:
  SSHPASS='12345678' sshpass -e ssh -A -o StrictHostKeyChecking=no root@$i './repo.sh; rm -f repo.sh'
done
