#!/bin/bash

##
##  Write Dmitry V. Demin
##  if are you have question 
##  send me email: d.demin@swemel.ru
##


_grub="/boot/grub/grub.conf"

if [ -e ${_grub} ]
then
 if [ $( egrep -c "$1" ${_grub} ) -eq 0 ]
  then 
     sed -i.ORIGINAL "/kernel \/vmlinuz-2.6/ s|$| elevator="$1" rd.fstab=no acpi=noirq noapic cgroup_enable=memory swapaccount=1 hugepagesz=1GB hugepages=1|" ${_grub}
	for i in $(lsblk | grep disk | awk {'print$1'})
	do
	 echo "$1" > /sys/block/${i}/queue/scheduler
	done
 fi
fi



