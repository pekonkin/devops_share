#!/bin/bash 

##
##  Write Dmitry V. Demin
##  if are you have question 
##  send me email: d.demin@swemel.ru
##


_net="/sys/class/net"
_net_conf="/etc/sysconfig/network-scripts/ifcfg"
_net_tune="/etc/udev/rules.d/70-persistent-net.rules"

if [ -d /sys/class/net ]
then
	for _interface in `ls -1 ${_net} | grep -v lo`
          do
            if [ -e ${_net_conf}-${_interface} ]
             then 
               sed -i 's/BOOTPROTO=none/BOOTPROTO=static/g' ${_net_conf}-${_interface}
               ### Only for local chek
               if [ $( egrep -c '10.1.30.5' ${_net_conf}-${_interface} ) -eq 1 ]
                then
	          echo "create backup"
                  cat ${_net_conf}-${_interface} > ${_net_conf}-${_interface}.BACKUP
                  sed -i '/GATEWAY=10.1.30.5/d' ${_net_conf}-${_interface}
               fi
	       ### Prepare network tune
            fi
        done
               if [ -e ${_net_tune} ]
                then
		      sed -i '/^KERNEL==/d' ${_net_tune}
		      for i in $(ls -1 ${_net} | grep -v lo | sed 's/[[:digit:]]/*/g' | sed -e '/\*\*/d' -e '/virbr/d' | sort -u )
                      do
                      sed -i '1 i\KERNEL=="'${i}'", RUN+="/sbin/ip link set %k txqueuelen 5000"' ${_net_tune}
                      done
               fi
else
	exit 1
fi
## Reload config
$(`whereis udevadm | cut -d ' ' -f2` trigger)
