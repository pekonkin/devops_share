#!/usr/bin/env bash
set -vx
FROMUSER=pekonkin@swemel.ru
SMTP=10.1.10.173
MAIL_ARGS='-o message-content-type=text -o message-charset=UTF-8'
SUBJECT="Re: [Тестирование СПО Размах - features #9698] Большой питерский стенд"
PACKET_NAME='frontier-user-app|frontier-global-app|data-exchange-global-app'
ADDR=$1
ADDRGLOBAL=$2
RELEASE=$3
VERSIONS1=`ssh root@${ADDR} 'rpm -qa' | egrep "${PACKET_NAME}"`
VERSIONS2=`ssh root@${ADDRGLOBAL} 'rpm -qa' | egrep "${PACKET_NAME}"`
MSG="Все задеплоилось версия: ${VERSIONS1},  ${VERSIONS2}, branch:${RELEASE}"
emails="rdm@swemel.ru"
sendemail -f "${FROMUSER}" -t ${emails} -u "${SUBJECT}" -m "${MSG}" -s "${SMTP}" $MAIL_ARGS
exit 0