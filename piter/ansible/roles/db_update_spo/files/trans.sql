--functions
create or replace function pg_temp.insert_or_update_bool(text, bool)
returns void as $$
begin
 insert into app.boolean_property (key, value) values ($1, $2);
 raise notice 'insert % %', $1, $2;
exception when others then 
 update app.boolean_property set value=$2 where key=$1;
 raise notice 'update % %', $1, $2;
end;
$$ language 'plpgsql';

create or replace function pg_temp.insert_or_update_int(text, int)
returns void as $$
begin
 insert into app.integer_property (key, value) values ($1, $2);
 raise notice 'insert % %', $1, $2;
exception when others then 
 update app.integer_property set value=$2 where key=$1;
 raise notice 'update % %', $1, $2;
end;
$$ language 'plpgsql';

create or replace function pg_temp.insert_or_update_string(text, text)
returns void as $$
begin
 insert into app.string_property (key, value) values ($1, $2);
 raise notice 'insert % %', $1, $2;
exception when others then 
 update app.string_property set value=$2 where key=$1;
 raise notice 'update % %', $1, $2;
end;
$$ language 'plpgsql';


-- parameters
select pg_temp.insert_or_update_bool('astk.service.active', true);
select pg_temp.insert_or_update_int('astk.service.interval', 120);
select pg_temp.insert_or_update_string('astk.service.url', 'http://10.1.47.122:8101');
select pg_temp.insert_or_update_string('astk.service.directory', '/tmp');
