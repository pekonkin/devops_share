#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh


# параметры ВМ и интерфейс, смотрящий в сеть данных (приложений)

VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c32768.wf.tr1        em1-labeled 192.168.38.2/24   sl-data

VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c32768.wf.tr2        em1-labeled 192.168.38.3/24   sl-data

VCPUS=1 VRAM=15000 VDISKSIZE=140000000000  VDISKTYPE=unenc KICKSTARTFILE=psql.ks $DC l1c262143.psql.tr1 em1-labeled 192.168.39.2/24 sl-data

VCPUS=2 VRAM=1000 VDISKSIZE=40000000000   KICKSTARTFILE=repoz.ks    $DC  l1c262143.repo.tr1   em1-labeled 192.168.39.3/24  sl-data

VCPUS=1 VRAM=2000  VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c262143.wf.tr1        em1-labeled 10.300.300.1/24   	sl-data

VCPUS=1 VRAM=2000  VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c262143.wf.tr2        em1-labeled 10.300.300.2/24   	sl-data


sleep 60


sudo DomainControl l1c32768.wf.tr1 				stop
sudo DomainControl l1c32768.wf.tr2				stop

sudo DomainControl l1c262143.psql.tr1 			stop
sudo DomainControl l1c262143.repo.tr1 			stop

sudo DomainControl l1c262143.wf.tr1 			stop
sudo DomainControl l1c262143.wf.tr2				stop


# добавление IP-адреса сети общей зоны в файл описания ВМ (xml) и gateway

$NAR2 l1c32768.wf.tr1       	em1-labeled 192.168.39.0/24 192.168.38.254
$NAR2 l1c32768.wf.tr2       	em1-labeled 192.168.39.0/24 192.168.38.254

$NAR2 l1c262143.psql.tr1       	em1-labeled 192.168.39.0/24 192.168.39.254
$NAR2 l1c262143.repo.tr1       	em1-labeled 192.168.39.0/24 192.168.39.254

$NAR2 l1c262143.wf.tr1       	em1-labeled 192.168.39.0/24 192.168.39.254
$NAR2 l1c262143.wf.tr2      	em1-labeled 192.168.39.0/24 192.168.39.254



# фильтр с какой сети разрешить обмен (192.168.34.0/24 - общая зона, 192.168.22.0/24 - пользовательские зоны)

$NAR l1c32768.wf.tr1       	em1-labeled 192.168.200.0/24 192.169.1.0/24
$NAR l1c32768.wf.tr2       	em1-labeled 192.168.200.0/24 192.169.1.0/24

$NAR l1c262143.psql.vb1       	em1-labeled 192.168.200.0/24 192.168.31.0/24 192.168.77.0/24
$NAR l1c262143.psql.vb2       	em1-labeled 192.168.200.0/24 192.168.31.0/24 192.168.77.0/24

$NAR l1c262143.wf.vb1       	em1-labeled 192.168.200.0/24 192.168.31.0/24 192.168.77.0/24
$NAR l1c262143.wf.vb2       	em1-labeled 192.168.200.0/24 192.168.31.0/24 192.168.77.0/24




sudo DomainControl l1c32768.wf.tr1 				start
sudo DomainControl l1c32768.wf.tr2				start

sudo DomainControl l1c262143.psql.tr1 			start
sudo DomainControl l1c262143.repo.tr1 			start

sudo DomainControl l1c262143.wf.tr1 			start
sudo DomainControl l1c262143.wf.tr2				start