#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh


# параметры ВМ и интерфейс, смотрящий в сеть данных (приложений)

VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c1.wf.pu1         em1-labeled 192.168.1.4/24   sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c2.wf.pu1         em1-labeled 192.168.2.4/24   sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c4.wf.pu1         em1-labeled 192.168.3.4/24   sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c8.wf.pu1         em1-labeled 192.168.4.4/24   sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c16.wf.pu1        em1-labeled 192.168.5.4/24   sl-data
VCPUS=1 VRAM=15000 VDISKSIZE=140000000000  VDISKTYPE=unenc KICKSTARTFILE=psql.ks $DC l1c262143.psql.pu1 em1-labeled 192.168.200.5/24 sl-data
VCPUS=1 VRAM=1000 VDISKSIZE=160000000000   KICKSTARTFILE=repoz.ks    $DC  l1c262143.repo.pu1 em1-labeled 192.168.200.20/24  sl-data



sleep 60


sudo DomainControl l1c1.wf.pu1 				stop
sudo DomainControl l1c2.wf.pu1				stop
sudo DomainControl l1c4.wf.pu1 				stop
sudo DomainControl l1c8.wf.pu1 				stop
sudo DomainControl l1c16.wf.pu1 			stop
sudo DomainControl l1c262143.psql.pu1 		stop
sudo DomainControl l1C262143.repo.pu1 		stop


# добавление IP-адреса сети общей зоны в файл описания ВМ (xml) и gateway

$NAR2 l1c1.wf.pu1       	em1-labeled 192.168.200.0/24 192.168.1.254
$NAR2 l1c2.wf.pu1 			em1-labeled 192.168.200.0/24 192.168.2.254
$NAR2 l1c4.wf.pu1 			em1-labeled 192.168.200.0/24 192.168.3.254
$NAR2 l1c8.wf.pu1 			em1-labeled 192.168.200.0/24 192.168.4.254
$NAR2 l1c16.wf.pu1 			em1-labeled 192.168.200.0/24 192.168.5.254
$NAR2 l1c262143.psql.pu1   	em1-labeled 192.168.50.0/24 192.168.200.254
$NAR2 l1C262143.repo.pu1   	em1-labeled 192.168.50.0/24 192.168.200.254



# фильтр с какой сети разрешить обмен (192.168.34.0/24 - общая зона, 192.168.22.0/24 - пользовательские зоны)

$NAR l1c1.wf.pu1  	     	em1-labeled 192.168.50.0/24
$NAR l1c2.wf.pu1 			em1-labeled 192.168.50.0/24
$NAR l1c4.wf.pu1 			em1-labeled 192.168.50.0/24
$NAR l1c8.wf.pu1 			em1-labeled 192.168.50.0/24
$NAR l1c16.wf.pu1 			em1-labeled 192.168.50.0/24
$NAR l1c262143.psql.pu1		em1-labeled 192.168.1.0/24 192.168.2.0/24 192.168.3.0/24 192.168.4.0/24 192.168.5.0/24 192.168.6.0/24 192.168.7.0/24 192.168.8.0/24 192.168.13.0/24
$NAR l1C262143.repo.pu1		em1-labeled 192.168.1.0/24 192.168.2.0/24 192.168.3.0/24 192.168.4.0/24 192.168.5.0/24 192.168.6.0/24 192.168.7.0/24 192.168.8.0/24 192.168.13.0/24

sudo DomainControl l1c1.wf.pu1 				start
sudo DomainControl l1c2.wf.pu1				start
sudo DomainControl l1c4.wf.pu1 				start
sudo DomainControl l1c8.wf.pu1 				start
sudo DomainControl l1c16.wf.pu1 			start
sudo DomainControl l1c262143.psql.pu1 		start
sudo DomainControl l1C262143.repo.pu1 		start