#!/bin/sh
ZONE=$1
INTERFACE=$2
IPADDR=$3
# Network type {sl-data-rtr; sl-data; sl-term}
NWFILTER=$4

execscript() {
sudo BridgeAttach $ZONE << EOF
$INTERFACE
$IPADDR
$NWFILTER
EOF

}

execscript