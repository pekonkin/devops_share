#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh


# параметры ВМ и интерфейс, смотрящий в сеть данных (приложений)
VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c1.wf.pu2         em1-labeled 192.168.1.5/24   sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c2.wf.pu2         em1-labeled 192.168.2.5/24   sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c4.wf.pu2         em1-labeled 192.168.3.5/24   sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c8.wf.pu2         em1-labeled 192.168.4.5/24   sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c16.wf.pu2        em1-labeled 192.168.5.5/24   sl-data
VCPUS=1 VRAM=15000 VDISKSIZE=140000000000  VDISKTYPE=unenc KICKSTARTFILE=psql.ks $DC l1c262143.psql.pu2 em1-labeled 192.168.200.6/24 sl-data




sleep 60


sudo DomainControl l1c1.wf.pu2 				stop
sudo DomainControl l1c2.wf.pu2				stop
sudo DomainControl l1c4.wf.pu2 				stop
sudo DomainControl l1c8.wf.pu2 				stop
sudo DomainControl l1c16.wf.pu2 			stop
sudo DomainControl l1c262143.psql.pu2 		stop



# добавление IP-адреса сети общей зоны в файл описания ВМ (xml) и gateway

$NAR2 l1c1.wf.pu2       	em1-labeled 192.168.200.0/24 192.168.1.254
$NAR2 l1c2.wf.pu2 			em1-labeled 192.168.200.0/24 192.168.2.254
$NAR2 l1c4.wf.pu2 			em1-labeled 192.168.200.0/24 192.168.3.254
$NAR2 l1c8.wf.pu2 			em1-labeled 192.168.200.0/24 192.168.4.254
$NAR2 l1c16.wf.pu2 			em1-labeled 192.168.200.0/24 192.168.5.254
$NAR2 l1c262143.psql.pu2   	em1-labeled 192.168.50.0/24 192.168.200.254




# фильтр с какой сети разрешить обмен (192.168.34.0/24 - общая зона, 192.168.22.0/24 - пользовательские зоны)

$NAR l1c1.wf.pu2  	     	em1-labeled 192.168.50.0/24
$NAR l1c2.wf.pu2 			em1-labeled 192.168.50.0/24
$NAR l1c4.wf.pu2 			em1-labeled 192.168.50.0/24
$NAR l1c8.wf.pu2 			em1-labeled 192.168.50.0/24
$NAR l1c16.wf.pu2 			em1-labeled 192.168.50.0/24
$NAR l1c262143.psql.pu2 		em1-labeled 192.168.1.0/24 192.168.2.0/24 192.168.3.0/24 192.168.4.0/24 192.168.5.0/24 192.168.6.0/24 192.168.7.0/24 192.168.8.0/24 192.168.13.0/24


sudo DomainControl l1c1.wf.pu2 				start
sudo DomainControl l1c2.wf.pu2				start
sudo DomainControl l1c4.wf.pu2 				start
sudo DomainControl l1c8.wf.pu2 				start
sudo DomainControl l1c16.wf.pu2 			start
sudo DomainControl l1c262143.psql.pu2 		start


