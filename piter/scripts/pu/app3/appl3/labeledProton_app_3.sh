#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh


# параметры ВМ и интерфейс, смотрящий в сеть данных (приложений)

VCPUS=1 VRAM=2000  VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c32.wf.pu1          em1-labeled 192.168.6.4/24   	sl-data
VCPUS=1 VRAM=2000  VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c64.wf.pu1          em1-labeled 192.168.7.4/24   	sl-data
VCPUS=1 VRAM=2000  VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c262143.kuf.pu1       em1-labeled 192.168.200.11/24   	sl-data
VCPUS=1 VRAM=2000  VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c262143.wf.pu1        em1-labeled 10.50.50.100/24   	sl-data
VCPUS=1 VRAM=3000   VDISKSIZE=140000000000  KICKSTARTFILE=gis.ks     $DC l1c262143.gis.pu1       em1-labeled 192.168.200.14/24   	sl-data
VCPUS=1 VRAM=2000  VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l0c1.ob.pu1        em1-labeled 192.168.13.2/24  	sl-data





sleep 60

sudo DomainControl l1c32.wf.pu1         stop
sudo DomainControl l1c64.wf.pu1 		stop
sudo DomainControl l1c262143.wf.pu1     stop
sudo DomainControl l0c1.ob.pu1 			stop
sudo DomainControl l1c262143.kuf.pu1 	stop
sudo DomainControl l1c262143.gis.pu1 	stop



# добавление IP-адреса сети общей зоны в файл описания ВМ (xml) и gateway

$NAR2 l1c32.wf.pu1      	em1-labeled 192.168.200.0/24 192.168.6.254
$NAR2 l1c64.wf.pu1      	em1-labeled 192.168.200.0/24 192.168.7.254
$NAR2 l0c1.ob.pu1 	        em1-labeled 192.168.200.0/24 192.168.20.254
$NAR2 l1c262143.wf.pu1 	    em1-labeled 192.168.50.0/24 192.168.200.254
$NAR2 l1c262143.kuf.pu1       em1-labeled 192.168.50.0/24 192.168.200.254
$NAR2 l1c262143.gis.pu1       em1-labeled 192.168.50.0/24 192.168.200.254




# фильтр с какой сети разрешить обмен (192.168.34.0/24 - общая зона, 192.168.22.0/24 - пользовательские зоны)

$NAR l1c32.wf.pu1      		em1-labeled 192.168.50.0/24
$NAR l1c64.wf.pu1      		em1-labeled 192.168.50.0/24
$NAR l0c1.ob.pu1  	   	    em1-labeled 192.168.50.0/24
$NAR l1c262143.wf.pu1  	   	em1-labeled 192.168.1.0/24 192.168.2.0/24 192.168.3.0/24 192.168.4.0/24 192.168.5.0/24 192.168.6.0/24 192.168.7.0/24 192.168.8.0/24 192.168.13.0/24
$NAR l1c262143.kuf.pu1 	    em1-labeled 192.168.1.0/24 192.168.2.0/24 192.168.3.0/24 192.168.4.0/24 192.168.5.0/24 192.168.6.0/24 192.168.7.0/24 192.168.8.0/24 192.168.13.0/24
$NAR l1c262143.gis.pu1 	    em1-labeled 192.168.1.0/24 192.168.2.0/24 192.168.3.0/24 192.168.4.0/24 192.168.5.0/24 192.168.6.0/24 192.168.7.0/24 192.168.8.0/2492.168.13.0/24


sudo DomainControl l1c32.wf.pu1 		start
sudo DomainControl l1c64.wf.pu1 		start
sudo DomainControl l0c1.ob.pu1			start
sudo DomainControl l1c262143.wf.pu1 		start
sudo DomainControl l1c262143.kuf.pu1 		start
sudo DomainControl l1c262143.gis.pu1 		start