#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh



VCPUS=4 VRAM=12000 VDISKSIZE=150000000000  KICKSTARTFILE=repoz.ks    $DC l1c262143.repo   em1-labeled 192.168.200.1/24  sl-data


sleep 60


sudo DomainControl l1c262143.repo	stop


# добавление IP-адреса сети общей зоны в файл описания ВМ (xml) и gateway

$NAR2 l1c262143.repo	em1-labeled 192.168.200.0/24 192.168.200.254




# фильтр с какой сети разрешить обмен (192.168.34.0/24 - общая зона, 192.168.122.0/24 - пользовательские зоны)


$NAR l1c262143.repo	em1-labeled 192.168.1.0/24






# взаимодействие с открытым контуром (кластерные IP-адреса)


sudo DomainControl l1c262143.repo		start

