#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh


# параметры ВМ и интерфейс, смотрящий в сеть данных (приложений)

VCPUS=4 VRAM=2000  VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c32.wf.pu2          em1-labeled 192.168.6.4/24   	sl-data
VCPUS=4 VRAM=2000  VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c64.wf.pu2          em1-labeled 192.168.7.4/24   	sl-data
VCPUS=4 VRAM=2000  VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c262143.kuf.pu2       em1-labeled 192.168.200.12/24   	sl-data
VCPUS=4 VRAM=2000  VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c262143.wf.pu2        em1-labeled 10.50.50.101/24   	sl-data
VCPUS=4 VRAM=3000   VDISKSIZE=140000000000  KICKSTARTFILE=gis.ks     $DC l1c262143.gis.pu2       em1-labeled 192.168.200.15/24   	sl-data






sleep 60

sudo DomainControl l1c32.wf.pu2 		stop
sudo DomainControl l1c64.wf.pu2 		stop
sudo DomainControl l1c262143.wf.pu2 	    stop
sudo DomainControl l1c2621431.kuf.pu2 		stop
sudo DomainControl l1c262143.gis.pu2 		stop



# добавление IP-адреса сети общей зоны в файл описания ВМ (xml) и gateway

$NAR2 l1c32.wf.pu2      	em1-labeled 192.168.200.0/24 192.168.6.254
$NAR2 l1c64.wf.pu2      	em1-labeled 192.168.200.0/24 192.168.7.254
$NAR2 l1c262143.wf.pu2 	    em1-labeled 192.168.50.0/24 192.168.200.254
$NAR2 l1c262143.kuf.pu2       em1-labeled 192.168.50.0/24 192.168.200.254
$NAR2 l1c262143.gis.pu2       em1-labeled 192.168.50.0/24 192.168.200.254




# фильтр с какой сети разрешить обмен (192.168.34.0/24 - общая зона, 192.168.22.0/24 - пользовательские зоны)

$NAR l1c32.wf.pu2      		em1-labeled 192.168.50.0/24
$NAR l1c64.wf.pu2      		em1-labeled 192.168.50.0/24
$NAR l1c262143.wf.pu2  	   	em1-labeled 192.168.1.0/24 192.168.2.0/24 192.168.3.0/24 192.168.4.0/24 192.168.5.0/24 192.168.6.0/24 192.168.7.0/24 192.168.8.0/24 192.168.13.0/24
$NAR l1c262143.kuf.pu2 	    em1-labeled 192.168.1.0/24 192.168.2.0/24 192.168.3.0/24 192.168.4.0/24 192.168.5.0/24 192.168.6.0/24 192.168.7.0/24 192.168.8.0/24 192.168.13.0/24
$NAR l1c262143.gis.pu2 	    em1-labeled 192.168.1.0/24 192.168.2.0/24 192.168.3.0/24 192.168.4.0/24 192.168.5.0/24 192.168.6.0/24 192.168.7.0/24 192.168.8.0/24 192.168.13.0/24


sudo DomainControl l1c32.wf.pu2 		start
sudo DomainControl l1c64.wf.pu2 		start
sudo DomainControl l1c262143.wf.pu2 		start
sudo DomainControl l1c262143.kuf.pu2 		start
sudo DomainControl l1c262143.gis.pu2 		start


