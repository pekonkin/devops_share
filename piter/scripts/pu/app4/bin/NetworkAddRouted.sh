#!/bin/sh
PARAMCOUNT=$#
ZONE=$1
INTERFACE=$2
NETWORK=""

execscript() {
sudo NetworkAddRouted $ZONE << EOF
$INTERFACE
$NETWORK
none
EOF

}

for ((i=3 ; i <= $PARAMCOUNT ; i++))
do
    NETWORK=$3
    execscript
    shift
done

