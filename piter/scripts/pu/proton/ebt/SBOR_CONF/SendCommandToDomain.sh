#!/bin/bash

if [ $# -ge 2 ]; then
    VMNAME=$1
    shift
    COMMAND=$*
    if [ $(virsh list | grep -v '^$' | tail -n +3 | awk '{print $2}'| grep "^$VMNAME$") ]; then
	if [ -x "socat" ]; then
	    echo "$COMMAND" |socat -t 1 - `virsh ttyconsole $VMNAME`|grep -v "$COMMAND\|\[.*]#"
	    exit 0
	else
    	    echo "Error: Please install socat and try again."
	    exit 1
	fi
    else
	echo "Error: Domain is not running or not exist."
	exit 1
    fi
else
    echo "Error: You must specify not less than two parameters."
    exit 1
fi