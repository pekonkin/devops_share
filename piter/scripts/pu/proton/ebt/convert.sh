ls -l /home/vmdomains/
GEN=$(uuidgen)
echo $GEN
echo "Название IMG на конвертацию: " 
read IMG
echo "Название IMG на выходе: "
read IMG_EBT

mkdir -p img_dir
#touch img/$IMG.xml
cat <<EOF > img_dir/$IMG.xml
<secret ephemeral='no' private='no'>
   <uuid>$GEN</uuid>
   <usage type='volume'>
     <volume>/home/vmdomains/$IMG_EBT</volume>
   </usage>
</secret>
EOF

virsh secret-define img_dir/$IMG.xml

GEN_PASS=$(/opt/z36c/sbin/GenPassword)
echo "При запросе введи пароль:"
echo $GEN_PASS

HUITA=$(echo -n "$GEN_PASS"|base64)
echo $HUITA

echo
virsh secret-set-value $GEN --base64 $HUITA

qemu-img convert /home/vmdomains/$IMG -O qcow2 /home/vmdomains/$IMG_EBT -p -o encryption

#qemu-img convert -f raw /home/vmdomains/$IMG -O qcow2 /home/vmdomains/$IMG_EBT -p -o encryption
#glance image-update --property hw_disk_bus='ide' image_id


echo "В файле описании виртуальной машины (l1c*.xml) в описание второго" 
echo "диска добавляешь строчки аналогично основному диску:" 
#заменив d0c609b9-7717-4d76-9994-bac828a12fbb на то, что выдала uuidgen в шаге 2.
echo "Замени на блок ниже, для сконвертированного диска:"
echo 
echo "<driver name='qemu' type='qcow2' cache='none'/>"
echo "<source file='/home/vmdomains/$IMG_EBT'/>"
echo "<target dev='vdb' bus='virtio'/>" 
echo "<encryption format='qcow'>"
echo "<secret type='passphrase' uuid='$GEN'/>"
echo "</encryption>"
echo 
echo "raw меняется qcow2"  
echo "<source file='/home/vmdomains/$IMG'/>"
echo "на" 
echo "<source file='/home/vmdomains/$IMG_EBT'/>"
echo "Всё"



