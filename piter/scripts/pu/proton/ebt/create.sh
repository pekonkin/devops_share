ls -l /home/vmdomains/
GEN=$(uuidgen)
echo $GEN
echo "Название ЗОНЫ: " 
read ZONE
echo "Название IMG на выходе: "
read IMG_EBT

virsh destroy $ZONE

echo "Размер диска в GB: "
read SI_ZE

SIZE=$[$SI_ZE * 1000000000]


mkdir -p img_dir

cat <<EOF > img_dir/$IMG_EBT.disk.xml
<volume>
  <name>$IMG_EBT</name>
  <capacity>$SIZE</capacity>
  <target>
    <path>/home/vmdomains/$IMG_EBT</path>
    <format type='qcow2'/>
    <encryption format='qcow'>
      <secret type='passphrase' uuid='$GEN'/>
    </encryption>
  </target>
</volume>
EOF

cat <<EOF > img_dir/$IMG_EBT.disk_secret.xml 
<secret ephemeral='no' private='no'>
  <uuid>$GEN</uuid>
  <usage type='volume'>
    <volume>/home/vmdomains/$IMG_EBT</volume>
  </usage>
</secret>
EOF

cat <<EOF > img_dir/$IMG_EBT.device.xml
 <disk type='file' device='disk'>
      <driver name='qemu' type='qcow2' cache='none'/>
      <source file='/home/vmdomains/$IMG_EBT'/>
      <target dev='vdh' bus='virtio'/>
      <encryption format='qcow'>
        <secret type='passphrase' uuid='$GEN'/>
      </encryption>
    </disk>
EOF

virsh secret-define img_dir/$IMG_EBT.disk_secret.xml

GEN_PASS=$(/opt/z36c/sbin/GenPassword)
echo "При запросе введи пароль:"
echo $GEN_PASS

HUITA=$(echo -n "$GEN_PASS"|base64)
echo $HUITA

echo
virsh secret-set-value $GEN --base64 $HUITA

virsh vol-create vmdomains img_dir/$IMG_EBT.disk.xml

virsh attach-device --config $ZONE img_dir/$IMG_EBT.device.xml

virsh start $ZONE
