#!/bin/bash

#DOMLIST="$(virsh list --all|tail -n +3|grep -v '^$'|awk '{print $2}')"
DOMLIST="l1c2"
DBDIR="$(pwd)"
HOSTNAME="$(hostname)"


function CheckRPM {

    RPM_LIST=$(chroot $MNTDIR /bin/rpm -qa|sort)
    ERRORS="$DOMDIR/errors.log"
    RPM_DIFFS="$DOMDIR/rpm_diff.log"
    
    for RPM in $RPM_LIST
    do
	echo "Checking $RPM package in domain $DOM"
	RPM_MD5=$(chroot $MNTDIR /bin/rpm -q --qf "%{SIGMD5}" $RPM)
	ISINREPO=1
        ISMD5MATCH=1
        while read CHKSUM RPM_NAME
        do
        	if [ "$RPM" == "$RPM_NAME" ]; then
        		ISINREPO=0
        		    if [ "$RPM_MD5" == "$CHKSUM" ]; then
        			    ISMD5MATCH=0
        			    RPM_VERIFY="$(chroot $MNTDIR /bin/rpm -V $RPM)"

        			    if [ -n "$RPM_VERIFY" ]; then
        				     echo "------------------------------------------------------" >>$RPM_DIFFS
        				     echo "Package: $RPM" >>$RPM_DIFFS
        				     echo >>$RPM_DIFFS
        				     echo "Files are different from those in the package:" >>$RPM_DIFFS
        				     echo "$RPM_VERIFY" >>$RPM_DIFFS
        			    fi
        		    fi
        		break
        	fi
        done < $REPO_TYPE
        if [ "$ISINREPO" -eq "1" ]; then
    		echo "------------------------------------------------------" >>$ERRORS
        	echo "$RPM is not found in repo" >>$ERRORS
        else
        	if [ "$ISMD5MATCH" -eq "1" ]; then
        		echo "------------------------------------------------------" >>$ERRORS
        		echo "Package $RPM SIGMD5 is not match with repo" >>$ERRORS
        		echo "  repo md5 is : $CHKSUM" >>$ERRORS
        		echo "  real md5 is : $RPM_MD5" >>$ERRORS
        		echo >>$ERRORS
        		
        	fi
        fi
    done
}

for DOM in $DOMLIST
do
    DOMDIR="$DBDIR/$HOSTNAME/$DOM"

    if [ ! -d "$DOMDIR" ]; then
            mkdir -p "$DOMDIR"
    fi

    DESC="$(virsh desc $DOM)"
    OSTYPE="${DESC/*:/}"
    case "$OSTYPE" in
        	    native)
        		    MNTDIR=$(mktemp -d)
        		    REPO_TYPE="$(/opt/z36c/sbin/GetRepoForDomain "$DOM")"
        		    /opt/z36c/sbin/MountDomainFs "$DOM" "$MNTDIR"
        		    if [ "$?" -eq "0" ]; then
				    CheckRPM
			    else
				echo "Error while mounting $DOM file system"
				exit 1
        		    fi
        		    /opt/z36c/sbin/UmountDomainFs "$DOM" "$MNTDIR"
        		    rm -rf "$MNTDIR"
        	    ;;
		
		    *)
			    echo "$DOM has unsupported os type"
		    ;;
		
    esac
done
