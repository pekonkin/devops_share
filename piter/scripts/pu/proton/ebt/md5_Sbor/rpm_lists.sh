#!/bin/bash

REPOLIST="$(find /media/zircon-36c/ -type d -name "Packages")"

for REPO in $REPOLIST
do
    REPO_FILENAME="$(echo "$REPO"|cut -d "/" -f 4)"
    echo "Create $REPO list"
    if [ "$REPO_FILENAME" == "Packages" ]; then
	REPO_FILENAME="host"
    fi
    find $REPO -type f -name "*.rpm" -exec rpm -q --qf "%{SIGMD5}  " -p {} \; -exec rpm -q -p {} \;|sort -k2 >>$REPO_FILENAME
done
