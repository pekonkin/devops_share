#!/bin/bash

ZONE=$1
INTERFACE=$2
NETWORK=$3
NWFILTER=$4


PEREMEN () {
echo "-|------------------|-"
echo $ZONE
echo $INTERFACE
echo $NETWORK
echo $NWFILTER
echo "-|------------------|-"
}

SBROS () {
unset VCPUS
echo $VCPUS
unset VRAM
echo $VRAM
unset VDISKSIZE
echo $VDISKSIZE
}


execscript() {
sudo -E DomainCreate $ZONE << EOF
$INTERFACE
$NETWORK
$NWFILTER
EOF

SBROS
sudo DomainControl $ZONE start
SBROS
}

PEREMEN
execscript
SBROS