#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh

# параметры ВМ и интерфейса, смотрящего в сеть данных (em1-labeled)

VCPUS=4 VRAM=12000 VDISKSIZE=60000000000 KICKSTARTFILE=term.ks $DC l1c1.pu em1-labeled  192.168.1.10/24  sl-data



sleep 100


sudo DomainControl l1c1.pu  stop



# доступ к ADMINLOW (LDAP на сервере приложений) через маршрутизатор зоны (например, 192.168.1.254)

$NAR2 l1c1.pu   em1-labeled 192.168.50.0/24 192.168.1.254



# фильтр из какой зоны разрешить обмен (здесь указываем общую зону)

$NAR l1c1.pu    em1-labeled 192.168.200.0/24


sudo DomainControl l1c1.pu start