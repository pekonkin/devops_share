#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh


# параметры ВМ и интерфейс, смотрящий в сеть данных (приложений)

VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c256.wf.vb1         em1-labeled 192.168.23.2/24   sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c256.wf.vb2         em1-labeled 192.168.23.3/24   sl-data


VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c512.wf.vb1         em1-labeled 192.168.24.2/24   sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c512.wf.vb2         em1-labeled 192.168.24.3/24   sl-data


VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c1024.wf.vb1         em1-labeled 192.168.25.2/24   sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c1024.wf.vb2         em1-labeled 192.168.25.3/24   sl-data


VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c2048.wf.vb1         em1-labeled 192.168.26.2/24   sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c2048.wf.vb2         em1-labeled 192.168.26.3/24   sl-data


VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c4096.wf.vb1        em1-labeled 192.168.27.2/24   sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c4096.wf.vb2        em1-labeled 192.168.27.3/24   sl-data


VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c8192.wf.vb1        em1-labeled 192.168.28.2/24   sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c8192.wf.vb2        em1-labeled 192.168.28.3/24   sl-data


VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c16384.wf.vb1        em1-labeled 192.168.29.2/24   sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c16384.wf.vb2        em1-labeled 192.168.29.3/24   sl-data


VCPUS=1 VRAM=15000 VDISKSIZE=140000000000  VDISKTYPE=unenc KICKSTARTFILE=psql.ks $DC l1c262143.psql.vb1 em1-labeled 192.168.31.2/24 sl-data
VCPUS=1 VRAM=15000 VDISKSIZE=140000000000  VDISKTYPE=unenc KICKSTARTFILE=psql.ks $DC l1c262143.psql.vb2 em1-labeled 192.168.31.3/24 sl-data


VCPUS=1 VRAM=2000  VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c262143.wf.vb1        em1-labeled 10.100.100.2/24   	sl-data
VCPUS=1 VRAM=2000  VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c262143.wf.vb2        em1-labeled 10.100.100.3/24   	sl-data


sleep 60


sudo DomainControl l1c256.wf.vb1 				stop
sudo DomainControl l1c256.wf.vb2 				stop

sudo DomainControl l1c512.wf.vb1 				stop
sudo DomainControl l1c512.wf.vb2 				stop

sudo DomainControl l1c1024.wf.vb1 				stop
sudo DomainControl l1c1024.wf.vb2				stop

sudo DomainControl l1c2048.wf.vb1 				stop
sudo DomainControl l1c2048.wf.vb2 				stop

sudo DomainControl l1c4096.wf.vb1 				stop
sudo DomainControl l1c4096.wf.vb2 				stop

sudo DomainControl l1c8192.wf.vb1 				stop
sudo DomainControl l1c8192.wf.vb2 				stop

sudo DomainControl l1c16384.wf.vb1 				stop
sudo DomainControl l1c16384.wf.vb2 				stop

sudo DomainControl l1c262143.psql.vb1 				stop
sudo DomainControl l1c262143.psql.vb2				stop

sudo DomainControl l1c262143.wf.vb1 				stop
sudo DomainControl l1c262143.wf.vb2 				stop




# добавление IP-адреса сети общей зоны в файл описания ВМ (xml) и gateway

$NAR2 l1c256.wf.vb1       	em1-labeled 192.168.31.0/24 192.168.23.254
$NAR2 l1c256.wf.vb2       	em1-labeled 192.168.31.0/24 192.168.23.254

$NAR2 l1c512.wf.vb1       	em1-labeled 192.168.31.0/24 192.168.24.254
$NAR2 l1c512.wf.vb2       	em1-labeled 192.168.31.0/24 192.168.24.254

$NAR2 l1c1024.wf.vb1       	em1-labeled 192.168.31.0/24 192.168.25.254
$NAR2 l1c1024.wf.vb2       	em1-labeled 192.168.31.0/24 192.168.25.254

$NAR2 l1c2048.wf.vb1       	em1-labeled 192.168.31.0/24 192.168.26.254
$NAR2 l1c2048.wf.vb2       	em1-labeled 192.168.31.0/24 192.168.26.254

$NAR2 l1c4096.wf.vb1       	em1-labeled 192.168.31.0/24 192.168.27.254
$NAR2 l1c4096.wf.vb2       	em1-labeled 192.168.31.0/24 192.168.27.254

$NAR2 l1c8192.wf.vb1       	em1-labeled 192.168.31.0/24 192.168.28.254
$NAR2 l1c8192.wf.vb2       	em1-labeled 192.168.31.0/24 192.168.28.254

$NAR2 l1c16384.wf.vb1       	em1-labeled 192.168.31.0/24 192.168.29.254
$NAR2 l1c16384.wf.vb2       	em1-labeled 192.168.31.0/24 192.168.29.254

$NAR2 l1c262143.psql.vb1       	em1-labeled 192.168.31.0/24 192.168.31.254
$NAR2 l1c262143.psql.vb2       	em1-labeled 192.168.31.0/24 192.168.31.254

$NAR2 l1c262143.wf.vb1       	em1-labeled 192.168.31.0/24 192.168.31.254
$NAR2 l1c262143.wf.vb2       	em1-labeled 192.168.31.0/24 192.168.31.254



# фильтр с какой сети разрешить обмен (192.168.34.0/24 - общая зона, 192.168.22.0/24 - пользовательские зоны)

$NAR l1c256.wf.vb1       	em1-labeled 192.168.200.0/24 192.169.1.0/24
$NAR l1c256.wf.vb2       	em1-labeled 192.168.200.0/24 192.169.1.0/24

$NAR l1c512.wf.vb1       	em1-labeled 192.168.200.0/24 192.169.1.0/24
$NAR l1c512.wf.vb2       	em1-labeled 192.168.200.0/24 192.169.1.0/24

$NAR l1c1024.wf.vb1       	em1-labeled 192.168.200.0/24 192.169.1.0/24
$NAR l1c1024.wf.vb2       	em1-labeled 192.168.200.0/24 192.169.1.0/24

$NAR l1c2048.wf.vb1       	em1-labeled 192.168.200.0/24 192.169.1.0/24
$NAR l1c2048.wf.vb2       	em1-labeled 192.168.200.0/24 192.169.1.0/24

$NAR l1c4096.wf.vb1       	em1-labeled 192.168.200.0/24 192.169.1.0/24
$NAR l1c4096.wf.vb2       	em1-labeled 192.168.200.0/24 192.169.1.0/24

$NAR l1c8192.wf.vb1       	em1-labeled 192.168.200.0/24 192.169.1.0/24
$NAR l1c8192.wf.vb2       	em1-labeled 192.168.200.0/24 192.169.1.0/24

$NAR l1c16384.wf.vb1       	em1-labeled 192.168.200.0/24 192.169.1.0/24
$NAR l1c16384.wf.vb2       	em1-labeled 192.168.200.0/24 192.169.1.0/24

$NAR l1c262143.psql.vb1       	em1-labeled 192.168.200.0/24 192.168.23.0/24 192.168.24.0/24 192.168.25.0/24 192.168.26.0/24 192.168.27.0/24 192.168.28.0/24 192.168.29.0/24
$NAR l1c262143.psql.vb2       	em1-labeled 192.168.200.0/24 192.168.23.0/24 192.168.24.0/24 192.168.25.0/24 192.168.26.0/24 192.168.27.0/24 192.168.28.0/24 192.168.29.0/24

$NAR l1c262143.wf.vb1       	em1-labeled 192.168.200.0/24
$NAR l1c262143.wf.vb2       	em1-labeled 192.168.200.0/24




sudo DomainControl l1c256.wf.vb1 				start
sudo DomainControl l1c256.wf.vb2 				start

sudo DomainControl l1c512.wf.vb1 				start
sudo DomainControl l1c512.wf.vb2 				start

sudo DomainControl l1c1024.wf.vb1 				start
sudo DomainControl l1c1024.wf.vb2				start

sudo DomainControl l1c2048.wf.vb1 				start
sudo DomainControl l1c2048.wf.vb2 				start

sudo DomainControl l1c4096.wf.vb1 				start
sudo DomainControl l1c4096.wf.vb2 				start

sudo DomainControl l1c8192.wf.vb1 				start
sudo DomainControl l1c8192.wf.vb2 				start

sudo DomainControl l1c16384.wf.vb1 				start
sudo DomainControl l1c16384.wf.vb2 				start

sudo DomainControl l1c262143.psql.vb1 				start
sudo DomainControl l1c262143.psql.vb2				start

sudo DomainControl l1c262143.wf.vb1 				start
sudo DomainControl l1c262143.wf.vb2 				start