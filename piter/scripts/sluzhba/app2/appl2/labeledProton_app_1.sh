#!/bin/bash
sudo -v

UpdateTime() {
            while true
                do
                        sudo -v
                        sleep 60
                done
}

UpdateTime &

DC=../bin/DomainCreate.sh
NAR=../bin/NetworkAddRouted.sh
NAR2=../bin/NetworkAddRouted2.sh
BA=../bin/BridgeAttach.sh


# параметры ВМ и интерфейс, смотрящий в сеть данных (приложений)


VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c1.wf.sb1         em1-labeled 192.168.68.2/24   sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c1.wf.sb2         em1-labeled 192.168.68.3/24   sl-data


VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c256.wf.sb1         em1-labeled 192.168.69.2/24   sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c256.wf.sb2         em1-labeled 192.168.69.3/24   sl-data


VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c512.wf.sb1         em1-labeled 192.168.70.2/24   sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c512.wf.sb2         em1-labeled 192.168.70.3/24   sl-data


VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c1024.wf.sb1         em1-labeled 192.168.71.2/24   sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c1024.wf.sb2         em1-labeled 192.168.71.3/24   sl-data


VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c2048.wf.sb1         em1-labeled 192.168.72.2/24   sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c2048.wf.sb2         em1-labeled 192.168.72.3/24   sl-data


VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c4096.wf.sb1        em1-labeled 192.168.73.2/24   sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c4096.wf.sb2        em1-labeled 192.168.73.3/24   sl-data


VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c8192.wf.sb1        em1-labeled 192.168.74.2/24   sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c8192.wf.sb2        em1-labeled 192.168.74.3/24   sl-data


VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c16384.wf.sb1        em1-labeled 192.168.75.2/24   sl-data
VCPUS=1 VRAM=2000 VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c16384.wf.sb2        em1-labeled 192.168.75.3/24   sl-data


VCPUS=1 VRAM=15000 VDISKSIZE=140000000000  VDISKTYPE=unenc KICKSTARTFILE=psql.ks $DC l1c262143.psql.sb1 em1-labeled 192.168.77.2/24 sl-data
VCPUS=1 VRAM=15000 VDISKSIZE=140000000000  VDISKTYPE=unenc KICKSTARTFILE=psql.ks $DC l1c262143.psql.sb2 em1-labeled 192.168.77.3/24 sl-data

VCPUS=2 VRAM=1000 VDISKSIZE=40000000000   KICKSTARTFILE=repoz.ks    $DC  l1c262143.repo.sb2   em1-labeled 192.168.77.30/24  sl-data

VCPUS=1 VRAM=2000  VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c262143.wf.sb1        em1-labeled 10.200.200.2/24   	sl-data
VCPUS=1 VRAM=2000  VDISKSIZE=60000000000  KICKSTARTFILE=wf.ks      $DC l1c262143.wf.sb2        em1-labeled 10.200.200.3/24   	sl-data


sleep 60
sudo DomainControl l1c262143.repo.sb2		stop

sudo DomainControl l1c1.wf.sb1 				stop
sudo DomainControl l1c1.wf.sb2 				stop

sudo DomainControl l1c256.wf.sb1 				stop
sudo DomainControl l1c256.wf.sb2 				stop

sudo DomainControl l1c512.wf.sb1 				stop
sudo DomainControl l1c512.wf.sb2 				stop

sudo DomainControl l1c1024.wf.sb1 				stop
sudo DomainControl l1c1024.wf.sb2				stop

sudo DomainControl l1c2048.wf.sb1 				stop
sudo DomainControl l1c2048.wf.sb2 				stop

sudo DomainControl l1c4096.wf.sb1 				stop
sudo DomainControl l1c4096.wf.sb2 				stop

sudo DomainControl l1c8192.wf.sb1 				stop
sudo DomainControl l1c8192.wf.sb2 				stop

sudo DomainControl l1c16384.wf.sb1 				stop
sudo DomainControl l1c16384.wf.sb2 				stop

sudo DomainControl l1c262143.psql.sb1 				stop
sudo DomainControl l1c262143.psql.sb2				stop

sudo DomainControl l1c262143.wf.sb1 				stop
sudo DomainControl l1c262143.wf.sb2 				stop




# добавление IP-адреса сети общей зоны в файл описания ВМ (xml) и gateway

$NAR2 l1c1.wf.sb1       	em1-labeled 192.168.77.0/24 192.168.68.254
$NAR2 l1c1.wf.sb2       	em1-labeled 192.168.77.0/24 192.168.68.254

$NAR2 l1c256.wf.sb1       	em1-labeled 192.168.77.0/24 192.168.69.254
$NAR2 l1c256.wf.sb2       	em1-labeled 192.168.77.0/24 192.168.69.254

$NAR2 l1c512.wf.sb1       	em1-labeled 192.168.77.0/24 192.168.70.254
$NAR2 l1c512.wf.sb2       	em1-labeled 192.168.77.0/24 192.168.70.254

$NAR2 l1c1024.wf.sb1       	em1-labeled 192.168.77.0/24 192.168.71.254
$NAR2 l1c1024.wf.sb2       	em1-labeled 192.168.77.0/24 192.168.71.254

$NAR2 l1c2048.wf.sb1       	em1-labeled 192.168.77.0/24 192.168.72.254
$NAR2 l1c2048.wf.sb2       	em1-labeled 192.168.77.0/24 192.168.72.254

$NAR2 l1c4096.wf.sb1       	em1-labeled 192.168.77.0/24 192.168.73.254
$NAR2 l1c4096.wf.sb2       	em1-labeled 192.168.77.0/24 192.168.73.254

$NAR2 l1c8192.wf.sb1       	em1-labeled 192.168.77.0/24 192.168.74.254
$NAR2 l1c8192.wf.sb2       	em1-labeled 192.168.77.0/24 192.168.74.254

$NAR2 l1c16384.wf.sb1       	em1-labeled 192.168.77.0/24 192.168.75.254
$NAR2 l1c16384.wf.sb2       	em1-labeled 192.168.77.0/24 192.168.75.254

$NAR2 l1c262143.psql.sb1       	em1-labeled 192.168.77.0/24 192.168.77.254
$NAR2 l1c262143.psql.sb2       	em1-labeled 192.168.77.0/24 192.168.77.254

$NAR2 l1c262143.wf.sb1       	em1-labeled 192.168.77.0/24 192.168.77.254
$NAR2 l1c262143.wf.sb2       	em1-labeled 192.168.77.0/24 192.168.77.254

$NAR2 l1c262143.repo.sb2        em1-labeled 192.168.77.0/24 192.168.77.254

# фильтр с какой сети разрешить обмен (192.168.34.0/24 - общая зона, 192.168.22.0/24 - пользовательские зоны)

$NAR l1c1.wf.sb1        	em1-labeled 192.168.200.0/24 192.168.1.0/24
$NAR l1c1.wf.sb1        	em1-labeled 192.168.200.0/24 192.168.1.0/24

$NAR l1c256.wf.sb1       	em1-labeled 192.168.200.0/24 192.168.1.0/24
$NAR l1c256.wf.sb2       	em1-labeled 192.168.200.0/24 192.168.1.0/24

$NAR l1c512.wf.sb1       	em1-labeled 192.168.200.0/24 192.168.1.0/24
$NAR l1c512.wf.sb2       	em1-labeled 192.168.200.0/24 192.168.1.0/24

$NAR l1c1024.wf.sb1       	em1-labeled 192.168.200.0/24 192.168.1.0/24
$NAR l1c1024.wf.sb2       	em1-labeled 192.168.200.0/24 192.168.1.0/24

$NAR l1c2048.wf.sb1       	em1-labeled 192.168.200.0/24 192.168.1.0/24
$NAR l1c2048.wf.sb2       	em1-labeled 192.168.200.0/24 192.168.1.0/24

$NAR l1c4096.wf.sb1       	em1-labeled 192.168.200.0/24 192.168.1.0/24
$NAR l1c4096.wf.sb2       	em1-labeled 192.168.200.0/24 192.168.1.0/24

$NAR l1c8192.wf.sb1       	em1-labeled 192.168.200.0/24 192.168.1.0/24
$NAR l1c8192.wf.sb2       	em1-labeled 192.168.200.0/24 192.168.1.0/24

$NAR l1c16384.wf.sb1       	em1-labeled 192.168.200.0/24 192.168.1.0/24
$NAR l1c16384.wf.sb2       	em1-labeled 192.168.200.0/24 192.168.1.0/24

$NAR l1c262143.psql.sb1       	em1-labeled 192.168.200.0/24 192.168.68.0/24 192.168.69.0/24 192.168.70.0/24 192.168.71.0/24 192.168.72.0/24 192.168.73.0/24 192.168.75.0/24 92.168.77.0/24
$NAR l1c262143.psql.sb1       	em1-labeled 192.168.200.0/24 192.168.68.0/24 192.168.69.0/24 192.168.70.0/24 192.168.71.0/24 192.168.72.0/24 192.168.73.0/24 192.168.75.0/24 92.168.77.0/24
$NAR l1c262143.wf.sb1       	em1-labeled 192.168.200.0/24
$NAR l1c262143.wf.sb2       	em1-labeled 192.168.200.0/24
$NAR l1c262143.repo.sb1         em1-labeled 192.168.200.0/24 192.168.68.0/24 192.168.69.0/24 192.168.70.0/24 192.168.71.0/24 192.168.72.0/24 192.168.73.0/24 192.168.75.0/24 92.168.77.0/24


sudo DomainControl l1c1.wf.sb1 			     	start
sudo DomainControl l1c1.wf.sb1  				start

sudo DomainControl l1c256.wf.sb1 				start
sudo DomainControl l1c256.wf.sb2 				start

sudo DomainControl l1c512.wf.sb1 				start
sudo DomainControl l1c512.wf.sb2 				start

sudo DomainControl l1c1024.wf.sb1 				start
sudo DomainControl l1c1024.wf.sb2				start

sudo DomainControl l1c2048.wf.sb1 				start
sudo DomainControl l1c2048.wf.sb2 				start

sudo DomainControl l1c4096.wf.sb1 				start
sudo DomainControl l1c4096.wf.sb2 				start

sudo DomainControl l1c8192.wf.sb1 				start
sudo DomainControl l1c8192.wf.sb2 				start

sudo DomainControl l1c16384.wf.sb1 				start
sudo DomainControl l1c16384.wf.sb2 				start

sudo DomainControl l1c262143.psql.sb1 				start
sudo DomainControl l1c262143.psql.sb2				start

sudo DomainControl l1c262143.wf.sb1 				start
sudo DomainControl l1c262143.wf.sb2 				start

sudo DomainControl l1c262143.repo.sb2               start