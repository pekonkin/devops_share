#!/bin/sh
#LOGDIR=/var/log/protonScripts
ZONE=$1
INTERFACE=$2
NETWORK=$3
# Network type {sl-data-rtr; sl-data; sl-term}
NWFILTER=$4

execscript() {
sudo -E DomainCreate $ZONE << EOF
$INTERFACE
$NETWORK
$NWFILTER
EOF

sudo DomainControl $ZONE start
}

execscript
