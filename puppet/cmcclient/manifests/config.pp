class cmcclient::config (
) inherits cmcclient::params
{
    if 'libcmccommon' in $cmcclient::package_names {
        file { "${cmcclient::confdir}/base.conf":
          ensure  => $cmcclient::ensure,
          content => template("${module_name}/base.conf.erb"),
        }
    }

    if 'cmchttp' in $cmcclient::package_names {
        file { "${cmcclient::confdir}/cmchttpd.conf":
          ensure  => $cmcclient::ensure,
          content => template("${module_name}/cmchttpd.conf.erb"),
        }

        file { "${cmcclient::confdir}/cmchttpclone.conf":
          ensure  => $cmcclient::ensure,
          content => template( "${module_name}/cmchttpclone.conf.erb" ),
        }
    }
}
