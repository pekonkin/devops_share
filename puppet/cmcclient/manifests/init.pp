class cmcclient (
    # base
    $ensure           = $cmcclient::params::ensure,
    $ensure_install   = $cmcclient::params::ensure_install,
    $ensure_running   = $cmcclient::params::ensure_running,
    $confdir          = $cmcclient::params::confdir,
    $package_names    = $cmcclient::params::package_names,

    #libcmccommon
    $log_level        = $cmcclient::params::log_level,
    $db_ip            = $cmcclient::params::db_ip,
    $db_port          = $cmcclient::params::db_port,
    $db_name          = $cmcclient::params::db_name,
    $db_user          = $cmcclient::params::db_user,
    $db_password      = $cmcclient::params::db_password,
    $mq_ip            = $cmcclient::params::mq_ip,
    $mq_port          = $cmcclient::params::mq_port,

    #cmchttp
    $http_ip          = $cmcclient::params::http_ip,
    $http_port        = $cmcclient::params::http_port,
    $http_server_ip   = $cmcclient::params::http_server_ip,
    $http_server_port = $cmcclient::params::http_server_port,
    $http_folder      = $cmcclient::params::http_folder
) inherits cmcclient::params
{
    contain "${module_name}::install"
    contain "${module_name}::config"
    contain "${module_name}::service"
    contain "${module_name}::backup"

    Class["${module_name}::install"]
    -> Class["${module_name}::config"]
    ~> Class["${module_name}::service"]
    -> Class["${module_name}::backup"]
}
