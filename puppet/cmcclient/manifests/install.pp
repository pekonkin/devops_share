class cmcclient::install (
) inherits cmcclient::params
{
    exec { 'update-repodata':
      command => 'apt-get update',
      path    => '/usr/local/bin/:/bin/:/usr/bin:/sbin/:/usr/sbin/'
    }

    package { $cmcclient::package_names:
        ensure => $cmcclient::ensure_install
    }
}
