class cmcclient::params (
    # base
    $ensure           = 'present',
    $ensure_install   = 'latest',
    $ensure_running   = 'running',
    $confdir          = '/opt/niitp/cmc/etc/',
    $package_names    = [ 'libcmccommon',
                          'libcmcmqcl',
                          'libcmcgui',
                          'cmcmdi',
                          'cmcconf',
                          'cmchelp',
                          'cmcdoc',
                          'cmchttp',
                          'libcmcmdi-conditions-plugin',
                          'libcmcmdi-dutyshifts-plugin',
                          'libcmcmdi-events-plugin',
                          'libcmcmdi-files-plugin',
                          'libcmcmdi-help-plugin',
                          'libcmcmdi-plans-plugin',
                          'libcmcmdi-workflow-plugin',
                          'libcmcmdi-drp-plugin'
                        ],

    #libcmccommon
    $log_level        = 'DEBUG',
    $db_ip            = '10.34.144.43',
    $db_port          = '5432',
    $db_name          = 'cmc',
    $db_user          = 'cmc',
    $db_password      = 'ltqh25',
    $mq_ip            = '10.34.144.45',
    $mq_port          = '5555',

    #cmchttp
    $http_ip          = '*',
    $http_port        = '6666',
    $http_server_ip   = '10.34.144.45',
    $http_server_port = '6666',
    $http_folder      = '/tmp3/SPOFDRP/CMC/TO'
)  {}
