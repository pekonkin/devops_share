class cmcclient::service (
) inherits cmcclient::params
{
    #cmchttp
    if 'cmchttp' in $cmcclient::package_names {
        service { 'cmchttp' :
          ensure => $cmcclient::ensure_running,
          name   => 'cmchttpd',
          enable => true,
        }
    }
}
