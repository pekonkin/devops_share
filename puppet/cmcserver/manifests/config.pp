class cmcserver::config (
) inherits cmcserver::params
{
    if 'libcmccommon' in $cmcserver::package_names {
        file { "${cmcserver::confdir}/base.conf":
          ensure  => $cmcserver::ensure,
          content => template("${module_name}/base.conf.erb"),
        }
    }

    if 'cmchttp' in $cmcserver::package_names {
        file { "${cmcserver::confdir}/cmchttpd.conf":
          ensure  => $cmcserver::ensure,
          content => template("${module_name}/cmchttpd.conf.erb"),
        }
    }

    if 'cmcmq' in $cmcserver::package_names {
        file { "${cmcserver::confdir}/cmcmqd.conf":
          ensure  => $cmcserver::ensure,
          content => template("${module_name}/cmcmqd.conf.erb")
        }
    }
}
