class cmcserver (
    # base
    $ensure            = $cmcserver::params::ensure,
    $ensure_install    = $cmcserver::params::ensure_install,
    $ensure_running    = $cmcserver::params::ensure_running,
    $confdir           = $cmcserver::params::confdir,
    $package_names     = $cmcserver::params::package_names,

    #libcmccommon
    $log_level         = $cmcserver::params::log_level,
    $db_ip             = $cmcserver::params::db_ip,
    $db_port           = $cmcserver::params::db_port,
    $db_name           = $cmcserver::params::db_name,
    $db_user           = $cmcserver::params::db_user,
    $db_password       = $cmcserver::params::db_password,
    $mq_ip             = $cmcserver::params::mq_ip,
    $mq_port           = $cmcserver::params::mq_port,

    #cmchttp
    $http_ip           = $cmcserver::params::http_ip,
    $http_port         = $cmcserver::params::http_port,

    #cmcmq
    $mq_server_port    = $cmcserver::params::mq_server_port,
    $mq_server_maxconn = $cmcserver::params::mq_server_maxconn,
) inherits cmcserver::params
{
    contain "${module_name}::install"
    contain "${module_name}::config"
    contain "${module_name}::service"
#    contain "${module_name}::monitoring"
    contain "${module_name}::backup"

    Class["${module_name}::install"]
    -> Class["${module_name}::config"]
    ~> Class["${module_name}::service"]
#    -> Class["${module_name}::monitoring"]
    -> Class["${module_name}::backup"]
}
