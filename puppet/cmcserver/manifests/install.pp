class cmcserver::install (
) inherits cmcserver::params
{
    exec { 'update-repodata':
      command => 'apt-get update',
      path    => '/usr/local/bin/:/bin/:/usr/bin:/sbin/:/usr/sbin/'
    }

    package { $cmcserver::package_names:
        ensure => $cmcserver::ensure_install
    }
}
