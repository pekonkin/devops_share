class cmcserver::monitoring (
) inherits cmcserver::params
{
    if defined( 'nagios::check' ) {
        file { '/etc/nrpe.d/cmcserver.cfg':
          ensure  => $cmcserver::ensure,
          content => template("${module_name}/nrpe/cmcserver.cfg.erb"),
          notify  => Service['nrpe'],
          require => Package['nrpe']
        }

        file { '/usr/lib/nagios/plugins/cmcserver':
          ensure  => directory,
        }

        file { '/usr/lib/nagios/plugins/cmcserver/cmcserver_status.sh':
          ensure  => $cmcserver::ensure,
          mode    => '+x',
          content => template("${module_name}/nrpe/cmcserver_status.sh.erb"),
          require => file['/usr/lib/nagios/plugins/cmcserver'],
        }

#        nagios::check { "${::fqdn}-cmcserver-status":
#          ensure              => $cmcserver::ensure,
#          export              => true,
#          host_name           => $::fqdn,
#          service_description => 'cmcserver status',
#          check_command       => 'check_nrpe!cmcserver_status',
#          use                 => 'local-service'
#        }
    }
}
