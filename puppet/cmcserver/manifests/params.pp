class cmcserver::params (
    # base
    $ensure           = 'present',
    $ensure_install   = 'latest',
    $ensure_running   = 'running',
    $confdir          = '/opt/niitp/cmc/etc/',
    $package_names    = [ 'libcmccommon',
                          'libcmcmqcl',
                          'cmccm',
                          'cmchttp',
                          'cmcmq',
                          'libcmcfom',
                          'cmcproc',
                          'libcmcfop-archive-plugin',
                          'libcmcfop-cp-plugin',
                          'libcmcfop-json2xml-plugin',
                          'libcmcfop-export-plugin',
                          'libcmcfop-http-plugin',
                          'libcmcfop-import-plugin',
                          'libcmcfop-rm-plugin',
                          'libcmcfop-unzip-plugin',
                          'libcmcfop-zip-plugin',
                          'cmcfoconf'
                        ],

    #libcmccommon
    $log_level         = 'DEBUG',
    $db_ip             = '10.34.144.43',
    $db_port           = '5432',
    $db_name           = 'cmc',
    $db_user           = 'cmc',
    $db_password       = 'ltqh25',
    $mq_ip             = '10.34.144.45',
    $mq_port           = '5555',

    #cmchttp
    $http_ip           = '*',
    $http_port         = '6666',

    #cmcmq
    $mq_server_port    = '5555',
    $mq_server_maxconn = '1000'
)  {}
