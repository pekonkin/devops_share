class cmcserver::service (
) inherits cmcserver::params
{
    if 'cmccm' in $cmcserver::package_names {
        service { 'cmccm' :
          ensure => $cmcserver::ensure_running,
          name   => 'cmccmd',
          enable => true,
        }
    }

    if 'cmchttp' in $cmcserver::package_names {
        service { 'cmchttp' :
          ensure => $cmcserver::ensure_running,
          name   => 'cmchttpd',
          enable => true,
        }
    }

    if 'cmcmq' in $cmcserver::package_names {
        service { 'cmcmq' :
          ensure => $cmcserver::ensure_running,
          name   => 'cmcmqd',
          enable => true,
        }
    }

    if 'cmcproc' in $cmcserver::package_names {
        service { 'cmcproc' :
          ensure => $cmcserver::ensure_running,
          name   => 'cmcprocd',
          enable => true,
        }
    }
}
