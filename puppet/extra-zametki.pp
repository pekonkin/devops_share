exec { 'docker exec':
  command     => "docker exec --tty $docker_container_name /etc/opt/workerCeleryArchive/start.sh",
  path    => '/usr/bin/'
}