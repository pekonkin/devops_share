#!/usr/bin/env python
import os
from subprocess import *

# Container name is unique
containerName = "pgsql"

# Port detection
netstat = Popen(["netstat", "-tpln"], stdout=PIPE)
grep = Popen(["grep", "tcp .*postgres"], stdin=netstat.stdout, stdout=PIPE).stdout.read()
if "postgres" not in grep:
  exit(0)
port = grep.split()[3].split(':')[1]
# Container confirmation
try:
  dockerps = Popen(["docker", "ps"], stdout=PIPE)
  grep = Popen(["grep", "-c", "postgresql"], stdin=dockerps.stdout, stdout=PIPE).stdout.read()
  if int(grep) < 1:
    exit(0)
  else:
    docker = True
except OSError: # Astra Linux
  docker = False
  pass

def dockerExec(container, command):
  FNULL = open(os.devnull, 'w')
  args = ["docker", "exec", "-i", container]
  for element in command:
      args.append(element)
  return Popen(args, stdout=PIPE, stderr=FNULL).stdout.read().strip()

def psql(db, query):
  if docker:
    return dockerExec(containerName, ["/opt/postgresql/bin/psql", "-p", port, "-t", "-c", "%s" % query, db, "postgres"])
  else: # Astra Linux
    FNULL = open(os.devnull, 'w')
    return Popen(["/usr/bin/psql", "-p", port, "-t", "-c", "%s" % query, db, "postgres"], stdout=PIPE, stderr=FNULL).stdout.read().strip()

def getVersion(db):
  return psql(db, "SELECT COUNT(version) FROM settings.db_migrate_log")

# Reading all availiable databases
try:
  dbListRaw = dockerExec(containerName, ["/opt/postgresql/bin/psql", "-p", port, "-U", "postgres", "-t", "-c", '\list']).splitlines()
except OSError: # Astra Linux
  FNULL = open(os.devnull, 'w')
  dbListRaw = Popen(["/usr/bin/psql", "-p", port, "-U", "postgres", "-t", "-c", '\list'], stdout=PIPE, stderr=FNULL).stdout.read().strip().splitlines()

# Getting out names (excluding service DBs)
dbList = []
filter = ["postgres"]
for line in dbListRaw:
  if line[1].isalnum():
    dbName = line.split("|", 1)[0].strip()
    if dbName in filter or "template" in dbName:
      continue
    dbList.append(dbName)

# Getting versions
for db in dbList:
  version = getVersion(db)
  if version == '':
    version = 0
  print "db_%s_version=%s" % (db,version)
