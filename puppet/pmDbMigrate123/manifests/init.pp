# = Define: db_migrate
#
# == Назначение
#
# Puppet-модуль для развёртывания структур баз данных postgresql и миграции на нужную версию структуры.
#
# == Детали реализации
#
# Модуль работает исключительно в связке с модулем postgresql.
# Перед развёртыванием структуры убедитесь, что в кластере присутствуют все необходимые роли.

# === Использование локально
#
# Модуль зависит от глобальной переменной в Foreman с репозиторием dbScripts. При необходимости применения локально через "puppet apply", можно выставить параметры версии и структуры для базы в модуле Postgresql, применить манифест и дождаться завершения С ОШИБКОЙ. После этого закоментировать связанные с миграцией параметры в манифесте, вписать адрес репозитория с dbScripts в файл /var/data/docker/pgsql/db_migrate_buffer/repo и запустить контейнер через init-скрипт: /etc/init.d/docker_db_migrate_{название базы} start. Менять версию в дальнейшем можно в init-скрипте в параметрах запуска контейнера в команде start.
#
# == Параметры
#
# [*db*]
#   Параметр указывает на базу, над которой проводятся действия.
#   Передается в виде строки.
#   Стандартное значение: undef
#
# [*structure*]
#   Параметры означает название структуры для развёртывания.
#   Передаётся в виде строки.
#   Стандартное значение: undef
#
# [*version*]
#   Параметр задаёт номер версии структуры.
#   Передаётся в виде числа.
#   Стандартное значение: 0
#
# [*overwrite*]
#   Параметр отражает необходимость пересоздания базы в процессе миграции.
#   Внимание: отсутствие данного параметра в значении `true` при понижении версии приведёт к ошибке!
#   Передаётся в виде булевого значения.
#   Стандартное значение: false
#
define db_migrate (
  $db = undef,
  $structure = undef,
  $version = "0",
  $overwrite = 'false',
  $baseline_overwrite = undef,


)  {
  include postgresql

  $current_version = getvar("db_${title}_version")

  if $::operatingsystem == 'Debian' and $::lsbdistid == 'AstraLinuxSE' and $::lsbdistrelease == '1.6'
  {

    ensure_resource('exec', 'apt-get update', {'path' => '/usr/local/bin/:/bin/:/usr/bin:/sbin/:/usr/sbin/', 'unless' => 'dpkg -s dbmigrate'})

    ensure_resource('package', [ 'dbmigrate', 'db-archive', 'db-classifier', 'db-cmc', 'db-fdrp', 'db-reference-data', 'db-task', 'db-uoi'],
      {'ensure' => 'latest'})

    if $baseline_overwrite {
      exec { "${db}-${structure}-baseline-overwrite":
        command => "/bin/echo \"$baseline_location\" > /opt/dbScripts/$structure/baseline.url",
        onlyif  => "/bin/grep \"$baseline_location\" /opt/dbScripts/$structure/baseline.url",
        before  => Exec["db_migrate_${title}"],
      }
    }

    if "$current_version" != "$version" and "$structure" != undef {
      if $overwrite {
        exec { "db_migrate_${title}":
          command => "/usr/bin/db_migrate migrate -H '127.0.0.1' -p ${postgresql::port} -d ${db} -u postgres -s ${structure} -v ${version} -o",
        }
      }
      else {
        exec { "db_migrate_${title}":
          command => "/usr/bin/db_migrate migrate -H '127.0.0.1' -p ${postgresql::port} -d ${db} -u postgres -s ${structure} -v ${version}",
        }
      }
    }
  }
}