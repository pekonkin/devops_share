class ntp::config (
  $ensure = $::ntp::ensure,
) inherits ntp::params
{
  file { '/etc/ntp.conf':
    content => template("${module_name}/ntp.conf.erb"),
    ensure  => $ensure,
  }

}
