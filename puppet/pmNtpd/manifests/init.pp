class ntp (
  $ensure = $::ntp::params::ensure,
) inherits ntp::params
{

  contain "::${module_name}::install"
  contain "::${module_name}::config"
  contain "::${module_name}::service"
#  contain "::${module_name}::monitoring"

  Class["::${module_name}::install"]
  -> Class["::${module_name}::config"]
  ~> Class["::${module_name}::service"]
#  -> Class["::${module_name}::monitoring"]
}