class ntp::install (
  $ensure = $::ntp::ensure,
) inherits ntp::params
{
  exec { 'update-repodata':
    command     => "apt-get update",
    path    => '/usr/local/bin/:/bin/:/usr/bin:/sbin/:/usr/sbin/'
  }

  package { 'ntp' :
    ensure => $ensure,
  }

}