class ntp::monitoring (
  $ensure = $::ntp::ensure,
) inherits ntp::params

{

  file { "/etc/nrpe.d/check_ntp_sync.cfg":
    ensure  => $ensure,
    content => "command[check_ntp_sync]=/usr/lib/nagios/plugins/ntpd/check_ntp_sync.sh",
    require => Package['nrpe'],
    notify  => Service["nrpe"],
  }

  file { "/etc/nrpe.d/check_ntp_offset.cfg":
    ensure  => $ensure,
    content => "command[check_ntp_offset]=/usr/lib/nagios/plugins/ntpd/check_ntp_offset.sh",
    require => Package['nrpe'],
    notify  => Service["nrpe"],
  }

  nagios::check { "$fqdn-check_ntp_sync":
    ensure              => $ensure,
    export              => true,
    check_command       => "check_ntp_sync_host!$::ipaddress",
    host_name           => $fqdn,
    service_description => "PM::ntpd::Synchronization",
    use                 => "local-service",
  }
  
  nagios::check { "$fqdn-check_ntp_offset":
    ensure              => $ensure,
    export              => true,
    check_command       => "check_ntp_offset_host!$::ipaddress",
    host_name           => $fqdn,
    service_description => "PM::ntpd::Offset",
    use                 => "local-service",
  }

  nagios::check { "$fqdn-check_ntpd_service":
    ensure              => $ensure,
    export              => true,
    check_command       => "check_ntpd_service_host!$::ipaddress!$ntphost",
    host_name           => $fqdn,
    service_description => "PM::ntpd::Service availability",
    event_handler       => "restart_ntp_host",
    use                 => "local-service",
  }

}
