class ntp::params (
  $ensure  = 'present',
  $ensure_running = 'running',
  $ntp_server_address = 'sm1.otd4.niitp.in',
  $ntphost = localhost
)  {}