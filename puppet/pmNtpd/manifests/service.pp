class ntp::service (
  $ensure = $::ntp::ensure_running,
) inherits ntp::params
{
    service { 'ntp':
    ensure  => $ensure_running,
  }
}
