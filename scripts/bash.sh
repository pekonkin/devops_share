#!/usr/bin/env bash
pg_dump -U postgres -h $PG_HOST -Fc $DB_FRONTIER -f $DUMP_DIR/$DB_FRONTIER.dump.$DATE
sed -i "s:^ENABLE_SZI_APPLY=.*$:ENABLE_SZI_APPLY=false:g" /opt/swm_spo_db_update/frontier/conf.ini
sed -i "s:^DB_HOST=.*$:DB_HOST=$PG_HOST:g" /opt/swm_spo_db_update/frontier/conf.ini
sed -i "s:^DB_NAME=.*$:DB_NAME=$DB_FRONTIER:g" /opt/swm_spo_db_update/frontier/conf.ini
sed -i "s:^REGION=.*$:REGION=$REGION:g" /opt/swm_spo_db_update/frontier/conf.ini

rsync -e 'ssh -p 22' -avzp /var/www/bamboorepoDeps/*.rpm root@10.1.39.20:/var/www/html/proton/Packages/;ssh root@10.1.39.20 createrepo_c --update /var/www/html/proton/Packages; ssh root@10.1.39.20 yum clean all
