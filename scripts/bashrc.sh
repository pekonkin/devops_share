#!/usr/bin/env bash
# .bashrc

# Source global definitions
if [[ -f /etc/bashrc ]]; then
        . /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
force_color_prompt=yes
export PS1='\[\033[01;31m\]\u\[\033[01;33m\]@\[\033[01;36m\]\h \[\033[01;33m\]\w \[\033[01;35m\]\$ \[\033[00m\]'
export LANG=ru_RU.UTF-8
export GDM_LANG=ru_RU.UTF-8
export HISTSIZE=5000
HISTFILESIZE=-1

export HISTTIMEFORMAT="%Y%m%d-%H%M%S "
[[ -z "$PROMPT_COMMAND" ]] &&
 PROMPT_COMMAND="history -a" ||
 PROMPT_COMMAND="$PROMPT_COMMAND; history -a"