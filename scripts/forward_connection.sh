#!/bin/bash


LOCAL_IP=$(/sbin/ip route get 8.8.8.8 | grep 8.8.8.8 | sed 's%^.*src \([0-9.][0-9.]*\).*%\1%')

/sbin/ip addr | grep -q ${LOCAL_IP} || {
    echo "$LOCAL_IP not found on this host" >&2
    exit 4
}


[[ $(ps uax | grep -v grep | grep -c "$0 $*") -gt 2 ]] && {
    # Запущен второй инстанс. Либо он поднимает коннекты, либо ждет, когда коннекты станут недоступными
    exit 0
}

prog=$(basename $0)

Log(){
    str="$(date +%Y%m%d-%H%M) $@"
    echo "$str" >> /var/log/${prog}.log
    logger "$str"
}

remote_user=$1             ; shift
remote_key=$1              ; shift
remote_host=$(eval echo $1); shift
remote_port=$1             ; shift
forward_port=$1            ; shift
socks_port=$1              ; shift
forward_ip=$1              ; shift
remote_forward_port=$1     ; shift
description="$*"


# Программа будет запущена второй раз и она будет ждать, пока что-то упадет, тогда погнали поднимать
while true; do
    nmap -PN ${LOCAL_IP} -p ${forward_port} --system-dns | egrep -q '[[:space:]](open|filtered)[[:space:]]' || break

    if [ ${socks_port} != '-' ] ; then
        nmap -PN ${LOCAL_IP} -p ${socks_port} --system-dns | egrep -q '[[:space:]](open|filtered)[[:space:]]' || break
    fi
    sleep 1;
done

# Если удаленный порт на удаленной машине не доступен, пропускаем (ну а что ловить, порт все равно закрыт)
nmap -Pn ${remote_host} -p ${remote_port} --system-dns | egrep -q '[[:space:]](closed|unfiltered)[[:space:]]' &&
    exit 2

CMD_F="ssh -l ${remote_user} -i ${remote_key} ${remote_host} -p ${remote_port} -L ${forward_port}:${forward_ip}:${remote_forward_port} -N -g -f -A -o ConnectTimeout=6000"

# Выполняем проверку
#
#  Если выполнятеся больше одного инстанса, убиваем лишние (см. tail -n +2)
ps uax | grep -v grep | grep "${CMD_F}" | tail -n +2 | while read u _pid _garbage; do kill ${_pid}; done


# проверяем на недоступность. Если недоступен, прокидываем ssh порт
nmap -PN ${LOCAL_IP} -p ${forward_port} --system-dns | egrep -q '[[:space:]](open|filtered)[[:space:]]' || {
    Log "Порт ${LOCAL_IP}:${forward_port} ($description) закрыт. Пробуем восстановить"
    ps uax | grep -v grep | grep "${CMD_F}" | while read u _pid _garbage; do kill ${_pid}; done
    $CMD_F
}

# то же самое для socks proxy, если он задан
if [ ${socks_port} != '-' ] ; then
    CMD_S="ssh -l ${remote_user} -i ${remote_key} ${LOCAL_IP} -p ${forward_port} -D ${socks_port} -N -g -f -A -o ConnectTimeout=6000"

    #  Если выполнятеся больше одного инстанса, убиваем лишние (см. tail -n +2)
    ps uax | grep -v grep | grep "${CMD_S}" | tail -n +2 | while read u _pid _garbage; do kill ${_pid}; done

    # проверяем на недоступность и прокидываем ssh порт
    nmap -PN ${LOCAL_IP} -p ${socks_port} --system-dns | egrep -q '[[:space:]](open|filtered)[[:space:]]' || {
        Log "Порт ${LOCAL_IP}:${socks_port} ($description) закрыт. Пробуем восстановить"
        ps uax | grep -v grep | grep "${CMD_S}"| while read u _pid _garbage; do kill ${_pid}; done
        $CMD_S
    }
fi