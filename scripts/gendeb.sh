#!/usr/bin/env bash
#set -vx
debname=$1
version=$2
description=$3
pathtosystem=$4
postinstall=$6
depends=$5

if [[ "$1" == "-h" ]] ; then
    echo "Для того чторбы создать deb пакет нужно задать переменные:"
    echo "./gendeb.sh debname version artifactpath descripton"
    echo "--debname       - имя пакета"
    echo "--version       - версия пакета"
    echo "--pathtosystem  - полный путь где должны распологаться в системе, обычно это /opt/"
    echo "где находится скрипт создания deb пакета"
    echo "--description   - описание deb пакета"
    echo "пример содания deb пакета ./gendeb.sh dbMigrate 1.0.1 'dbMigrate for postgresql' /opt/db_migrate postinstall 'ruby-i18n,ruby-pg,rubygems-integration'"
    echo "Если нуждно применть postinstall отредактируйте секцию postinstall, при генерации"
    echo "нужно выставить параметр postinstall пример:"
    echo "./gendeb.sh dbMigrate 1.0.1 'dbMigrate for postgresql' /opt/db_migrate 'ruby-i18n,ruby-pg,rubygems-integration' postinstall"
    echo "Если нужно выставить параметр depends (зависимости)"
    echo "пример: ./gendeb.sh dbMigrate 1.0.1 'dbMigrate for postgresql' './' 'ruby-i18n,ruby-pg,rubygems-integration' postinstall"
    echo "скрипт выподняем только в папке с программой, например, все исходники нужно рассортировать по нужным папкам и относительным путям."
    exit 0

if [[ -z "$1" ]] ; then
    echo "Не задан параметр --debname воспользуйтесь справкой -h"
    exit 0

elif [[ -z "$2" ]] ; then
    echo "Не задан параметр --version воспользуйтесь справкой -h"
    exit 0

elif [[ -z "$3" ]] ; then
    echo "Не задан параметр --description справкой -h"
    exit 0

elif [[ -z "$4" ]] ; then
    echo "Не задан параметр --pathtosystem воспользуйтесь справкой -h"
    exit 0
fi

if [[ ! -f /usr/bin/fakeroot ]] ; then
   sudo apt-get install autoconf automake libtool autotools-dev dpkg-dev fakeroot -y &> /dev/null
fi

if [[ ! -f /usr/bin/fakeroot ]] ; then
   echo "нужно установить debian пакеты: autoconf automake libtool autotools-dev dpkg-dev fakeroot"
fi

if [[ ! -d ${debname}_${version}_amd64.deb/DEBIAN ]] ; then
   mkdir -p ${debname}_${version}_amd64.deb/DEBIAN
fi

if [[ ! -f ${debname}_${version}_amd64.deb/DEBIAN/control ]] ; then
touch ${debname}_${version}_amd64.deb/DEBIAN/control
touch ${debname}_${version}_amd64.deb/DEBIAN/{control,postinst,postrm,preinst,prerm}
cat >>${debname}_${version}_amd64.deb/DEBIAN/control<<EOF
Package: ${debname}
Version: ${version}
Architecture: amd64
Section: misk
Maintainer: niitp
Description: ${description}
EOF
fi

if [[ ! -z "$5" ]] ; then
cat >>${debname}_${version}_amd64.deb/DEBIAN/control<<EOF
Depends: ${depends}
EOF
fi

if [[ ! -z "$6" ]] ; then
cat >>${debname}_${version}_amd64.deb/DEBIAN/postinst<<EOF
#!/usr/bin/env bash
set -x
cp -r /opt/db_migrate/bin/db_migrate /usr/bin/
cp -r /opt/db_migrate/bin/db_migrate /bin/
EOF
fi

if [[ ! -d ${debname}_${version}_amd64.deb ]] ; then
   mkdir -p ${debname}_${version}_amd64.deb
fi

if [[ ! -d ${debname}_${version}_amd64.deb/${pathtosystem} ]] ; then
   mkdir -p ${debname}_${version}_amd64.deb/${pathtosystem}
fi

rsync -a ./* --exclude="${debname}_${version}_amd64.deb" --exclude='gendeb.sh' --exclude="debpack" --exclude="${pathtosystem}"  ${debname}_${version}_amd64.deb/${pathtosystem}/
if [[ ! -d ./debpack ]] ; then
   mkdir ./debpack
fi
chmod -R 0755 ./${debname}_${version}_amd64.deb
fakeroot dpkg-deb --build ./${debname}_${version}_amd64.deb ./debpack/${debname}_${version}_amd64.deb > /dev/null
if [[ $? == 0 ]] ; then
echo "Ваш пакет находится ./debpack/${debname}_${version}_amd64.deb"
fi
rm -rf ./${debname}_${version}_amd64.deb
fi