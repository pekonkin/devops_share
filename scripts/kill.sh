#!/usr/bin/env bash
for i in $(ps aux | grep -v grep| grep chro | gawk '{print $2}')
  do kill -9 $i
done
for i in $(ps aux | grep -v grep| grep -i xvfb | gawk '{print $2}')
  do kill -9 $i
done
rm -rf /tmp/*
sync && echo 1 > /proc/sys/vm/drop_caches
sync && echo 2 > /proc/sys/vm/drop_caches
sync && echo 3 > /proc/sys/vm/drop_caches
sync
echo service\'s stopping