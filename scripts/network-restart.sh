#!/usr/bin/env bash
x=$(ping -c1 google.com 2>&1 | grep 'Destination Net Unreachable'; ping -c1 google.com 2>&1 | grep 'Name or service not known')
y=$(route -n | grep eth0 |   awk '{print $2}'  |sed '2,4d')
if [[ ! "${x}" = "" ]]; then
        echo "It's down!! Attempting to restart."
        /usr/sbin/route add default gw ${y} eth0; /usr/sbin/route add default gw 192.168.42.129 enp0s20f0u1
fi