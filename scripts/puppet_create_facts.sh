#!/usr/bin/env bash
set -vx
uname=$(uname | tr "[:upper:]" "[:lower:]")
list=('Button_Switch' 'Power_Unit' 'System_ACPI_Power_State' 'Physical_Security' 'Watchdog_2' 'Entity_Presence' 'Add_In_Card' 'Fan' 'Watchdog_1' 'System_Event' 'Power_Supply' 'Management_Subsystem_Health' 'Voltage' 'Current' 'Version_Change' 'Critical_Interrupt' 'OS_Boot' 'Monitor_ASIC_IC' 'Cooling_Device' 'Battery' 'Module_Board' 'Temperature' 'OS_Critical_Stop' 'Other_Units_Based_Sensor' 'System_Firmware_Progress' 'Cable_Interconnect' 'Terminator' 'Platform_Security_Violation_Attempt' 'System_Boot_Initiated' 'POST_Memory_Resize' 'OEM_Reserved' 'Session_Audit' 'Processor' 'Chip_Set' 'Slot_Connector' 'Event_Logging_Disabled' 'Chassis' 'LAN' 'Other_Fru' 'FRU_State' 'Drive_Slot' 'Microcontroller_Coprocessor' 'Boot_Error' 'Memory' 'Platform_Alert')
ipmi_sensor()
{
var=$(/usr/lib64/nagios/plugins/check_ipmi_sensor -T ${j} --nosel | grep "empty"  || echo "1")
}
if [[ -f /usr/lib64/nagios/plugins/check_ipmi_sensor ]] && [[  "${uname}" == "linux" ]]; then
    for j in ${list[@]}
    do
    ipmi_sensor
    ${j,,}=${var}
    echo ${j,,}=${var}
    done
else
exit 1
echo "файл /usr/lib64/nagios/plugins/check_ipmi_sensor не найден"
fi