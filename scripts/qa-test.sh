#!/bin/bash

set -x

#TESTPROJ=frontier-testing; BRANCH=master ; PABOT_ARGS='-i smoke test'     ; SUITESFROM=smokerange.xml
#TESTPROJ=frontier-testing; BRANCH=master ; PABOT_ARGS='test'              ; SUITESFROM=fullrange.xml
#TESTPROJ=frontier-testing; BRANCH=develop; PABOT_ARGS='test'              ; SUITESFROM=fullrange.xml
TESTPROJ=flygres-ui      ; BRANCH=master ; PABOT_ARGS='-i smoke -esync* .'; SUITESFROM=''
#TESTPROJ=flygres-ui      ; BRANCH=master ; PABOT_ARGS='-esync* .'         ; SUITESFROM=''
#TESTPROJ=flygres-ui      ; BRANCH=dev    ; PABOT_ARGS='-esync* .'         ; SUITESFROM=''

[ -z "${TESTPROJ}" ] && exit 4

ADDR=fly155.local
        HSTNAME=`ssh root@${ADDR} hostname` || exit 6

emails='mailgroup_autotests@swemel.ru'
#emails="$emails zorin@swemel.ru"
#emails="$emails zhelanov@swemel.ru"
#emails="$emails o.boriskina@swemel.ru"
#emails="$emails s.boiko@swemel.ru"
#emails="$emails titov@swemel.ru"
#emails="$emails dulceva@swemel.ru"
#emails="$emails s.lukyanov@swemel.ru"
#emails="$emails n.saveleva@swemel.ru"
#emails="$emails pekonkin@swemel.ru"
#emails="$emails o.duda@swemel.ru"
#emails="$emails potokin@swemel.ru"
#emails="$emails r.slepov@swemel.ru"


WORKDIR=/var/tmp/${ADDR}-${TESTPROJ}-${BRANCH}      # было что-то вроде fly155smoke

PABOT_SERVER=10.1.47.241
        ip a | grep $PABOT_SERVER || exit 8  # проверим, что выполнение идет на правильном сервере
WWW_ROOT=/opt/nginx/html/report

FROMUSER=bamboo@swemel.ru
SMTP=10.1.10.173
MAIL_ARGS='-o message-content-type=text -o message-charset=UTF-8'

[ -z "$WORKDIR" ] && exit 9
[ -d "$WORKDIR" ] && rm -rf $WORKDIR
mkdir -p  $WORKDIR && cd  $WORKDIR || exit 10
trap "rm -rf $WORKDIR" 0

git clone -b $BRANCH git@git.swemel.grp:test/${TESTPROJ}.git && cd ${TESTPROJ} || exit 12

DATADIR="$(date +%Y%m%d-%H%M)-${ADDR}-${BRANCH}-$(git describe --always)"

if [ "$TESTPROJ" =  frontier-testing ]; then
	sed -i "s%^\${HOST}.*$%\${HOST}  ${ADDR}%" environment.robot || exit 14;
fi

[ "$SUITESFROM" = 'fullrange.xml' ] && {
	gunzip fullrange.xml.gz || exit 16
}
[ ! -z "$SUITESFROM" ] && SUITESFROM_ARGS="--suitesfrom $SUITESFROM"
pabot ${SUITESFROM_ARGS} --verbose --processes 6 --pythonpath ./ \
    --listener listener/TestLinkListener.py:10.1.100.102:c0d81f22e4705b579141387037ed8eea:653680 \
    -d report -L DEBUG ${PABOT_ARGS}
[ -d report ] || exit 17

VAR="$(python utils/statistic.py -i report/output.xml)"

if [ "$TESTPROJ" =  frontier-testing ]; then PACKET_NAME=frontier-user; fi
if [ "$TESTPROJ" =  flygres-ui ];       then  PACKET_NAME='spo-web-arctic-portal|flygres-app-min'; fi

VERSIONS=`ssh root@${ADDR} 'rpm -qa' | egrep "$PACKET_NAME"`


SUBJECT="Автотест ${ADDR}:${TESTPROJ}/${BRANCH} ${SUITESFROM}. $VAR"
MSG="
Тесты:    ${TESTPROJ}/${BRANCH} ${SUITESFROM}
Дата:     $(date)
Стенд:    $HSTNAME (${ADDR})

Версия пакетов на сервере:
$VERSIONS

Результат выполнения тестов: $VAR

http://$PABOT_SERVER/report/$DATADIR
"

mkdir -p ${WWW_ROOT}/$DATADIR &&
    cp -arpf report/* ${WWW_ROOT}/$DATADIR ||
    exit 18
chcon -R -u system_u -r object_r -t httpd_sys_content_t ${WWW_ROOT}/$DATADIR/*

sendemail -f "$FROMUSER" -t ${emails} -u "${SUBJECT}" -m "${MSG}" -s "$SMTP" $MAIL_ARGS || exit 20
sleep 5m