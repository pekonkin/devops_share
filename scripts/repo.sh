#!/usr/bin/env bash
set -vx
REPOLIST="${bamboo_RPM_REPOLIST}"

FIRSTREPO=""
for REPO in ${REPOLIST}
do
    # Копируем файл в первую репу, в остальные берем из
    # первой, не плодим разных версий!
    if [ "x${FIRSTREPO}" = "x" ]
    then
        FIRSTREPO="${REPO}"
    fi

    REPOTOUCH=f
    for i in flg/flygres-*/target/rpm/flygres-*/RPMS/noarch/*.noarch.rpm
    do
        FILE=`basename $i`
        # Копируем файл в первую репу, в остальные берем из
        # первой, не плодим разных версий!

        if [ "x${FIRSTREPO}" = "x${REPO}" ]
        then
            if [ ! -e ${REPO}/${FILE} ]
            then
                cp ${i} ${REPO}/${FILE}
                REPOTOUCH=t
            fi
        else
            if [ ! -e ${REPO}/${FILE} ]
            then
                cp -a ${FIRSTREPO}/${FILE} ${REPO}/${FILE}
                REPOTOUCH=t
            fi
        fi
    done
    if [ "x${REPOTOUCH}" = "xt" ]
    then
        flock -x -w300 "${REPO}".lck -c "createrepo_c --update ${REPO}"
    fi
done