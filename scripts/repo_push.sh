#!/bin/sh

REPOLIST="${1}"
RPMLIST="${2}"

FIRSTREPO=""

if [ "x${REPOLIST}" = "x" ]; then
    echo "Repolist is empty.  Check your build script." >&2; exit 1;
fi

if [ "x${RPMLIST}" = "x" ]; then
    echo "Rpmlist is empty.  Check your build." >&2; exit 1;
fi

for REPO in ${REPOLIST}; do
    # Копируем файл в первую репу, в остальные берем из
    # первой, не плодим разных версий!
    if [ "x${FIRSTREPO}" = "x" ]; then
        FIRSTREPO="${REPO}"
    fi

    REPOTOUCH=f
    for i in ${RPMLIST}; do
        FILE=`basename $i`
        # Копируем файл в первую репу, в остальные берем из
        # первой, не плодим разных версий!

        if [ "x${FIRSTREPO}" = "x${REPO}" ]; then
            if [ ! -e "${REPO}"/"${FILE}" ]; then
                cp ${i} ${REPO}/${FILE}
                REPOTOUCH=t
            fi
        else
            if [ ! -e "${REPO}"/"${FILE}" ]; then
                cp -a ${FIRSTREPO}/${FILE} ${REPO}/${FILE}
                REPOTOUCH=t
            fi
        fi
    done
    if [ "x${REPOTOUCH}" = "xt" ]; then
        flock -x -w300 "${REPO}".lck -c "createrepo --workers $(($(nproc) + 1)) ${REPO}"
    fi
done