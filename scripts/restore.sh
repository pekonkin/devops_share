#!/bin/sh
# FS - File Server
#TIMESTAMP=`date +%Y.%m.%d_%H.%M.%S`
FSIP="bkp-db.otd4.niitp.in"
FSF="backup"
FSU="repository_maker"
FSP="12345678"
FQDN="ensk-db-test.otd4.niitp.in"
BINPATH="/usr/bin"
DUMPNAME="${2}"
DB=$1


#Монтирование директории с резервными копиями на удалённом сервере
mount -o username=$FSU,password=$FSP //${FSIP}/${FSF} /media

if [ $? != 0 ]; then
  logger -p local0.err -t pg_restore "[uniapp app_name=pg_restore code=fail filename=${DUMPNAME}.pg_dump] Не удалось восстановить из резервной копии базу ${DB}. Ошибка соединения с сервером хранения резервных копий."
  echo "Не удалось восстановить из резервной копии базу ${DB}. Ошибка соединения с сервером хранения резервных копий."
  exit 1
fi

#Разрыв соединений
$BINPATH/psql -t -U postgres -h 127.0.0.1 -p 5432 -c "SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = '${DB}';"
#Удаление имеющейся базы
$BINPATH/dropdb --username=postgres -h 127.0.0.1 -p 5432 --no-password $DB 2>&1
#Создание чистой базы
$BINPATH/createdb --username=postgres -h 127.0.0.1 -p 5432 $DB 2>&1

#Восстановление
cd /media/
$BINPATH/pg_restore -h 127.0.0.1 -p 5432 -d ${DB} -U postgres ${DUMPNAME}.pg_dump >/var/log/pgsql/pg_restore 2>&1
RESULT=$?

if [ $RESULT -eq 0 ]; then
  logger -p local0.info -t pg_restore "[uniapp app_name=pg_restore code=success filename=${DUMPNAME}.pg_dump] База ${DB} восстановлена из резервной копии"
  echo "База ${DB} восстановлена из резервной копии"
else
  logger -p local0.err -t pg_restore "[uniapp app_name=pg_restore code=fail filename=${DUMPNAME}.pg_dump] Не удалось восстановить из резервной копии базу ${DB}"
  echo "Не удалось восстановить из резервной копии базу ${DB}"
fi


#Отключение от сервера с резервными копиями
cd ../
umount /media

exit $RESULT