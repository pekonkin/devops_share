#!/bin/bash

cd $(dirname $0)

# Default target database parameters
TARGET_HOST='localhost'
TARGET_PORT=5432
TARGET_DATABASE='flygres_production'
TARGET_USER='flygres'
TARGET_PASSWORD='flygres_password'

# Default target parameter for user with superuser permissions.
TARGET_SUPERUSER=''
TARGET_SUPERPASSWORD=''

# Rest of default values
JDBC_CLASSPATH='/opt/wildfly10/modules/system/add-ons/flygres/org/postgresql/main/postgresql-9.4.1212.jar'

function usage()
{
        echo -e \
"Usage: ./run.sh [-c <cmd>] [--debug]
        -c, --cmd=command               Command to execute, default 'migrate'.
        -d, --debug                     Enable debug mode.
            --schema=schema             Schema name, by default 'liquibase'.
            --changeSetIDs=id[,id]      List of ids for -c clearCheckSums.
"
}

# If config file exists - use it instead of default parameters
if [ -f ./main.conf ]
then
        source ./main.conf
fi


# parse command line
while [[ $# -gt 0 ]]; do
        key="${1}"
        case "${key}" in
        -c)
                shift;
                if [[ $# -eq 0 ]]; then
                        echo "Missing value for '-c' option!";
                        exit 1;
                fi
                key="-c${1}"
                ;&
        -c*)
                key="${key#-c}"
                CMD="${key}"
                ;;
        --cmd=*)
                key="${key#--cmd=}"
                CMD="${key}"
                ;;
        --schema=*)
                key="${key#--schema=}"
                SCHEMA="--defaultSchemaName=${key}"
                ;;
        --changeSetIDs=*)
                key="${key#--changeSetIDs=}"
                IFS=',' read -a CHANGE_SET_IDS <<< "${key}"
                ;;
        -d|--debug)
                DEBUG="--logLevel=debug"
                ;;
        *)
                usage
                exit 1
                ;;
        esac
        shift
done

CMD=${CMD:-migrate}
SCHEMA=${SCHEMA:---defaultSchemaName=liquibase}

if [ "x${CMD}" = "xclearCheckSums" -a ${#CHANGE_SET_IDS[@]} -gt 0 ]; then
        for _ID in "${CHANGE_SET_IDS[@]}"; do
                cat <<EOF | PGOPTIONS='--client-min-messages=warning' PGPASSWORD=${TARGET_PASSWORD} psql -q -t -U ${TARGET_USER} -h "${TARGET_HOST}" -p "${TARGET_PORT}" -d "${TARGET_DATABASE}" --set ON_ERROR_ROLLBACK=on --set ON_ERROR_STOP=on || exit 1
-- Clear checksum for the changeset ${_ID}
UPDATE liquibase.databasechangelog SET md5sum = null WHERE id = '${_ID}';
EOF
                echo "Checksum for the changeset ${_ID} was cleared!"
        done
        exit 0
else
        # If super user password is given, check and update extensions.
        if [ "x${TARGET_SUPERUSER}" != "x" -a "x${TARGET_SUPERPASSWORD}" != "x" ]
        then
cat <<EOF | PGOPTIONS='--client-min-messages=warning' PGPASSWORD=${TARGET_SUPERPASSWORD} psql -q -t -U ${TARGET_SUPERUSER} -h "${TARGET_HOST}" -p "${TARGET_PORT}" --set ON_ERROR_ROLLBACK=on || exit 1
-- Drop all connections except our:
SELECT datname, pid, application_name, client_addr, pg_terminate_backend(pid)
        FROM pg_stat_activity
        WHERE datname = '${TARGET_DATABASE}'
        AND pid != pg_backend_pid();

\c "${TARGET_DATABASE}"
-- Temporary increase rights to install extensions.
ALTER USER "${TARGET_USER}" SUPERUSER;
set ROLE '${TARGET_USER}';
CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;
CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;

-- Set rights back after extensions install.
ALTER USER "${TARGET_USER}" NOSUPERUSER;
EOF
        else
                # Else, if super user password is not set, just verify
                cat <<EOF | PGPASSWORD=${TARGET_PASSWORD} psql -q -t -U ${TARGET_USER} -h "${TARGET_HOST}" -p "${TARGET_PORT}" --set ON_ERROR_STOP=on || exit 1
DO \$\$
DECLARE
        exts varchar array;
BEGIN
        select array (select v.ext from (values ('postgis'), ('uuid-ossp'), ('adminpack')) as v(ext) where not exists (select 1 from pg_extension pae where pae.extname = v.ext)) into exts;
        IF array_length(exts, 1) > 0
        THEN
                raise EXCEPTION 'One or more extensions need to be installed, but not found: %', exts;
        END IF;
END;
\$\$ LANGUAGE plpgsql;
EOF
        fi

        liquibase --driver=org.postgresql.Driver \
                --classpath="${JDBC_CLASSPATH}" \
                --changeLogFile=/opt/spo/flygres/db_update/db_changesets/master-changelog.xml \
                --url="jdbc:postgresql://${TARGET_HOST}:${TARGET_PORT}/${TARGET_DATABASE}" \
                --username=${TARGET_USER} \
                --password="${TARGET_PASSWORD}" \
                ${SCHEMA} \
                ${DEBUG} \
                "${CMD}"
fi