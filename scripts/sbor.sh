#!/usr/bin/env bash
set -x
version_fr=$(git describe --tags --always --long | sed -e 's/-[a-z0-9][a-z0-9]*$//')
version_front=$(git describe --tags --always --long | sed -e 's/^\(.*\)-\([a-z0-9][a-z0-9]*\)$/\1.\2/')
cd ./class
version_cl=$(git describe --tags --always --long | sed -e 's/-[a-z0-9][a-z0-9]*$//')
cd ..
cd ./acces
version_acces=$(git describe --tags --always --long | cut -d - -f 2- | tr - .)
cd ..
cd ./ex
version_ex=$(git describe --tags --always --long | sed -e 's/^\(.*\)-\([a-z0-9][a-z0-9]*\)$/\1.\2/')
cd ..
dt=bild-${version_front}-`date '+%d.%m.%Y_%H.%M_MSK'`
mkdir -p spo/${dt}
Err(){
    echo "ERROR: $@" >&2
    exit 1
}
ls -l /var/www/repo/bamboorepoRPD/*frontier-user-app-${version_front}* || Err "нет файла frontier-user-app-${version_fr}"
ls -l /var/www/repo/bamboorepoRPD/*frontier-global-app-${version_fr}* || Err "нет файла frontier-global-app-${version_fr}"
ls -l /var/www/repo/bamboorepoRPD/*szi-abhazia.pu-${version_fr}* || Err "нет файла szi-abhazia.pu-${version_fr}"
ls -l /var/www/repo/bamboorepoRPD/*swm_spo_db_update_frontier_app-${version_fr}* || Err "нет файла swm_spo_db_update_frontier_app-${version_fr}"
ls -l /var/www/repo/bamboorepoRPD/*szi-astrahan.pu-${version_fr}* || Err "нет файла szi-astrahan.pu-${version_fr}"
ls -l /var/www/repo/bamboorepoRPD/*szi-bryansk.pu-${version_fr}* || Err "нет файла szi-bryansk.pu-${version_fr}"
ls -l /var/www/repo/bamboorepoRPD/*szi-classifier-${version_fr}* || Err "нет файла szi-classifier-${version_fr}"
ls -l /var/www/repo/bamboorepoRPD/*szi-data-exchange-${version_fr}* || Err "нет файла szi-data-exchange-${version_fr}"
ls -l /var/www/repo/bamboorepoRPD/*szi-kaliningrad.pu-${version_fr}* || Err "нет файла szi-kaliningrad.pu-${version_fr}"
ls -l /var/www/repo/bamboorepoRPD/*szi-piter.pu-${version_fr}* || Err "нет файла szi-piter.pu-${version_fr}"

scp /var/www/repo/bamboorepoRPD/*${version_front}* ./spo/${dt}
scp /var/www/repo/bamboorepoRPD/*${version_cl}* ./spo/${dt}
scp /var/www/repo/bamboorepoRPD/*${version_ex}* ./spo/${dt}
scp /var/www/repo/bamboorepoRPD/*${version_fr}.${version_acces}* ./spo/${dt}
cd ./spo
tar cvpf ${dt}.tgz ./${dt}/ ../classifier.tar
cd ..
owncloudcmd  ./spo/ https://shlipov:anlvfVaq1@cloud.swemel.ru/remote.php/webdav/bild || Err "ownclou.swemel.ru не доступен"
echo "Сбор билда выложен на ownloud.swemel.ru"