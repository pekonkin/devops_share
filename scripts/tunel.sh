#!/bin/bash
#
# программа предполагается, запускается по крону раз в минуту, стартует форвардинг, после чего форвардинг
# ждет, когда отвалятся порты и сразу же рестартит их по-новой.
# Следующий запуск по крону этой программы снова запустит ожидающий освобождения портов форвардинг.
# Таким образом достигается баланс между достаточно редкими запусками и моментальным стартом

cd $(dirname $0)

LIST=${1:-remotes.lst}

cat $LIST | sed 's%#.*$%%; /^[[:space:]]*$/d' |
    while read remote_user remote_key remote_host remote_port forward_port socks_port forward_ip remote_forward_port description
do
    [ -z "$remote_forward_port" ] && continue
    ./forward_connection.sh ${remote_user} ${remote_key} ${remote_host} ${remote_port} ${forward_port} ${socks_port} ${forward_ip} ${remote_forward_port} "${description}" &
done

exit 0