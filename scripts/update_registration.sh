#!/bin/bash

set -euo pipefail

#
# Скрипт выполняет обновления базы данных и
# регистрирует выполненные накаты в отдельной таблице
#


CONF_FILE=conf.ini                           # Файл конфигурации

#DB_HOST=10.10.2.75                           # Хост базы
DB_PORT=5432                                 # Порт базы
DB_NAME=frontier                             # Наименование базы
DB_OWNER=frontier                            # Владелец (роль, пользователь) базы
#SCRIPT_DIR=/opt/fr_db_updates                # Директория со скриптами
HISTORY_TABLE=public.update_history          # Таблица истории обновлений
LOG_FILE=./update_history.log                # Лог файл
SKIP_FAIL=false                              # Игнорировать ошибки (true = игнорировать, false = выход при первой ошибке)
ENABLE_SZI_APPLY=true                        # Включение скрипта наката SZI
SZI_APPLY_FILE=/opt/szi/script_all_PU.sql    # Файл скрипта наката SZI
                                            ## resource_frontier
ENABLE_FRONTIER_RESOURCE_SCRIPT=false
FRONTIER_RESOURCE_SCRIPT=/opt/szi/resource_frontier.sql

[ -f "${CONF_FILE}" ] || exit 6

echo "Загружаем конфигурационный файл"
. ${CONF_FILE}

[ -z "${DB_HOST}" ]                  && exit 101
[ -z "${DB_PORT}" ]                  && exit 102
[ -z "${DB_NAME}" ]                  && exit 103
[ -z "${DB_OWNER}" ]                 && exit 104
[ -z "${SCRIPT_DIR}" ]               && exit 105
[ -z "${HISTORY_TABLE}" ]            && exit 106
[ -z "${LOG_FILE}" ]                 && exit 107
[ -z "${SKIP_FAIL}" ]                && exit 108
[ -z "${ENABLE_SZI_APPLY}" ]         && exit 109
[ -z "${SZI_APPLY_FILE}" ]           && exit 110
[ -z "${ENABLE_FRONTIER_RESOURCE_SCRIPT}" ] && exit 111
[ -z "${FRONTIER_RESOURCE_SCRIPT}" ] && exit 112


# parse command line
while [[ $# -gt 0 ]]; do
    key="${1}"
    case "${key}" in
        --enable-frontier-resource-script)
            ENABLE_FRONTIER_RESOURCE_SCRIPT=true
            ;;
        --disable-frontier-resource-script)
            ENABLE_FRONTIER_RESOURCE_SCRIPT=false
            ;;
        --enable-szi-apply)
            ENABLE_SZI_APPLY=true
            ;;
        --disable-szi-apply)
            ENABLE_SZI_APPLY=false
            ;;
        --frontier-resource-script)
            shift;
            if [[ $# -eq 0 ]]; then
                break;
            fi
            key="--frontier-resource-script=${1}"
            ;&
        --frontier-resource-script=*)
            key="${key#--frontier-resource-script=}"
            FRONTIER_RESOURCE_SCRIPT="${key}"
            ;;
        # Ignore all the rest
    esac
    shift
done

{
    echo ""
    echo "=================================================================="
    echo " Current configuration "
    echo "=================================================================="
    echo "DB_HOST=$DB_HOST"
    echo "DB_PORT=$DB_PORT"
    echo "DB_NAME=$DB_NAME"
    echo "DB_OWNER=$DB_OWNER"
    echo "SCRIPT_DIR=$SCRIPT_DIR"
    echo "HISTORY_TABLE=$HISTORY_TABLE"
    echo "LOG_FILE=$LOG_FILE"
    echo "SKIP_FAIL=$SKIP_FAIL"
    echo "ENABLE_FRONTIER_RESOURCE_SCRIPT=${ENABLE_FRONTIER_RESOURCE_SCRIPT}"
    echo "FRONTIER_RESOURCE_SCRIPT=${FRONTIER_RESOURCE_SCRIPT}"
    echo "=================================================================="
} | tee $LOG_FILE


tlist=$(psql -h $DB_HOST -p $DB_PORT -U $DB_OWNER -d $DB_NAME -t -c "SELECT gid FROM pg_prepared_xacts where database = '$DB_NAME'")

for t in $tlist ; do
    psql -h $DB_HOST -p $DB_PORT -U $DB_OWNER -d $DB_NAME -c "ROLLBACK PREPARED '$t'"
done


if [ "x${ENABLE_SZI_APPLY}" != "xtrue" ]; then
    echo "Очистка от СЗИ: отключено, пропускаю."
else
    echo "Очистка от СЗИ"
    revoke_and_drop_file=./drop_and_revoke_all_PU.sql
    cat $SZI_APPLY_FILE |
        sed 's%;%;\n%' |
        grep -E " *REVOKE.*;$| *DROP.*;$" |
        grep -v "DROP SCHEMA" > $revoke_and_drop_file
    cat $SZI_APPLY_FILE |
        sed 's%;%;\n%' |
        grep -E " *DROP SCHEMA.*;$" >> $revoke_and_drop_file
    rad_log=./revoke_and_drop.log
    psql -h $DB_HOST -p $DB_PORT -U $DB_OWNER -f $revoke_and_drop_file $DB_NAME > $rad_log 2>&1
    ! grep -q ERROR $rad_log && {
        echo "СЗИ очищено без ошибок."
        rm -f $rad_log
    } || {
        echo -ne "ERROR: При очистке от СЗИ есть ошибки; "
        [ "$(cat $rad_log | wc -l)" -le 200 ] && {
            echo
            cat $rad_log
        } || {
            echo "Для просмотра: "
            echo "less $rad_log"
        }
        echo "=========================================================================="
        exit 5
    }
fi

errors=0

sql_files=$(ls $SCRIPT_DIR |sed 's/ /\//g' |grep "\.sql")
for sql_file in $sql_files
do
    file_name=$(echo $sql_file |sed 's/\// /g')
    file_executed=$(psql -h $DB_HOST -p $DB_PORT -U $DB_OWNER -d $DB_NAME -q -t \
            -c "select executed from $HISTORY_TABLE where filename like '$file_name'" |head -1 |sed 's/^ //g' |sed 's/ /\~/g')
    #echo $file_executed
    if [ -z "$file_executed" ]; then
        tmp_name=$(echo $file_name |sed 's/.sql$//g')
        tmp_file=${SCRIPT_DIR}/${tmp_name}.tmp
        echo "" > $tmp_file
        cat ${SCRIPT_DIR}/${file_name} >> $tmp_file

        record_exist=$(psql -h $DB_HOST -p $DB_PORT -U $DB_OWNER -d $DB_NAME -q -t \
            -c "select count(*) from $HISTORY_TABLE where filename like '$file_name'" |head -1 |sed 's/^ *//g')
        if [ $record_exist == 0 ] ; then
            printf "\ninsert into $HISTORY_TABLE (filename, executed, sysuser, sqluser) values ('$file_name', localtimestamp, '$USER', current_user);\n" >> $tmp_file
        else
            printf "\nupdate $HISTORY_TABLE set executed=localtimestamp, sysuser='$USER', sqluser=current_user  where filename like '$file_name';\n" >> $tmp_file
        fi
        psql -h $DB_HOST -p $DB_PORT -U $DB_OWNER -d $DB_NAME -1 -e -f $tmp_file >> ${file_name}.log 2>&1
        result=$(psql -h $DB_HOST -p $DB_PORT -U $DB_OWNER -d $DB_NAME -q -t \
            -c "select count(*) from $HISTORY_TABLE where filename like '$file_name' and executed is not null" |head -1 |sed 's/^ *//g')
        if [ $result -gt 0 ] ; then
            echo "Скрипт $file_name выполнен успешно!"
            rm -f $tmp_file
        else
            echo "ERROR: Скрипт $file_name не выполнен"
            errors=$(expr $errors + 1)
            echo "=========================================================================="
            [ "$(cat ${file_name}.log | wc -l)" -le 100 ] && {
                cat ${file_name}.log
            } || {
                echo "Для просмотра ошибки:"
                echo "less ${file_name}.log"
            }
            echo "=========================================================================="
            [ "$SKIP_FAIL" == "false" ] && {
                echo "Выходим"
                exit 1;
            } || {
              cat ${file_name}.log >> $LOG_FILE
            }
        fi
    fi
done
if [ $errors -gt 0 ] ; then
    echo "=========================================================================="
    echo " Ошибок ($errors). Смотрите журнал: cat $LOG_FILE"
    echo "=========================================================================="
    echo ""
fi

if [ "x${ENABLE_FRONTIER_RESOURCE_SCRIPT}" = "xtrue" ]; then
    echo "Применяю resource_frontier"
    echo "==========================" >> "${LOG_FILE}"
    echo "Применяю resource_frontier" >> "${LOG_FILE}"
    echo "==========================" >> "${LOG_FILE}"
    psql -h ${DB_HOST} -p ${DB_PORT} -U ${DB_OWNER} -f "${FRONTIER_RESOURCE_SCRIPT}" ${DB_NAME} >> "${LOG_FILE}" 2>&1
    echo "Готово. Журнал наката добавлен в файл ${LOG_FILE}"
fi

if [ "x${ENABLE_SZI_APPLY}" = "xtrue" ]; then
    echo "Накат СЗИ"
    szi_apply_log=./szi_apply.log
    psql -h $DB_HOST -p $DB_PORT -U $DB_OWNER -f $SZI_APPLY_FILE $DB_NAME > $szi_apply_log 2>&1
    echo "Готово. Смотрите журнал наката: less $szi_apply_log "
else
    echo "Накат СЗИ: отключено, пропускаю."
fi