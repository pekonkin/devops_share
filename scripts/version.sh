#!/usr/bin/env bash
version_fr=$(git describe --tags --always --long | cut -f 1 -d '-')
version_fr2=$(git describe --tags --always --long | cut -f 2 -d '-')
cd ./acces
git_hash=$(git describe --tags --always --long | cut -f 2,3 -d '-' | sed 's/-/./g')
cd ..
mkdir -p rpmbuild/{SOURCES,SPECS}
cp -f ./run-scripts/*.tar rpmbuild/SOURCES
cp -f ./accesde/run-scripts/*.tar rpmbuild/SOURCES
cp -f ./acces/run-scripts/*.tar rpmbuild/SOURCES
touch rpmbuild/SOURCES/null
cp -f ./acces/szi-arktic.spec rpmbuild/SPECS/szi.spec
cd rpmbuild/SPECS; sed -i "s/XXX/$version_fr/g" szi.spec; sed -i "s/YYY/$version_fr2.$git_hash/g" szi.spec
cd ..
rpmbuild --quiet --define="_topdir $(pwd)" -ba SPECS/szi.spec