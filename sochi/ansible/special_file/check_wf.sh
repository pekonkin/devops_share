#!/bin/bash
### Пауза между проверками
CHECK_DEPLOY_SEC=60
### Количество проверок
CHECK_DEPLOY_COUNT=10

if [[ -z $1 ]]; then
	echo "Не переданы аргументы версии"
	exit 1
fi

if [[ $1 -eq 10 ]]; then
	dir=/opt/wildfly/standalone/deployments
elif [[ $1 -eq 8 ]]; then
	dir=/opt/wildfly8/standalone/deployments
fi

check=0
while :; do
	count_ear=`ls $dir | egrep -v '.dodeploy$|.skipdeploy$|.isdeploying$|.deployed$|.failed$|.isundeploying$|.undeployed$|.pending$|.txt$' | wc -l`
	count_deploy=`ls $dir | grep '.deployed$' | wc -l`
	if [[ $count_deploy -eq $count_ear ]]; then
		echo "Все запущено"
		exit 0
	fi
	if [[ $check -eq $CHECK_DEPLOY_COUNT ]]; then
		echo "Достигнут таймаут"
		exit 1
	fi
	let check=$check+1
	sleep $CHECK_DEPLOY_SEC
done