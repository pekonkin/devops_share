Summary: qt port of the java transport database synchronisation
Name: qtransport
Version: %VER%
Release: %REL%
License: Proprietary
Group: Applications/Engineering
Source: %{name}-%{version}-%{release}.tgz
Packager: Pavel Vasilev <vasilev@swemel.ru>

BuildRequires: tar
BuildRequires: gcc-c++
BuildRequires: gzip
BuildRequires: qt5-qtbase-devel >= 5.4
Requires: qt5-qtbase-postgresql >= 5.4
Requires: qt5-qtbase >= 5.4

%description
%{summary}

%prep
%setup -n qtransport

%build
qmake-qt5 qtransport.pro
make all

%install
INSTALL_ROOT=%{buildroot}/usr make install
mkdir -p %{buildroot}/etc/init.d
mkdir -p %{buildroot}/etc/qtransport
mkdir -p %{buildroot}/etc/logrotate.d
mkdir -p %{buildroot}/var/log/qtransport
cp qtransport.init.d.sh %{buildroot}/etc/init.d/qtransport
cp qtransport-dblayer.init.d.sh %{buildroot}/etc/init.d/qtransport-dblayer
cp qtransport-dblayer-daemon %{buildroot}/usr/bin/qtransport-dblayer-daemon
cp config_example.ini %{buildroot}/etc/qtransport/qtransport.ini
cp qtransport.logrotate %{buildroot}/etc/logrotate.d/qtransport

%post

%postun

%files
%{_bindir}/*
/etc/init.d/qtransport
/etc/init.d/qtransport-dblayer
/etc/logrotate.d/qtransport
%config(noreplace) /etc/qtransport/qtransport.ini
/var/log/qtransport/
#%{_libdir}/*

%changelog
