#!/usr/bin/env bash
#set -vx
fly=$1
FROMUSER=bamboo@swemel.ru
SMTP=10.1.10.173
MAIL_ARGS='-o message-content-type=text -o message-charset=UTF-8'
SUBJECT="Deploy_${fly}"
ADDR="${fly}"
PACKET_NAME='spo-web-arctic-portal|flygres-app-min'
VERSIONS=`ssh root@${ADDR} 'rpm -qa' | egrep "${PACKET_NAME}"`
MSG="
Сегодняшний деплой зафейлился, сервер ${fly} был восстановлен к предыдущему своему состоянию.
Логи можно найти по адресу: http://10.1.100.105/reports/${fly}.tgz
Версии SPO:
${VERSIONS}
'
 -----
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||'"


emails=''
emails="$emails potokin@swemel.ru"
emails="$emails zorin@swemel.ru"
emails="$emails zhelanov@swemel.ru"
emails="$emails o.boriskina@swemel.ru"
emails="$emails s.boiko@swemel.ru"
emails="$emails dulceva@swemel.ru"
emails="$emails s.lukyanov@swemel.ru"
emails="$emails n.saveleva@swemel.ru"
emails="$emails pekonkin@swemel.ru"
emails="$emails o.duda@swemel.ru"
emails="$emails potokin@swemel.ru"
emails="$emails r.slepov@swemel.ru"


sendemail -f "${FROMUSER}" -t ${emails} -u "${SUBJECT}" -m "${MSG}" -s "${SMTP}" $MAIL_ARGS
exit 0
