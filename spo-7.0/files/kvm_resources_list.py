#!/usr/bin/env python

import libvirt
conn=libvirt.open("qemu:///system")

for id in conn.listDomainsID():
   dom = conn.lookupByID(id)
   infos = dom.info()
   print '%d \tName =  %s \tState = %d \tMax Memory = %4.2f Gb \tNumber of virt CPUs = %d \tCPU Time (in ns) = %d' % (id, dom.name(), infos[0], infos[1]/1024.0/1024, infos[3], infos[2])

   #print 'Name =  %s' % dom.name()
   #print 'State = %d' % infos[0]
   #print 'Max Memory = %d' % infos[1]
   #print 'infos[2' % infos[3]
   #print 'CPU Time (in ns) = %d' % infos[2]
   #print ' '
