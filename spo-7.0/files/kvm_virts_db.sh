#!/bin/bash

virsh list | tail -n+3 | head -n-1 | while read vid vnm vst; do echo -ne "$vid $vnm \t\t\t "; a=$(grep source\ file /etc/libvirt/qemu/$vnm.xml | sed "s%^.*='%%; s%'.*$%%"); du -msx $a | awk 'BEGIN{s=0} {s=s+$1} END{printf "%3.1f Gb\n", s/1024}' ; done

