#!/bin/bash

## SET Perm

PASSW="12345678"
DC="dc=var,dc=local"
OU="ou=People,ou=L2C1,${DC}"

KEY='ZeNrlXNl2mzAQfc9X9Pi5X9C/GUtjrFpbtZC6Pfn3CnASG0bGgGOUNCdPCIbRaJY7C/779C39bYAroYUPDoKocfPj29/2erfmgmAS/cXV01M8XQwu4vfLBeQi0CsOlWlfQKzVAp9PK28LL+/3bBj4ArjQUW3R3YePsXc5/BWFF4Ha9hlV8tngTNxKVHAguSVf/XRGZAMa5NGLy2c3HBRUSOyeurv/th1IjxerLwOJGJl00GiaTifjlsz34epJJLnlt1PP3ZDnkpQwB12hG5zsBmvUwQ+v+72wKi2di/1V2MwoFbVgw70ntZfSi0qvrvlVBMcVek8rwAOY6YlMJx2XfWE5vrqgjOOtXqzLhUfmMIATEEo4LSdSiIHBca3m0i+5iz4YJf4Q5oe/2b4x9Btd6Lm9JA9s/VFte0q6hjJION4raC1iwwD3bI9qigNZZIrbn8jC+ra4ohZcKPrOONazuE2KLzsjauOqBAAjC9HNUHbQQsH6eg4ueZlqfaiaDecdN4yZqAPF6VVur3M8xnWWcwqIOayafOBYDoOkoFFXQiO6ex06/kYWx/BnCQdzBSTfTmkhFmwA7UPyIBV8AaDu5B4ncMKFl4ZB0fqUl1ZeVkvkaBMOMVojFSu2HjXDYkU1Q+N0VB+AJkx0bEaMTl5De2scpcI1CAlbIUU4Fit/jhYcCU8+j3Mugcm4MHBEj4XDguDNFDzQFq+CYD6zPGocy51tGWE9eHsYyu1zaHVxgLXHYDAlGs1Fdig0b3IW06sUXF5/L4++P5bSSkXUTnhzIoKCTGOKmN4Ym9PM5e+jiBFsUzMVjEq/Rh9uLB3cMaBT6/cbLJ++AxPE9IcSMlPC+3SKvq0HHGo/gwgJL0YRjWqK3GQ6MV7TEDodFLCh+lUO7F4wblhUp5L81N3IhJhy9G+mEGArcRmZcLTYkDoj0rYZ5sgr2UavAMR5W6sctizAIdBX0z/bv8fE3h2N4UMH5XsrbeKYXhaItaY6WKOUBGbeeKixY5xYyD3UdZOMO3/fpSiMFmm9Xz3YgHCZpVWKWrWAIph4lePKxTXCSyRdbMwCXVK8DscRkZZZ0sXcFInHo/EtEflqVCZAwynt8cwJm0VfJfOuDG1C9+R7Kk+ZBsR8euhqwRr3zLDsA7qKT5ltffPdQWqKBC74YOwdalVkyCsF4P800aVE0hfLYAJZu6hZPo1j0nicy+WtXHQFwGU57q+IsdwqkGfG4dbMM6aHnIG3yATI5AjZwa+oC/mK5CQyrziPpvLQfhQNyh+MkHYiAR0SuLaNx9zyRBM0Af08UpkRBU2N7kh0oeCKdEC2SlnM4clsvnRILR6RJEoVzpFfyoHt2tItx22aBAqGo8cnl5WuF92Veh0DLZm9m8ZzR2nlCr1l9QFm4InMACVVdZHglK+c4MtEyWBrAlSo29sXEZIsdjPTC0+37FDQTVU3hellu9wfk+tVKW6bhbaQG1T4Muniwaq783ZzkgjNbHe2XPcQFtp2SMmuzjphNdRCLW0YNx5zofNIzvIaFnpQ4v0R+vL10FaWkw8bO25aV5cdlh0RW6+gfQ7HT1j71mH/6bg+IrjPVMXOFEBQzmibt9Px639n1bWtZ3xwct5bXXlaVsFDJoOtPxBuLG95d8IGWSOZS3/yvOZjj7PfSF+Xmz9G45zBBwrobJi10y3tYKd8ZQjWuuxmP8j+tsSuThnHf5C6G04M+7wVmbr5mXITL18bfhBLWyIjCuJnxEeyIjJud/7oAyrfyw26Hx0YDu3oU44/GNaJu97Xo8mu5OkbqxmzXcwZ77eZb3VvmE8UteCRyt/Hv4LECiTqQFU1x+cBTRC7/K5fy6Vtjbu5d2mZ4lmEPXfwDHIWNTpqg/eg+dnA4SS1enp5+geNCWHT'

USER=`cat users`

var_l="10.1.39.17"

for _uid in ${USER}
  do
    echo "dn: uid=${_uid},ou=People,${DC}
changetype: modify
replace: permission
permission: $(echo ${KEY})
" > ${_uid}.ldif
    ldapmodify -w ${PASSW} -x -h ${var_l} -D "cn=Manager,${DC}" -f ${_uid}.ldif

    echo "dn: uid=${_uid},${OU}
changetype: modify
replace: permission
permission: $(echo ${KEY})
" > ${_uid}.ldif

    ldapmodify -w ${PASSW} -x -h ${var_l} -D "cn=Manager,${DC}" -f ${_uid}.ldif

    rm -f ${_uid}.ldif
  done
echo "Edit user finish"
