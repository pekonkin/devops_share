#!/bin/bash


Usage() {
    echo "Add User to Flygres and Ldap if user is not create and exits."
    echo
    echo "$0 [-u user] [-f user_filename] [-r] [-g GID] [-p PASSWORD] [-h (host|ip|file_with_hostname)] [-l LDAP_ADDR]"
    echo
    echo "Add user:                         -u demin "
    echo "Add users from file:              -f filename.txt"
    echo "Change GID      in LDAP:          -g 0"
    echo "       PASSWORD in LDAP:          -p demin"
    echo
    echo
    echo '      by default LDAP User have password = "1"'
    echo '      by default LDAP IP 10.1.39.17 if you want change "-l 192.168.39.17"'
    echo
    echo '      If flygres need set HOSTNAME, key for "-h fly156.local" or IP addres "-h 192.168.0.1" or -h host.txt'
    echo
    echo '      Example: "flyadd.sh -u demin -h spo153.local -p MySuperPa$$W0Rd"'
    echo
    echo '      If you need reset password or change gid for user you need set key -r'
    echo '      Example: "flyadd.sh -r -p MySuperPa$$W0Rd" -u demin'
    echo
}

LDAP_IP="10.1.39.17"
USERPASSW="1"
var_g="0"

while getopts ":u:f:rg:p:h:l:" Option; do
    case $Option; in
        r ) ;;
        f ) USERS_FILE="$OPTARG";;
        u ) USERNAME="$OPTARG";;
        g ) var_g="$OPTARG";;
        p ) USERPASSW="$OPTARG";;
        h ) var_h="$OPTARG";;
        l ) LDAP_IP="$OPTARG";;
        * ) echo "Enter params";;
    esac
done

if [ -z "${USERNAME}" -a -z "${USERS_FILE}" -a -z "${var_h}" ]; then
    Usage
    exit
fi

## SET Perm

LDAPPASSW="12345678"
DC="dc=var,dc=local"
OU="ou=People,ou=L2C1,${DC}"
## NEED CHANGE
WEBUSER="prok"
WEBPASS="12345678"

KEY='ZeNrlXNl2mzAQfc9X9Pi5X9C/GUtjrFpbtZC6Pfn3CnASG0bGgGOUNCdPCIbRaJY7C/779C39bYAroYUPDoKocfPj29/2erfmgmAS/cXV01M8XQwu4vfLBeQi0CsOlWlfQKzVAp9PK28LL+/3bBj4ArjQUW3R3YePsXc5/BWFF4Ha9hlV8tngTNxKVHAguSVf/XRGZAMa5NGLy2c3HBRUSOyeurv/th1IjxerLwOJGJl00GiaTifjlsz34epJJLnlt1PP3ZDnkpQwB12hG5zsBmvUwQ+v+72wKi2di/1V2MwoFbVgw70ntZfSi0qvrvlVBMcVek8rwAOY6YlMJx2XfWE5vrqgjOOtXqzLhUfmMIATEEo4LSdSiIHBca3m0i+5iz4YJf4Q5oe/2b4x9Btd6Lm9JA9s/VFte0q6hjJION4raC1iwwD3bI9qigNZZIrbn8jC+ra4ohZcKPrOONazuE2KLzsjauOqBAAjC9HNUHbQQsH6eg4ueZlqfaiaDecdN4yZqAPF6VVur3M8xnWWcwqIOayafOBYDoOkoFFXQiO6ex06/kYWx/BnCQdzBSTfTmkhFmwA7UPyIBV8AaDu5B4ncMKFl4ZB0fqUl1ZeVkvkaBMOMVojFSu2HjXDYkU1Q+N0VB+AJkx0bEaMTl5De2scpcI1CAlbIUU4Fit/jhYcCU8+j3Mugcm4MHBEj4XDguDNFDzQFq+CYD6zPGocy51tGWE9eHsYyu1zaHVxgLXHYDAlGs1Fdig0b3IW06sUXF5/L4++P5bSSkXUTnhzIoKCTGOKmN4Ym9PM5e+jiBFsUzMVjEq/Rh9uLB3cMaBT6/cbLJ++AxPE9IcSMlPC+3SKvq0HHGo/gwgJL0YRjWqK3GQ6MV7TEDodFLCh+lUO7F4wblhUp5L81N3IhJhy9G+mEGArcRmZcLTYkDoj0rYZ5sgr2UavAMR5W6sctizAIdBX0z/bv8fE3h2N4UMH5XsrbeKYXhaItaY6WKOUBGbeeKixY5xYyD3UdZOMO3/fpSiMFmm9Xz3YgHCZpVWKWrWAIph4lePKxTXCSyRdbMwCXVK8DscRkZZZ0sXcFInHo/EtEflqVCZAwynt8cwJm0VfJfOuDG1C9+R7Kk+ZBsR8euhqwRr3zLDsA7qKT5ltffPdQWqKBC74YOwdalVkyCsF4P800aVE0hfLYAJZu6hZPo1j0nicy+WtXHQFwGU57q+IsdwqkGfG4dbMM6aHnIG3yATI5AjZwa+oC/mK5CQyrziPpvLQfhQNyh+MkHYiAR0SuLaNx9zyRBM0Af08UpkRBU2N7kh0oeCKdEC2SlnM4clsvnRILR6RJEoVzpFfyoHt2tItx22aBAqGo8cnl5WuF92Veh0DLZm9m8ZzR2nlCr1l9QFm4InMACVVdZHglK+c4MtEyWBrAlSo29sXEZIsdjPTC0+37FDQTVU3hellu9wfk+tVKW6bhbaQG1T4Muniwaq783ZzkgjNbHe2XPcQFtp2SMmuzjphNdRCLW0YNx5zofNIzvIaFnpQ4v0R+vL10FaWkw8bO25aV5cdlh0RW6+gfQ7HT1j71mH/6bg+IrjPVMXOFEBQzmibt9Px639n1bWtZ3xwct5bXXlaVsFDJoOtPxBuLG95d8IGWSOZS3/yvOZjj7PfSF+Xmz9G45zBBwrobJi10y3tYKd8ZQjWuuxmP8j+tsSuThnHf5C6G04M+7wVmbr5mXITL18bfhBLWyIjCuJnxEeyIjJud/7oAyrfyw26Hx0YDu3oU44/GNaJu97Xo8mu5OkbqxmzXcwZ77eZb3VvmE8UteCRyt/Hv4LECiTqQFU1x+cBTRC7/K5fy6Vtjbu5d2mZ4lmEPXfwDHIWNTpqg/eg+dnA4SS1enp5+geNCWHT'

if [ $( rpm -qa | grep -c openldap-clients ) -eq 0 ]; then
   echo "Need install openldap-clients"
   if [ $(id -u) -eq 0 ]; then
        yum -y install curl openldap-clients nmap
   fi
   exit
fi


## Check file
if [[ -f ${USERS_FILE} && -s ${USERS_FILE} ]];  then
    USER=`cat ${USERS_FILE}`
fi

if [ ! -z ${USERNAME} ]; then
    USER=`echo ${USERNAME}`
fi


## Reset password
##

if [ $( echo $@ | grep -c "\-r" ) -eq 1 ]; then
    nmap -Pn ${LDAP_IP} -p 389 > /dev/null 2>&1
    if [ $? -ge 1 ]; then
        echo "LDAP ${LDAP_IP} is DOWN"
        exit
    fi
    for _uid in ${USER}; do
        {
            echo "dn: uid=${_uid},${OU}"
            echo "changetype: modify"
            echo "replace: userPassword"
            echo "userPassword: ${USERPASSW} "                                                                                                                                                     -
            echo "replace: permission"
            echo "permission: $(echo ${KEY})"
            echo "-"
            echo "replace: gidNumber"
            echo "gidNumber: ${var_g}"
            echo ""
        } > ${_uid}.ldif
        ldapmodify -w ${LDAPPASSW} -x -h ${var_h} -D "cn=Manager,${DC}" -f ${_uid}.ldif
        rm -f ${_uid}.ldif
    done
    echo "Edit user finish"
    exit
fi

## Check connect
if [ -f ${var_h} ]; then
   var_h=`cat ${var_h}`
fi

for var_h1 in ${var_h}; do
    if [ -n ${var_h1} ]; then
        ##Try connect
        ping -c1 -q ${var_h1} > /dev/null 2>&1
        if [ ! $? -eq 0 ]; then
            ## Try undestand eror
            if [ $( echo ${var_h1} | egrep -c "^[a-zA-Z]" ) -eq 1 ]; then
                if [ $( egrep -ci ${var_h1} /etc/hosts ) -eq 0 ]; then
                    echo "Not fount record in /etc/hosts"
                fi
            elif [ $( echo ${var_h1} | egrep -c '^[0-9]' ) -eq 1 ]; then
              echo ${var_h1} |
                  egrep -o "(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)"
              [ $? -eq 0 ] || echo "Incorrect ip address ${var_h1}"
            fi
        else
            nmap -Pn ${var_h1} -p 80 > /dev/null 2>&1
            if [ $? -ge 1 ]; then
               echo "WebServer ${var_h1} port 80 close"
            fi
        fi
    fi
done

for _uid in ${USER}; do
  	## Check user in LDAP if need add to LDAP
   	if [ $(ldapsearch -w${LDAPPASSW} -x -h ${LDAP_IP} -D "cn=Manager,${DC}" -b ${OU}  uid=${_uid} | egrep -c "uid: ${_uid}") -eq 0 ]; then
      	SUM=`ldapsearch -w ${LDAPPASSW} -x -h ${LDAP_IP} -D "cn=Manager,${DC}" -b ${OU} | egrep 'uidNumber' | sort -n -k2 | tail -n1 | cut -d ' ' -f 2`

        {
            echo ""
            echo "dn: uid=${_uid},ou=People,${DC}"
            echo "objectClass: top"
            echo "objectClass: posixAccount"
            echo "objectClass: shadowAccount"
            echo "objectClass: organizationalPerson"
            echo "objectClass: inetOrgPerson"
            echo "objectClass: person"
            echo "objectClass: Security"
            echo "objectClass: orgstruct"
            echo "objectClass: mail"
            echo "objectClass: Kiosk"
            echo "cn: ${_uid}"
            echo "sn: $(uuidgen)"
            echo "uid: ${_uid}"
            echo "uidNumber: $(( $SUM+1 ))"
            echo "gidNumber: 0"
            echo "homeDirectory: /home/${_uid}"
            echo "loginShell: /bin/bash"
            echo "gecos: ${_uid}"
            echo "userPassword: ${USERPASSW}"
            echo "PersonLabels: L2C1"
            echo "permission: $(echo ${KEY})"
            echo ""
        } > ${_uid}.ldif

        ldapadd -x -w${LDAPPASSW} -h${LDAP_IP} -D "cn=Manager,${DC}" -f ${_uid}.ldif
        rm ${_uid}.ldif

        {
            echo ""
            echo "dn: uid=${_uid},${OU}"
            echo "objectClass: top"
            echo "objectClass: posixAccount"
            echo "objectClass: shadowAccount"
            echo "objectClass: organizationalPerson"
            echo "objectClass: inetOrgPerson"
            echo "objectClass: person"
            echo "objectClass: Security"
            echo "objectClass: orgstruct"
            echo "objectClass: mail"
            echo "objectClass: Kiosk"
            echo "cn: ${_uid}"
            echo "sn: $(uuidgen)"
            echo "uid: ${_uid}"
            echo "uidNumber: $(( $SUM+1 ))"
            echo "gidNumber: 0"
            echo "homeDirectory: /home/${_uid}"
            echo "loginShell: /bin/bash"
            echo "gecos: ${_uid}"
            echo "userPassword: ${USERPASSW}"
            echo "PersonLabels: L2C1"
            echo "permission: $(echo ${KEY})"
            echo ""
        } > ${_uid}.ldif

        ldapadd -x -w${LDAPPASSW} -h${LDAP_IP} -D "cn=Manager,${DC}" -f ${_uid}.ldif
        rm ${_uid}.ldif

    fi
done

exit 1

## Add user to LDAP Site
##
#for _host in ${var_h}; do
#    function _session() {
#        curl --connect-timeout 1 -m1 -XPOST 'http://'${_host}'/arctic/api/identity/login' -d '{'\"username\"\:\"''${WEBUSER}''\"\,\ \"password\"\:\"''${WEBPASS}''\"'}' -s -k -v 2>&1
#    }
#
#    _isession=`_session | egrep -c JSESSIONID`
#
#    if [ ${_isession} -eq 1 ]; then
#        _get_session=`_session | egrep -o 'JSESSIONID=.+;' | sed -e "s/;//g"`
#    else
#        echo "Can not get cookie, database is disable ${i}"
#    fi
#
#
#    curl -i -s -k  -X $'POST' \
#        -H $'Host: '${_host}'' \
#        -H $'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0' \
#        -H $'Accept: application/json, text/plain, */*' \
#        -H $'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3' \
#        -H $'Accept-Encoding: gzip, deflate' \
#        -H $'Content-Type: application/json;charset=utf-8' \
#        -H $'X-Requested-With: XMLHttpRequest' \
#        -H $'Referer: http://'${_host}'/mvs/page/users' \
#        -H $'Cookie: '${_get_session}'' \
#        -H $'Connection: close' \
#        -b "${_get_session}" \
#        -d '{'\"username\"\:\"''${WEBUSER}''\"\,\ \"password\"\:\"''${WEBPASS}''\"'}' \
#        $'http://'${_host}'/arctic/api/identity/login' 2>&1 >/dev/null
#
#    for _uid in ${USER}; do
#        curl -i -s -k  -X $'POST' \
#            -H $'Host: '${_host}'' \
#            -H $'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0' \
#            -H $'Accept: application/json, text/plain, */*' \
#            -H $'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3' \
#            -H $'Accept-Encoding: gzip, deflate' \
#            -H $'Content-Type: application/json;charset=utf-8' \
#            -H $'X-Requested-With: XMLHttpRequest' \
#           -H $'Referer: http://'${_host}'/mvs/page/users' \
#            -H $'Cookie: '${_get_session}'' \
#            -H $'Connection: close' \
#            -b "${_get_session}" \
#            --data-binary $'{'\"type\":\"3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2\",\"email\":\"''${_uid}''@swemel.ru\",\"firstName\":\"''${_uid}''\",\"lastName\":\"''${_uid}''\",\"systemUsersLink\":{\"type\":\"6d13d494-7bf5-4d11-9f32-424c4f8b6450\",\"login\":\"''${_uid}''\"'}}' \
#            $'http://'${_host}'/arctic/api/nodes?type=foivPersonnel' | tee ${_uid}-${_host}.txt 2>&1 > /dev/null
#
#        _sn=`cat ${_uid}-${_host}.txt | awk -F\" {'print$6'} | egrep -v '^$' | sed -e 's/ //g'`
#
#        curl -i -s -k  -X $'PUT' \
#            -H $'Host: '${_host}'' \
#            -H $'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0' \
#           -H $'Accept: application/json, text/plain, */*' \
#            -H $'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3' \
#            -H $'Accept-Encoding: gzip, deflate' \
#            -H $'Content-Type: application/json;charset=utf-8' \
#            -H $'X-Requested-With: XMLHttpRequest' \
#            -H $'Referer: http://'${_host}'/mvs/page/users' \
#            -H $'Cookie: '${_get_session}'' \
#            -H $'Connection: close' \
#            -b "${_get_session}" \
#            --data-binary $'{'\"id\":\"''${_sn}''\",\"type\":\"3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2\",\"systemUsersLink\":{\"type\":\"6d13d494-7bf5-4d11-9f32-424c4f8b6450\",\"login\":\"''${_uid}''\"'}}' \
#            $'http://'${_host}'/arctic/api/nodes/'${_sn}'' 2>&1 > /dev/null
#
#
#        curl -i -s -k  -X $'POST' \
#            -H $'Host: '${_host}'' \
#            -H $'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0' \
#           -H $'Accept: application/json, text/plain, */*' \
#            -H $'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3' \
#            -H $'Accept-Encoding: gzip, deflate' \
#            -H $'Content-Type: application/json;charset=utf-8' \
#            -H $'X-Requested-With: XMLHttpRequest' \
#            -H $'Referer: http://'${_host}'/mvs/page/users' \
#            -H $'Cookie: '${_get_session}'' \
#            -H $'Connection: close' \
#            -b "${_get_session}" \
#            --data-binary $'{\"type\":\"foivPersonnel\",\"count\":true}' \
#            $'http://'${_host}'/arctic/api/graph/gquery?type=foivPersonnel&count=true' 2>&1 > /dev/null
#
#
#        curl -i -s -k  -X $'POST' \
#            -H $'Host: '${_host}'' \
#            -H $'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0' \
#            -H $'Accept: application/json, text/plain, */*' \
#            -H $'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3' \
#            -H $'Accept-Encoding: gzip, deflate' \
#            -H $'Content-Type: application/json;charset=utf-8' \
#            -H $'X-Requested-With: XMLHttpRequest' \
#            -H $'Referer: http://'${_host}'/mvs/page/users' \
#            -H $'Cookie: '${_get_session}'' \
#            -H $'Connection: close' \
#            -b "${_get_session}" \
#            --data-binary $'{\"type\":\"foivPersonnel\",\"select\":[\"id\",\"name\",\"version\",\"type\",\"lastName\",\"firstName\",\"middleName\",{\"orgFoivDepartment\":[\"id\",\"version\",\"type\",\"name\"]}],\"limit\":20,\"offset\":0,\"order\":[{\"desc\":[\"$version\"]}]}' \
#            $'http://'${_host}'/arctic/api/graph/gquery?type=foivPersonnel' 2>&1 > /dev/null
#
#        rm -f ${_uid}-${_host}.txt
#
#        echo "User ${_uid} has been add"
#    done
#done
