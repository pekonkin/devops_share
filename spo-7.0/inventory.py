#!/usr/bin/env python
'''
Dyanmic inventory script, converting from ./hosts format of "address name [group]..."
usage $0 --list
'''



import os
import sys
import argparse
from yaml import safe_dump as yamldump
from collections import defaultdict

try:
    import json
except ImportError:
    import simplejson as json


def strip_comment(line, comment='#'):
    pos = line.find(comment)
    if pos >=0:
        line = line[:pos]
    return line


class ExampleInventory(object):

    def __init__(self):
        self.inventory = {}
        self.read_cli_args()

        if self.args.list:
            # Called with `--list`.
            self.inventory = self.example_inventory()
        elif self.args.host:
            # Called with `--host [hostname]`.
            # Not implemented, since we return _meta info `--list`.
            self.inventory = self.empty_inventory()
        # If no groups or vars are present, return empty inventory.
        else:
            self.inventory = self.empty_inventory()

        print yamldump(self.inventory, default_flow_style=False, tags=None, version=None, encoding='utf-8', explicit_start=True)

    # Example inventory for testing.
    def example_inventory(self):
        lines = [strip_comment(line.rstrip('\n')).strip() for line in open('hosts')]
        lines = [line for line in lines if line != '']
        d = defaultdict(lambda: defaultdict(list))
        for l in lines:
            for k in l.split()[1:]:
                d[k]['hosts'].append(l.split()[0])
        # need to return pure dict. If not, there will be garbage in yaml
        return json.loads(json.dumps(d))

    # Empty inventory for testing.
    def empty_inventory(self):
        return {'_meta': {'hostvars': {}}}

    # Read the command line args passed to the script.
    def read_cli_args(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('--list', action = 'store_true')
        parser.add_argument('--host', action = 'store')
        self.args = parser.parse_args()

# Get the inventory.
ExampleInventory()
