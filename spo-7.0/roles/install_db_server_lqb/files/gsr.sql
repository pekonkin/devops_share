insert into service.plugin_reglament
(
    id,
    qualifier,
    lastrun,
    cron_second,
    cron_minute,
    cron_hour,
    cron_weekday,
    cron_monthday,
    cron_month,
    cron_year
)
values
(
    '78c85180-4f72-11e8-9c2d-fa7ae01bbede',
    'gsr',
    '2017-12-31 23:59:59',
    '0',
    '*/5',
    '*/1',  -- запускать каждый час
    '*',
    '*',
    '*',
    '*'
) on conflict do nothing;