-- Удаление триггеров sync


DO $$
declare
  r record;
begin
	for r in select t.tgname, n.nspname, substring(t.tgname from 5 for 36) as table_name from pg_trigger t
		inner join pg_class p ON t.tgrelid = p.oid
		inner join pg_namespace n ON n.oid = p.relnamespace
		where n.nspname = any(sync.func_get_constant('allow_schema_audit_array')::text[])
		and t.tgname like 'trg_%' loop
			execute 'drop trigger "'||r.tgname||'" on '||r.nspname||'."'||r.table_name||'";';
	end loop;
END $$;  


DELETE FROM sync.sync_tree;  -- Удаление кустов
DELETE FROM sync.sync_reglament; -- Удаление регламентов




 -- Добавление регламентов ок
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (27,'node','1b7ce077-a6a3-4998-b0cb-a31cf583ad1c','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["cabotagenew"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (28,'node','a0827785-2638-4862-b9eb-d2f7b022c068','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["asmpPermissionsGrid"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (29,'node','ec22745e-c16a-4eb4-8bb3-791218cf6a37','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["foreignStateFlagTradShipPermGrid"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (30,'node','0c278c97-e689-4999-9949-14d5362aebc6','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["marineScienResearchPermGrid"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (31,'node','cac77a10-ff1b-48bd-adf8-bc28d6688c09','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["rosprirodnadzorPermGrid"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (32,'node','750a27dd-4e98-46b3-bca1-bc50ddbe6647','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["applCrossBorderGrid2"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (33,'node','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["foreignShipRepCrosPermGrid"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (34,'node','65be91de-2d44-413a-8330-d1fab285ff77','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["suspensionNpggNoticeJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (35,'node','dc05e486-d549-445f-a1a8-2cd2f6de7f58','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["noticeNPGGRejectJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (36,'node','f6f72a69-61c4-40ef-a7bc-65e0107302f6','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["statementToCrossGGForTransVBRJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (37,'node','1acf6821-78a5-4a03-9726-7ab541a0066a','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["permissionForTransVBRJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (38,'node','a1e43695-a4d4-4e89-a03f-dca04148405f','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["noticeForCancelTransVBRpermissionJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (39,'node','07a7f96d-efd1-48f1-8000-7d695c372204','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["gridPort"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (40,'node','6a88ac7e-835a-42cf-85bd-5c50257429f2','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["industryobjectGrid"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (41,'node','83528fff-3732-4cc3-8780-06483ad2cf13','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["smallShipJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (42,'node','80c86284-b5e0-413d-ae4f-4906f8cce360','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["pripGrid"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (43,'node','7eb9e81f-e390-4f1b-a421-447d1092812f','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["navipGrid"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (44,'node','dad9abaf-6515-4307-9a8d-f979c2363c14','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["hydrometeoGrid"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (45,'node','d004f2da-b348-4622-8910-02ec3596d2fc','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["weatherForecast"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (46,'node','a58dcbde-daf9-420d-a5ce-c35559997464','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["currentUnfavourableGMUJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (117,'node','d55f6865-7d3e-4776-89f7-cccd2a452891','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,null,null,null,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (5,'node','d943c4a4-239c-4b6e-aa50-0da53cf712e9','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["alarmsGrid"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (95,'timeseries','ff9a6d9f-37e7-4055-961b-b8bfd508777a','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'[]'::json,'[]'::json,null,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (88,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'[]'::json,'[]'::json,null,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (119,'node','f943ce62-643e-4b7d-b361-29bac44e5054','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,null,null,null,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (91,'node','ab3bc3f2-c035-4cdc-9bf4-f113889911bd','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["tableCatchVbrPermissionKpm"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (92,'node','f6fd4eb7-3eb5-4b2a-88af-2b5dbb8bffe7','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["ssdGrid"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (93,'node','b8009fe7-5921-44ca-b067-ebcef697ad9e','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["ratiosource"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (100,'node','54533d5d-e680-4598-b811-f29217874d92','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,false,null,null,null,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (97,'node','49cfe497-2c77-40f3-8942-793203ca29ee','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,null,null,null,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (99,'node','32d58f4f-fc82-4364-a9b5-5b899f57032b','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,false,null,null,null,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (52,'node','20059f4e-e0e5-4033-b6ca-f64a27ef3bf8','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["dangerousPhenomenaESIMO"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (53,'node','bee0575f-cb7f-4490-92a9-56b7db8f8991','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["icesituationGrid"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (6,'node','eb632aeb-bc4f-4560-8305-4981d502746a','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["callHeaderMainJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (7,'node','022d3cf7-6c3e-4e79-88e2-c9612df1f801','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["kpmShip"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (8,'node','7fbd40a8-c0db-4269-99ff-622e7752a3f1','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["kpmKPUFormOK"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (9,'node','0a7d8c62-4d0a-416d-8db9-e3045df14de4','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["typePlanInteractionEvent"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (10,'node','3aca4bf0-b0e4-46f3-9370-263d88559a19','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["eventsTableMVS"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (101,'node','8e346996-381e-42c8-b256-69f44e322fa4','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,false,null,null,null,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (103,'node','a765b215-60ab-46f7-bbb9-19a0089df9d2','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,false,null,null,null,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (104,'node','f6af63fe-d19b-479b-9425-406bb0c2c182','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,false,null,null,null,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (105,'node','498869e4-24d3-4775-a3ff-2403b6a7a9fc','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,false,null,null,null,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (106,'node','a2add7ba-7f14-4a1a-a54e-eeb02388f296','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,false,null,null,null,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (107,'node','08978c9e-9ca2-41ef-a391-b9a05c10e512','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,false,null,null,null,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (108,'node','68e215a3-c550-408f-ad1b-01cc1cbf62d3','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,false,null,null,null,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (109,'node','1c94b873-cb6f-4c57-84d9-98f6086f8b3f','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,false,null,null,null,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (110,'node','31beb151-33e8-458f-8788-b70403d2b8d9','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,false,null,null,null,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (111,'node','cb417a3d-4e3a-4c10-8fe3-dcc64e397e21','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,false,null,null,null,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (112,'node','df09be79-2377-4a99-b445-1347c1a16188','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,false,null,null,null,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (113,'node','e54c5601-3ca7-4115-a5eb-8e418715f381','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,false,null,null,null,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (11,'node','57ec70e6-23f7-4576-9a64-353b9a2adbc3','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["fishingCatchViolationJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (12,'node','5c8db12a-8d2c-4fbe-b376-5c124548e845','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["odu"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (14,'node','84c3d1fe-19b6-40ce-b0c6-448418c73613','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["fishingUnloadPortGrid"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (15,'node','1e3cf9cd-9768-46b1-9208-ae3763b7cce0','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["sourceJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (16,'node','45d60fd0-bed3-4abf-849d-204f219741be','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["gridSmallShip"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (17,'node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["regSmallShipPSJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (18,'node','c4c53bf2-0b46-4a55-b780-4617838056c7','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["activityStartNoticeJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (19,'node','50a9b985-42e2-4721-96d5-4ab8c7d575b0','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["withdrawalNoticeJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (20,'node','72910d25-aad4-489e-a55c-d6ef0966ca23','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["exitrfnpgg"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (21,'node','19a88372-0f65-4f39-8100-64cbaaefd006','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["noticeToStartActivitiesJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (22,'node','d0023997-ebca-4f13-9fab-be74f28cf23d','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["noticeChangeMPSDataJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (23,'node','db985f58-084c-4676-b8ca-854818267af5','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["shipLocationNPGGJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (24,'node','b2899d4f-7ba5-4602-9a84-bfca9b1ad310','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["registrMPSAndIcyTSNoticeJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (25,'node','22223124-80ba-4136-82c5-fc1bbf35d400','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["legalEntityGrid"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (26,'node','18177e57-bcc9-4a9b-b389-cf794ac3c727','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["individualsGrid"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (114,'node','3fceb216-4989-44bf-bb51-52736e188d2f','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,false,null,null,null,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (115,'node','a5af5c8a-7677-4c72-89d3-954fb1c0951e','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,false,null,null,null,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (47,'node','f0d3ba8c-0310-4f61-b081-15fce525c2d7','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["platformGMUData"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (48,'node','b016460e-68cf-4218-8108-9abc92d5f883','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["shipGMUData"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (49,'node','a9238f0a-5842-40db-9985-6e5d6b934fc7','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["forecastThreedayESIMO"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (50,'node','84b8768b-b9e5-40da-a5d3-6b20feff3146','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["windForecastESIMO"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (51,'node','75a70c5e-d596-49f8-8a81-455b6a120f6d','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["stormWarningESIMO"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (54,'node','4eb98b16-7d92-4b35-8b0e-3ce3748b9419','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["weatherconditionsGrid"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (55,'node','ce5e9334-f197-4c09-a3c5-c706d2592a21','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["inspectorsDoingListJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (56,'node','88051951-88db-40c7-a9cc-af977295a5d1','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["inspectorSquadPositionJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (57,'node','c4813711-099b-46a4-96fe-d74482d9c841','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["unloadForeign"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (58,'node','ecb1f432-7fb4-4138-9783-65a1614c48e8','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["unloadback"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (59,'node','bcaacdf8-c90d-431b-a115-5c1afaf06c08','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["applicationmkp"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (60,'node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["orgstructureFoivGrid"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (61,'node','b61affec-d01e-4bb8-b07d-693a3e0d7152','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["plansInteraction"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (62,'node','0f516482-6869-4115-b458-c4eeed1e6a50','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["caseGivenNumberJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (63,'node','5d3272d2-a131-4c89-a0b5-de0be03ae5ca','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["admOffencesCaseJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (64,'node','237623ba-b2e2-4153-8677-fde24ffa7f06','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["adminProtokolJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (65,'node','1da3d221-a235-4d4c-8135-590b6ef6ae4f','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["illegalCasesJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (66,'node','e32f01c9-0429-496c-97f9-7217b39ecccf','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["districtRegistry"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (67,'node','ab2a7d5f-dfaa-4011-85ce-e80fcb610cb1','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["commercialFishingRegionsJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (68,'node','520a99af-0f89-4ae1-a1d7-9fc9fd04f2d6','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["platformsMeteoList"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (69,'node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["machineListJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (70,'node','07aca350-6f0d-11e4-9803-0800200c9a66','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["npdGrid"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (71,'node','ae32043f-3f35-411d-9e9a-f105122b5f55','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["loadedTracingPaperJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (73,'node','a5bf2c91-3b59-4b97-99ac-f2147c6d7748','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["passApplicationGrid"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (74,'node','6cb1a579-4d85-433f-8113-df4bff433b04','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["passBorderZoneGrid"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (75,'node','1e5b9667-7f23-417c-99f2-90b6c651ace8','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["rejectPassNoticeGrid"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (76,'node','6c5d9069-6a70-4126-977d-4d7e7b052afd','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["childIndividualBorderPassJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (77,'node','7b30cc82-f54e-41c3-9eb0-943c032be9a3','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["childBlankBorderPassJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (78,'node','aa59c41b-50d2-4a8b-9154-29e1d955e2a6','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["childBorderPassRejectJornal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (79,'node','a2a93625-4858-4c08-b2c2-c246abbd6806','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["collectiveBorderPassJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (80,'node','3b69bc6c-8902-4e61-abc9-92303a3f290a','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["collectiveBlankBorderPassJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (81,'node','c2e023d5-e864-48b8-b6c9-5c77f5e7d839','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["collectiveBorderPassRejectJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (82,'node','013fc384-177b-4613-bf1a-a15f2d9c35a1','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["applicationPermissionGrid2"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (83,'node','6a52334b-56f3-439f-8be7-47e82dd4d323','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["permissionsGrid"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (84,'node','fbe34e57-8e49-4f35-9546-650b4bcccc52','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["permRejectNoticeGrid"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (85,'node','fdf14d4e-5b31-4e94-9c45-7aa44452226e','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["researchAnnouncePermissonjournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (86,'node','bc1b9bfd-8c66-4779-a5d4-add478db3fa4','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["permissionsPromResearchJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (87,'node','19528aa3-caee-4d65-a3e0-1b98fc79bc1f','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["noticePermissionRejectedJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (13,'node','15b0e47c-a215-471a-ae09-b1e8c0dd91e9','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["quotesFishing"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (90,'node','a264bf75-34c6-4acc-a56b-a9790e440eb1','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["fishingPermissionGrid"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (94,'node','dd1730ec-20c1-4cb0-a1bd-2b1a85222e0e','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'[]'::json,'[]'::json,null,null,null);



-- Добавление кустов

insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),90,'node','a264bf75-34c6-4acc-a56b-a9790e440eb1','id','rel','a264bf75-34c6-4acc-a56b-a9790e440eb1','from_id',1,2,'<tbl_to>.attribute_id !=''c016de65-624c-4aee-a91a-27e351f5288a''');
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),31,'rel','cac77a10-ff1b-48bd-adf8-bc28d6688c09','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),31,'rel','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),32,'rel','750a27dd-4e98-46b3-bca1-bc50ddbe6647','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),32,'rel','d332d316-c355-4ba0-98b6-4331d8cffa5c','to_id','node','e879ccd2-bc40-45cb-89a3-7829bb578936','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),32,'node','750a27dd-4e98-46b3-bca1-bc50ddbe6647','id','rel','750a27dd-4e98-46b3-bca1-bc50ddbe6647','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),32,'rel','d332d316-c355-4ba0-98b6-4331d8cffa5c','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),32,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),32,'rel','d332d316-c355-4ba0-98b6-4331d8cffa5c','to_id','node','9446b1d1-2671-433b-b3ab-db3ef81bd66b','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),32,'rel','750a27dd-4e98-46b3-bca1-bc50ddbe6647','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),32,'rel','750a27dd-4e98-46b3-bca1-bc50ddbe6647','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),32,'rel','750a27dd-4e98-46b3-bca1-bc50ddbe6647','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),32,'rel','750a27dd-4e98-46b3-bca1-bc50ddbe6647','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),32,'rel','750a27dd-4e98-46b3-bca1-bc50ddbe6647','to_id','node','d332d316-c355-4ba0-98b6-4331d8cffa5c','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),32,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),32,'node','d332d316-c355-4ba0-98b6-4331d8cffa5c','id','rel','d332d316-c355-4ba0-98b6-4331d8cffa5c','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),33,'rel','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),33,'node','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','id','rel','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),33,'node','d332d316-c355-4ba0-98b6-4331d8cffa5c','id','rel','d332d316-c355-4ba0-98b6-4331d8cffa5c','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),33,'rel','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),33,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),33,'node','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','id','rel','65be91de-2d44-413a-8330-d1fab285ff77','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),33,'rel','65be91de-2d44-413a-8330-d1fab285ff77','from_id','node','65be91de-2d44-413a-8330-d1fab285ff77','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),33,'node','750a27dd-4e98-46b3-bca1-bc50ddbe6647','id','rel','750a27dd-4e98-46b3-bca1-bc50ddbe6647','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),33,'rel','d332d316-c355-4ba0-98b6-4331d8cffa5c','to_id','node','9446b1d1-2671-433b-b3ab-db3ef81bd66b','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),33,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),33,'rel','d332d316-c355-4ba0-98b6-4331d8cffa5c','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),33,'rel','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','to_id','node','750a27dd-4e98-46b3-bca1-bc50ddbe6647','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),33,'rel','d332d316-c355-4ba0-98b6-4331d8cffa5c','to_id','node','e879ccd2-bc40-45cb-89a3-7829bb578936','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),33,'rel','750a27dd-4e98-46b3-bca1-bc50ddbe6647','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),33,'rel','750a27dd-4e98-46b3-bca1-bc50ddbe6647','to_id','node','d332d316-c355-4ba0-98b6-4331d8cffa5c','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),34,'rel','65be91de-2d44-413a-8330-d1fab285ff77','to_id','node','8891c765-5f0d-42d8-87b2-e55b9c8dc930','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),34,'rel','750a27dd-4e98-46b3-bca1-bc50ddbe6647','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),34,'rel','750a27dd-4e98-46b3-bca1-bc50ddbe6647','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),34,'node','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','id','rel','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),34,'node','65be91de-2d44-413a-8330-d1fab285ff77','id','rel','65be91de-2d44-413a-8330-d1fab285ff77','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),34,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),34,'rel','750a27dd-4e98-46b3-bca1-bc50ddbe6647','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),34,'rel','65be91de-2d44-413a-8330-d1fab285ff77','to_id','node','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),34,'rel','65be91de-2d44-413a-8330-d1fab285ff77','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),34,'rel','65be91de-2d44-413a-8330-d1fab285ff77','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),34,'node','750a27dd-4e98-46b3-bca1-bc50ddbe6647','id','rel','750a27dd-4e98-46b3-bca1-bc50ddbe6647','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),34,'rel','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','to_id','node','750a27dd-4e98-46b3-bca1-bc50ddbe6647','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),35,'rel','dc05e486-d549-445f-a1a8-2cd2f6de7f58','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),35,'node','750a27dd-4e98-46b3-bca1-bc50ddbe6647','id','rel','750a27dd-4e98-46b3-bca1-bc50ddbe6647','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),35,'rel','750a27dd-4e98-46b3-bca1-bc50ddbe6647','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),35,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),35,'rel','dc05e486-d549-445f-a1a8-2cd2f6de7f58','to_id','node','750a27dd-4e98-46b3-bca1-bc50ddbe6647','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),35,'rel','750a27dd-4e98-46b3-bca1-bc50ddbe6647','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),35,'rel','750a27dd-4e98-46b3-bca1-bc50ddbe6647','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),35,'node','dc05e486-d549-445f-a1a8-2cd2f6de7f58','id','rel','dc05e486-d549-445f-a1a8-2cd2f6de7f58','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),36,'node','f6f72a69-61c4-40ef-a7bc-65e0107302f6','id','rel','f6f72a69-61c4-40ef-a7bc-65e0107302f6','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),36,'rel','f6f72a69-61c4-40ef-a7bc-65e0107302f6','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),36,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),36,'rel','f6f72a69-61c4-40ef-a7bc-65e0107302f6','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),36,'rel','f6f72a69-61c4-40ef-a7bc-65e0107302f6','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),36,'rel','f6f72a69-61c4-40ef-a7bc-65e0107302f6','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),36,'rel','f6f72a69-61c4-40ef-a7bc-65e0107302f6','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),37,'node','1acf6821-78a5-4a03-9726-7ab541a0066a','id','rel','1acf6821-78a5-4a03-9726-7ab541a0066a','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),37,'rel','1acf6821-78a5-4a03-9726-7ab541a0066a','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),37,'node','f6f72a69-61c4-40ef-a7bc-65e0107302f6','id','rel','f6f72a69-61c4-40ef-a7bc-65e0107302f6','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),37,'rel','f6f72a69-61c4-40ef-a7bc-65e0107302f6','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),37,'rel','1acf6821-78a5-4a03-9726-7ab541a0066a','to_id','node','f6f72a69-61c4-40ef-a7bc-65e0107302f6','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),37,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),38,'node','a1e43695-a4d4-4e89-a03f-dca04148405f','id','rel','a1e43695-a4d4-4e89-a03f-dca04148405f','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),38,'rel','a1e43695-a4d4-4e89-a03f-dca04148405f','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),38,'node','f6f72a69-61c4-40ef-a7bc-65e0107302f6','id','rel','f6f72a69-61c4-40ef-a7bc-65e0107302f6','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),38,'rel','f6f72a69-61c4-40ef-a7bc-65e0107302f6','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),38,'rel','a1e43695-a4d4-4e89-a03f-dca04148405f','to_id','node','f6f72a69-61c4-40ef-a7bc-65e0107302f6','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),38,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),39,'rel','07a7f96d-efd1-48f1-8000-7d695c372204','to_id','node','72eefd7e-19fc-4907-afba-f3c7bacc8c06','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),39,'rel','07a7f96d-efd1-48f1-8000-7d695c372204','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),39,'node','72eefd7e-19fc-4907-afba-f3c7bacc8c06','id','rel','72eefd7e-19fc-4907-afba-f3c7bacc8c06','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),39,'rel','72eefd7e-19fc-4907-afba-f3c7bacc8c06','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),39,'node','07a7f96d-efd1-48f1-8000-7d695c372204','id','rel','07a7f96d-efd1-48f1-8000-7d695c372204','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),41,'rel','fdf14d4e-5b31-4e94-9c45-7aa44452226e','from_id','node','fdf14d4e-5b31-4e94-9c45-7aa44452226e','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),41,'rel','bc1b9bfd-8c66-4779-a5d4-add478db3fa4','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',10,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),41,'node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id','rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),41,'rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','to_id','node','70709cc4-f6a5-4e8e-9f6d-07c710a460df','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),41,'rel','83528fff-3732-4cc3-8780-06483ad2cf13','to_id','node','bc1b9bfd-8c66-4779-a5d4-add478db3fa4','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),41,'rel','83528fff-3732-4cc3-8780-06483ad2cf13','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),41,'rel','83528fff-3732-4cc3-8780-06483ad2cf13','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),41,'node','8dff0edf-033f-427e-a74c-ec09b6f2af30','id','rel','fdf14d4e-5b31-4e94-9c45-7aa44452226e','to_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),41,'rel','83528fff-3732-4cc3-8780-06483ad2cf13','to_id','node','c4e5bf39-0a19-4b82-a570-5dcef42192d8','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),41,'node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id','rel','8dff0edf-033f-427e-a74c-ec09b6f2af30','to_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),41,'node','bc1b9bfd-8c66-4779-a5d4-add478db3fa4','id','rel','bc1b9bfd-8c66-4779-a5d4-add478db3fa4','from_id',9,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),41,'rel','83528fff-3732-4cc3-8780-06483ad2cf13','to_id','node','cac77a10-ff1b-48bd-adf8-bc28d6688c09','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),41,'node','83528fff-3732-4cc3-8780-06483ad2cf13','id','rel','83528fff-3732-4cc3-8780-06483ad2cf13','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),41,'rel','83528fff-3732-4cc3-8780-06483ad2cf13','to_id','node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),41,'rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','to_id','node','ff9a6d9f-37e7-4055-961b-b8bfd508777a','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),41,'node','fdf14d4e-5b31-4e94-9c45-7aa44452226e','id','rel','bc1b9bfd-8c66-4779-a5d4-add478db3fa4','to_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),41,'rel','8dff0edf-033f-427e-a74c-ec09b6f2af30','from_id','node','8dff0edf-033f-427e-a74c-ec09b6f2af30','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),41,'rel','bc1b9bfd-8c66-4779-a5d4-add478db3fa4','from_id','node','bc1b9bfd-8c66-4779-a5d4-add478db3fa4','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),41,'rel','83528fff-3732-4cc3-8780-06483ad2cf13','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),41,'rel','83528fff-3732-4cc3-8780-06483ad2cf13','to_id','node','19a88372-0f65-4f39-8100-64cbaaefd006','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),42,'node','80c86284-b5e0-413d-ae4f-4906f8cce360','id','rel','80c86284-b5e0-413d-ae4f-4906f8cce360','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),42,'rel','80c86284-b5e0-413d-ae4f-4906f8cce360','to_id','node','f7b9666e-3e15-4340-b7a5-28ab3cef491e','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),43,'node','7eb9e81f-e390-4f1b-a421-447d1092812f','id','rel','7eb9e81f-e390-4f1b-a421-447d1092812f','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),43,'rel','7eb9e81f-e390-4f1b-a421-447d1092812f','to_id','node','f7b9666e-3e15-4340-b7a5-28ab3cef491e','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),44,'rel','dad9abaf-6515-4307-9a8d-f979c2363c14','to_id','node','f7b9666e-3e15-4340-b7a5-28ab3cef491e','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),44,'node','dad9abaf-6515-4307-9a8d-f979c2363c14','id','rel','dad9abaf-6515-4307-9a8d-f979c2363c14','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),44,'rel','dad9abaf-6515-4307-9a8d-f979c2363c14','to_id','node','e32f01c9-0429-496c-97f9-7217b39ecccf','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),45,'rel','d004f2da-b348-4622-8910-02ec3596d2fc','to_id','node','917dd6ea-f619-4abc-ba0c-dd687c5f2836','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),45,'node','ecc86414-e8fb-4e6f-b10e-1e7656ff3739','id','rel','ecc86414-e8fb-4e6f-b10e-1e7656ff3739','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),45,'rel','d004f2da-b348-4622-8910-02ec3596d2fc','to_id','node','ecc86414-e8fb-4e6f-b10e-1e7656ff3739','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),45,'node','d004f2da-b348-4622-8910-02ec3596d2fc','id','rel','d004f2da-b348-4622-8910-02ec3596d2fc','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),45,'rel','d004f2da-b348-4622-8910-02ec3596d2fc','to_id','node','695f949f-9f5e-4a5c-9ece-23b20caa37d2','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),45,'rel','d004f2da-b348-4622-8910-02ec3596d2fc','to_id','node','daabc926-6762-4e07-af27-d40f31328699','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),45,'rel','d004f2da-b348-4622-8910-02ec3596d2fc','to_id','node','7bbf476b-4a42-45bd-a7ab-04cc9c097a9e','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),45,'node','9c948f34-ed15-4403-8742-a5b7a863af68','id','rel','9c948f34-ed15-4403-8742-a5b7a863af68','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),45,'rel','d004f2da-b348-4622-8910-02ec3596d2fc','to_id','node','9c948f34-ed15-4403-8742-a5b7a863af68','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),45,'node','bef0c39d-59d0-4c48-880b-7505190f1332','id','rel','bef0c39d-59d0-4c48-880b-7505190f1332','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),45,'rel','d004f2da-b348-4622-8910-02ec3596d2fc','to_id','node','f7b9666e-3e15-4340-b7a5-28ab3cef491e','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),45,'rel','d004f2da-b348-4622-8910-02ec3596d2fc','to_id','node','e32f01c9-0429-496c-97f9-7217b39ecccf','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),45,'rel','d004f2da-b348-4622-8910-02ec3596d2fc','to_id','node','bef0c39d-59d0-4c48-880b-7505190f1332','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),46,'rel','a58dcbde-daf9-420d-a5ce-c35559997464','to_id','node','f7b9666e-3e15-4340-b7a5-28ab3cef491e','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),46,'node','a58dcbde-daf9-420d-a5ce-c35559997464','id','rel','a58dcbde-daf9-420d-a5ce-c35559997464','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),47,'node','f0d3ba8c-0310-4f61-b081-15fce525c2d7','id','rel','520a99af-0f89-4ae1-a1d7-9fc9fd04f2d6','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),47,'rel','520a99af-0f89-4ae1-a1d7-9fc9fd04f2d6','from_id','node','520a99af-0f89-4ae1-a1d7-9fc9fd04f2d6','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),53,'node','bee0575f-cb7f-4490-92a9-56b7db8f8991','id','rel','bee0575f-cb7f-4490-92a9-56b7db8f8991','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),53,'rel','bee0575f-cb7f-4490-92a9-56b7db8f8991','to_id','node','9af91107-b4ef-4c68-9471-8e180fc1f636','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),53,'rel','bee0575f-cb7f-4490-92a9-56b7db8f8991','to_id','node','f7b9666e-3e15-4340-b7a5-28ab3cef491e','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),53,'rel','bee0575f-cb7f-4490-92a9-56b7db8f8991','to_id','node','9d350e45-33de-46d5-8ea9-6c7e78016c20','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),53,'rel','bee0575f-cb7f-4490-92a9-56b7db8f8991','to_id','node','fc2b7cdd-19ba-4e45-86c8-62ae00131ebf','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),53,'rel','bee0575f-cb7f-4490-92a9-56b7db8f8991','to_id','node','e55322a1-6d83-46b0-9390-459006a6547d','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),53,'rel','bee0575f-cb7f-4490-92a9-56b7db8f8991','to_id','node','ed8d159f-1a43-4707-9c15-ea6dea23f37f','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),54,'rel','4eb98b16-7d92-4b35-8b0e-3ce3748b9419','to_id','node','9d350e45-33de-46d5-8ea9-6c7e78016c20','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),54,'rel','4eb98b16-7d92-4b35-8b0e-3ce3748b9419','to_id','node','fc2b7cdd-19ba-4e45-86c8-62ae00131ebf','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),54,'rel','4eb98b16-7d92-4b35-8b0e-3ce3748b9419','to_id','node','e55322a1-6d83-46b0-9390-459006a6547d','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),54,'rel','4eb98b16-7d92-4b35-8b0e-3ce3748b9419','to_id','node','f7b9666e-3e15-4340-b7a5-28ab3cef491e','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),54,'rel','4eb98b16-7d92-4b35-8b0e-3ce3748b9419','to_id','node','ed8d159f-1a43-4707-9c15-ea6dea23f37f','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),54,'rel','4eb98b16-7d92-4b35-8b0e-3ce3748b9419','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),54,'node','4eb98b16-7d92-4b35-8b0e-3ce3748b9419','id','rel','4eb98b16-7d92-4b35-8b0e-3ce3748b9419','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),54,'rel','4eb98b16-7d92-4b35-8b0e-3ce3748b9419','to_id','node','9af91107-b4ef-4c68-9471-8e180fc1f636','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),55,'node','ce5e9334-f197-4c09-a3c5-c706d2592a21','id','rel','ce5e9334-f197-4c09-a3c5-c706d2592a21','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),55,'rel','ce5e9334-f197-4c09-a3c5-c706d2592a21','to_id','node','70f9f45c-ab4e-4c20-992b-7e915808be55','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),55,'node','70f9f45c-ab4e-4c20-992b-7e915808be55','id','rel','70f9f45c-ab4e-4c20-992b-7e915808be55','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),55,'rel','70f9f45c-ab4e-4c20-992b-7e915808be55','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),56,'rel','dd8e3978-099a-4d6a-b048-2d7d67fd6774','to_id','node','01674964-6ac4-4bc2-9ac5-ef1361386c25','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),56,'rel','dd8e3978-099a-4d6a-b048-2d7d67fd6774','to_id','node','8c2ee112-29c9-47a8-8e0f-a94896f8be01','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),56,'rel','88051951-88db-40c7-a9cc-af977295a5d1','to_id','node','dd8e3978-099a-4d6a-b048-2d7d67fd6774','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),56,'rel','dd8e3978-099a-4d6a-b048-2d7d67fd6774','to_id','node','d24149d2-0cd0-4af2-9622-38ee9c0b0ec0','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),56,'rel','dd8e3978-099a-4d6a-b048-2d7d67fd6774','to_id','node','f847cfd5-0392-4f9d-9825-444b63776435','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),56,'node','88051951-88db-40c7-a9cc-af977295a5d1','id','rel','88051951-88db-40c7-a9cc-af977295a5d1','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),56,'node','dd8e3978-099a-4d6a-b048-2d7d67fd6774','id','rel','dd8e3978-099a-4d6a-b048-2d7d67fd6774','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),56,'rel','dd8e3978-099a-4d6a-b048-2d7d67fd6774','to_id','node','e32f01c9-0429-496c-97f9-7217b39ecccf','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),57,'node','b0694243-9063-49b1-9083-42ae67d8c815','id','rel','eb632aeb-bc4f-4560-8305-4981d502746a','to_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),57,'rel','b0694243-9063-49b1-9083-42ae67d8c815','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),57,'node','c4813711-099b-46a4-96fe-d74482d9c841','id','rel','c4813711-099b-46a4-96fe-d74482d9c841','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),57,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),57,'rel','eb632aeb-bc4f-4560-8305-4981d502746a','from_id','node','eb632aeb-bc4f-4560-8305-4981d502746a','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),57,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),57,'node','b0694243-9063-49b1-9083-42ae67d8c815','id','rel','b0694243-9063-49b1-9083-42ae67d8c815','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),57,'rel','eb632aeb-bc4f-4560-8305-4981d502746a','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),57,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),57,'rel','b0694243-9063-49b1-9083-42ae67d8c815','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),57,'node','eb632aeb-bc4f-4560-8305-4981d502746a','id','rel','eb632aeb-bc4f-4560-8305-4981d502746a','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),57,'rel','c4813711-099b-46a4-96fe-d74482d9c841','to_id','node','b0694243-9063-49b1-9083-42ae67d8c815','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),58,'node','b0694243-9063-49b1-9083-42ae67d8c815','id','rel','eb632aeb-bc4f-4560-8305-4981d502746a','to_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),58,'rel','b0694243-9063-49b1-9083-42ae67d8c815','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),58,'rel','eb632aeb-bc4f-4560-8305-4981d502746a','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),58,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),58,'rel','eb632aeb-bc4f-4560-8305-4981d502746a','from_id','node','eb632aeb-bc4f-4560-8305-4981d502746a','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),58,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),58,'node','b0694243-9063-49b1-9083-42ae67d8c815','id','rel','b0694243-9063-49b1-9083-42ae67d8c815','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),58,'rel','ecb1f432-7fb4-4138-9783-65a1614c48e8','to_id','node','cea46397-3b1a-4741-b507-6e7e5542034d','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),58,'rel','eb632aeb-bc4f-4560-8305-4981d502746a','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),58,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),58,'rel','ecb1f432-7fb4-4138-9783-65a1614c48e8','to_id','node','5906884f-ca52-471b-96a5-e5362b329c78','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),58,'rel','b0694243-9063-49b1-9083-42ae67d8c815','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),58,'rel','ecb1f432-7fb4-4138-9783-65a1614c48e8','to_id','node','b0694243-9063-49b1-9083-42ae67d8c815','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),58,'node','eb632aeb-bc4f-4560-8305-4981d502746a','id','rel','eb632aeb-bc4f-4560-8305-4981d502746a','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),58,'node','ecb1f432-7fb4-4138-9783-65a1614c48e8','id','rel','ecb1f432-7fb4-4138-9783-65a1614c48e8','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),59,'rel','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),59,'rel','bcaacdf8-c90d-431b-a115-5c1afaf06c08','to_id','node','fea1d58c-b5ba-4db1-b81d-65b00befcd1d','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),59,'node','54214b0d-98fb-46f5-ac99-188b510754c8','id','rel','54214b0d-98fb-46f5-ac99-188b510754c8','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),59,'rel','bcaacdf8-c90d-431b-a115-5c1afaf06c08','to_id','node','54214b0d-98fb-46f5-ac99-188b510754c8','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),59,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),59,'rel','bcaacdf8-c90d-431b-a115-5c1afaf06c08','to_id','node','a264bf75-34c6-4acc-a56b-a9790e440eb1','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),59,'node','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','id','rel','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),59,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),59,'node','bcaacdf8-c90d-431b-a115-5c1afaf06c08','id','rel','bcaacdf8-c90d-431b-a115-5c1afaf06c08','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),59,'rel','bcaacdf8-c90d-431b-a115-5c1afaf06c08','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),59,'rel','bcaacdf8-c90d-431b-a115-5c1afaf06c08','to_id','node','3e8360f5-bbb5-483d-b9f5-96c4910be8d6','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),59,'rel','54214b0d-98fb-46f5-ac99-188b510754c8','to_id','node','0242b28e-7b2e-43ff-bc19-26386bbf6a09','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),60,'node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id','rel','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),60,'node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id','rel','3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),60,'rel','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','to_id','node','39b3108d-3d24-4505-856a-8691ee9a5e51','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),60,'rel','3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2','from_id','node','3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),61,'rel','b61affec-d01e-4bb8-b07d-693a3e0d7152','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),61,'node','b61affec-d01e-4bb8-b07d-693a3e0d7152','id','rel','b61affec-d01e-4bb8-b07d-693a3e0d7152','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),61,'rel','b61affec-d01e-4bb8-b07d-693a3e0d7152','to_id','node','3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),61,'rel','b61affec-d01e-4bb8-b07d-693a3e0d7152','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),62,'node','0f516482-6869-4115-b458-c4eeed1e6a50','id','rel','0f516482-6869-4115-b458-c4eeed1e6a50','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),62,'rel','0f516482-6869-4115-b458-c4eeed1e6a50','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),62,'rel','0f516482-6869-4115-b458-c4eeed1e6a50','to_id','node','5d3272d2-a131-4c89-a0b5-de0be03ae5ca','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'node','237623ba-b2e2-4153-8677-fde24ffa7f06','id','rel','237623ba-b2e2-4153-8677-fde24ffa7f06','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','78d96263-a96d-4815-8060-6735c24f90c7','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'rel','6df974ad-2548-442a-b0d5-4b372aa7a4e4','to_id','node','8c011a81-7629-42e1-adc9-8c5b62764e28','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'node','5d3272d2-a131-4c89-a0b5-de0be03ae5ca','id','rel','6df974ad-2548-442a-b0d5-4b372aa7a4e4','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','2dd28c00-886c-4134-aa4a-f5b024cc10bc','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'rel','5d3272d2-a131-4c89-a0b5-de0be03ae5ca','to_id','node','237623ba-b2e2-4153-8677-fde24ffa7f06','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'rel','0f516482-6869-4115-b458-c4eeed1e6a50','from_id','node','0f516482-6869-4115-b458-c4eeed1e6a50','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'rel','6df974ad-2548-442a-b0d5-4b372aa7a4e4','to_id','node','48e32bc5-0d63-4288-a82d-959eda7b83c1','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'rel','6df974ad-2548-442a-b0d5-4b372aa7a4e4','to_id','node','2dd28c00-886c-4134-aa4a-f5b024cc10bc','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','848e8572-2e34-48c8-89e7-87f218431f15','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'rel','6df974ad-2548-442a-b0d5-4b372aa7a4e4','from_id','node','6df974ad-2548-442a-b0d5-4b372aa7a4e4','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','f5168e79-eb3e-40e1-9a17-f40c5f606749','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','ab2a7d5f-dfaa-4011-85ce-e80fcb610cb1','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'node','5d3272d2-a131-4c89-a0b5-de0be03ae5ca','id','rel','5d3272d2-a131-4c89-a0b5-de0be03ae5ca','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'rel','5d3272d2-a131-4c89-a0b5-de0be03ae5ca','to_id','node','20b99236-662b-4617-9766-49e723e5e12a','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','3a9d498a-b1c6-4759-8239-af728dcc06bd','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'rel','5d3272d2-a131-4c89-a0b5-de0be03ae5ca','to_id','node','4c743487-421e-4140-aa50-3d1ff5511ec5','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'node','6df974ad-2548-442a-b0d5-4b372aa7a4e4','id','rel','6df974ad-2548-442a-b0d5-4b372aa7a4e4','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'rel','5d3272d2-a131-4c89-a0b5-de0be03ae5ca','to_id','node','9fc50598-40a8-49c4-92a8-a165c31e786d','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','4c743487-421e-4140-aa50-3d1ff5511ec5','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'node','5d3272d2-a131-4c89-a0b5-de0be03ae5ca','id','rel','0f516482-6869-4115-b458-c4eeed1e6a50','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),63,'rel','5d3272d2-a131-4c89-a0b5-de0be03ae5ca','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),64,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','848e8572-2e34-48c8-89e7-87f218431f15','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),64,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','2dd28c00-886c-4134-aa4a-f5b024cc10bc','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),64,'node','237623ba-b2e2-4153-8677-fde24ffa7f06','id','rel','237623ba-b2e2-4153-8677-fde24ffa7f06','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),64,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','78d96263-a96d-4815-8060-6735c24f90c7','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),64,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),64,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','4c743487-421e-4140-aa50-3d1ff5511ec5','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),64,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','ab2a7d5f-dfaa-4011-85ce-e80fcb610cb1','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),64,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),64,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','3a9d498a-b1c6-4759-8239-af728dcc06bd','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),64,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),64,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','f5168e79-eb3e-40e1-9a17-f40c5f606749','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),65,'node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id','rel','18177e57-bcc9-4a9b-b389-cf794ac3c727','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),65,'rel','1da3d221-a235-4d4c-8135-590b6ef6ae4f','to_id','node','779e2a2e-e59c-4603-90a5-91b6dcd4a110','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),65,'rel','3383b87d-4082-4b5a-806f-22a517744067','to_id','node','8e3cce57-dbc5-479b-ab49-45df75e49807','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),65,'node','3383b87d-4082-4b5a-806f-22a517744067','id','rel','3383b87d-4082-4b5a-806f-22a517744067','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),65,'rel','1da3d221-a235-4d4c-8135-590b6ef6ae4f','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),65,'node','1da3d221-a235-4d4c-8135-590b6ef6ae4f','id','rel','1da3d221-a235-4d4c-8135-590b6ef6ae4f','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),65,'node','8e3cce57-dbc5-479b-ab49-45df75e49807','id','rel','8e3cce57-dbc5-479b-ab49-45df75e49807','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),65,'rel','1da3d221-a235-4d4c-8135-590b6ef6ae4f','to_id','node','7126c993-7f92-41ee-a186-e05154b71ab2','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),65,'rel','1da3d221-a235-4d4c-8135-590b6ef6ae4f','to_id','node','9c185f63-9897-4a71-b854-0473e61a0191','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),65,'rel','18177e57-bcc9-4a9b-b389-cf794ac3c727','to_id','node','354e91a9-976b-4bee-b873-1dbc53280ac1','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),65,'node','354e91a9-976b-4bee-b873-1dbc53280ac1','id','rel','354e91a9-976b-4bee-b873-1dbc53280ac1','from_id',9,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),65,'rel','1da3d221-a235-4d4c-8135-590b6ef6ae4f','to_id','node','3383b87d-4082-4b5a-806f-22a517744067','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),65,'rel','8e3cce57-dbc5-479b-ab49-45df75e49807','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),66,'node','e32f01c9-0429-496c-97f9-7217b39ecccf','id','rel','e32f01c9-0429-496c-97f9-7217b39ecccf','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),66,'rel','e32f01c9-0429-496c-97f9-7217b39ecccf','to_id','node','f7b9666e-3e15-4340-b7a5-28ab3cef491e','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),66,'rel','e32f01c9-0429-496c-97f9-7217b39ecccf','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),67,'rel','ab2a7d5f-dfaa-4011-85ce-e80fcb610cb1','to_id','node','0242b28e-7b2e-43ff-bc19-26386bbf6a09','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),67,'rel','ab2a7d5f-dfaa-4011-85ce-e80fcb610cb1','to_id','node','f7b9666e-3e15-4340-b7a5-28ab3cef491e','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),67,'rel','ab2a7d5f-dfaa-4011-85ce-e80fcb610cb1','to_id','node','a5a48f7e-a92e-49de-b0dd-aafdfd9ea7c3','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),67,'rel','ab2a7d5f-dfaa-4011-85ce-e80fcb610cb1','to_id','node','aed618c4-5e23-4a3f-beff-a9e3f0a05e33','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),67,'node','ab2a7d5f-dfaa-4011-85ce-e80fcb610cb1','id','rel','ab2a7d5f-dfaa-4011-85ce-e80fcb610cb1','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),67,'rel','ab2a7d5f-dfaa-4011-85ce-e80fcb610cb1','to_id','node','d311e190-89fa-4c7b-b76e-694fb3b2f4d0','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),67,'rel','ab2a7d5f-dfaa-4011-85ce-e80fcb610cb1','to_id','node','fea1d58c-b5ba-4db1-b81d-65b00befcd1d','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),67,'rel','ab2a7d5f-dfaa-4011-85ce-e80fcb610cb1','to_id','node','9f1b1e20-0574-46cb-88e7-3d32f22619e3','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),69,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),69,'rel','e08e0f7c-85f7-44af-9afe-aeedb38260af','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),87,'node','19528aa3-caee-4d65-a3e0-1b98fc79bc1f','id','rel','19528aa3-caee-4d65-a3e0-1b98fc79bc1f','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),87,'rel','fdf14d4e-5b31-4e94-9c45-7aa44452226e','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),87,'rel','fdf14d4e-5b31-4e94-9c45-7aa44452226e','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),69,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','397d7b11-3bf4-44fb-be5e-d1d77ea07543','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),69,'rel','397d7b11-3bf4-44fb-be5e-d1d77ea07543','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),69,'node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id','rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),69,'node','397d7b11-3bf4-44fb-be5e-d1d77ea07543','id','rel','397d7b11-3bf4-44fb-be5e-d1d77ea07543','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),69,'rel','397d7b11-3bf4-44fb-be5e-d1d77ea07543','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),69,'node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id','rel','e08e0f7c-85f7-44af-9afe-aeedb38260af','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),69,'rel','e08e0f7c-85f7-44af-9afe-aeedb38260af','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),70,'rel','07aca350-6f0d-11e4-9803-0800200c9a66','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),70,'node','07aca350-6f0d-11e4-9803-0800200c9a66','id','rel','07aca350-6f0d-11e4-9803-0800200c9a66','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),71,'node','ae32043f-3f35-411d-9e9a-f105122b5f55','id','rel','ae32043f-3f35-411d-9e9a-f105122b5f55','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),71,'rel','ae32043f-3f35-411d-9e9a-f105122b5f55','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),73,'node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id','rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),73,'node','07d5185d-a2d4-44bc-91d8-e83a43683b68','id','rel','07d5185d-a2d4-44bc-91d8-e83a43683b68','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),73,'rel','07d5185d-a2d4-44bc-91d8-e83a43683b68','to_id','node','a53e285c-63a0-4107-83cf-be9666495371','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),73,'rel','a5bf2c91-3b59-4b97-99ac-f2147c6d7748','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),73,'rel','a5bf2c91-3b59-4b97-99ac-f2147c6d7748','to_id','node','07d5185d-a2d4-44bc-91d8-e83a43683b68','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),73,'node','a5bf2c91-3b59-4b97-99ac-f2147c6d7748','id','rel','a5bf2c91-3b59-4b97-99ac-f2147c6d7748','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),73,'rel','07d5185d-a2d4-44bc-91d8-e83a43683b68','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),73,'rel','8c2ee112-29c9-47a8-8e0f-a94896f8be01','to_id','node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),73,'rel','07d5185d-a2d4-44bc-91d8-e83a43683b68','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),73,'rel','397d7b11-3bf4-44fb-be5e-d1d77ea07543','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',10,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),73,'rel','07d5185d-a2d4-44bc-91d8-e83a43683b68','to_id','node','8c2ee112-29c9-47a8-8e0f-a94896f8be01','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),73,'node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id','rel','e08e0f7c-85f7-44af-9afe-aeedb38260af','from_id',9,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),73,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),73,'rel','397d7b11-3bf4-44fb-be5e-d1d77ea07543','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',10,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),73,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','397d7b11-3bf4-44fb-be5e-d1d77ea07543','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),73,'rel','07d5185d-a2d4-44bc-91d8-e83a43683b68','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),73,'node','8c2ee112-29c9-47a8-8e0f-a94896f8be01','id','rel','8c2ee112-29c9-47a8-8e0f-a94896f8be01','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),73,'node','397d7b11-3bf4-44fb-be5e-d1d77ea07543','id','rel','397d7b11-3bf4-44fb-be5e-d1d77ea07543','from_id',9,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),74,'node','a5bf2c91-3b59-4b97-99ac-f2147c6d7748','id','rel','a5bf2c91-3b59-4b97-99ac-f2147c6d7748','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),74,'rel','6cb1a579-4d85-433f-8113-df4bff433b04','to_id','node','a5bf2c91-3b59-4b97-99ac-f2147c6d7748','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),74,'rel','07d5185d-a2d4-44bc-91d8-e83a43683b68','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),74,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','397d7b11-3bf4-44fb-be5e-d1d77ea07543','id',10,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),74,'rel','397d7b11-3bf4-44fb-be5e-d1d77ea07543','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',12,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),74,'node','397d7b11-3bf4-44fb-be5e-d1d77ea07543','id','rel','397d7b11-3bf4-44fb-be5e-d1d77ea07543','from_id',11,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),74,'rel','a5bf2c91-3b59-4b97-99ac-f2147c6d7748','to_id','node','07d5185d-a2d4-44bc-91d8-e83a43683b68','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),74,'node','8c2ee112-29c9-47a8-8e0f-a94896f8be01','id','rel','8c2ee112-29c9-47a8-8e0f-a94896f8be01','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),74,'node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id','rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','from_id',9,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),74,'rel','8c2ee112-29c9-47a8-8e0f-a94896f8be01','to_id','node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),74,'rel','6cb1a579-4d85-433f-8113-df4bff433b04','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),74,'rel','6cb1a579-4d85-433f-8113-df4bff433b04','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),74,'rel','07d5185d-a2d4-44bc-91d8-e83a43683b68','to_id','node','8c2ee112-29c9-47a8-8e0f-a94896f8be01','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),74,'rel','397d7b11-3bf4-44fb-be5e-d1d77ea07543','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',12,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),74,'rel','07d5185d-a2d4-44bc-91d8-e83a43683b68','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),74,'node','6cb1a579-4d85-433f-8113-df4bff433b04','id','rel','6cb1a579-4d85-433f-8113-df4bff433b04','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),74,'node','07d5185d-a2d4-44bc-91d8-e83a43683b68','id','rel','07d5185d-a2d4-44bc-91d8-e83a43683b68','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),75,'node','a5bf2c91-3b59-4b97-99ac-f2147c6d7748','id','rel','a5bf2c91-3b59-4b97-99ac-f2147c6d7748','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),75,'rel','1e5b9667-7f23-417c-99f2-90b6c651ace8','to_id','node','a5bf2c91-3b59-4b97-99ac-f2147c6d7748','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),75,'rel','07d5185d-a2d4-44bc-91d8-e83a43683b68','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),75,'rel','a5bf2c91-3b59-4b97-99ac-f2147c6d7748','to_id','node','07d5185d-a2d4-44bc-91d8-e83a43683b68','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),75,'rel','1e5b9667-7f23-417c-99f2-90b6c651ace8','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),75,'rel','1e5b9667-7f23-417c-99f2-90b6c651ace8','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),75,'node','1e5b9667-7f23-417c-99f2-90b6c651ace8','id','rel','1e5b9667-7f23-417c-99f2-90b6c651ace8','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),75,'rel','07d5185d-a2d4-44bc-91d8-e83a43683b68','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),75,'node','07d5185d-a2d4-44bc-91d8-e83a43683b68','id','rel','07d5185d-a2d4-44bc-91d8-e83a43683b68','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),76,'node','6c5d9069-6a70-4126-977d-4d7e7b052afd','id','rel','6c5d9069-6a70-4126-977d-4d7e7b052afd','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),76,'rel','3219a8bd-9950-4cfd-8cb9-6472901013b8','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),76,'node','3219a8bd-9950-4cfd-8cb9-6472901013b8','id','rel','3219a8bd-9950-4cfd-8cb9-6472901013b8','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),76,'rel','3219a8bd-9950-4cfd-8cb9-6472901013b8','to_id','node','a53e285c-63a0-4107-83cf-be9666495371','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),76,'rel','6c5d9069-6a70-4126-977d-4d7e7b052afd','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),76,'rel','6c5d9069-6a70-4126-977d-4d7e7b052afd','to_id','node','3219a8bd-9950-4cfd-8cb9-6472901013b8','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),76,'rel','3219a8bd-9950-4cfd-8cb9-6472901013b8','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),77,'rel','7b30cc82-f54e-41c3-9eb0-943c032be9a3','to_id','node','6c5d9069-6a70-4126-977d-4d7e7b052afd','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),77,'node','7b30cc82-f54e-41c3-9eb0-943c032be9a3','id','rel','7b30cc82-f54e-41c3-9eb0-943c032be9a3','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),77,'rel','6c5d9069-6a70-4126-977d-4d7e7b052afd','to_id','node','3219a8bd-9950-4cfd-8cb9-6472901013b8','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),77,'rel','7b30cc82-f54e-41c3-9eb0-943c032be9a3','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),77,'node','6c5d9069-6a70-4126-977d-4d7e7b052afd','id','rel','6c5d9069-6a70-4126-977d-4d7e7b052afd','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),77,'rel','3219a8bd-9950-4cfd-8cb9-6472901013b8','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),77,'rel','7b30cc82-f54e-41c3-9eb0-943c032be9a3','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),77,'node','3219a8bd-9950-4cfd-8cb9-6472901013b8','id','rel','3219a8bd-9950-4cfd-8cb9-6472901013b8','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),78,'rel','6c5d9069-6a70-4126-977d-4d7e7b052afd','to_id','node','3219a8bd-9950-4cfd-8cb9-6472901013b8','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),78,'node','6c5d9069-6a70-4126-977d-4d7e7b052afd','id','rel','6c5d9069-6a70-4126-977d-4d7e7b052afd','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),78,'rel','3219a8bd-9950-4cfd-8cb9-6472901013b8','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),78,'rel','aa59c41b-50d2-4a8b-9154-29e1d955e2a6','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),78,'rel','aa59c41b-50d2-4a8b-9154-29e1d955e2a6','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),78,'rel','aa59c41b-50d2-4a8b-9154-29e1d955e2a6','to_id','node','6c5d9069-6a70-4126-977d-4d7e7b052afd','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),78,'node','3219a8bd-9950-4cfd-8cb9-6472901013b8','id','rel','3219a8bd-9950-4cfd-8cb9-6472901013b8','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),78,'node','aa59c41b-50d2-4a8b-9154-29e1d955e2a6','id','rel','aa59c41b-50d2-4a8b-9154-29e1d955e2a6','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'rel','2370cd39-2426-4002-abba-6af70d1c8989','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id','rel','18177e57-bcc9-4a9b-b389-cf794ac3c727','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id','rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'rel','2370cd39-2426-4002-abba-6af70d1c8989','to_id','node','8e3cce57-dbc5-479b-ab49-45df75e49807','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'rel','8c2ee112-29c9-47a8-8e0f-a94896f8be01','to_id','node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'rel','2370cd39-2426-4002-abba-6af70d1c8989','to_id','node','8c2ee112-29c9-47a8-8e0f-a94896f8be01','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'rel','397d7b11-3bf4-44fb-be5e-d1d77ea07543','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',10,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'node','8e3cce57-dbc5-479b-ab49-45df75e49807','id','rel','8e3cce57-dbc5-479b-ab49-45df75e49807','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'rel','2370cd39-2426-4002-abba-6af70d1c8989','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'rel','a2a93625-4858-4c08-b2c2-c246abbd6806','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'node','a2a93625-4858-4c08-b2c2-c246abbd6806','id','rel','a2a93625-4858-4c08-b2c2-c246abbd6806','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id','rel','e08e0f7c-85f7-44af-9afe-aeedb38260af','from_id',9,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'rel','18177e57-bcc9-4a9b-b389-cf794ac3c727','to_id','node','354e91a9-976b-4bee-b873-1dbc53280ac1','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'rel','2370cd39-2426-4002-abba-6af70d1c8989','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'rel','a2a93625-4858-4c08-b2c2-c246abbd6806','to_id','node','2370cd39-2426-4002-abba-6af70d1c8989','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'node','354e91a9-976b-4bee-b873-1dbc53280ac1','id','rel','354e91a9-976b-4bee-b873-1dbc53280ac1','from_id',9,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'rel','397d7b11-3bf4-44fb-be5e-d1d77ea07543','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',10,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','397d7b11-3bf4-44fb-be5e-d1d77ea07543','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'rel','2370cd39-2426-4002-abba-6af70d1c8989','to_id','node','a53e285c-63a0-4107-83cf-be9666495371','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'node','8c2ee112-29c9-47a8-8e0f-a94896f8be01','id','rel','8c2ee112-29c9-47a8-8e0f-a94896f8be01','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'node','397d7b11-3bf4-44fb-be5e-d1d77ea07543','id','rel','397d7b11-3bf4-44fb-be5e-d1d77ea07543','from_id',9,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'node','2370cd39-2426-4002-abba-6af70d1c8989','id','rel','2370cd39-2426-4002-abba-6af70d1c8989','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'rel','8e3cce57-dbc5-479b-ab49-45df75e49807','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),80,'node','354e91a9-976b-4bee-b873-1dbc53280ac1','id','rel','354e91a9-976b-4bee-b873-1dbc53280ac1','from_id',11,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),80,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','397d7b11-3bf4-44fb-be5e-d1d77ea07543','id',10,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),80,'node','2370cd39-2426-4002-abba-6af70d1c8989','id','rel','2370cd39-2426-4002-abba-6af70d1c8989','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),80,'rel','397d7b11-3bf4-44fb-be5e-d1d77ea07543','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',12,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),80,'node','3b69bc6c-8902-4e61-abc9-92303a3f290a','id','rel','3b69bc6c-8902-4e61-abc9-92303a3f290a','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),80,'node','397d7b11-3bf4-44fb-be5e-d1d77ea07543','id','rel','397d7b11-3bf4-44fb-be5e-d1d77ea07543','from_id',11,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),80,'node','8c2ee112-29c9-47a8-8e0f-a94896f8be01','id','rel','8c2ee112-29c9-47a8-8e0f-a94896f8be01','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),80,'node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id','rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','from_id',9,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),80,'rel','2370cd39-2426-4002-abba-6af70d1c8989','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),80,'rel','8c2ee112-29c9-47a8-8e0f-a94896f8be01','to_id','node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),80,'rel','3b69bc6c-8902-4e61-abc9-92303a3f290a','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),80,'node','8e3cce57-dbc5-479b-ab49-45df75e49807','id','rel','8e3cce57-dbc5-479b-ab49-45df75e49807','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),80,'node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id','rel','18177e57-bcc9-4a9b-b389-cf794ac3c727','from_id',9,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),80,'rel','3b69bc6c-8902-4e61-abc9-92303a3f290a','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),80,'rel','3b69bc6c-8902-4e61-abc9-92303a3f290a','to_id','node','a2a93625-4858-4c08-b2c2-c246abbd6806','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),80,'rel','18177e57-bcc9-4a9b-b389-cf794ac3c727','to_id','node','354e91a9-976b-4bee-b873-1dbc53280ac1','id',10,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),80,'rel','2370cd39-2426-4002-abba-6af70d1c8989','to_id','node','8c2ee112-29c9-47a8-8e0f-a94896f8be01','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),80,'rel','397d7b11-3bf4-44fb-be5e-d1d77ea07543','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',12,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),80,'rel','a2a93625-4858-4c08-b2c2-c246abbd6806','to_id','node','2370cd39-2426-4002-abba-6af70d1c8989','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),80,'rel','2370cd39-2426-4002-abba-6af70d1c8989','to_id','node','8e3cce57-dbc5-479b-ab49-45df75e49807','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),80,'rel','8e3cce57-dbc5-479b-ab49-45df75e49807','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),80,'rel','2370cd39-2426-4002-abba-6af70d1c8989','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),80,'node','a2a93625-4858-4c08-b2c2-c246abbd6806','id','rel','a2a93625-4858-4c08-b2c2-c246abbd6806','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),81,'node','2370cd39-2426-4002-abba-6af70d1c8989','id','rel','2370cd39-2426-4002-abba-6af70d1c8989','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),81,'rel','2370cd39-2426-4002-abba-6af70d1c8989','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),81,'rel','c2e023d5-e864-48b8-b6c9-5c77f5e7d839','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),81,'rel','c2e023d5-e864-48b8-b6c9-5c77f5e7d839','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),81,'rel','a2a93625-4858-4c08-b2c2-c246abbd6806','to_id','node','2370cd39-2426-4002-abba-6af70d1c8989','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),81,'rel','c2e023d5-e864-48b8-b6c9-5c77f5e7d839','to_id','node','a2a93625-4858-4c08-b2c2-c246abbd6806','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),81,'node','c2e023d5-e864-48b8-b6c9-5c77f5e7d839','id','rel','c2e023d5-e864-48b8-b6c9-5c77f5e7d839','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),81,'rel','2370cd39-2426-4002-abba-6af70d1c8989','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),81,'node','a2a93625-4858-4c08-b2c2-c246abbd6806','id','rel','a2a93625-4858-4c08-b2c2-c246abbd6806','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'rel','8e3cce57-dbc5-479b-ab49-45df75e49807','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'rel','013fc384-177b-4613-bf1a-a15f2d9c35a1','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id','rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','to_id','node','70709cc4-f6a5-4e8e-9f6d-07c710a460df','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'rel','013fc384-177b-4613-bf1a-a15f2d9c35a1','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'rel','013fc384-177b-4613-bf1a-a15f2d9c35a1','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'node','8c2ee112-29c9-47a8-8e0f-a94896f8be01','id','rel','8c2ee112-29c9-47a8-8e0f-a94896f8be01','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'rel','013fc384-177b-4613-bf1a-a15f2d9c35a1','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'node','8e3cce57-dbc5-479b-ab49-45df75e49807','id','rel','8e3cce57-dbc5-479b-ab49-45df75e49807','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'rel','013fc384-177b-4613-bf1a-a15f2d9c35a1','to_id','node','a53e285c-63a0-4107-83cf-be9666495371','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'rel','013fc384-177b-4613-bf1a-a15f2d9c35a1','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'rel','18177e57-bcc9-4a9b-b389-cf794ac3c727','to_id','node','354e91a9-976b-4bee-b873-1dbc53280ac1','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'rel','8c2ee112-29c9-47a8-8e0f-a94896f8be01','to_id','node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'node','354e91a9-976b-4bee-b873-1dbc53280ac1','id','rel','354e91a9-976b-4bee-b873-1dbc53280ac1','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'rel','013fc384-177b-4613-bf1a-a15f2d9c35a1','to_id','node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id','rel','e08e0f7c-85f7-44af-9afe-aeedb38260af','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'rel','013fc384-177b-4613-bf1a-a15f2d9c35a1','to_id','node','8e3cce57-dbc5-479b-ab49-45df75e49807','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'node','013fc384-177b-4613-bf1a-a15f2d9c35a1','id','rel','013fc384-177b-4613-bf1a-a15f2d9c35a1','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id','rel','18177e57-bcc9-4a9b-b389-cf794ac3c727','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'rel','013fc384-177b-4613-bf1a-a15f2d9c35a1','to_id','node','8c2ee112-29c9-47a8-8e0f-a94896f8be01','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id','rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','397d7b11-3bf4-44fb-be5e-d1d77ea07543','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),83,'node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id','rel','18177e57-bcc9-4a9b-b389-cf794ac3c727','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),83,'node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id','rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),83,'rel','6a52334b-56f3-439f-8be7-47e82dd4d323','to_id','node','013fc384-177b-4613-bf1a-a15f2d9c35a1','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),83,'rel','013fc384-177b-4613-bf1a-a15f2d9c35a1','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),83,'rel','013fc384-177b-4613-bf1a-a15f2d9c35a1','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),83,'rel','8c2ee112-29c9-47a8-8e0f-a94896f8be01','to_id','node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),83,'rel','013fc384-177b-4613-bf1a-a15f2d9c35a1','to_id','node','8c2ee112-29c9-47a8-8e0f-a94896f8be01','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),83,'node','8e3cce57-dbc5-479b-ab49-45df75e49807','id','rel','8e3cce57-dbc5-479b-ab49-45df75e49807','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),83,'node','013fc384-177b-4613-bf1a-a15f2d9c35a1','id','rel','013fc384-177b-4613-bf1a-a15f2d9c35a1','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),83,'rel','013fc384-177b-4613-bf1a-a15f2d9c35a1','to_id','node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),83,'rel','013fc384-177b-4613-bf1a-a15f2d9c35a1','to_id','node','8e3cce57-dbc5-479b-ab49-45df75e49807','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),83,'rel','18177e57-bcc9-4a9b-b389-cf794ac3c727','to_id','node','354e91a9-976b-4bee-b873-1dbc53280ac1','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),83,'node','354e91a9-976b-4bee-b873-1dbc53280ac1','id','rel','354e91a9-976b-4bee-b873-1dbc53280ac1','from_id',9,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),83,'rel','6a52334b-56f3-439f-8be7-47e82dd4d323','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),83,'node','6a52334b-56f3-439f-8be7-47e82dd4d323','id','rel','6a52334b-56f3-439f-8be7-47e82dd4d323','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),83,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','397d7b11-3bf4-44fb-be5e-d1d77ea07543','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),83,'rel','013fc384-177b-4613-bf1a-a15f2d9c35a1','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),83,'node','8c2ee112-29c9-47a8-8e0f-a94896f8be01','id','rel','8c2ee112-29c9-47a8-8e0f-a94896f8be01','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),83,'rel','6a52334b-56f3-439f-8be7-47e82dd4d323','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),83,'rel','8e3cce57-dbc5-479b-ab49-45df75e49807','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),84,'rel','fbe34e57-8e49-4f35-9546-650b4bcccc52','to_id','node','013fc384-177b-4613-bf1a-a15f2d9c35a1','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),84,'rel','013fc384-177b-4613-bf1a-a15f2d9c35a1','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),84,'node','013fc384-177b-4613-bf1a-a15f2d9c35a1','id','rel','013fc384-177b-4613-bf1a-a15f2d9c35a1','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),84,'node','fbe34e57-8e49-4f35-9546-650b4bcccc52','id','rel','fbe34e57-8e49-4f35-9546-650b4bcccc52','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),84,'rel','fbe34e57-8e49-4f35-9546-650b4bcccc52','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),84,'rel','013fc384-177b-4613-bf1a-a15f2d9c35a1','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),84,'rel','fbe34e57-8e49-4f35-9546-650b4bcccc52','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),85,'rel','8e3cce57-dbc5-479b-ab49-45df75e49807','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),85,'rel','fdf14d4e-5b31-4e94-9c45-7aa44452226e','to_id','node','8dff0edf-033f-427e-a74c-ec09b6f2af30','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),85,'rel','fdf14d4e-5b31-4e94-9c45-7aa44452226e','to_id','node','a53e285c-63a0-4107-83cf-be9666495371','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),85,'rel','fdf14d4e-5b31-4e94-9c45-7aa44452226e','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),85,'node','8e3cce57-dbc5-479b-ab49-45df75e49807','id','rel','8e3cce57-dbc5-479b-ab49-45df75e49807','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),85,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),85,'rel','8dff0edf-033f-427e-a74c-ec09b6f2af30','to_id','node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),85,'rel','fdf14d4e-5b31-4e94-9c45-7aa44452226e','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),85,'rel','fdf14d4e-5b31-4e94-9c45-7aa44452226e','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),85,'rel','fdf14d4e-5b31-4e94-9c45-7aa44452226e','to_id','node','8e3cce57-dbc5-479b-ab49-45df75e49807','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),85,'rel','8dff0edf-033f-427e-a74c-ec09b6f2af30','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),85,'node','fdf14d4e-5b31-4e94-9c45-7aa44452226e','id','rel','fdf14d4e-5b31-4e94-9c45-7aa44452226e','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),85,'rel','18177e57-bcc9-4a9b-b389-cf794ac3c727','to_id','node','354e91a9-976b-4bee-b873-1dbc53280ac1','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),85,'node','354e91a9-976b-4bee-b873-1dbc53280ac1','id','rel','354e91a9-976b-4bee-b873-1dbc53280ac1','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),85,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),85,'node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id','rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),85,'rel','fdf14d4e-5b31-4e94-9c45-7aa44452226e','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),85,'rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','to_id','node','70709cc4-f6a5-4e8e-9f6d-07c710a460df','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),85,'node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id','rel','18177e57-bcc9-4a9b-b389-cf794ac3c727','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),85,'node','8dff0edf-033f-427e-a74c-ec09b6f2af30','id','rel','8dff0edf-033f-427e-a74c-ec09b6f2af30','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),86,'node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id','rel','18177e57-bcc9-4a9b-b389-cf794ac3c727','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),86,'rel','fdf14d4e-5b31-4e94-9c45-7aa44452226e','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),86,'node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id','rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),86,'node','8dff0edf-033f-427e-a74c-ec09b6f2af30','id','rel','8dff0edf-033f-427e-a74c-ec09b6f2af30','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),86,'rel','bc1b9bfd-8c66-4779-a5d4-add478db3fa4','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),86,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),86,'node','bc1b9bfd-8c66-4779-a5d4-add478db3fa4','id','rel','bc1b9bfd-8c66-4779-a5d4-add478db3fa4','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),86,'rel','8dff0edf-033f-427e-a74c-ec09b6f2af30','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),86,'rel','fdf14d4e-5b31-4e94-9c45-7aa44452226e','to_id','node','8dff0edf-033f-427e-a74c-ec09b6f2af30','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),86,'rel','bc1b9bfd-8c66-4779-a5d4-add478db3fa4','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),86,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),86,'node','8e3cce57-dbc5-479b-ab49-45df75e49807','id','rel','8e3cce57-dbc5-479b-ab49-45df75e49807','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),86,'rel','8dff0edf-033f-427e-a74c-ec09b6f2af30','to_id','node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),86,'rel','fdf14d4e-5b31-4e94-9c45-7aa44452226e','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),86,'rel','bc1b9bfd-8c66-4779-a5d4-add478db3fa4','to_id','node','fdf14d4e-5b31-4e94-9c45-7aa44452226e','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),86,'rel','18177e57-bcc9-4a9b-b389-cf794ac3c727','to_id','node','354e91a9-976b-4bee-b873-1dbc53280ac1','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),86,'node','fdf14d4e-5b31-4e94-9c45-7aa44452226e','id','rel','fdf14d4e-5b31-4e94-9c45-7aa44452226e','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),86,'node','354e91a9-976b-4bee-b873-1dbc53280ac1','id','rel','354e91a9-976b-4bee-b873-1dbc53280ac1','from_id',9,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),86,'rel','fdf14d4e-5b31-4e94-9c45-7aa44452226e','to_id','node','8e3cce57-dbc5-479b-ab49-45df75e49807','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),86,'rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','to_id','node','70709cc4-f6a5-4e8e-9f6d-07c710a460df','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),86,'rel','8e3cce57-dbc5-479b-ab49-45df75e49807','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','ba35ad74-89eb-4802-bef8-9d8443e26fa4','to_id','node','6a88ac7e-835a-42cf-85bd-5c50257429f2','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',9,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'node','ba35ad74-89eb-4802-bef8-9d8443e26fa4','id','rel','ba35ad74-89eb-4802-bef8-9d8443e26fa4','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'node','817e8177-e175-4544-8074-f7f2f2ab8966','id','rel','817e8177-e175-4544-8074-f7f2f2ab8966','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','1e3cf9cd-9768-46b1-9208-ae3763b7cce0','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',10,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','3aca4bf0-b0e4-46f3-9370-263d88559a19','to_id','node','fe9021c6-3a6e-418f-97e4-7e305e275804','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','1e3cf9cd-9768-46b1-9208-ae3763b7cce0','to_id','node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id',10,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'node','9c5f0ddb-96d2-4012-a851-ae27b8f19809','id','rel','9c5f0ddb-96d2-4012-a851-ae27b8f19809','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'node','80c4786c-8d64-4a90-9ee6-de05537fc619','id','rel','80c4786c-8d64-4a90-9ee6-de05537fc619','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','817e8177-e175-4544-8074-f7f2f2ab8966','to_id','node','3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','1e3cf9cd-9768-46b1-9208-ae3763b7cce0','to_id','node','397d7b11-3bf4-44fb-be5e-d1d77ea07543','id',10,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','817e8177-e175-4544-8074-f7f2f2ab8966','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'node','3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2','id','rel','3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id','rel','e08e0f7c-85f7-44af-9afe-aeedb38260af','from_id',11,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'node','3aca4bf0-b0e4-46f3-9370-263d88559a19','id','rel','3aca4bf0-b0e4-46f3-9370-263d88559a19','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'node','3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2','id','rel','3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'node','1e3cf9cd-9768-46b1-9208-ae3763b7cce0','id','rel','1e3cf9cd-9768-46b1-9208-ae3763b7cce0','from_id',9,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','3aca4bf0-b0e4-46f3-9370-263d88559a19','to_id','node','af21194a-62d3-49b2-a39c-2d620c7c72e7','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'node','62a015af-7c63-4678-9f72-e117b9f4326e','id','rel','62a015af-7c63-4678-9f72-e117b9f4326e','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'node','20e7624f-9b5b-46a6-96a7-930a30d19474','id','rel','20e7624f-9b5b-46a6-96a7-930a30d19474','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','20e7624f-9b5b-46a6-96a7-930a30d19474','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','3aca4bf0-b0e4-46f3-9370-263d88559a19','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','3aca4bf0-b0e4-46f3-9370-263d88559a19','to_id','node','ba35ad74-89eb-4802-bef8-9d8443e26fa4','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','817e8177-e175-4544-8074-f7f2f2ab8966','to_id','node','9c5f0ddb-96d2-4012-a851-ae27b8f19809','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','20e7624f-9b5b-46a6-96a7-930a30d19474','to_id','node','3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','3aca4bf0-b0e4-46f3-9370-263d88559a19','to_id','node','20e7624f-9b5b-46a6-96a7-930a30d19474','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','3aca4bf0-b0e4-46f3-9370-263d88559a19','to_id','node','3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','3aca4bf0-b0e4-46f3-9370-263d88559a19','to_id','node','62a015af-7c63-4678-9f72-e117b9f4326e','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',11,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','20e7624f-9b5b-46a6-96a7-930a30d19474','to_id','node','817e8177-e175-4544-8074-f7f2f2ab8966','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','9c5f0ddb-96d2-4012-a851-ae27b8f19809','to_id','node','464ab448-df1f-40c6-8f74-d6a9d43da184','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),5,'node','022d3cf7-6c3e-4e79-88e2-c9612df1f801','id','rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),5,'node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id','rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),5,'rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','to_id','node','70709cc4-f6a5-4e8e-9f6d-07c710a460df','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),5,'rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','to_id','node','fe9021c6-3a6e-418f-97e4-7e305e275804','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),5,'rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','to_id','node','022d3cf7-6c3e-4e79-88e2-c9612df1f801','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),5,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),5,'rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','to_id','node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),5,'rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),5,'rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),5,'node','d943c4a4-239c-4b6e-aa50-0da53cf712e9','id','rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),5,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),5,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','2b287cff-a1dd-4b42-8452-42a01080025b','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),5,'rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','to_id','node','1ddc9a28-b1f5-4836-bf54-5fa401ce1305','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),6,'rel','eb632aeb-bc4f-4560-8305-4981d502746a','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),6,'rel','b0694243-9063-49b1-9083-42ae67d8c815','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),6,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),6,'node','eb632aeb-bc4f-4560-8305-4981d502746a','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),6,'node','eb632aeb-bc4f-4560-8305-4981d502746a','id','rel','eb632aeb-bc4f-4560-8305-4981d502746a','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),6,'node','b0694243-9063-49b1-9083-42ae67d8c815','id','rel','b0694243-9063-49b1-9083-42ae67d8c815','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),6,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),6,'rel','eb632aeb-bc4f-4560-8305-4981d502746a','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),6,'rel','b0694243-9063-49b1-9083-42ae67d8c815','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),6,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),6,'rel','eb632aeb-bc4f-4560-8305-4981d502746a','to_id','node','b0694243-9063-49b1-9083-42ae67d8c815','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),6,'rel','b0694243-9063-49b1-9083-42ae67d8c815','to_id','node','3046ceb1-beae-4ae4-8956-1e004fa55a32','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),6,'node','3046ceb1-beae-4ae4-8956-1e004fa55a32','id','rel','3046ceb1-beae-4ae4-8956-1e004fa55a32','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'node','237623ba-b2e2-4153-8677-fde24ffa7f06','id','rel','237623ba-b2e2-4153-8677-fde24ffa7f06','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','78d96263-a96d-4815-8060-6735c24f90c7','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','547a53c4-6222-42e4-af42-19ebb37b7e7f','to_id','node','0242b28e-7b2e-43ff-bc19-26386bbf6a09','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'node','1d5bd027-c4c8-4022-a2ee-70fc518b2861','id','rel','1d5bd027-c4c8-4022-a2ee-70fc518b2861','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','237623ba-b2e2-4153-8677-fde24ffa7f06','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','69311000-056b-49f0-9417-90d0b638bdf3','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'node','ab3bc3f2-c035-4cdc-9bf4-f113889911bd','id','rel','ab3bc3f2-c035-4cdc-9bf4-f113889911bd','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','3eca1e48-860f-42d9-a28b-0e628b6df529','to_id','node','f5414afa-bd72-4717-96c8-85f575c91638','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','42fa1d4c-4e43-4f2e-a7be-e90a06e36213','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','ab3bc3f2-c035-4cdc-9bf4-f113889911bd','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','1d5bd027-c4c8-4022-a2ee-70fc518b2861','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'node','de8be436-442b-431d-a811-1cb8c8a5e5da','id','rel','de8be436-442b-431d-a811-1cb8c8a5e5da','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','02fdcafb-a15b-4e4d-99e6-5d6dc39e7c7b','to_id','node','d279b64e-b341-4bc6-991d-87f291adbeb0','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','e44f80fd-31a3-4bef-82b4-6e7573811128','to_id','node','0242b28e-7b2e-43ff-bc19-26386bbf6a09','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','ac9a1554-f453-4f0a-93dc-2852eb44719c','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'node','f5414afa-bd72-4717-96c8-85f575c91638','id','rel','f5414afa-bd72-4717-96c8-85f575c91638','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','42fa1d4c-4e43-4f2e-a7be-e90a06e36213','to_id','node','a264bf75-34c6-4acc-a56b-a9790e440eb1','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'node','a9456a63-e94f-4be5-b0f3-04fcd62760c7','id','rel','a9456a63-e94f-4be5-b0f3-04fcd62760c7','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','f58824b8-1cd7-415d-a01c-fc9af33571ed','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','f5414afa-bd72-4717-96c8-85f575c91638','to_id','node','a264bf75-34c6-4acc-a56b-a9790e440eb1','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'node','42fa1d4c-4e43-4f2e-a7be-e90a06e36213','id','rel','42fa1d4c-4e43-4f2e-a7be-e90a06e36213','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'node','02fdcafb-a15b-4e4d-99e6-5d6dc39e7c7b','id','rel','02fdcafb-a15b-4e4d-99e6-5d6dc39e7c7b','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','a264bf75-34c6-4acc-a56b-a9790e440eb1','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','e44f80fd-31a3-4bef-82b4-6e7573811128','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','2dd28c00-886c-4134-aa4a-f5b024cc10bc','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','a9456a63-e94f-4be5-b0f3-04fcd62760c7','to_id','node','0242b28e-7b2e-43ff-bc19-26386bbf6a09','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'node','022d3cf7-6c3e-4e79-88e2-c9612df1f801','id','rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','3eca1e48-860f-42d9-a28b-0e628b6df529','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','2b287cff-a1dd-4b42-8452-42a01080025b','to_id','node','265480e3-ec90-4fd3-b7c0-b01e96e91ad0','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','2b287cff-a1dd-4b42-8452-42a01080025b','to_id','node','547a53c4-6222-42e4-af42-19ebb37b7e7f','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','fea1d58c-b5ba-4db1-b81d-65b00befcd1d','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'node','a264bf75-34c6-4acc-a56b-a9790e440eb1','id','rel','a264bf75-34c6-4acc-a56b-a9790e440eb1','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','ac9a1554-f453-4f0a-93dc-2852eb44719c','to_id','node','f5414afa-bd72-4717-96c8-85f575c91638','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','1fd668dc-0719-4ce3-8902-0ca224566032','to_id','node','b8009fe7-5921-44ca-b067-ebcef697ad9e','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','a006e6f2-3d93-40a9-959b-0f0a91cf9af0','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','848e8572-2e34-48c8-89e7-87f218431f15','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','a264bf75-34c6-4acc-a56b-a9790e440eb1','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','a006e6f2-3d93-40a9-959b-0f0a91cf9af0','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'node','1fd668dc-0719-4ce3-8902-0ca224566032','id','rel','1fd668dc-0719-4ce3-8902-0ca224566032','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'node','e44f80fd-31a3-4bef-82b4-6e7573811128','id','rel','e44f80fd-31a3-4bef-82b4-6e7573811128','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','1d5bd027-c4c8-4022-a2ee-70fc518b2861','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','ac9a1554-f453-4f0a-93dc-2852eb44719c','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','ac9a1554-f453-4f0a-93dc-2852eb44719c','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','2b287cff-a1dd-4b42-8452-42a01080025b','to_id','node','02fdcafb-a15b-4e4d-99e6-5d6dc39e7c7b','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'node','547a53c4-6222-42e4-af42-19ebb37b7e7f','id','rel','547a53c4-6222-42e4-af42-19ebb37b7e7f','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','f5168e79-eb3e-40e1-9a17-f40c5f606749','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','ab2a7d5f-dfaa-4011-85ce-e80fcb610cb1','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'node','f58824b8-1cd7-415d-a01c-fc9af33571ed','id','rel','f58824b8-1cd7-415d-a01c-fc9af33571ed','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'node','ac9a1554-f453-4f0a-93dc-2852eb44719c','id','rel','ac9a1554-f453-4f0a-93dc-2852eb44719c','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'node','a006e6f2-3d93-40a9-959b-0f0a91cf9af0','id','rel','a006e6f2-3d93-40a9-959b-0f0a91cf9af0','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'node','2b287cff-a1dd-4b42-8452-42a01080025b','id','rel','2b287cff-a1dd-4b42-8452-42a01080025b','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','3a9d498a-b1c6-4759-8239-af728dcc06bd','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','2b287cff-a1dd-4b42-8452-42a01080025b','to_id','node','a9456a63-e94f-4be5-b0f3-04fcd62760c7','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','2b287cff-a1dd-4b42-8452-42a01080025b','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','f5414afa-bd72-4717-96c8-85f575c91638','to_id','node','f58824b8-1cd7-415d-a01c-fc9af33571ed','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'node','3eca1e48-860f-42d9-a28b-0e628b6df529','id','rel','3eca1e48-860f-42d9-a28b-0e628b6df529','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','f5414afa-bd72-4717-96c8-85f575c91638','to_id','node','1fd668dc-0719-4ce3-8902-0ca224566032','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','f58824b8-1cd7-415d-a01c-fc9af33571ed','to_id','node','b8009fe7-5921-44ca-b067-ebcef697ad9e','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','a006e6f2-3d93-40a9-959b-0f0a91cf9af0','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','4c743487-421e-4140-aa50-3d1ff5511ec5','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','9fc41760-9cd7-4c69-bbec-b3f5b9a0eb35','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','265480e3-ec90-4fd3-b7c0-b01e96e91ad0','to_id','node','a264bf75-34c6-4acc-a56b-a9790e440eb1','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','de8be436-442b-431d-a811-1cb8c8a5e5da','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','ab3bc3f2-c035-4cdc-9bf4-f113889911bd','to_id','node','b8009fe7-5921-44ca-b067-ebcef697ad9e','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'node','265480e3-ec90-4fd3-b7c0-b01e96e91ad0','id','rel','265480e3-ec90-4fd3-b7c0-b01e96e91ad0','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','3eca1e48-860f-42d9-a28b-0e628b6df529','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','3eca1e48-860f-42d9-a28b-0e628b6df529','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'node','237623ba-b2e2-4153-8677-fde24ffa7f06','id','rel','237623ba-b2e2-4153-8677-fde24ffa7f06','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','78d96263-a96d-4815-8060-6735c24f90c7','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','547a53c4-6222-42e4-af42-19ebb37b7e7f','to_id','node','0242b28e-7b2e-43ff-bc19-26386bbf6a09','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'node','1d5bd027-c4c8-4022-a2ee-70fc518b2861','id','rel','1d5bd027-c4c8-4022-a2ee-70fc518b2861','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'node','58788976-5f4c-4957-8bc2-c5b61ffaba6c','id','rel','58788976-5f4c-4957-8bc2-c5b61ffaba6c','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','7fbd40a8-c0db-4269-99ff-622e7752a3f1','to_id','node','a264bf75-34c6-4acc-a56b-a9790e440eb1','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','1d5bd027-c4c8-4022-a2ee-70fc518b2861','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','3f20b3a6-0e38-49c3-a7fd-3880aaca2885','to_id','node','0242b28e-7b2e-43ff-bc19-26386bbf6a09','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','02fdcafb-a15b-4e4d-99e6-5d6dc39e7c7b','to_id','node','d279b64e-b341-4bc6-991d-87f291adbeb0','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'node','a9456a63-e94f-4be5-b0f3-04fcd62760c7','id','rel','a9456a63-e94f-4be5-b0f3-04fcd62760c7','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'node','3f20b3a6-0e38-49c3-a7fd-3880aaca2885','id','rel','3f20b3a6-0e38-49c3-a7fd-3880aaca2885','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'node','7fbd40a8-c0db-4269-99ff-622e7752a3f1','id','rel','7fbd40a8-c0db-4269-99ff-622e7752a3f1','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'node','02fdcafb-a15b-4e4d-99e6-5d6dc39e7c7b','id','rel','02fdcafb-a15b-4e4d-99e6-5d6dc39e7c7b','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','a264bf75-34c6-4acc-a56b-a9790e440eb1','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','2dd28c00-886c-4134-aa4a-f5b024cc10bc','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','a9456a63-e94f-4be5-b0f3-04fcd62760c7','to_id','node','0242b28e-7b2e-43ff-bc19-26386bbf6a09','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','2b287cff-a1dd-4b42-8452-42a01080025b','to_id','node','265480e3-ec90-4fd3-b7c0-b01e96e91ad0','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','7fbd40a8-c0db-4269-99ff-622e7752a3f1','to_id','node','58788976-5f4c-4957-8bc2-c5b61ffaba6c','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','2b287cff-a1dd-4b42-8452-42a01080025b','to_id','node','547a53c4-6222-42e4-af42-19ebb37b7e7f','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'node','a264bf75-34c6-4acc-a56b-a9790e440eb1','id','rel','a264bf75-34c6-4acc-a56b-a9790e440eb1','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','58788976-5f4c-4957-8bc2-c5b61ffaba6c','to_id','node','fea1d58c-b5ba-4db1-b81d-65b00befcd1d','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','848e8572-2e34-48c8-89e7-87f218431f15','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','7fbd40a8-c0db-4269-99ff-622e7752a3f1','to_id','node','237623ba-b2e2-4153-8677-fde24ffa7f06','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','2b287cff-a1dd-4b42-8452-42a01080025b','to_id','node','02fdcafb-a15b-4e4d-99e6-5d6dc39e7c7b','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'node','547a53c4-6222-42e4-af42-19ebb37b7e7f','id','rel','547a53c4-6222-42e4-af42-19ebb37b7e7f','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','f5168e79-eb3e-40e1-9a17-f40c5f606749','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','ab2a7d5f-dfaa-4011-85ce-e80fcb610cb1','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'node','2b287cff-a1dd-4b42-8452-42a01080025b','id','rel','2b287cff-a1dd-4b42-8452-42a01080025b','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','3a9d498a-b1c6-4759-8239-af728dcc06bd','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','7fbd40a8-c0db-4269-99ff-622e7752a3f1','to_id','node','3f20b3a6-0e38-49c3-a7fd-3880aaca2885','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','2b287cff-a1dd-4b42-8452-42a01080025b','to_id','node','a9456a63-e94f-4be5-b0f3-04fcd62760c7','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','7fbd40a8-c0db-4269-99ff-622e7752a3f1','to_id','node','ab2a7d5f-dfaa-4011-85ce-e80fcb610cb1','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','7fbd40a8-c0db-4269-99ff-622e7752a3f1','to_id','node','2b287cff-a1dd-4b42-8452-42a01080025b','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','7fbd40a8-c0db-4269-99ff-622e7752a3f1','to_id','node','1d5bd027-c4c8-4022-a2ee-70fc518b2861','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','4c743487-421e-4140-aa50-3d1ff5511ec5','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','265480e3-ec90-4fd3-b7c0-b01e96e91ad0','to_id','node','a264bf75-34c6-4acc-a56b-a9790e440eb1','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'node','265480e3-ec90-4fd3-b7c0-b01e96e91ad0','id','rel','265480e3-ec90-4fd3-b7c0-b01e96e91ad0','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'rel','9c5f0ddb-96d2-4012-a851-ae27b8f19809','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',9,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'node','817e8177-e175-4544-8074-f7f2f2ab8966','id','rel','817e8177-e175-4544-8074-f7f2f2ab8966','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'rel','1e3cf9cd-9768-46b1-9208-ae3763b7cce0','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',10,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'rel','1e3cf9cd-9768-46b1-9208-ae3763b7cce0','to_id','node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id',10,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'rel','0a7d8c62-4d0a-416d-8db9-e3045df14de4','to_id','node','f7b9666e-3e15-4340-b7a5-28ab3cef491e','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'node','9c5f0ddb-96d2-4012-a851-ae27b8f19809','id','rel','9c5f0ddb-96d2-4012-a851-ae27b8f19809','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'node','80c4786c-8d64-4a90-9ee6-de05537fc619','id','rel','80c4786c-8d64-4a90-9ee6-de05537fc619','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'rel','0a7d8c62-4d0a-416d-8db9-e3045df14de4','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'rel','817e8177-e175-4544-8074-f7f2f2ab8966','to_id','node','3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'rel','1e3cf9cd-9768-46b1-9208-ae3763b7cce0','to_id','node','397d7b11-3bf4-44fb-be5e-d1d77ea07543','id',10,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'rel','817e8177-e175-4544-8074-f7f2f2ab8966','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'rel','3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'node','3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2','id','rel','3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id','rel','e08e0f7c-85f7-44af-9afe-aeedb38260af','from_id',11,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),28,'node','a0827785-2638-4862-b9eb-d2f7b022c068','id','rel','a0827785-2638-4862-b9eb-d2f7b022c068','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),28,'rel','a0827785-2638-4862-b9eb-d2f7b022c068','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),29,'rel','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),29,'rel','ec22745e-c16a-4eb4-8bb3-791218cf6a37','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),29,'rel','ec22745e-c16a-4eb4-8bb3-791218cf6a37','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),29,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),29,'node','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','id','rel','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),29,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),29,'node','ec22745e-c16a-4eb4-8bb3-791218cf6a37','id','rel','ec22745e-c16a-4eb4-8bb3-791218cf6a37','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),29,'rel','ec22745e-c16a-4eb4-8bb3-791218cf6a37','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),30,'node','0c278c97-e689-4999-9949-14d5362aebc6','id','rel','0c278c97-e689-4999-9949-14d5362aebc6','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),30,'node','2723abd5-ecb1-4923-b45d-662f16816972','id','rel','2723abd5-ecb1-4923-b45d-662f16816972','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),30,'rel','566c3cda-1c51-4aa6-b0d3-0d96daa5bc77','to_id','node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),30,'rel','0c278c97-e689-4999-9949-14d5362aebc6','to_id','node','6ad77ac3-7bc9-40b1-99b6-d700d68313fc','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),30,'rel','0c278c97-e689-4999-9949-14d5362aebc6','to_id','node','8e789eab-97c9-4b56-bd98-5e7733357f7d','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),30,'rel','0c278c97-e689-4999-9949-14d5362aebc6','to_id','node','f7b9666e-3e15-4340-b7a5-28ab3cef491e','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),30,'rel','0c278c97-e689-4999-9949-14d5362aebc6','to_id','node','d56564ef-5b6e-4b75-92cf-1899eb2405eb','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),30,'rel','d56564ef-5b6e-4b75-92cf-1899eb2405eb','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),30,'rel','0c278c97-e689-4999-9949-14d5362aebc6','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),30,'node','d56564ef-5b6e-4b75-92cf-1899eb2405eb','id','rel','d56564ef-5b6e-4b75-92cf-1899eb2405eb','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),30,'rel','0c278c97-e689-4999-9949-14d5362aebc6','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),30,'rel','2723abd5-ecb1-4923-b45d-662f16816972','to_id','node','9319dc10-9057-40db-a01c-16bab1d3c4a2','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),30,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),30,'rel','0c278c97-e689-4999-9949-14d5362aebc6','to_id','node','566c3cda-1c51-4aa6-b0d3-0d96daa5bc77','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),30,'rel','d56564ef-5b6e-4b75-92cf-1899eb2405eb','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),30,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),30,'rel','0c278c97-e689-4999-9949-14d5362aebc6','to_id','node','2723abd5-ecb1-4923-b45d-662f16816972','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),30,'rel','0c278c97-e689-4999-9949-14d5362aebc6','to_id','node','cae6f7f4-1a8e-4bce-aed3-b524e5e15264','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),30,'node','566c3cda-1c51-4aa6-b0d3-0d96daa5bc77','id','rel','566c3cda-1c51-4aa6-b0d3-0d96daa5bc77','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),30,'rel','566c3cda-1c51-4aa6-b0d3-0d96daa5bc77','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),31,'rel','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),31,'node','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','id','rel','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),31,'node','cac77a10-ff1b-48bd-adf8-bc28d6688c09','id','rel','cac77a10-ff1b-48bd-adf8-bc28d6688c09','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),31,'rel','cac77a10-ff1b-48bd-adf8-bc28d6688c09','to_id','node','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),31,'rel','cac77a10-ff1b-48bd-adf8-bc28d6688c09','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'rel','0a7d8c62-4d0a-416d-8db9-e3045df14de4','to_id','node','20e7624f-9b5b-46a6-96a7-930a30d19474','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'rel','3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'node','0a7d8c62-4d0a-416d-8db9-e3045df14de4','id','rel','0a7d8c62-4d0a-416d-8db9-e3045df14de4','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'rel','0a7d8c62-4d0a-416d-8db9-e3045df14de4','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'node','3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2','id','rel','3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'node','1e3cf9cd-9768-46b1-9208-ae3763b7cce0','id','rel','1e3cf9cd-9768-46b1-9208-ae3763b7cce0','from_id',9,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'node','20e7624f-9b5b-46a6-96a7-930a30d19474','id','rel','20e7624f-9b5b-46a6-96a7-930a30d19474','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'rel','20e7624f-9b5b-46a6-96a7-930a30d19474','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'rel','817e8177-e175-4544-8074-f7f2f2ab8966','to_id','node','9c5f0ddb-96d2-4012-a851-ae27b8f19809','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'rel','20e7624f-9b5b-46a6-96a7-930a30d19474','to_id','node','3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',11,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'rel','20e7624f-9b5b-46a6-96a7-930a30d19474','to_id','node','817e8177-e175-4544-8074-f7f2f2ab8966','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'rel','9c5f0ddb-96d2-4012-a851-ae27b8f19809','to_id','node','464ab448-df1f-40c6-8f74-d6a9d43da184','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'rel','9c5f0ddb-96d2-4012-a851-ae27b8f19809','to_id','node','1e3cf9cd-9768-46b1-9208-ae3763b7cce0','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','9c5f0ddb-96d2-4012-a851-ae27b8f19809','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','ba35ad74-89eb-4802-bef8-9d8443e26fa4','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','3aca4bf0-b0e4-46f3-9370-263d88559a19','to_id','node','f7b9666e-3e15-4340-b7a5-28ab3cef491e','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','9c5f0ddb-96d2-4012-a851-ae27b8f19809','to_id','node','1e3cf9cd-9768-46b1-9208-ae3763b7cce0','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),10,'rel','ba35ad74-89eb-4802-bef8-9d8443e26fa4','to_id','node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),11,'node','57ec70e6-23f7-4576-9a64-353b9a2adbc3','id','rel','57ec70e6-23f7-4576-9a64-353b9a2adbc3','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),11,'rel','57ec70e6-23f7-4576-9a64-353b9a2adbc3','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),11,'rel','57ec70e6-23f7-4576-9a64-353b9a2adbc3','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),11,'rel','57ec70e6-23f7-4576-9a64-353b9a2adbc3','to_id','node','d943c4a4-239c-4b6e-aa50-0da53cf712e9','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),11,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),11,'rel','57ec70e6-23f7-4576-9a64-353b9a2adbc3','to_id','node','0242b28e-7b2e-43ff-bc19-26386bbf6a09','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),11,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),11,'rel','57ec70e6-23f7-4576-9a64-353b9a2adbc3','to_id','node','fea1d58c-b5ba-4db1-b81d-65b00befcd1d','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),11,'rel','57ec70e6-23f7-4576-9a64-353b9a2adbc3','to_id','node','d311e190-89fa-4c7b-b76e-694fb3b2f4d0','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),12,'rel','5c8db12a-8d2c-4fbe-b376-5c124548e845','to_id','node','e4ab9e50-a81f-48df-8731-9fc311a179cb','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),12,'rel','5c8db12a-8d2c-4fbe-b376-5c124548e845','to_id','node','fea1d58c-b5ba-4db1-b81d-65b00befcd1d','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),12,'node','5c8db12a-8d2c-4fbe-b376-5c124548e845','id','rel','5c8db12a-8d2c-4fbe-b376-5c124548e845','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),12,'rel','5c8db12a-8d2c-4fbe-b376-5c124548e845','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),12,'rel','5c8db12a-8d2c-4fbe-b376-5c124548e845','to_id','node','0242b28e-7b2e-43ff-bc19-26386bbf6a09','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),13,'rel','15b0e47c-a215-471a-ae09-b1e8c0dd91e9','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),13,'rel','15b0e47c-a215-471a-ae09-b1e8c0dd91e9','to_id','node','0242b28e-7b2e-43ff-bc19-26386bbf6a09','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),13,'node','22223124-80ba-4136-82c5-fc1bbf35d400','id','rel','22223124-80ba-4136-82c5-fc1bbf35d400','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),13,'rel','15b0e47c-a215-471a-ae09-b1e8c0dd91e9','to_id','node','e4ab9e50-a81f-48df-8731-9fc311a179cb','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),13,'rel','22223124-80ba-4136-82c5-fc1bbf35d400','to_id','node','67714c94-7c1c-453f-99a1-368388b4d224','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),13,'rel','15b0e47c-a215-471a-ae09-b1e8c0dd91e9','to_id','node','d311e190-89fa-4c7b-b76e-694fb3b2f4d0','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),13,'rel','15b0e47c-a215-471a-ae09-b1e8c0dd91e9','to_id','node','fea1d58c-b5ba-4db1-b81d-65b00befcd1d','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),14,'node','84c3d1fe-19b6-40ce-b0c6-448418c73613','id','rel','84c3d1fe-19b6-40ce-b0c6-448418c73613','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),14,'rel','84c3d1fe-19b6-40ce-b0c6-448418c73613','to_id','node','fea1d58c-b5ba-4db1-b81d-65b00befcd1d','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),14,'rel','84c3d1fe-19b6-40ce-b0c6-448418c73613','to_id','node','b8009fe7-5921-44ca-b067-ebcef697ad9e','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),14,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),14,'rel','84c3d1fe-19b6-40ce-b0c6-448418c73613','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),14,'rel','84c3d1fe-19b6-40ce-b0c6-448418c73613','to_id','node','f58824b8-1cd7-415d-a01c-fc9af33571ed','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),14,'node','f58824b8-1cd7-415d-a01c-fc9af33571ed','id','rel','f58824b8-1cd7-415d-a01c-fc9af33571ed','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),14,'rel','84c3d1fe-19b6-40ce-b0c6-448418c73613','to_id','node','d279b64e-b341-4bc6-991d-87f291adbeb0','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),14,'rel','84c3d1fe-19b6-40ce-b0c6-448418c73613','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),14,'rel','84c3d1fe-19b6-40ce-b0c6-448418c73613','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),14,'rel','f58824b8-1cd7-415d-a01c-fc9af33571ed','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),15,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','b3f757b3-58b6-46e8-9ad2-5da39dc0d5a1','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),15,'rel','1e3cf9cd-9768-46b1-9208-ae3763b7cce0','to_id','node','397d7b11-3bf4-44fb-be5e-d1d77ea07543','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),15,'node','b3f757b3-58b6-46e8-9ad2-5da39dc0d5a1','id','rel','b3f757b3-58b6-46e8-9ad2-5da39dc0d5a1','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),15,'rel','1e3cf9cd-9768-46b1-9208-ae3763b7cce0','to_id','node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),15,'rel','1e3cf9cd-9768-46b1-9208-ae3763b7cce0','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),15,'rel','1e3cf9cd-9768-46b1-9208-ae3763b7cce0','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),15,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),15,'node','1e3cf9cd-9768-46b1-9208-ae3763b7cce0','id','rel','1e3cf9cd-9768-46b1-9208-ae3763b7cce0','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),15,'node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id','rel','e08e0f7c-85f7-44af-9afe-aeedb38260af','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),16,'rel','2db3d798-85ff-44be-95b8-9cc77e9ec0fe','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),16,'node','779cad0c-84d0-4da9-9574-a1c1eff2ed8f','id','rel','779cad0c-84d0-4da9-9574-a1c1eff2ed8f','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),16,'node','45d60fd0-bed3-4abf-849d-204f219741be','id','rel','45d60fd0-bed3-4abf-849d-204f219741be','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),16,'node','70709cc4-f6a5-4e8e-9f6d-07c710a460df','id','rel','70709cc4-f6a5-4e8e-9f6d-07c710a460df','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),16,'rel','45d60fd0-bed3-4abf-849d-204f219741be','to_id','node','779cad0c-84d0-4da9-9574-a1c1eff2ed8f','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),16,'rel','70709cc4-f6a5-4e8e-9f6d-07c710a460df','to_id','node','9f1b1e20-0574-46cb-88e7-3d32f22619e3','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),16,'rel','45d60fd0-bed3-4abf-849d-204f219741be','to_id','node','d3d23233-a039-4e06-9250-785519900345','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),16,'node','d3d23233-a039-4e06-9250-785519900345','id','rel','d3d23233-a039-4e06-9250-785519900345','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),16,'rel','70709cc4-f6a5-4e8e-9f6d-07c710a460df','to_id','node','2db3d798-85ff-44be-95b8-9cc77e9ec0fe','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),16,'rel','2db3d798-85ff-44be-95b8-9cc77e9ec0fe','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),16,'rel','d3d23233-a039-4e06-9250-785519900345','to_id','node','60cc4c73-9e89-4471-9030-3e5480228d5e','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),16,'rel','45d60fd0-bed3-4abf-849d-204f219741be','to_id','node','70709cc4-f6a5-4e8e-9f6d-07c710a460df','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),16,'node','2db3d798-85ff-44be-95b8-9cc77e9ec0fe','id','rel','2db3d798-85ff-44be-95b8-9cc77e9ec0fe','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),16,'rel','70709cc4-f6a5-4e8e-9f6d-07c710a460df','to_id','node','c4e5bf39-0a19-4b82-a570-5dcef42192d8','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),17,'rel','2db3d798-85ff-44be-95b8-9cc77e9ec0fe','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),17,'node','779cad0c-84d0-4da9-9574-a1c1eff2ed8f','id','rel','779cad0c-84d0-4da9-9574-a1c1eff2ed8f','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),17,'node','70709cc4-f6a5-4e8e-9f6d-07c710a460df','id','rel','70709cc4-f6a5-4e8e-9f6d-07c710a460df','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),17,'rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','to_id','node','70709cc4-f6a5-4e8e-9f6d-07c710a460df','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),17,'node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id','rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),17,'node','d3d23233-a039-4e06-9250-785519900345','id','rel','d3d23233-a039-4e06-9250-785519900345','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),17,'rel','d0023997-ebca-4f13-9fab-be74f28cf23d','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),17,'rel','70709cc4-f6a5-4e8e-9f6d-07c710a460df','to_id','node','2db3d798-85ff-44be-95b8-9cc77e9ec0fe','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),17,'rel','d0023997-ebca-4f13-9fab-be74f28cf23d','from_id','node','d0023997-ebca-4f13-9fab-be74f28cf23d','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),17,'rel','2db3d798-85ff-44be-95b8-9cc77e9ec0fe','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),17,'rel','d3d23233-a039-4e06-9250-785519900345','to_id','node','60cc4c73-9e89-4471-9030-3e5480228d5e','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),17,'rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','to_id','node','779cad0c-84d0-4da9-9574-a1c1eff2ed8f','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),17,'rel','d0023997-ebca-4f13-9fab-be74f28cf23d','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),17,'node','d0023997-ebca-4f13-9fab-be74f28cf23d','id','rel','d0023997-ebca-4f13-9fab-be74f28cf23d','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),17,'node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id','rel','d0023997-ebca-4f13-9fab-be74f28cf23d','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),17,'rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','to_id','node','d3d23233-a039-4e06-9250-785519900345','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),17,'node','2db3d798-85ff-44be-95b8-9cc77e9ec0fe','id','rel','2db3d798-85ff-44be-95b8-9cc77e9ec0fe','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),17,'rel','70709cc4-f6a5-4e8e-9f6d-07c710a460df','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),17,'rel','70709cc4-f6a5-4e8e-9f6d-07c710a460df','to_id','node','c4e5bf39-0a19-4b82-a570-5dcef42192d8','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'rel','8e3cce57-dbc5-479b-ab49-45df75e49807','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'rel','c4c53bf2-0b46-4a55-b780-4617838056c7','to_id','node','221f88ff-3a93-4ae6-8fb7-d0a77ee02365','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'rel','397d7b11-3bf4-44fb-be5e-d1d77ea07543','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'node','8c2ee112-29c9-47a8-8e0f-a94896f8be01','id','rel','8c2ee112-29c9-47a8-8e0f-a94896f8be01','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'rel','e08e0f7c-85f7-44af-9afe-aeedb38260af','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'rel','c4c53bf2-0b46-4a55-b780-4617838056c7','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'node','8e3cce57-dbc5-479b-ab49-45df75e49807','id','rel','8e3cce57-dbc5-479b-ab49-45df75e49807','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'rel','c4c53bf2-0b46-4a55-b780-4617838056c7','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'rel','c4c53bf2-0b46-4a55-b780-4617838056c7','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'node','397d7b11-3bf4-44fb-be5e-d1d77ea07543','id','rel','397d7b11-3bf4-44fb-be5e-d1d77ea07543','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'rel','e08e0f7c-85f7-44af-9afe-aeedb38260af','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'rel','c4c53bf2-0b46-4a55-b780-4617838056c7','to_id','node','8e3cce57-dbc5-479b-ab49-45df75e49807','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'rel','18177e57-bcc9-4a9b-b389-cf794ac3c727','to_id','node','354e91a9-976b-4bee-b873-1dbc53280ac1','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'rel','8c2ee112-29c9-47a8-8e0f-a94896f8be01','to_id','node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'node','354e91a9-976b-4bee-b873-1dbc53280ac1','id','rel','354e91a9-976b-4bee-b873-1dbc53280ac1','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'rel','397d7b11-3bf4-44fb-be5e-d1d77ea07543','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'rel','c4c53bf2-0b46-4a55-b780-4617838056c7','to_id','node','8c2ee112-29c9-47a8-8e0f-a94896f8be01','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id','rel','e08e0f7c-85f7-44af-9afe-aeedb38260af','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'rel','c4c53bf2-0b46-4a55-b780-4617838056c7','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id','rel','18177e57-bcc9-4a9b-b389-cf794ac3c727','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id','rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'node','c4c53bf2-0b46-4a55-b780-4617838056c7','id','rel','c4c53bf2-0b46-4a55-b780-4617838056c7','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','397d7b11-3bf4-44fb-be5e-d1d77ea07543','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),19,'rel','50a9b985-42e2-4721-96d5-4ab8c7d575b0','to_id','node','e60f36f1-2cd2-44d5-a797-aaf357b9c26e','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),19,'node','e60f36f1-2cd2-44d5-a797-aaf357b9c26e','id','rel','e60f36f1-2cd2-44d5-a797-aaf357b9c26e','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),19,'node','50a9b985-42e2-4721-96d5-4ab8c7d575b0','id','rel','50a9b985-42e2-4721-96d5-4ab8c7d575b0','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),19,'rel','d332d316-c355-4ba0-98b6-4331d8cffa5c','to_id','node','e879ccd2-bc40-45cb-89a3-7829bb578936','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),19,'node','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','id','rel','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),19,'rel','d332d316-c355-4ba0-98b6-4331d8cffa5c','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),19,'rel','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),19,'rel','50a9b985-42e2-4721-96d5-4ab8c7d575b0','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),19,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),19,'rel','50a9b985-42e2-4721-96d5-4ab8c7d575b0','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),19,'rel','d332d316-c355-4ba0-98b6-4331d8cffa5c','to_id','node','9446b1d1-2671-433b-b3ab-db3ef81bd66b','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),19,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),19,'rel','50a9b985-42e2-4721-96d5-4ab8c7d575b0','to_id','node','d332d316-c355-4ba0-98b6-4331d8cffa5c','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),19,'rel','50a9b985-42e2-4721-96d5-4ab8c7d575b0','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),19,'rel','50a9b985-42e2-4721-96d5-4ab8c7d575b0','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),19,'rel','e60f36f1-2cd2-44d5-a797-aaf357b9c26e','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),19,'rel','50a9b985-42e2-4721-96d5-4ab8c7d575b0','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),19,'node','d332d316-c355-4ba0-98b6-4331d8cffa5c','id','rel','d332d316-c355-4ba0-98b6-4331d8cffa5c','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),19,'rel','50a9b985-42e2-4721-96d5-4ab8c7d575b0','to_id','node','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),20,'rel','72910d25-aad4-489e-a55c-d6ef0966ca23','to_id','node','3f068a23-951b-400a-aeba-a0f875444292','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),20,'node','e60f36f1-2cd2-44d5-a797-aaf357b9c26e','id','rel','e60f36f1-2cd2-44d5-a797-aaf357b9c26e','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),20,'node','72910d25-aad4-489e-a55c-d6ef0966ca23','id','rel','72910d25-aad4-489e-a55c-d6ef0966ca23','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),20,'rel','d332d316-c355-4ba0-98b6-4331d8cffa5c','to_id','node','e879ccd2-bc40-45cb-89a3-7829bb578936','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),20,'rel','d332d316-c355-4ba0-98b6-4331d8cffa5c','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),20,'rel','72910d25-aad4-489e-a55c-d6ef0966ca23','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),20,'rel','72910d25-aad4-489e-a55c-d6ef0966ca23','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),20,'rel','72910d25-aad4-489e-a55c-d6ef0966ca23','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),20,'rel','72910d25-aad4-489e-a55c-d6ef0966ca23','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),20,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),20,'rel','72910d25-aad4-489e-a55c-d6ef0966ca23','to_id','node','d332d316-c355-4ba0-98b6-4331d8cffa5c','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),20,'rel','d332d316-c355-4ba0-98b6-4331d8cffa5c','to_id','node','9446b1d1-2671-433b-b3ab-db3ef81bd66b','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),20,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),20,'rel','72910d25-aad4-489e-a55c-d6ef0966ca23','to_id','node','e60f36f1-2cd2-44d5-a797-aaf357b9c26e','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),20,'rel','72910d25-aad4-489e-a55c-d6ef0966ca23','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),20,'rel','72910d25-aad4-489e-a55c-d6ef0966ca23','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),20,'rel','e60f36f1-2cd2-44d5-a797-aaf357b9c26e','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),20,'node','d332d316-c355-4ba0-98b6-4331d8cffa5c','id','rel','d332d316-c355-4ba0-98b6-4331d8cffa5c','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),21,'node','19a88372-0f65-4f39-8100-64cbaaefd006','id','rel','19a88372-0f65-4f39-8100-64cbaaefd006','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),21,'node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id','rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),21,'rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','to_id','node','70709cc4-f6a5-4e8e-9f6d-07c710a460df','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),21,'rel','19a88372-0f65-4f39-8100-64cbaaefd006','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),21,'rel','19a88372-0f65-4f39-8100-64cbaaefd006','to_id','node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),21,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),21,'rel','19a88372-0f65-4f39-8100-64cbaaefd006','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),21,'rel','19a88372-0f65-4f39-8100-64cbaaefd006','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),21,'rel','19a88372-0f65-4f39-8100-64cbaaefd006','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),21,'rel','19a88372-0f65-4f39-8100-64cbaaefd006','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),22,'rel','70709cc4-f6a5-4e8e-9f6d-07c710a460df','to_id','node','c4e5bf39-0a19-4b82-a570-5dcef42192d8','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),22,'node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id','rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),22,'rel','d0023997-ebca-4f13-9fab-be74f28cf23d','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),22,'rel','70709cc4-f6a5-4e8e-9f6d-07c710a460df','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),22,'rel','d0023997-ebca-4f13-9fab-be74f28cf23d','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),22,'rel','2db3d798-85ff-44be-95b8-9cc77e9ec0fe','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),22,'rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','to_id','node','70709cc4-f6a5-4e8e-9f6d-07c710a460df','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),22,'rel','d0023997-ebca-4f13-9fab-be74f28cf23d','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),22,'node','2db3d798-85ff-44be-95b8-9cc77e9ec0fe','id','rel','2db3d798-85ff-44be-95b8-9cc77e9ec0fe','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),22,'rel','70709cc4-f6a5-4e8e-9f6d-07c710a460df','to_id','node','2db3d798-85ff-44be-95b8-9cc77e9ec0fe','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),22,'node','d0023997-ebca-4f13-9fab-be74f28cf23d','id','rel','d0023997-ebca-4f13-9fab-be74f28cf23d','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),22,'rel','2db3d798-85ff-44be-95b8-9cc77e9ec0fe','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),22,'rel','d0023997-ebca-4f13-9fab-be74f28cf23d','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),22,'rel','d0023997-ebca-4f13-9fab-be74f28cf23d','to_id','node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),22,'node','70709cc4-f6a5-4e8e-9f6d-07c710a460df','id','rel','70709cc4-f6a5-4e8e-9f6d-07c710a460df','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),23,'rel','db985f58-084c-4676-b8ca-854818267af5','to_id','node','773d98ec-ea07-4889-817f-834951d19283','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),23,'rel','db985f58-084c-4676-b8ca-854818267af5','to_id','node','2e38170d-679c-4453-9304-b482530a6c4d','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),23,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),23,'node','db985f58-084c-4676-b8ca-854818267af5','id','rel','db985f58-084c-4676-b8ca-854818267af5','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),23,'rel','db985f58-084c-4676-b8ca-854818267af5','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),24,'rel','2db3d798-85ff-44be-95b8-9cc77e9ec0fe','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),24,'node','779cad0c-84d0-4da9-9574-a1c1eff2ed8f','id','rel','779cad0c-84d0-4da9-9574-a1c1eff2ed8f','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),24,'node','70709cc4-f6a5-4e8e-9f6d-07c710a460df','id','rel','70709cc4-f6a5-4e8e-9f6d-07c710a460df','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),24,'rel','b2899d4f-7ba5-4602-9a84-bfca9b1ad310','to_id','node','70709cc4-f6a5-4e8e-9f6d-07c710a460df','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),24,'rel','70709cc4-f6a5-4e8e-9f6d-07c710a460df','to_id','node','2db3d798-85ff-44be-95b8-9cc77e9ec0fe','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),24,'rel','2db3d798-85ff-44be-95b8-9cc77e9ec0fe','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),24,'rel','b2899d4f-7ba5-4602-9a84-bfca9b1ad310','to_id','node','779cad0c-84d0-4da9-9574-a1c1eff2ed8f','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),24,'node','2db3d798-85ff-44be-95b8-9cc77e9ec0fe','id','rel','2db3d798-85ff-44be-95b8-9cc77e9ec0fe','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),24,'rel','70709cc4-f6a5-4e8e-9f6d-07c710a460df','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),24,'node','b2899d4f-7ba5-4602-9a84-bfca9b1ad310','id','rel','b2899d4f-7ba5-4602-9a84-bfca9b1ad310','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),24,'rel','70709cc4-f6a5-4e8e-9f6d-07c710a460df','to_id','node','c4e5bf39-0a19-4b82-a570-5dcef42192d8','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),25,'node','22223124-80ba-4136-82c5-fc1bbf35d400','id','rel','22223124-80ba-4136-82c5-fc1bbf35d400','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),25,'rel','22223124-80ba-4136-82c5-fc1bbf35d400','to_id','node','67714c94-7c1c-453f-99a1-368388b4d224','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),26,'rel','18177e57-bcc9-4a9b-b389-cf794ac3c727','to_id','node','354e91a9-976b-4bee-b873-1dbc53280ac1','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),26,'rel','18177e57-bcc9-4a9b-b389-cf794ac3c727','to_id','node','b6501577-cdd3-42a3-a7a1-1ac89184e9b4','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),26,'rel','18177e57-bcc9-4a9b-b389-cf794ac3c727','to_id','node','67714c94-7c1c-453f-99a1-368388b4d224','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),26,'node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id','rel','18177e57-bcc9-4a9b-b389-cf794ac3c727','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),26,'node','354e91a9-976b-4bee-b873-1dbc53280ac1','id','rel','354e91a9-976b-4bee-b873-1dbc53280ac1','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),27,'rel','1b7ce077-a6a3-4998-b0cb-a31cf583ad1c','to_id','node','50a9b985-42e2-4721-96d5-4ab8c7d575b0','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),27,'rel','72910d25-aad4-489e-a55c-d6ef0966ca23','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),27,'rel','e60f36f1-2cd2-44d5-a797-aaf357b9c26e','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),27,'rel','50a9b985-42e2-4721-96d5-4ab8c7d575b0','to_id','node','e60f36f1-2cd2-44d5-a797-aaf357b9c26e','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),27,'node','72910d25-aad4-489e-a55c-d6ef0966ca23','id','rel','72910d25-aad4-489e-a55c-d6ef0966ca23','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),27,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),27,'rel','1b7ce077-a6a3-4998-b0cb-a31cf583ad1c','to_id','node','c4c53bf2-0b46-4a55-b780-4617838056c7','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),27,'node','50a9b985-42e2-4721-96d5-4ab8c7d575b0','id','rel','50a9b985-42e2-4721-96d5-4ab8c7d575b0','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),27,'rel','1b7ce077-a6a3-4998-b0cb-a31cf583ad1c','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),27,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),27,'rel','72910d25-aad4-489e-a55c-d6ef0966ca23','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),27,'rel','50a9b985-42e2-4721-96d5-4ab8c7d575b0','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),27,'rel','1b7ce077-a6a3-4998-b0cb-a31cf583ad1c','to_id','node','72910d25-aad4-489e-a55c-d6ef0966ca23','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),27,'node','e60f36f1-2cd2-44d5-a797-aaf357b9c26e','id','rel','e60f36f1-2cd2-44d5-a797-aaf357b9c26e','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),27,'node','1b7ce077-a6a3-4998-b0cb-a31cf583ad1c','id','rel','1b7ce077-a6a3-4998-b0cb-a31cf583ad1c','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),27,'rel','1b7ce077-a6a3-4998-b0cb-a31cf583ad1c','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),27,'rel','72910d25-aad4-489e-a55c-d6ef0966ca23','to_id','node','e60f36f1-2cd2-44d5-a797-aaf357b9c26e','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),27,'rel','1b7ce077-a6a3-4998-b0cb-a31cf583ad1c','to_id','node','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),28,'rel','a0827785-2638-4862-b9eb-d2f7b022c068','to_id','node','72e113cd-2260-484c-90f6-724011fd98cb','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),28,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),28,'rel','a0827785-2638-4862-b9eb-d2f7b022c068','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),87,'node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id','rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),87,'node','8dff0edf-033f-427e-a74c-ec09b6f2af30','id','rel','8dff0edf-033f-427e-a74c-ec09b6f2af30','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),87,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),87,'rel','19528aa3-caee-4d65-a3e0-1b98fc79bc1f','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),87,'rel','19528aa3-caee-4d65-a3e0-1b98fc79bc1f','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),87,'rel','8dff0edf-033f-427e-a74c-ec09b6f2af30','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),87,'rel','fdf14d4e-5b31-4e94-9c45-7aa44452226e','to_id','node','8dff0edf-033f-427e-a74c-ec09b6f2af30','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),87,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),87,'rel','8dff0edf-033f-427e-a74c-ec09b6f2af30','to_id','node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),87,'rel','fdf14d4e-5b31-4e94-9c45-7aa44452226e','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),87,'rel','19528aa3-caee-4d65-a3e0-1b98fc79bc1f','to_id','node','fdf14d4e-5b31-4e94-9c45-7aa44452226e','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),87,'node','fdf14d4e-5b31-4e94-9c45-7aa44452226e','id','rel','fdf14d4e-5b31-4e94-9c45-7aa44452226e','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),87,'rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','to_id','node','70709cc4-f6a5-4e8e-9f6d-07c710a460df','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','57ec70e6-23f7-4576-9a64-353b9a2adbc3','from_id','node','57ec70e6-23f7-4576-9a64-353b9a2adbc3','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','19a88372-0f65-4f39-8100-64cbaaefd006','from_id','node','19a88372-0f65-4f39-8100-64cbaaefd006','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','a264bf75-34c6-4acc-a56b-a9790e440eb1','from_id','node','a264bf75-34c6-4acc-a56b-a9790e440eb1','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','02fdcafb-a15b-4e4d-99e6-5d6dc39e7c7b','to_id','node','d279b64e-b341-4bc6-991d-87f291adbeb0','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','a006e6f2-3d93-40a9-959b-0f0a91cf9af0','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','e44f80fd-31a3-4bef-82b4-6e7573811128','to_id','node','15b0e47c-a215-471a-ae09-b1e8c0dd91e9','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','84c3d1fe-19b6-40ce-b0c6-448418c73613','from_id','node','84c3d1fe-19b6-40ce-b0c6-448418c73613','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','3217e8c2-4ab5-4d09-89e0-8367bd0de08f','id','rel','3217e8c2-4ab5-4d09-89e0-8367bd0de08f','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','022d3cf7-6c3e-4e79-88e2-c9612df1f801','id','rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','from_id','node','1b80db34-9947-44a6-8a7d-2ec385848873','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','f6f72a69-61c4-40ef-a7bc-65e0107302f6','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','f5414afa-bd72-4717-96c8-85f575c91638','to_id','node','a264bf75-34c6-4acc-a56b-a9790e440eb1','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','1fd668dc-0719-4ce3-8902-0ca224566032','id','rel','1fd668dc-0719-4ce3-8902-0ca224566032','from_id',9,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','50a9b985-42e2-4721-96d5-4ab8c7d575b0','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','84c3d1fe-19b6-40ce-b0c6-448418c73613','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','1b7ce077-a6a3-4998-b0cb-a31cf583ad1c','to_id','node','c4c53bf2-0b46-4a55-b780-4617838056c7','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','5d3272d2-a131-4c89-a0b5-de0be03ae5ca','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','f6f72a69-61c4-40ef-a7bc-65e0107302f6','id','rel','1acf6821-78a5-4a03-9726-7ab541a0066a','to_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','4c743487-421e-4140-aa50-3d1ff5511ec5','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','1b7ce077-a6a3-4998-b0cb-a31cf583ad1c','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','84c3d1fe-19b6-40ce-b0c6-448418c73613','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','57ec70e6-23f7-4576-9a64-353b9a2adbc3','to_id','node','0242b28e-7b2e-43ff-bc19-26386bbf6a09','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','72910d25-aad4-489e-a55c-d6ef0966ca23','to_id','node','d332d316-c355-4ba0-98b6-4331d8cffa5c','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','547a53c4-6222-42e4-af42-19ebb37b7e7f','to_id','node','0242b28e-7b2e-43ff-bc19-26386bbf6a09','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','d332d316-c355-4ba0-98b6-4331d8cffa5c','id','rel','d332d316-c355-4ba0-98b6-4331d8cffa5c','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','50a9b985-42e2-4721-96d5-4ab8c7d575b0','from_id','node','50a9b985-42e2-4721-96d5-4ab8c7d575b0','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','id','rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','265480e3-ec90-4fd3-b7c0-b01e96e91ad0','id','rel','265480e3-ec90-4fd3-b7c0-b01e96e91ad0','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','abbd736e-4d87-4525-8a3e-e7f66d436568','id','rel','abbd736e-4d87-4525-8a3e-e7f66d436568','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','e44f80fd-31a3-4bef-82b4-6e7573811128','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','72910d25-aad4-489e-a55c-d6ef0966ca23','from_id','node','72910d25-aad4-489e-a55c-d6ef0966ca23','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','a006e6f2-3d93-40a9-959b-0f0a91cf9af0','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','750a27dd-4e98-46b3-bca1-bc50ddbe6647','id','rel','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','to_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','ab3bc3f2-c035-4cdc-9bf4-f113889911bd','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','a264bf75-34c6-4acc-a56b-a9790e440eb1','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','b0694243-9063-49b1-9083-42ae67d8c815','from_id','node','b0694243-9063-49b1-9083-42ae67d8c815','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','bcaacdf8-c90d-431b-a115-5c1afaf06c08','to_id','node','fea1d58c-b5ba-4db1-b81d-65b00befcd1d','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','f6f72a69-61c4-40ef-a7bc-65e0107302f6','from_id','node','f6f72a69-61c4-40ef-a7bc-65e0107302f6','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','a264bf75-34c6-4acc-a56b-a9790e440eb1','id','rel','a264bf75-34c6-4acc-a56b-a9790e440eb1','from_id',9,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','50a9b985-42e2-4721-96d5-4ab8c7d575b0','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','750a27dd-4e98-46b3-bca1-bc50ddbe6647','from_id','node','750a27dd-4e98-46b3-bca1-bc50ddbe6647','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','fea1d58c-b5ba-4db1-b81d-65b00befcd1d','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','abbd736e-4d87-4525-8a3e-e7f66d436568','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','3217e8c2-4ab5-4d09-89e0-8367bd0de08f','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','19a88372-0f65-4f39-8100-64cbaaefd006','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','547a53c4-6222-42e4-af42-19ebb37b7e7f','id','rel','547a53c4-6222-42e4-af42-19ebb37b7e7f','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','84c3d1fe-19b6-40ce-b0c6-448418c73613','to_id','node','fea1d58c-b5ba-4db1-b81d-65b00befcd1d','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','2b287cff-a1dd-4b42-8452-42a01080025b','to_id','node','a9456a63-e94f-4be5-b0f3-04fcd62760c7','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','a006e6f2-3d93-40a9-959b-0f0a91cf9af0','to_id','node','a264bf75-34c6-4acc-a56b-a9790e440eb1','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','84c3d1fe-19b6-40ce-b0c6-448418c73613','id','rel','84c3d1fe-19b6-40ce-b0c6-448418c73613','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','to_id','node','fe9021c6-3a6e-418f-97e4-7e305e275804','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','e44f80fd-31a3-4bef-82b4-6e7573811128','id','rel','e44f80fd-31a3-4bef-82b4-6e7573811128','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','from_id','node','022d3cf7-6c3e-4e79-88e2-c9612df1f801','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','1b80db34-9947-44a6-8a7d-2ec385848873','id','rel','1b80db34-9947-44a6-8a7d-2ec385848873','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','3a9d498a-b1c6-4759-8239-af728dcc06bd','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','d56564ef-5b6e-4b75-92cf-1899eb2405eb','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','57ec70e6-23f7-4576-9a64-353b9a2adbc3','id','rel','57ec70e6-23f7-4576-9a64-353b9a2adbc3','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','ab2a7d5f-dfaa-4011-85ce-e80fcb610cb1','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','750a27dd-4e98-46b3-bca1-bc50ddbe6647','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','19a88372-0f65-4f39-8100-64cbaaefd006','id','rel','19a88372-0f65-4f39-8100-64cbaaefd006','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','19a88372-0f65-4f39-8100-64cbaaefd006','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','2b287cff-a1dd-4b42-8452-42a01080025b','to_id','node','02fdcafb-a15b-4e4d-99e6-5d6dc39e7c7b','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','0c278c97-e689-4999-9949-14d5362aebc6','from_id','node','0c278c97-e689-4999-9949-14d5362aebc6','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','ec22745e-c16a-4eb4-8bb3-791218cf6a37','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','1b7ce077-a6a3-4998-b0cb-a31cf583ad1c','to_id','node','50a9b985-42e2-4721-96d5-4ab8c7d575b0','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','f5168e79-eb3e-40e1-9a17-f40c5f606749','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','5d3272d2-a131-4c89-a0b5-de0be03ae5ca','id','rel','0f516482-6869-4115-b458-c4eeed1e6a50','to_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','2b10b5be-6492-46a1-a158-d2aa93218681','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','a9456a63-e94f-4be5-b0f3-04fcd62760c7','to_id','node','0242b28e-7b2e-43ff-bc19-26386bbf6a09','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','3eca1e48-860f-42d9-a28b-0e628b6df529','id','rel','3eca1e48-860f-42d9-a28b-0e628b6df529','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','from_id','node','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','b0694243-9063-49b1-9083-42ae67d8c815','id','rel','b0694243-9063-49b1-9083-42ae67d8c815','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','d56564ef-5b6e-4b75-92cf-1899eb2405eb','id','rel','d56564ef-5b6e-4b75-92cf-1899eb2405eb','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','cb672aba-4318-47d3-814c-815930e2da76','id','rel','cb672aba-4318-47d3-814c-815930e2da76','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','84c3d1fe-19b6-40ce-b0c6-448418c73613','to_id','node','8636a887-c4f5-406e-b019-2d5825bfafba','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','232b1704-f28e-424c-960a-a83e9e228ce0','id','rel','232b1704-f28e-424c-960a-a83e9e228ce0','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','d332d316-c355-4ba0-98b6-4331d8cffa5c','to_id','node','9446b1d1-2671-433b-b3ab-db3ef81bd66b','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','237623ba-b2e2-4153-8677-fde24ffa7f06','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','72910d25-aad4-489e-a55c-d6ef0966ca23','id','rel','72910d25-aad4-489e-a55c-d6ef0966ca23','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','a0827785-2638-4862-b9eb-d2f7b022c068','to_id','node','72e113cd-2260-484c-90f6-724011fd98cb','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','566c3cda-1c51-4aa6-b0d3-0d96daa5bc77','id','rel','0c278c97-e689-4999-9949-14d5362aebc6','to_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','69311000-056b-49f0-9417-90d0b638bdf3','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','a006e6f2-3d93-40a9-959b-0f0a91cf9af0','id','rel','a006e6f2-3d93-40a9-959b-0f0a91cf9af0','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','b3f757b3-58b6-46e8-9ad2-5da39dc0d5a1','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','ac9a1554-f453-4f0a-93dc-2852eb44719c','id','rel','ac9a1554-f453-4f0a-93dc-2852eb44719c','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','42fa1d4c-4e43-4f2e-a7be-e90a06e36213','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','2b287cff-a1dd-4b42-8452-42a01080025b','id','rel','2b287cff-a1dd-4b42-8452-42a01080025b','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','50a9b985-42e2-4721-96d5-4ab8c7d575b0','id','rel','50a9b985-42e2-4721-96d5-4ab8c7d575b0','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','1acf6821-78a5-4a03-9726-7ab541a0066a','from_id','node','1acf6821-78a5-4a03-9726-7ab541a0066a','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','232b1704-f28e-424c-960a-a83e9e228ce0','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','a264bf75-34c6-4acc-a56b-a9790e440eb1','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',10,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','72910d25-aad4-489e-a55c-d6ef0966ca23','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','id','rel','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','f6fd4eb7-3eb5-4b2a-88af-2b5dbb8bffe7','from_id','node','f6fd4eb7-3eb5-4b2a-88af-2b5dbb8bffe7','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','b0694243-9063-49b1-9083-42ae67d8c815','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','id','rel','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','9fc41760-9cd7-4c69-bbec-b3f5b9a0eb35','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','266f85da-4aa7-4b8d-8455-a92b2398861f','id','rel','266f85da-4aa7-4b8d-8455-a92b2398861f','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','0c278c97-e689-4999-9949-14d5362aebc6','id','rel','0c278c97-e689-4999-9949-14d5362aebc6','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','de8be436-442b-431d-a811-1cb8c8a5e5da','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','1b7ce077-a6a3-4998-b0cb-a31cf583ad1c','id','rel','1b7ce077-a6a3-4998-b0cb-a31cf583ad1c','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','50a9b985-42e2-4721-96d5-4ab8c7d575b0','to_id','node','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','848e8572-2e34-48c8-89e7-87f218431f15','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','02fdcafb-a15b-4e4d-99e6-5d6dc39e7c7b','id','rel','02fdcafb-a15b-4e4d-99e6-5d6dc39e7c7b','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','1b7ce077-a6a3-4998-b0cb-a31cf583ad1c','to_id','node','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','3eca1e48-860f-42d9-a28b-0e628b6df529','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','bcaacdf8-c90d-431b-a115-5c1afaf06c08','from_id','node','bcaacdf8-c90d-431b-a115-5c1afaf06c08','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','42fa1d4c-4e43-4f2e-a7be-e90a06e36213','id','rel','42fa1d4c-4e43-4f2e-a7be-e90a06e36213','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','to_id','node','1ddc9a28-b1f5-4836-bf54-5fa401ce1305','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','f5414afa-bd72-4717-96c8-85f575c91638','to_id','node','f58824b8-1cd7-415d-a01c-fc9af33571ed','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','ac9a1554-f453-4f0a-93dc-2852eb44719c','to_id','node','f5414afa-bd72-4717-96c8-85f575c91638','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','b3f757b3-58b6-46e8-9ad2-5da39dc0d5a1','id','rel','b3f757b3-58b6-46e8-9ad2-5da39dc0d5a1','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','2b287cff-a1dd-4b42-8452-42a01080025b','to_id','node','547a53c4-6222-42e4-af42-19ebb37b7e7f','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','3217e8c2-4ab5-4d09-89e0-8367bd0de08f','to_id','node','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','72910d25-aad4-489e-a55c-d6ef0966ca23','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','3eca1e48-860f-42d9-a28b-0e628b6df529','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','d332d316-c355-4ba0-98b6-4331d8cffa5c','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','2b287cff-a1dd-4b42-8452-42a01080025b','to_id','node','265480e3-ec90-4fd3-b7c0-b01e96e91ad0','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','b0694243-9063-49b1-9083-42ae67d8c815','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','2dd28c00-886c-4134-aa4a-f5b024cc10bc','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','cb672aba-4318-47d3-814c-815930e2da76','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','566c3cda-1c51-4aa6-b0d3-0d96daa5bc77','from_id','node','566c3cda-1c51-4aa6-b0d3-0d96daa5bc77','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','0c278c97-e689-4999-9949-14d5362aebc6','to_id','node','d56564ef-5b6e-4b75-92cf-1899eb2405eb','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','a0827785-2638-4862-b9eb-d2f7b022c068','id','rel','a0827785-2638-4862-b9eb-d2f7b022c068','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','from_id','node','d943c4a4-239c-4b6e-aa50-0da53cf712e9','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','266f85da-4aa7-4b8d-8455-a92b2398861f','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','f6fd4eb7-3eb5-4b2a-88af-2b5dbb8bffe7','to_id','node','79886cbe-4cbc-4db8-9287-a6483b8bd3a6','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','3217e8c2-4ab5-4d09-89e0-8367bd0de08f','to_id','node','cb672aba-4318-47d3-814c-815930e2da76','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','84c3d1fe-19b6-40ce-b0c6-448418c73613','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','57ec70e6-23f7-4576-9a64-353b9a2adbc3','to_id','node','fea1d58c-b5ba-4db1-b81d-65b00befcd1d','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','1d5bd027-c4c8-4022-a2ee-70fc518b2861','id','rel','1d5bd027-c4c8-4022-a2ee-70fc518b2861','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','bcaacdf8-c90d-431b-a115-5c1afaf06c08','id','rel','bcaacdf8-c90d-431b-a115-5c1afaf06c08','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','f58824b8-1cd7-415d-a01c-fc9af33571ed','id','rel','f58824b8-1cd7-415d-a01c-fc9af33571ed','from_id',9,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','1b7ce077-a6a3-4998-b0cb-a31cf583ad1c','from_id','node','1b7ce077-a6a3-4998-b0cb-a31cf583ad1c','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','ab3bc3f2-c035-4cdc-9bf4-f113889911bd','id','rel','ab3bc3f2-c035-4cdc-9bf4-f113889911bd','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','42fa1d4c-4e43-4f2e-a7be-e90a06e36213','to_id','node','a264bf75-34c6-4acc-a56b-a9790e440eb1','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','0f516482-6869-4115-b458-c4eeed1e6a50','from_id','node','0f516482-6869-4115-b458-c4eeed1e6a50','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','ec22745e-c16a-4eb4-8bb3-791218cf6a37','from_id','node','ec22745e-c16a-4eb4-8bb3-791218cf6a37','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','265480e3-ec90-4fd3-b7c0-b01e96e91ad0','to_id','node','a264bf75-34c6-4acc-a56b-a9790e440eb1','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','de8be436-442b-431d-a811-1cb8c8a5e5da','id','rel','de8be436-442b-431d-a811-1cb8c8a5e5da','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','3aa59886-4ff3-47d8-8821-4838566ba8ab','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','266f85da-4aa7-4b8d-8455-a92b2398861f','to_id','node','edd94aa2-433f-4eea-a80b-3082083092f6','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','bcaacdf8-c90d-431b-a115-5c1afaf06c08','to_id','node','3e8360f5-bbb5-483d-b9f5-96c4910be8d6','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','84c3d1fe-19b6-40ce-b0c6-448418c73613','to_id','node','d279b64e-b341-4bc6-991d-87f291adbeb0','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','a0827785-2638-4862-b9eb-d2f7b022c068','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','237623ba-b2e2-4153-8677-fde24ffa7f06','id','rel','237623ba-b2e2-4153-8677-fde24ffa7f06','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','2b287cff-a1dd-4b42-8452-42a01080025b','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','1b7ce077-a6a3-4998-b0cb-a31cf583ad1c','to_id','node','72910d25-aad4-489e-a55c-d6ef0966ca23','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','1b7ce077-a6a3-4998-b0cb-a31cf583ad1c','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','e44f80fd-31a3-4bef-82b4-6e7573811128','to_id','node','0242b28e-7b2e-43ff-bc19-26386bbf6a09','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','f6fd4eb7-3eb5-4b2a-88af-2b5dbb8bffe7','id','rel','f6fd4eb7-3eb5-4b2a-88af-2b5dbb8bffe7','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','f6fd4eb7-3eb5-4b2a-88af-2b5dbb8bffe7','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','3eca1e48-860f-42d9-a28b-0e628b6df529','to_id','node','f5414afa-bd72-4717-96c8-85f575c91638','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','1d5bd027-c4c8-4022-a2ee-70fc518b2861','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','b0694243-9063-49b1-9083-42ae67d8c815','to_id','node','3046ceb1-beae-4ae4-8956-1e004fa55a32','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','1d5bd027-c4c8-4022-a2ee-70fc518b2861','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','a9456a63-e94f-4be5-b0f3-04fcd62760c7','id','rel','a9456a63-e94f-4be5-b0f3-04fcd62760c7','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','d56564ef-5b6e-4b75-92cf-1899eb2405eb','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','57ec70e6-23f7-4576-9a64-353b9a2adbc3','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','f5414afa-bd72-4717-96c8-85f575c91638','to_id','node','1fd668dc-0719-4ce3-8902-0ca224566032','id',8,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','91d5f27c-a87e-4038-a1d8-da53a2588b5e','id','rel','91d5f27c-a87e-4038-a1d8-da53a2588b5e','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','a0827785-2638-4862-b9eb-d2f7b022c068','from_id','node','a0827785-2638-4862-b9eb-d2f7b022c068','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','f5414afa-bd72-4717-96c8-85f575c91638','id','rel','f5414afa-bd72-4717-96c8-85f575c91638','from_id',7,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','d943c4a4-239c-4b6e-aa50-0da53cf712e9','id','rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','f58824b8-1cd7-415d-a01c-fc9af33571ed','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',10,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','19a88372-0f65-4f39-8100-64cbaaefd006','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','3046ceb1-beae-4ae4-8956-1e004fa55a32','id','rel','3046ceb1-beae-4ae4-8956-1e004fa55a32','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','ac9a1554-f453-4f0a-93dc-2852eb44719c','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','abbd736e-4d87-4525-8a3e-e7f66d436568','to_id','node','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','bcaacdf8-c90d-431b-a115-5c1afaf06c08','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','237623ba-b2e2-4153-8677-fde24ffa7f06','to_id','node','78d96263-a96d-4815-8060-6735c24f90c7','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','566c3cda-1c51-4aa6-b0d3-0d96daa5bc77','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','to_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','19a88372-0f65-4f39-8100-64cbaaefd006','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),90,'rel','30549635-03b8-4f78-9d09-178c8184f7ff','to_id','node','d14be1bc-9c56-4570-9fa1-f9eab6cb1bf3','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),90,'rel','a264bf75-34c6-4acc-a56b-a9790e440eb1','to_id','node','30549635-03b8-4f78-9d09-178c8184f7ff','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),90,'node','d14be1bc-9c56-4570-9fa1-f9eab6cb1bf3','id','rel','d14be1bc-9c56-4570-9fa1-f9eab6cb1bf3','from_id',5,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),90,'rel','a264bf75-34c6-4acc-a56b-a9790e440eb1','to_id','node','9f1b1e20-0574-46cb-88e7-3d32f22619e3','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),90,'node','30549635-03b8-4f78-9d09-178c8184f7ff','id','rel','30549635-03b8-4f78-9d09-178c8184f7ff','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),90,'rel','30549635-03b8-4f78-9d09-178c8184f7ff','to_id','node','d311e190-89fa-4c7b-b76e-694fb3b2f4d0','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),90,'rel','30549635-03b8-4f78-9d09-178c8184f7ff','to_id','node','fea1d58c-b5ba-4db1-b81d-65b00befcd1d','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),90,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),90,'rel','30549635-03b8-4f78-9d09-178c8184f7ff','to_id','node','0242b28e-7b2e-43ff-bc19-26386bbf6a09','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),90,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),90,'rel','d14be1bc-9c56-4570-9fa1-f9eab6cb1bf3','to_id','node','a5a48f7e-a92e-49de-b0dd-aafdfd9ea7c3','id',6,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),90,'rel','a264bf75-34c6-4acc-a56b-a9790e440eb1','to_id','node','ab2a7d5f-dfaa-4011-85ce-e80fcb610cb1','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),90,'rel','a264bf75-34c6-4acc-a56b-a9790e440eb1','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),90,'rel','a264bf75-34c6-4acc-a56b-a9790e440eb1','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),91,'rel','ab3bc3f2-c035-4cdc-9bf4-f113889911bd','to_id','node','fea1d58c-b5ba-4db1-b81d-65b00befcd1d','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),91,'node','ab3bc3f2-c035-4cdc-9bf4-f113889911bd','id','rel','ab3bc3f2-c035-4cdc-9bf4-f113889911bd','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),91,'rel','ab3bc3f2-c035-4cdc-9bf4-f113889911bd','to_id','node','0242b28e-7b2e-43ff-bc19-26386bbf6a09','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),13,'node','15b0e47c-a215-471a-ae09-b1e8c0dd91e9','id','rel','15b0e47c-a215-471a-ae09-b1e8c0dd91e9','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','e44f80fd-31a3-4bef-82b4-6e7573811128','to_id','node','15b0e47c-a215-471a-ae09-b1e8c0dd91e9','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),92,'rel','f6fd4eb7-3eb5-4b2a-88af-2b5dbb8bffe7','to_id','node','07a7f96d-efd1-48f1-8000-7d695c372204','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),92,'rel','f6fd4eb7-3eb5-4b2a-88af-2b5dbb8bffe7','to_id','node','a264bf75-34c6-4acc-a56b-a9790e440eb1','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),92,'rel','f6fd4eb7-3eb5-4b2a-88af-2b5dbb8bffe7','to_id','node','d311e190-89fa-4c7b-b76e-694fb3b2f4d0','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),92,'rel','f6fd4eb7-3eb5-4b2a-88af-2b5dbb8bffe7','to_id','node','fea1d58c-b5ba-4db1-b81d-65b00befcd1d','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),92,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',3,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),92,'node','f6fd4eb7-3eb5-4b2a-88af-2b5dbb8bffe7','id','rel','f6fd4eb7-3eb5-4b2a-88af-2b5dbb8bffe7','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),92,'rel','f6fd4eb7-3eb5-4b2a-88af-2b5dbb8bffe7','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),92,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','id',4,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),92,'rel','f6fd4eb7-3eb5-4b2a-88af-2b5dbb8bffe7','to_id','node','79886cbe-4cbc-4db8-9287-a6483b8bd3a6','id',2,1,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),94,'node','dd1730ec-20c1-4cb0-a1bd-2b1a85222e0e','id','rel','dd1730ec-20c1-4cb0-a1bd-2b1a85222e0e','from_id',1,2,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),7,'rel','1d5bd027-c4c8-4022-a2ee-70fc518b2861','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',5,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),8,'rel','1d5bd027-c4c8-4022-a2ee-70fc518b2861','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',5,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),18,'rel','c4c53bf2-0b46-4a55-b780-4617838056c7','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),19,'rel','50a9b985-42e2-4721-96d5-4ab8c7d575b0','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),20,'rel','72910d25-aad4-489e-a55c-d6ef0966ca23','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),21,'rel','19a88372-0f65-4f39-8100-64cbaaefd006','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),22,'rel','d0023997-ebca-4f13-9fab-be74f28cf23d','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),9,'rel','0a7d8c62-4d0a-416d-8db9-e3045df14de4','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),28,'rel','a0827785-2638-4862-b9eb-d2f7b022c068','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),29,'rel','ec22745e-c16a-4eb4-8bb3-791218cf6a37','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),30,'rel','0c278c97-e689-4999-9949-14d5362aebc6','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),31,'rel','cac77a10-ff1b-48bd-adf8-bc28d6688c09','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),32,'rel','750a27dd-4e98-46b3-bca1-bc50ddbe6647','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),33,'rel','d9526e8e-ae7b-4e05-b93f-8422a6b4418c','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),34,'rel','65be91de-2d44-413a-8330-d1fab285ff77','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),35,'rel','dc05e486-d549-445f-a1a8-2cd2f6de7f58','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),36,'rel','f6f72a69-61c4-40ef-a7bc-65e0107302f6','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),37,'rel','1acf6821-78a5-4a03-9726-7ab541a0066a','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),38,'rel','a1e43695-a4d4-4e89-a03f-dca04148405f','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),39,'rel','07a7f96d-efd1-48f1-8000-7d695c372204','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),39,'rel','72eefd7e-19fc-4907-afba-f3c7bacc8c06','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',5,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),54,'rel','4eb98b16-7d92-4b35-8b0e-3ce3748b9419','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',5,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),61,'rel','b61affec-d01e-4bb8-b07d-693a3e0d7152','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),70,'rel','07aca350-6f0d-11e4-9803-0800200c9a66','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),71,'rel','ae32043f-3f35-411d-9e9a-f105122b5f55','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),73,'rel','07d5185d-a2d4-44bc-91d8-e83a43683b68','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',5,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),74,'rel','6cb1a579-4d85-433f-8113-df4bff433b04','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),75,'rel','1e5b9667-7f23-417c-99f2-90b6c651ace8','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),76,'rel','3219a8bd-9950-4cfd-8cb9-6472901013b8','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',5,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),77,'rel','7b30cc82-f54e-41c3-9eb0-943c032be9a3','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),78,'rel','aa59c41b-50d2-4a8b-9154-29e1d955e2a6','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),79,'rel','2370cd39-2426-4002-abba-6af70d1c8989','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',5,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),80,'rel','3b69bc6c-8902-4e61-abc9-92303a3f290a','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),81,'rel','c2e023d5-e864-48b8-b6c9-5c77f5e7d839','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),82,'rel','013fc384-177b-4613-bf1a-a15f2d9c35a1','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),83,'rel','6a52334b-56f3-439f-8be7-47e82dd4d323','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),84,'rel','fbe34e57-8e49-4f35-9546-650b4bcccc52','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),85,'rel','fdf14d4e-5b31-4e94-9c45-7aa44452226e','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),86,'rel','bc1b9bfd-8c66-4779-a5d4-add478db3fa4','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),87,'rel','19528aa3-caee-4d65-a3e0-1b98fc79bc1f','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),88,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),97,'node','49cfe497-2c77-40f3-8942-793203ca29ee','id','rel','49cfe497-2c77-40f3-8942-793203ca29ee','from_id',1,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),97,'rel','49cfe497-2c77-40f3-8942-793203ca29ee','to_id','node','89ad0249-9394-4d75-b63b-63813658b37e','id',2,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),97,'node','89ad0249-9394-4d75-b63b-63813658b37e','id','rel','89ad0249-9394-4d75-b63b-63813658b37e','from_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),60,'node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id','dataset','tree_ds','node_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),117,'node','d55f6865-7d3e-4776-89f7-cccd2a452891','id','rel','d55f6865-7d3e-4776-89f7-cccd2a452891','from_id',1,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),117,'rel','d55f6865-7d3e-4776-89f7-cccd2a452891','to_id','node','e6731e3e-ed7c-4ab7-911a-ccab24851cf6','id',2,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),117,'node','e6731e3e-ed7c-4ab7-911a-ccab24851cf6','id','rel','e6731e3e-ed7c-4ab7-911a-ccab24851cf6','from_id',3,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),119,'node','f943ce62-643e-4b7d-b361-29bac44e5054','id','rel','f943ce62-643e-4b7d-b361-29bac44e5054','from_id',1,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),119,'rel','f943ce62-643e-4b7d-b361-29bac44e5054','to_id','node','389868e5-19b5-4b80-8d4e-16ba29c2e36b','id',2,3,null);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num,where_criteria)
values(nextval('sync.sync_tree_id_seq'),119,'node','389868e5-19b5-4b80-8d4e-16ba29c2e36b','id','rel','389868e5-19b5-4b80-8d4e-16ba29c2e36b','from_id',3,3,null);



 -- Добавление регламентов зк
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (333,'node','053ebb98-e741-42dd-aa4c-4c1e8d69591c','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["callsignsOfficialsMainForm"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (334,'node','5d70df1e-aef4-4cee-b3d3-083321a33d6a','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["negotiationTableJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (335,'node','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["eventMainFrontier"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (336,'node','e2276a2f-9a58-46c4-b0c8-87cd18eb17ee','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["csOnDutyJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (337,'node','84bca5a4-3ebd-4ee3-b19f-57056f505abc','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["targetsLand"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (338,'node','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["alarmsLand"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (339,'node','ea076d36-1053-47fb-9bfc-4a75090e6721','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["aviaMovingJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (340,'node','d453ccd4-ff88-4001-923c-8d37e88319b3','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["aviaFlightJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (341,'node','4fba4935-b935-4b81-b17b-57c1d0a1d0e7','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["airSituationJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (342,'node','01e60aa5-1f85-4102-b526-28287dd5bf9f','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["logisticsAvailabilityJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (343,'node','7f7a1963-8971-43b5-a9a5-e83260048e69','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["logisticsSystemStatusJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (344,'node','87ed5dc7-e8b1-4f67-b288-12aab1626350','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["csInSeaJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (345,'node','dc7fe95b-679f-41fc-9228-aabbfc252d26','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["automobileChecklistJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (346,'node','92623287-26fa-42bf-8fec-0ab86d80cf89','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["tspkCatalogJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (347,'node','f805daff-04fc-4e4f-b3bc-6b1d5a87ec43','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["structuresRegistrListJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (348,'node','7a4fab53-5014-48a8-88ab-41bcd1626a57','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["meansOfComunicationMainJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (349,'node','16ce53a5-a172-49d2-97e6-69d026e24616','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["frontierGuardComunicationsForm"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (350,'node','8c45e434-bc21-4337-b406-00d50eba9950','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["callsignsCommunicationCenterMainJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (351,'node','de108002-4ff6-4be1-ad5a-60059be841c3','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["pathOfFrontierGuardJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (352,'node','43cfecae-33f8-4663-a87d-a0c6951249d9','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["pdAnnualPlanningJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (353,'node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["zkPUOrgstructureForm", "zkPUOrgstructurePersonalSquadForm"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (354,'node','da8ac0d4-4cbc-4875-abd2-3ea83eac1c88','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["zkPUOrgstructSubdivisionForm", "zkPUOrgstructSubdivisionPersonnelForm"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (355,'node','ab722707-0ffa-4113-89e8-a4ced25cd4d4','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["frontierGuardPlanJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (356,'node','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["tsogRegistrListJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (357,'node','c086f87e-c53b-452c-a1f4-c392e70de1db','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["foivPersonnelZKVSJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (358,'node','3415687c-4b36-45d7-ab0e-7619560167df','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["ks"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (359,'node','3c695dae-4b39-41f3-98bc-4ddb3aa21664','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["frontierDuty", "planElementNoLegendForm", "planElementForm"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (360,'node','691e35a6-e4ca-4d1b-8114-7d65a10f8abf','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["placesOfFrontierGuardJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (361,'node','67a36e83-a3ab-4c51-bac5-ae4a2fd0c653','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["frontierGuardAutomobileUsejournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (362,'node','f3605afe-86ac-46d5-b978-024132ae11e9','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["automobileTripMainJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (363,'node','f04190e4-9f58-41fa-a36a-7972f9acebad','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["eventMainArrestedLoadJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (364,'node','1b80db34-9947-44a6-8a7d-2ec385848873','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["eventMainTraitorsJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (365,'node','7fae9564-b0f2-4296-86e3-7d7e6f2124aa','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["callsignFrontierGuardMainJournal"]'::json,null,'[]'::json,null,null);


-- Добавление кустов

insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),365,'rel','7fae9564-b0f2-4296-86e3-7d7e6f2124aa','to_id','node','1cf1f968-302a-4fb8-86e3-31560532f4f8','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),365,'node','1cf1f968-302a-4fb8-86e3-31560532f4f8','id','rel','1cf1f968-302a-4fb8-86e3-31560532f4f8','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),365,'rel','7fae9564-b0f2-4296-86e3-7d7e6f2124aa','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),365,'node','7fae9564-b0f2-4296-86e3-7d7e6f2124aa','id','rel','7fae9564-b0f2-4296-86e3-7d7e6f2124aa','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),333,'rel','053ebb98-e741-42dd-aa4c-4c1e8d69591c','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),333,'node','1cf1f968-302a-4fb8-86e3-31560532f4f8','id','rel','1cf1f968-302a-4fb8-86e3-31560532f4f8','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),333,'rel','053ebb98-e741-42dd-aa4c-4c1e8d69591c','to_id','node','1cf1f968-302a-4fb8-86e3-31560532f4f8','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),333,'node','053ebb98-e741-42dd-aa4c-4c1e8d69591c','id','rel','053ebb98-e741-42dd-aa4c-4c1e8d69591c','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),333,'rel','053ebb98-e741-42dd-aa4c-4c1e8d69591c','to_id','node','b4a038ff-dd42-4686-b85d-51be1fd3faca','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),334,'rel','5d70df1e-aef4-4cee-b3d3-083321a33d6a','to_id','node','aa9a41d3-208e-4a53-a22b-21fe88990ac4','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),334,'node','5d70df1e-aef4-4cee-b3d3-083321a33d6a','id','rel','5d70df1e-aef4-4cee-b3d3-083321a33d6a','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),334,'rel','5d70df1e-aef4-4cee-b3d3-083321a33d6a','to_id','node','e3d85e95-dbfd-417a-b576-0d808781a911','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),336,'node','e2276a2f-9a58-46c4-b0c8-87cd18eb17ee','id','rel','e2276a2f-9a58-46c4-b0c8-87cd18eb17ee','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),336,'rel','e2276a2f-9a58-46c4-b0c8-87cd18eb17ee','to_id','node','bbd4ff84-d62b-4b70-ab3e-6395bfa28e44','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),336,'rel','e2276a2f-9a58-46c4-b0c8-87cd18eb17ee','to_id','node','3415687c-4b36-45d7-ab0e-7619560167df','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),336,'rel','e2276a2f-9a58-46c4-b0c8-87cd18eb17ee','to_id','node','bbbfeb4c-aa05-4d48-8b3a-f1948564fea2','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),336,'rel','bbbfeb4c-aa05-4d48-8b3a-f1948564fea2','to_id','node','063b2fde-8353-4092-95ce-3418c80ee3af','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),336,'rel','e2276a2f-9a58-46c4-b0c8-87cd18eb17ee','to_id','node','af8d7cfe-8b03-452d-87b3-2f0c8d593387','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),336,'node','bbbfeb4c-aa05-4d48-8b3a-f1948564fea2','id','rel','bbbfeb4c-aa05-4d48-8b3a-f1948564fea2','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),338,'rel','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),338,'rel','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','to_id','node','fe9021c6-3a6e-418f-97e4-7e305e275804','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),338,'node','84bca5a4-3ebd-4ee3-b19f-57056f505abc','id','rel','84bca5a4-3ebd-4ee3-b19f-57056f505abc','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),338,'node','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','id','rel','e36b3b7b-ee2f-401c-bf20-29a5eb04881e','to_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),338,'node','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','id','rel','84bca5a4-3ebd-4ee3-b19f-57056f505abc','to_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),338,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','from_id','node','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),338,'node','e36b3b7b-ee2f-401c-bf20-29a5eb04881e','id','rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),338,'rel','84bca5a4-3ebd-4ee3-b19f-57056f505abc','from_id','node','84bca5a4-3ebd-4ee3-b19f-57056f505abc','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),338,'node','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','id','rel','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),338,'rel','e36b3b7b-ee2f-401c-bf20-29a5eb04881e','from_id','node','e36b3b7b-ee2f-401c-bf20-29a5eb04881e','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),338,'rel','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','to_id','node','1ddc9a28-b1f5-4836-bf54-5fa401ce1305','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),341,'rel','4fba4935-b935-4b81-b17b-57c1d0a1d0e7','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),341,'node','4fba4935-b935-4b81-b17b-57c1d0a1d0e7','id','rel','4fba4935-b935-4b81-b17b-57c1d0a1d0e7','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),342,'node','01e60aa5-1f85-4102-b526-28287dd5bf9f','id','rel','01e60aa5-1f85-4102-b526-28287dd5bf9f','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),342,'node','258cb9aa-1969-4af4-9847-1a5913312c38','id','rel','258cb9aa-1969-4af4-9847-1a5913312c38','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),342,'rel','01e60aa5-1f85-4102-b526-28287dd5bf9f','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),342,'rel','01e60aa5-1f85-4102-b526-28287dd5bf9f','to_id','node','258cb9aa-1969-4af4-9847-1a5913312c38','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),343,'node','7f7a1963-8971-43b5-a9a5-e83260048e69','id','rel','7f7a1963-8971-43b5-a9a5-e83260048e69','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),343,'node','80b18b71-38f2-43a4-aed5-94c0d43fd387','id','rel','80b18b71-38f2-43a4-aed5-94c0d43fd387','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),343,'rel','7f7a1963-8971-43b5-a9a5-e83260048e69','to_id','node','80b18b71-38f2-43a4-aed5-94c0d43fd387','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),343,'rel','7f7a1963-8971-43b5-a9a5-e83260048e69','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),344,'node','87ed5dc7-e8b1-4f67-b288-12aab1626350','id','rel','87ed5dc7-e8b1-4f67-b288-12aab1626350','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),344,'rel','87ed5dc7-e8b1-4f67-b288-12aab1626350','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),344,'rel','87ed5dc7-e8b1-4f67-b288-12aab1626350','to_id','node','3415687c-4b36-45d7-ab0e-7619560167df','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),344,'node','3415687c-4b36-45d7-ab0e-7619560167df','id','rel','3415687c-4b36-45d7-ab0e-7619560167df','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),345,'node','dc7fe95b-679f-41fc-9228-aabbfc252d26','id','rel','dc7fe95b-679f-41fc-9228-aabbfc252d26','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),345,'rel','dc7fe95b-679f-41fc-9228-aabbfc252d26','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),345,'rel','dc7fe95b-679f-41fc-9228-aabbfc252d26','to_id','node','ec529fb1-87cd-430d-a322-fa086ad8bb2b','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),345,'rel','64c650a2-4d67-4977-9370-a3dcfbcf89db','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),345,'rel','dc7fe95b-679f-41fc-9228-aabbfc252d26','to_id','node','64c650a2-4d67-4977-9370-a3dcfbcf89db','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),345,'node','64c650a2-4d67-4977-9370-a3dcfbcf89db','id','rel','64c650a2-4d67-4977-9370-a3dcfbcf89db','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),346,'rel','92623287-26fa-42bf-8fec-0ab86d80cf89','to_id','node','ec529fb1-87cd-430d-a322-fa086ad8bb2b','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),346,'rel','92623287-26fa-42bf-8fec-0ab86d80cf89','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),346,'rel','64c650a2-4d67-4977-9370-a3dcfbcf89db','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),346,'node','92623287-26fa-42bf-8fec-0ab86d80cf89','id','rel','92623287-26fa-42bf-8fec-0ab86d80cf89','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),346,'node','a2811725-20e7-43cb-b238-43c9a4e3de3c','id','rel','a2811725-20e7-43cb-b238-43c9a4e3de3c','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),346,'rel','92623287-26fa-42bf-8fec-0ab86d80cf89','to_id','node','a2811725-20e7-43cb-b238-43c9a4e3de3c','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),346,'rel','92623287-26fa-42bf-8fec-0ab86d80cf89','to_id','node','64c650a2-4d67-4977-9370-a3dcfbcf89db','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),346,'rel','a2811725-20e7-43cb-b238-43c9a4e3de3c','to_id','node','53c7e37c-bbf3-47d0-ad69-c907c5158728','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),346,'node','64c650a2-4d67-4977-9370-a3dcfbcf89db','id','rel','64c650a2-4d67-4977-9370-a3dcfbcf89db','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),347,'rel','cb2ce019-a2ac-45ac-977e-52a9e8e2e8cc','to_id','node','513843f4-6dde-45c2-935c-9a904e494997','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),347,'rel','f805daff-04fc-4e4f-b3bc-6b1d5a87ec43','to_id','node','64c650a2-4d67-4977-9370-a3dcfbcf89db','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),347,'rel','f805daff-04fc-4e4f-b3bc-6b1d5a87ec43','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),347,'rel','cb2ce019-a2ac-45ac-977e-52a9e8e2e8cc','to_id','node','22f8b0ac-6545-4358-81b2-21476ac95a54','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),347,'node','22f8b0ac-6545-4358-81b2-21476ac95a54','id','rel','22f8b0ac-6545-4358-81b2-21476ac95a54','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),347,'rel','6d5001d0-0e66-489f-b8d8-c697bc8f62bd','to_id','node','3915d667-098e-4245-b641-1cfbf8e9e182','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),347,'node','3915d667-098e-4245-b641-1cfbf8e9e182','id','rel','3915d667-098e-4245-b641-1cfbf8e9e182','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),347,'rel','64c650a2-4d67-4977-9370-a3dcfbcf89db','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),347,'rel','f805daff-04fc-4e4f-b3bc-6b1d5a87ec43','to_id','node','cb2ce019-a2ac-45ac-977e-52a9e8e2e8cc','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),347,'node','11337db8-9012-43f2-a311-c16923cd802d','id','rel','11337db8-9012-43f2-a311-c16923cd802d','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),347,'node','f805daff-04fc-4e4f-b3bc-6b1d5a87ec43','id','rel','f805daff-04fc-4e4f-b3bc-6b1d5a87ec43','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),347,'rel','6d5001d0-0e66-489f-b8d8-c697bc8f62bd','to_id','node','25075024-75ed-4eca-ba73-54fc10dd533a','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),347,'rel','f805daff-04fc-4e4f-b3bc-6b1d5a87ec43','to_id','node','6d5001d0-0e66-489f-b8d8-c697bc8f62bd','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),347,'rel','513843f4-6dde-45c2-935c-9a904e494997','to_id','node','b4a038ff-dd42-4686-b85d-51be1fd3faca','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),347,'rel','22f8b0ac-6545-4358-81b2-21476ac95a54','to_id','node','b4a038ff-dd42-4686-b85d-51be1fd3faca','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),347,'node','6d5001d0-0e66-489f-b8d8-c697bc8f62bd','id','rel','6d5001d0-0e66-489f-b8d8-c697bc8f62bd','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),347,'rel','6d5001d0-0e66-489f-b8d8-c697bc8f62bd','to_id','node','37ce622a-45cd-4e4d-a027-2e880eb2e957','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),347,'rel','f805daff-04fc-4e4f-b3bc-6b1d5a87ec43','to_id','node','ec529fb1-87cd-430d-a322-fa086ad8bb2b','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),347,'rel','f805daff-04fc-4e4f-b3bc-6b1d5a87ec43','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),347,'node','cb2ce019-a2ac-45ac-977e-52a9e8e2e8cc','id','rel','cb2ce019-a2ac-45ac-977e-52a9e8e2e8cc','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),347,'node','513843f4-6dde-45c2-935c-9a904e494997','id','rel','513843f4-6dde-45c2-935c-9a904e494997','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),347,'node','64c650a2-4d67-4977-9370-a3dcfbcf89db','id','rel','64c650a2-4d67-4977-9370-a3dcfbcf89db','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),348,'node','7a4fab53-5014-48a8-88ab-41bcd1626a57','id','rel','7a4fab53-5014-48a8-88ab-41bcd1626a57','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),348,'rel','7a4fab53-5014-48a8-88ab-41bcd1626a57','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),348,'rel','64c650a2-4d67-4977-9370-a3dcfbcf89db','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),348,'rel','7a4fab53-5014-48a8-88ab-41bcd1626a57','to_id','node','ec529fb1-87cd-430d-a322-fa086ad8bb2b','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),348,'rel','7a4fab53-5014-48a8-88ab-41bcd1626a57','to_id','node','8c02785c-a94a-44c7-a1fa-0c42aa265375','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),348,'node','64c650a2-4d67-4977-9370-a3dcfbcf89db','id','rel','64c650a2-4d67-4977-9370-a3dcfbcf89db','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),348,'rel','7a4fab53-5014-48a8-88ab-41bcd1626a57','to_id','node','64c650a2-4d67-4977-9370-a3dcfbcf89db','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),350,'rel','8c45e434-bc21-4337-b406-00d50eba9950','to_id','node','1cf1f968-302a-4fb8-86e3-31560532f4f8','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),350,'node','1cf1f968-302a-4fb8-86e3-31560532f4f8','id','rel','1cf1f968-302a-4fb8-86e3-31560532f4f8','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),350,'rel','8c45e434-bc21-4337-b406-00d50eba9950','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),350,'node','8c45e434-bc21-4337-b406-00d50eba9950','id','rel','8c45e434-bc21-4337-b406-00d50eba9950','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),351,'rel','de108002-4ff6-4be1-ad5a-60059be841c3','to_id','node','a0d589cf-ea6f-4da6-b499-26618b84883b','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),351,'rel','de108002-4ff6-4be1-ad5a-60059be841c3','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),351,'node','de108002-4ff6-4be1-ad5a-60059be841c3','id','rel','de108002-4ff6-4be1-ad5a-60059be841c3','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),354,'node','da8ac0d4-4cbc-4875-abd2-3ea83eac1c88','id','rel','da8ac0d4-4cbc-4875-abd2-3ea83eac1c88','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),349,'node','ccb13fa4-411f-4916-9d4e-290ad476c846','id','rel','ccb13fa4-411f-4916-9d4e-290ad476c846','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),349,'rel','16ce53a5-a172-49d2-97e6-69d026e24616','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),349,'node','16ce53a5-a172-49d2-97e6-69d026e24616','id','rel','16ce53a5-a172-49d2-97e6-69d026e24616','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),349,'rel','16ce53a5-a172-49d2-97e6-69d026e24616','to_id','node','ccb13fa4-411f-4916-9d4e-290ad476c846','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),349,'node','166ff927-dbe1-4a1d-ada3-d0a29171f46a','id','rel','166ff927-dbe1-4a1d-ada3-d0a29171f46a','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),349,'rel','ccb13fa4-411f-4916-9d4e-290ad476c846','to_id','node','5d70df1e-aef4-4cee-b3d3-083321a33d6a','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),349,'rel','16ce53a5-a172-49d2-97e6-69d026e24616','to_id','node','166ff927-dbe1-4a1d-ada3-d0a29171f46a','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),354,'node','da8ac0d4-4cbc-4875-abd2-3ea83eac1c88','id','dataset','tree_ds','node_id',3,3);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),347,'rel','f805daff-04fc-4e4f-b3bc-6b1d5a87ec43','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),337,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','397d7b11-3bf4-44fb-be5e-d1d77ea07543','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),337,'rel','84bca5a4-3ebd-4ee3-b19f-57056f505abc','to_id','node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),337,'rel','84bca5a4-3ebd-4ee3-b19f-57056f505abc','to_id','node','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),337,'node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id','rel','e08e0f7c-85f7-44af-9afe-aeedb38260af','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),337,'node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id','rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),337,'rel','84bca5a4-3ebd-4ee3-b19f-57056f505abc','to_id','node','d13d2d68-f07f-429d-9427-ce9801b63b82','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),337,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),337,'rel','84bca5a4-3ebd-4ee3-b19f-57056f505abc','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),337,'node','84bca5a4-3ebd-4ee3-b19f-57056f505abc','id','rel','84bca5a4-3ebd-4ee3-b19f-57056f505abc','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),337,'rel','84bca5a4-3ebd-4ee3-b19f-57056f505abc','to_id','node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),337,'rel','84bca5a4-3ebd-4ee3-b19f-57056f505abc','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),337,'rel','84bca5a4-3ebd-4ee3-b19f-57056f505abc','to_id','node','d943c4a4-239c-4b6e-aa50-0da53cf712e9','id',1,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'node','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','id','rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'node','022d3cf7-6c3e-4e79-88e2-c9612df1f801','id','rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','1b80db34-9947-44a6-8a7d-2ec385848873','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','e36b3b7b-ee2f-401c-bf20-29a5eb04881e','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','5d3272d2-a131-4c89-a0b5-de0be03ae5ca','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','f04190e4-9f58-41fa-a36a-7972f9acebad','to_id','node','21edea70-88a6-48a1-9ce6-65b8791589ef','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id','rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','from_id',7,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'node','f04190e4-9f58-41fa-a36a-7972f9acebad','id','rel','f04190e4-9f58-41fa-a36a-7972f9acebad','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'node','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','id','rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','from_id',7,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'node','abbd736e-4d87-4525-8a3e-e7f66d436568','id','rel','abbd736e-4d87-4525-8a3e-e7f66d436568','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','to_id','node','0fc0c943-8d0b-4ce4-9c89-a1b774be873c','id',8,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'node','be0ef829-5bb8-4f21-8fa6-d969c21115af','id','rel','be0ef829-5bb8-4f21-8fa6-d969c21115af','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','1e987810-6ade-4b6b-b79f-b452d77ff289','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','a23493b0-6a3b-4654-940f-d575014d389e','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','e08e0f7c-85f7-44af-9afe-aeedb38260af','to_id','node','924c1085-530f-45a6-9d3c-d84b1839eba3','id',8,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','74ad9486-21e5-4df8-9217-d3d38332ad36','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','be0ef829-5bb8-4f21-8fa6-d969c21115af','to_id','node','fa134406-d856-4f4e-bbad-30fbbfe49648','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','abbd736e-4d87-4525-8a3e-e7f66d436568','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'node','d943c4a4-239c-4b6e-aa50-0da53cf712e9','id','rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','to_id','node','fc511fe9-713d-4741-936c-dbcf47117566','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','3efd6007-ed54-47c9-9b50-963ed44377ab','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','abbd736e-4d87-4525-8a3e-e7f66d436568','to_id','node','336ce6ea-a0c6-40fc-af7a-d410c41fedc5','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','232b1704-f28e-424c-960a-a83e9e228ce0','to_id','node','74ad9486-21e5-4df8-9217-d3d38332ad36','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','f04190e4-9f58-41fa-a36a-7972f9acebad','to_id','node','af4e93f5-5ffb-11e4-9803-0800200c9a66','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'node','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','id','rel','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'node','1b80db34-9947-44a6-8a7d-2ec385848873','id','rel','1b80db34-9947-44a6-8a7d-2ec385848873','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','384b717e-acd6-4570-a8a8-d2291664955f','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','62a015af-7c63-4678-9f72-e117b9f4326e','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','62a015af-7c63-4678-9f72-e117b9f4326e','to_id','node','edabd97d-aac4-4326-818f-6ce16f81010d','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','f04190e4-9f58-41fa-a36a-7972f9acebad','to_id','node','be0ef829-5bb8-4f21-8fa6-d969c21115af','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'node','5d3272d2-a131-4c89-a0b5-de0be03ae5ca','id','rel','0f516482-6869-4115-b458-c4eeed1e6a50','to_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'node','7fbd40a8-c0db-4269-99ff-622e7752a3f1','id','rel','7fbd40a8-c0db-4269-99ff-622e7752a3f1','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'node','232b1704-f28e-424c-960a-a83e9e228ce0','id','rel','232b1704-f28e-424c-960a-a83e9e228ce0','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','to_id','node','6a2e0e1c-817c-4445-b6d0-28f1b7dbb13a','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',7,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','91d5f27c-a87e-4038-a1d8-da53a2588b5e','id',8,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','232b1704-f28e-424c-960a-a83e9e228ce0','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','af4e93f5-5ffb-11e4-9803-0800200c9a66','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'node','266f85da-4aa7-4b8d-8455-a92b2398861f','id','rel','266f85da-4aa7-4b8d-8455-a92b2398861f','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','fe9021c6-3a6e-418f-97e4-7e305e275804','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'node','e36b3b7b-ee2f-401c-bf20-29a5eb04881e','id','rel','e36b3b7b-ee2f-401c-bf20-29a5eb04881e','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','7fbd40a8-c0db-4269-99ff-622e7752a3f1','to_id','node','ab2a7d5f-dfaa-4011-85ce-e80fcb610cb1','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'node','62a015af-7c63-4678-9f72-e117b9f4326e','id','rel','62a015af-7c63-4678-9f72-e117b9f4326e','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','232b1704-f28e-424c-960a-a83e9e228ce0','to_id','node','af556ac6-f34a-4fdf-ade9-d84db16731e4','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','to_id','node','fe9021c6-3a6e-418f-97e4-7e305e275804','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','d0d3d766-360c-4f51-a0d1-78145f40c9b4','id',8,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','e36b3b7b-ee2f-401c-bf20-29a5eb04881e','to_id','node','d943c4a4-239c-4b6e-aa50-0da53cf712e9','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','62a015af-7c63-4678-9f72-e117b9f4326e','to_id','node','36b7b2f7-4cb0-455d-8f44-98aedfbd4135','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','f04190e4-9f58-41fa-a36a-7972f9acebad','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','266f85da-4aa7-4b8d-8455-a92b2398861f','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id','rel','e08e0f7c-85f7-44af-9afe-aeedb38260af','from_id',7,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','0f516482-6869-4115-b458-c4eeed1e6a50','from_id','node','0f516482-6869-4115-b458-c4eeed1e6a50','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','e36b3b7b-ee2f-401c-bf20-29a5eb04881e','to_id','node','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','266f85da-4aa7-4b8d-8455-a92b2398861f','to_id','node','edd94aa2-433f-4eea-a80b-3082083092f6','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','af556ac6-f34a-4fdf-ade9-d84db16731e4','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','to_id','node','3aca4bf0-b0e4-46f3-9370-263d88559a19','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','266f85da-4aa7-4b8d-8455-a92b2398861f','to_id','node','198a2cca-afe7-4f72-9c7e-51ac96ab8510','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','to_id','node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','to_id','node','70709cc4-f6a5-4e8e-9f6d-07c710a460df','id',8,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','7fbd40a8-c0db-4269-99ff-622e7752a3f1','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id','rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','abbd736e-4d87-4525-8a3e-e7f66d436568','to_id','node','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','022d3cf7-6c3e-4e79-88e2-c9612df1f801','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','397d7b11-3bf4-44fb-be5e-d1d77ea07543','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),340,'rel','d453ccd4-ff88-4001-923c-8d37e88319b3','to_id','node','448f35fa-aa85-4dd1-ab0d-68770bf22950','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),340,'rel','d453ccd4-ff88-4001-923c-8d37e88319b3','to_id','node','b2b5d855-91c8-47e3-954e-bffb3071dd76','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),340,'rel','d453ccd4-ff88-4001-923c-8d37e88319b3','to_id','node','ea076d36-1053-47fb-9bfc-4a75090e6721','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),340,'node','d453ccd4-ff88-4001-923c-8d37e88319b3','id','rel','d453ccd4-ff88-4001-923c-8d37e88319b3','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),353,'rel','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),353,'node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id','rel','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),353,'node','a6d00127-e1ff-41dc-ac24-749ef8146a5e','id','rel','a6d00127-e1ff-41dc-ac24-749ef8146a5e','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),353,'rel','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','to_id','node','a6d00127-e1ff-41dc-ac24-749ef8146a5e','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),353,'node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id','dataset','tree_ds','node_id',3,3);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),335,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),356,'rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','to_id','node','7e820077-7d8c-4c73-9564-596b2914bf97','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),356,'rel','8426e2e7-0068-4f2f-acd1-7099c2622f13','to_id','node','024b7e52-56dc-4152-9629-de0f619df67c','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),356,'rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','to_id','node','b2a5331a-a7a8-4dcb-9308-bf207ca38c49','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),356,'rel','23a10797-7d81-4303-9228-f51625c7a8de','to_id','node','fe5770c4-49f6-4e97-a299-04db5ccdf75d','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),356,'node','8426e2e7-0068-4f2f-acd1-7099c2622f13','id','rel','8426e2e7-0068-4f2f-acd1-7099c2622f13','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),356,'node','ac7314eb-148d-4476-abed-ba7fc679a817','id','rel','ac7314eb-148d-4476-abed-ba7fc679a817','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),356,'rel','64c650a2-4d67-4977-9370-a3dcfbcf89db','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),356,'rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','to_id','node','ec529fb1-87cd-430d-a322-fa086ad8bb2b','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),356,'node','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','id','rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),356,'rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),356,'rel','8426e2e7-0068-4f2f-acd1-7099c2622f13','to_id','node','fe5770c4-49f6-4e97-a299-04db5ccdf75d','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),356,'rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','to_id','node','8426e2e7-0068-4f2f-acd1-7099c2622f13','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),356,'rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','to_id','node','23a10797-7d81-4303-9228-f51625c7a8de','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),356,'rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','to_id','node','64c650a2-4d67-4977-9370-a3dcfbcf89db','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),356,'rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),356,'node','23a10797-7d81-4303-9228-f51625c7a8de','id','rel','23a10797-7d81-4303-9228-f51625c7a8de','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),356,'node','64c650a2-4d67-4977-9370-a3dcfbcf89db','id','rel','64c650a2-4d67-4977-9370-a3dcfbcf89db','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),356,'rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','to_id','node','ac7314eb-148d-4476-abed-ba7fc679a817','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),358,'rel','3415687c-4b36-45d7-ab0e-7619560167df','to_id','node','3c695dae-4b39-41f3-98bc-4ddb3aa21664','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),358,'rel','513a2be6-efac-4f19-aa71-e3ec8f47fc91','to_id','node','ad7bab38-8a99-46d5-ad11-06ca00431ea4','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),358,'rel','3415687c-4b36-45d7-ab0e-7619560167df','to_id','node','48cedebe-45eb-4c1f-b2fa-ee7b410c8920','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),358,'node','3c695dae-4b39-41f3-98bc-4ddb3aa21664','id','rel','3c695dae-4b39-41f3-98bc-4ddb3aa21664','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),358,'rel','3415687c-4b36-45d7-ab0e-7619560167df','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),358,'rel','3415687c-4b36-45d7-ab0e-7619560167df','to_id','node','4673d875-5627-4769-aa6f-22b1029942de','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),358,'node','3415687c-4b36-45d7-ab0e-7619560167df','id','rel','3415687c-4b36-45d7-ab0e-7619560167df','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),358,'node','513a2be6-efac-4f19-aa71-e3ec8f47fc91','id','rel','513a2be6-efac-4f19-aa71-e3ec8f47fc91','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),358,'rel','3415687c-4b36-45d7-ab0e-7619560167df','to_id','node','c24346f6-5bb4-4865-bb68-4810dcff2eeb','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),358,'rel','3c695dae-4b39-41f3-98bc-4ddb3aa21664','to_id','node','6c2fab16-1b58-418d-b06e-0c5cb0252067','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),358,'rel','3c695dae-4b39-41f3-98bc-4ddb3aa21664','to_id','node','513a2be6-efac-4f19-aa71-e3ec8f47fc91','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),359,'node','560ea664-be2c-47b9-b602-9150f09f191e','id','rel','560ea664-be2c-47b9-b602-9150f09f191e','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),359,'rel','560ea664-be2c-47b9-b602-9150f09f191e','from_id','node','560ea664-be2c-47b9-b602-9150f09f191e','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),359,'node','3c695dae-4b39-41f3-98bc-4ddb3aa21664','id','rel','560ea664-be2c-47b9-b602-9150f09f191e','to_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),359,'rel','3c695dae-4b39-41f3-98bc-4ddb3aa21664','to_id','node','6c2fab16-1b58-418d-b06e-0c5cb0252067','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),359,'rel','3c695dae-4b39-41f3-98bc-4ddb3aa21664','to_id','node','513a2be6-efac-4f19-aa71-e3ec8f47fc91','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),359,'rel','560ea664-be2c-47b9-b602-9150f09f191e','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),359,'node','3c695dae-4b39-41f3-98bc-4ddb3aa21664','id','rel','3c695dae-4b39-41f3-98bc-4ddb3aa21664','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),357,'rel','c086f87e-c53b-452c-a1f4-c392e70de1db','to_id','node','3c695dae-4b39-41f3-98bc-4ddb3aa21664','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),336,'rel','e2276a2f-9a58-46c4-b0c8-87cd18eb17ee','to_id','node','4521425f-eb43-4a05-a464-1dfd8d1b1536','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'rel','ab722707-0ffa-4113-89e8-a4ced25cd4d4','to_id','node','09dd043e-813a-4835-a7ac-1b2c84d035cd','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'rel','09dd043e-813a-4835-a7ac-1b2c84d035cd','to_id','node','16ce53a5-a172-49d2-97e6-69d026e24616','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'rel','ab722707-0ffa-4113-89e8-a4ced25cd4d4','to_id','node','3c695dae-4b39-41f3-98bc-4ddb3aa21664','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'rel','ccb13fa4-411f-4916-9d4e-290ad476c846','to_id','node','5d70df1e-aef4-4cee-b3d3-083321a33d6a','id',8,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'node','16ce53a5-a172-49d2-97e6-69d026e24616','id','rel','16ce53a5-a172-49d2-97e6-69d026e24616','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'rel','43cfecae-33f8-4663-a87d-a0c6951249d9','from_id','node','43cfecae-33f8-4663-a87d-a0c6951249d9','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'node','3c695dae-4b39-41f3-98bc-4ddb3aa21664','id','rel','c086f87e-c53b-452c-a1f4-c392e70de1db','to_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'rel','43cfecae-33f8-4663-a87d-a0c6951249d9','to_id','node','691e35a6-e4ca-4d1b-8114-7d65a10f8abf','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'rel','7fae9564-b0f2-4296-86e3-7d7e6f2124aa','to_id','node','1cf1f968-302a-4fb8-86e3-31560532f4f8','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'node','ccb13fa4-411f-4916-9d4e-290ad476c846','id','rel','ccb13fa4-411f-4916-9d4e-290ad476c846','from_id',7,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'rel','ab722707-0ffa-4113-89e8-a4ced25cd4d4','to_id','node','687f084d-3978-4812-8f91-90b9bd3e06bd','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'rel','16ce53a5-a172-49d2-97e6-69d026e24616','to_id','node','ccb13fa4-411f-4916-9d4e-290ad476c846','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'rel','ad5afedb-5580-4d17-8488-1006b9d4ba40','from_id','node','ad5afedb-5580-4d17-8488-1006b9d4ba40','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'rel','16ce53a5-a172-49d2-97e6-69d026e24616','to_id','node','166ff927-dbe1-4a1d-ada3-d0a29171f46a','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'node','3c695dae-4b39-41f3-98bc-4ddb3aa21664','id','rel','43cfecae-33f8-4663-a87d-a0c6951249d9','to_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'rel','43cfecae-33f8-4663-a87d-a0c6951249d9','to_id','node','de108002-4ff6-4be1-ad5a-60059be841c3','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'rel','ab722707-0ffa-4113-89e8-a4ced25cd4d4','to_id','node','7fae9564-b0f2-4296-86e3-7d7e6f2124aa','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'node','7fae9564-b0f2-4296-86e3-7d7e6f2124aa','id','rel','7fae9564-b0f2-4296-86e3-7d7e6f2124aa','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'node','166ff927-dbe1-4a1d-ada3-d0a29171f46a','id','rel','166ff927-dbe1-4a1d-ada3-d0a29171f46a','from_id',7,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'node','09dd043e-813a-4835-a7ac-1b2c84d035cd','id','rel','09dd043e-813a-4835-a7ac-1b2c84d035cd','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'rel','43cfecae-33f8-4663-a87d-a0c6951249d9','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'node','43cfecae-33f8-4663-a87d-a0c6951249d9','id','rel','43cfecae-33f8-4663-a87d-a0c6951249d9','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'node','ab722707-0ffa-4113-89e8-a4ced25cd4d4','id','rel','ab722707-0ffa-4113-89e8-a4ced25cd4d4','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'node','c086f87e-c53b-452c-a1f4-c392e70de1db','id','rel','ad5afedb-5580-4d17-8488-1006b9d4ba40','to_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),355,'rel','c086f87e-c53b-452c-a1f4-c392e70de1db','from_id','node','c086f87e-c53b-452c-a1f4-c392e70de1db','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),352,'rel','43cfecae-33f8-4663-a87d-a0c6951249d9','to_id','node','de108002-4ff6-4be1-ad5a-60059be841c3','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),352,'rel','513a2be6-efac-4f19-aa71-e3ec8f47fc91','to_id','node','ad7bab38-8a99-46d5-ad11-06ca00431ea4','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),352,'rel','43cfecae-33f8-4663-a87d-a0c6951249d9','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),352,'node','43cfecae-33f8-4663-a87d-a0c6951249d9','id','rel','43cfecae-33f8-4663-a87d-a0c6951249d9','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),352,'node','3c695dae-4b39-41f3-98bc-4ddb3aa21664','id','rel','3c695dae-4b39-41f3-98bc-4ddb3aa21664','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),352,'rel','43cfecae-33f8-4663-a87d-a0c6951249d9','to_id','node','3c695dae-4b39-41f3-98bc-4ddb3aa21664','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),352,'node','513a2be6-efac-4f19-aa71-e3ec8f47fc91','id','rel','513a2be6-efac-4f19-aa71-e3ec8f47fc91','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),352,'rel','43cfecae-33f8-4663-a87d-a0c6951249d9','to_id','node','691e35a6-e4ca-4d1b-8114-7d65a10f8abf','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),352,'rel','3c695dae-4b39-41f3-98bc-4ddb3aa21664','to_id','node','6c2fab16-1b58-418d-b06e-0c5cb0252067','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),352,'rel','3c695dae-4b39-41f3-98bc-4ddb3aa21664','to_id','node','513a2be6-efac-4f19-aa71-e3ec8f47fc91','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),357,'rel','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','from_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),357,'node','2b14f1c4-6b1e-4895-84e4-3ede9e367c0b','id','rel','2b14f1c4-6b1e-4895-84e4-3ede9e367c0b','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),357,'node','c086f87e-c53b-452c-a1f4-c392e70de1db','id','rel','ad5afedb-5580-4d17-8488-1006b9d4ba40','to_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),357,'rel','c086f87e-c53b-452c-a1f4-c392e70de1db','to_id','node','2b14f1c4-6b1e-4895-84e4-3ede9e367c0b','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),357,'node','ad5afedb-5580-4d17-8488-1006b9d4ba40','id','rel','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','to_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),357,'rel','c086f87e-c53b-452c-a1f4-c392e70de1db','to_id','node','659c8886-09fc-4e87-b986-ebd7192ef4f5','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),357,'rel','ad5afedb-5580-4d17-8488-1006b9d4ba40','to_id','node','b4a038ff-dd42-4686-b85d-51be1fd3faca','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),357,'node','c086f87e-c53b-452c-a1f4-c392e70de1db','id','rel','c086f87e-c53b-452c-a1f4-c392e70de1db','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),357,'node','ad5afedb-5580-4d17-8488-1006b9d4ba40','id','rel','ad5afedb-5580-4d17-8488-1006b9d4ba40','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),357,'rel','ad5afedb-5580-4d17-8488-1006b9d4ba40','from_id','node','ad5afedb-5580-4d17-8488-1006b9d4ba40','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),357,'node','c086f87e-c53b-452c-a1f4-c392e70de1db','id','files','c086f87e-c53b-452c-a1f4-c392e70de1db','node_id',3,3);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),356,'rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),360,'rel','691e35a6-e4ca-4d1b-8114-7d65a10f8abf','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),360,'node','691e35a6-e4ca-4d1b-8114-7d65a10f8abf','id','rel','691e35a6-e4ca-4d1b-8114-7d65a10f8abf','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),360,'rel','691e35a6-e4ca-4d1b-8114-7d65a10f8abf','to_id','node','a0d589cf-ea6f-4da6-b499-26618b84883b','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),361,'rel','debc2b0d-3744-4e27-a9aa-99daa086b91a','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),361,'node','debc2b0d-3744-4e27-a9aa-99daa086b91a','id','rel','debc2b0d-3744-4e27-a9aa-99daa086b91a','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),361,'rel','debc2b0d-3744-4e27-a9aa-99daa086b91a','to_id','node','dc7fe95b-679f-41fc-9228-aabbfc252d26','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),361,'rel','67a36e83-a3ab-4c51-bac5-ae4a2fd0c653','to_id','node','debc2b0d-3744-4e27-a9aa-99daa086b91a','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),361,'node','dc7fe95b-679f-41fc-9228-aabbfc252d26','id','rel','dc7fe95b-679f-41fc-9228-aabbfc252d26','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),361,'node','67a36e83-a3ab-4c51-bac5-ae4a2fd0c653','id','rel','67a36e83-a3ab-4c51-bac5-ae4a2fd0c653','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),362,'rel','debc2b0d-3744-4e27-a9aa-99daa086b91a','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),362,'node','debc2b0d-3744-4e27-a9aa-99daa086b91a','id','rel','debc2b0d-3744-4e27-a9aa-99daa086b91a','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),362,'rel','debc2b0d-3744-4e27-a9aa-99daa086b91a','to_id','node','dc7fe95b-679f-41fc-9228-aabbfc252d26','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),362,'rel','f3605afe-86ac-46d5-b978-024132ae11e9','to_id','node','debc2b0d-3744-4e27-a9aa-99daa086b91a','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),362,'node','dc7fe95b-679f-41fc-9228-aabbfc252d26','id','rel','dc7fe95b-679f-41fc-9228-aabbfc252d26','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),362,'node','f3605afe-86ac-46d5-b978-024132ae11e9','id','rel','f3605afe-86ac-46d5-b978-024132ae11e9','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),362,'rel','f3605afe-86ac-46d5-b978-024132ae11e9','to_id','node','c9adafcf-61bd-4d60-8405-17e56a5f3a95','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),363,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','from_id','node','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),363,'node','f04190e4-9f58-41fa-a36a-7972f9acebad','id','rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),363,'node','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','id','rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),363,'node','f04190e4-9f58-41fa-a36a-7972f9acebad','id','rel','f04190e4-9f58-41fa-a36a-7972f9acebad','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),363,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),363,'node','be0ef829-5bb8-4f21-8fa6-d969c21115af','id','rel','be0ef829-5bb8-4f21-8fa6-d969c21115af','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','from_id','node','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','397d7b11-3bf4-44fb-be5e-d1d77ea07543','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'node','1b80db34-9947-44a6-8a7d-2ec385848873','id','rel','1b80db34-9947-44a6-8a7d-2ec385848873','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'rel','abbd736e-4d87-4525-8a3e-e7f66d436568','to_id','node','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','232b1704-f28e-424c-960a-a83e9e228ce0','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id','rel','e08e0f7c-85f7-44af-9afe-aeedb38260af','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id','rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'rel','266f85da-4aa7-4b8d-8455-a92b2398861f','to_id','node','edd94aa2-433f-4eea-a80b-3082083092f6','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'rel','0f516482-6869-4115-b458-c4eeed1e6a50','from_id','node','0f516482-6869-4115-b458-c4eeed1e6a50','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'node','266f85da-4aa7-4b8d-8455-a92b2398861f','id','rel','266f85da-4aa7-4b8d-8455-a92b2398861f','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'node','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','id','rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','abbd736e-4d87-4525-8a3e-e7f66d436568','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','5d3272d2-a131-4c89-a0b5-de0be03ae5ca','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'node','232b1704-f28e-424c-960a-a83e9e228ce0','id','rel','232b1704-f28e-424c-960a-a83e9e228ce0','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'node','5d3272d2-a131-4c89-a0b5-de0be03ae5ca','id','rel','0f516482-6869-4115-b458-c4eeed1e6a50','to_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','266f85da-4aa7-4b8d-8455-a92b2398861f','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'node','1b80db34-9947-44a6-8a7d-2ec385848873','id','rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'node','abbd736e-4d87-4525-8a3e-e7f66d436568','id','rel','abbd736e-4d87-4525-8a3e-e7f66d436568','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),364,'node','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','id','rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','from_id',5,2);


update sync.sync_reglament set out_node_mask = '01'::bit(128);