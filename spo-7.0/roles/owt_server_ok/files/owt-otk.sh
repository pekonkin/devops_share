#!/bin/bash

if [ $( rpm -qa | grep -c curl ) -eq 0 ]
  then
  yum -y install curl
fi

curl -s -k -v -i 'http://127.0.0.1:10007/login.html' -H 'Host: 127.0.0.1:10007' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8' --compressed -H 'Referer: http://10.1.70.17:10007/login.html' --data 'user=admin&pass=admin' | tee get_cooki.txt

_cookie=`egrep 'Set-Cookie:' get_cooki.txt | awk {'print$2'} | sed -e 's/;//g'`

curl 'http://127.0.0.1:10007/cpanel.shtml?chnl' \
-H 'Host: 127.0.0.1:10007' \
-H 'Cookie: '${_cookie}'' \
-H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0' \
-H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'  \
--compressed -H 'Referer: http://127.0.0.1:10007/cpanel.shtml?chnl' \
--data 'form_tag=form.create.channel&cnl_id=1&lz4_mode=1&action.create.channel=Create'

rm -f get_cooki.txt
