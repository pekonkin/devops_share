#!/bin/bash

ARCH="$(uname -i)|noarch"
packets_in_repos=$(
    yum list available 2>/dev/null |  # list all available in repo
        egrep -v '^[[:space:]]'|      # void uninformative lines
        egrep "$ARCH" |               # do not list packets with unsutable arch
        awk '{print $1}' |            # list only packet names
        sed -r "s%.($ARCH)%%" |       # remove arch name from listing
        sort |
        tr \\n \|                     # all packet names prepared to be used in egrep
)
may_need_downgrade=$(                 # all may need downgrading
    rpm -qa --qf '%{NAME}\n' |        # all instaled packet names
        egrep "^(${packets_in_repos%%|})$" # grep only avaliable in current repos
)
get_version(){                        # get packet version from yum
    side=$1;                          # avaliable or installed
    packet_name=$2;                   #
    yum info $side $packet_name |     # get packet info
        egrep '^(Version|Release)' |  # two parts of version
        cut -d: -f2 |                 # get only values
        tr -s \\n \  ;                # inline it onto one line
    echo;
}

downgrading_packets=$(
    for packet in $may_need_downgrade; do
        if [[ $(get_version available $packet) < $(get_version installed $packet) ]]; then
            echo $packet;
        fi                            # really need be downgradded
    done
)
[[ -n "$downgrading_packets" ]] && yum downgrade $downgrading_packets --assumeyes
