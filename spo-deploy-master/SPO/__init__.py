import logging
import requests
import paramiko
import re
import os
import time
from datetime import datetime, timedelta
from socket import gaierror
try:
  import socks
  socks_imported = True
except ImportError:
  socks_imported = False

def get_current_time(server='ru.pool.ntp.org'):
  import ntplib
  ntp_client = ntplib.NTPClient()
  ntp_servers = ['ru.pool.ntp.org', 'ntp5.stratum2.ru', '2.ru.pool.ntp.org', 'ntp4.stratum2.ru', 'ntp21.vniiftri.ru']
  for server in ntp_servers:
    try:
      response = ntp_client.request(server)
      return response
    except ntplib.NTPException:
      continue

class SPO(object):
  def __init__(self, **kwargs):
    """
    Create an object and authenticate on the host
    :param host: Host as IP or DNS
    """
    # If deploy_OK = False then there was found some error while checking
    self.deploy_OK = True

    self.sync = kwargs.get('sync')
    self.location = kwargs.get('location')
    self.remote = kwargs['remote'] if kwargs.get('remote') else False
    self.closed_contour = kwargs['closed_contour'] if kwargs.get('closed_contour') else False
    self.have_closed_contour = kwargs['have_closed_contour'] if kwargs.get('have_closed_contour') else False
    self.zones = kwargs['zones'] if kwargs.get('zones') else ['user']
    self.plugins = kwargs.get('plugins')

    # Don't check API if we check only global zone
    if self.zones == ['global']:
      self.noapi = True

    # Variables to do checks via api/ssh/web interfaces. 
    # By default we do ssh and api checks only
    self.noapi = kwargs.get('noapi')
    self.nossh = True if self.remote else kwargs.get('nossh')
    self.web = kwargs.get('web')

    # Create base lists of necessary processes and rpms for the software under test
    self.necessary_processes = []
    self.rpms = []
    if 'user' in self.zones:
      self.necessary_processes.append('nginx')
      self.rpms.extend(self.userzone_rpms)
    if 'global' in self.zones:
      self.rpms.extend(self.globalzone_rpms)

    # Set logins and passwords for API/ssh/web checks
    self.ssh_user = kwargs.get('ssh_user')
    self.ssh_pass = kwargs.get('ssh_pass')
    self.ssh_port = int(kwargs['ssh_port']) if kwargs.get('ssh_port') else 22
    self.web_user = kwargs['web_user'] if kwargs.get('web_user') else 'flygres'
    self.web_pass = kwargs['web_pass'] if kwargs.get('web_pass') else '1'

    # Set proxy string in the form like '10.1.48.19:5980'
    self.proxy = kwargs['proxy'] if kwargs.get('proxy') else None

    # Set redmine issue number, an API key and auxiliary variables
    # if it's necessary to post a result in Redmine.
    # For now (5.03.2019) it works only with web checks
    self.redmine_issue = int(kwargs['redmine_issue']) if kwargs.get('redmine_issue') else None
    self.redmine_key = kwargs['redmine_key'] if kwargs.get('redmine_key') else None
    self.problems = []
    self.screenshots = []


  # Base checklist. It's recommended to call this method from other scripts
  def check_deploy(self):
    if not self.nossh and self.auth_ssh():
      self.check_date()
      self.check_installed_RPMs()
      self.check_prosesses_running()
      self.check_EARs_deploy()
      self.check_disk_space()
      if self.sync and ('global' in self.zones):
        self.get_db_info()
        self.check_dbms_availability()
        self.check_db_version()
        self.check_sync()
    if ('user' in self.zones):
      if not self.noapi and self.auth_api():
        self.check_gis_availability()
        self.check_basic_requests()
        self.check_version()
      # For now we check only auth via web
      if self.web and self.auth_web():
        pass
    return self.deploy_OK


  # It's necessary to have virtual display to draw webbrowser's window so
  # here we start X frame buffer service
  def start_xvfb(self):
    try:
      from pyvirtualdisplay import Display
    except ImportError as e:
      self.report_error(e.message)
      print(u"You can install it with command 'pip install pyvirtualdisplay'")
      exit(1)
    self.display = Display(visible=0, size=(1680, 1050))
    self.display.start()


  # Just start browser to do web checks. This method doesn't work on
  # Zircon OS because of python 2.6 (Selenium isn't support it anymore)
  def start_browser(self):
    try:
      from selenium import webdriver
    except ImportError as e:
      self.report_error(e.message)
      print(u"You can install it with command 'pip install selenium'")
      print(u"WARNING! Selenium is not support python-2.6 anymore")
      exit(1)
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--ignore-certificate-errors')
    if self.proxy:
      chrome_options.add_argument('--proxy-server=socks4://' + self.proxy)
    self.browser = webdriver.Chrome(chrome_options=chrome_options)
    self.browser.set_window_size(1680, 1050)
    self.browser.delete_all_cookies()


  # Execute shell command via ssh and return stdout/err objects with
  # command stdout/err, exit code, etc.
  def run_ssh_command(self, command):
    """
    :returns: stdout, stderr of executed command
    """
    stdin, stdout, stderr = self.ssh_client.exec_command(command)
    _err = stderr.readlines()
    if len(_err) > 0:
      self.report_error(u"Shell command execution failed with RC {0}:\n{1}".format(stdout.channel.recv_exit_status(), "".join(l for l in _err)))
    return stdout, stderr


  # Methods to write messages to stdout and log file
  def report_error(self, msg, *args):
    self.deploy_OK = False
    logging.error(msg)
    self.problems.append("FAIL - " + msg)
    print("FAIL - " + msg)

  def report_warning(self, msg, *args):
    logging.warning(msg)
    self.problems.append("WARN - " + msg)
    print("WARN - " + msg)

  def report_info(self, msg):
    logging.info(msg)
    print("OK - " + msg)


  def auth_ssh(self):
    """
    Authenticate on the host via SSH
    """
    try:
      self.ssh_client = paramiko.SSHClient()
      self.ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
      if self.proxy:
        if not socks_imported:
          self.report_error("socks module not imported")
          return
        _proxy = re.search("(.*):(.*)", self.proxy)
        proxy_addr = _proxy.group(1)
        proxy_port = int(_proxy.group(2))
        try:
          sock = socks.socksocket()
          sock.set_proxy(proxy_type=socks.SOCKS4, addr=proxy_addr, port=proxy_port)
          sock.connect((self.host, self.ssh_port))
        except (socks.ProxyConnectionError, socks.GeneralProxyError) as e:
          self.report_error(e.msg + ". Maybe proxy is running on another port (tried port {0})?".format(proxy_port))
          exit(1)
        except Exception as e:
          exit(1)
        self.ssh_client.connect(hostname=self.host, username=self.ssh_user,
                                password=self.ssh_pass, port=self.ssh_port, sock=sock)
      else:
        self.ssh_client.connect(hostname=self.host, username=self.ssh_user,
                                password=self.ssh_pass, port=self.ssh_port)
      self.ssh_client.exec_command("export LANG=en_US.UTF-8")
    except paramiko.ssh_exception.AuthenticationException as e:
      self.report_error(u"""SSH {0} 
                    Host: {1}
                    Port: {2}
                    User: {3}
                    Password: {4}""".format(e.args[0], self.host, self.ssh_port, self.ssh_user, self.ssh_pass))
      print("SSH {0} Host: {1}".format(e.args[0], self.host))
      return False
    except paramiko.ssh_exception.NoValidConnectionsError as e:
      self.report_error(e.message)
      exit(1)
    except gaierror:
      self.report_error(u"Name or service {0} not known. If you try to connect to DNS name then maybe it's not specified in your /etc/hosts file?".format(self.host))
      exit(1)
    except Exception as e:
      self.report_error(e.message)
      exit(1)
    self.report_info(u"Auth via ssh")
    return True


  def auth_api(self):
    """
    Authenticate on the host via API
    :returns: False if auth was failed
    """
    self.session = requests.Session()
    if self.proxy:
      try: 
        self.session.proxies.update(dict(http='socks4://'+str(self.proxy)))
      except requests.packages.urllib3.exceptions.ProxySchemeUnknown:
        self.report_error(e.message)
        print(u"You can install it with command 'pip install -U requests[socks]'")
        exit(1)
    headers = {'Content-Type': 'application/json;charset=UTF-8'}
    try:
      self.session.get(self.get_cookies_url)
      self.cookies = self.session.cookies.get_dict()
      res = self.session.post(self.login_url, data=self.login_data, 
                        cookies=self.cookies, headers=headers)
      self.cookies = self.session.cookies.get_dict()
      res.raise_for_status()
    except requests.exceptions.ConnectionError as e:
      self.report_error(str(e.args[0]))
      exit(1)
    except requests.exceptions.RequestException as e:
      self.report_error(u"HTTP auth on the host {0} failed".format(self.host))
      if res and res.text:
        self.report_error(u"Response: {0}".format(res.text))
      return False
    self.report_info(u"Auth via API")
    return True


  def auth_web(self):
    self.start_xvfb()
    self.start_browser()
    from selenium.webdriver.common.keys import Keys
    from selenium.webdriver.common.by import By
    from selenium.webdriver.support.ui import WebDriverWait
    from selenium.webdriver.support import expected_conditions as EC
    from selenium.common.exceptions import TimeoutException
    try:
      self.browser.get(self.web_login_url)
      # Wait for username field to appear
      WebDriverWait(self.browser, timeout=30, poll_frequency=1).until(
                        EC.presence_of_element_located((By.ID, 'username')))
      try:
        username_field = self.browser.find_element_by_name('username')
        password_field = self.browser.find_element_by_name('password')
        username_field.send_keys(self.web_user)
        password_field.send_keys(self.web_pass)
        password_field.send_keys(Keys.ENTER)
        # Wait for table with entries to appear
        WebDriverWait(self.browser, timeout=60, poll_frequency=1).until(
                          EC.presence_of_element_located((By.TAG_NAME, 'tbody')))
      except TimeoutException:
        self.take_screenshot('web_auth.png')
        self.report_error(u"Login via web interface: timeout (60s) exceeded")
        return False
    except TimeoutException:
      self.take_screenshot('web_auth.png')
      self.report_error(u"Go to login page via web interface")
      return False
    self.take_screenshot('web_auth.png')
    self.report_info(u"Auth via web interface")
    return True
  

  def take_screenshot(self, name):
    if self.location:
      if self.closed_contour:
        nginx_share_path = '/usr/share/nginx/html/{0}_zk/'.format(self.location)
      else:
        nginx_share_path = '/usr/share/nginx/html/{0}_ok/'.format(self.location)
    else:
      nginx_share_path = '/usr/share/nginx/html/'
    screenshot_name = datetime.now().strftime("%Y%m%d_%H%M%S") + '_' + name
    if not os.path.exists(nginx_share_path) or not os.path.isdir(nginx_share_path):
      os.makedirs(nginx_share_path)
    screenshot_path = nginx_share_path + screenshot_name
    self.browser.save_screenshot(screenshot_path)
    self.screenshots.append(screenshot_name)


  def check_version(self):
    res = self.do_API_request(self.version_url)
    try:
      self.version = res.json()
    except AttributeError:
      self.report_error(u"Can't get software version - there is no json in server's answer")


  def do_API_request(self, uri, method=None, data=None):
    request = self.base_url + uri
    try:
      start = time.time()
      if method == None or method.upper() == 'GET':
        res = self.session.get(request, data=data, cookies=self.cookies, timeout=300)
      elif method.upper() == 'POST':
        res = self.session.post(request, data=data, cookies=self.cookies, timeout=300)
      elif method.upper() == 'DELETE':
        res = self.session.delete(request, data=data, cookies=self.cookies, timeout=300)
      response_time = time.time() - start
      res.raise_for_status()
      self.report_info(u"Request: {0}. Response time: {1:.2f} s".format(request, response_time))
    except requests.HTTPError as e:
      self.report_error(u"""Request FAILED: {0}
      Code: {1}
      Response: {2}
      Response Headers: {3}
      Response Time: {4:.2f} s""".format(request, res.status_code, res.text, res.headers, response_time))
      return False
    except requests.exceptions.RequestException as e:
      self.report_error(u"Request FAILED: {0}".format(request))
      self.deploy_OK = False
      return False
    except AttributeError as e:
      self.report_error(u"AttributeError. Request: {0}".format(request))
      return False
    # self.report_info(u"Request OK: {0}. Response time: {1:.2f} s".format(request, response_time))
    return res


  def do_API_requests(self, requests_list):
    """
    Get a list if dicts and do API requests for every entry
    :param requests_dict: list of dicts with urls, methods(opt), data(opt)
    """
    for entry in requests_list:
      self.do_API_request(entry.get('url'), entry.get('method'), entry.get('data'))


  def check_gis_availability(self):
    try:
      res = self.session.get(self.gis_url, timeout=5)
      res.raise_for_status()
    except requests.exceptions.RequestException as e:
      self.report_error(u"""GIS {0} is not available""".format(self.gis_url))
      return False
    self.report_info(u"GIS: {0}".format(self.gis_url))


  def check_prosesses_running(self):
    for p in self.necessary_processes:
      out, err = self.run_ssh_command('ps ax | grep {0}'.format("[" + p[:1] + "]" + p[1:]))
      if not out.channel.recv_exit_status() == 0:
        self.report_error(u"{0} is not running".format(p))
      else:
        self.report_info(u"{0} is running".format(re.sub('.*\.\*', '', p)))


  def check_EARs_deploy(self):
    out, err = self.run_ssh_command('ls {0} | grep failed'.format(self.deployments_path))
    if out.channel.recv_exit_status() == 0:
      for o in out.readlines():
        self.report_error(u'EARs deployment. Found failed deployment: {0}'
                            .format(re.sub('.failed\n', '', o)))
    else:
      self.report_info(u"EARs deployment")


  def check_installed_RPMs(self):
    result = True
    for rpm in self.rpms:
      out, err = self.run_ssh_command('rpm -q {0}'.format(rpm))
      if not out.channel.recv_exit_status() == 0:
        self.report_error(u'RPM is not installed: {0}'.format(rpm))
        result = False
      else:
        self.report_info(u'RPM {0} installed'.format(re.sub('\n', '', out.readline())))


  def check_disk_space(self):
    out, err = self.run_ssh_command('df -P -l -h | egrep -v "Filesystem|/dev/sr|/dev/cdrom/|\.iso"')
    for line in out.readlines()[1:]:
      partition_info = re.split("\s+", line)
      if int(partition_info[4].strip('%')) > 95:
        self.report_error(u"Disk partition {0} mounted in {1} have >95% of space in use"
          .format(partition_info[0], partition_info[5]))
      elif int(partition_info[4].strip('%')) > 80:
        self.report_warning(u"Disk partition {0} mounted in {1} have >80% of space in use"
          .format(partition_info[0], partition_info[5]))
      else:
        self.report_info(u"Disk partition {0} mounted in {1} have enough free space"
          .format(partition_info[0], partition_info[5]))

  
  def check_basic_requests(self):
    for entry in self.basic_requests:
      self.do_API_request(entry.get('url'), 
                          method=entry.get('method'), 
                          data=entry.get('url'))


  def post_result_to_redmine(self):
    try:
      from redminelib import Redmine, exceptions
    except ImportError as e:
      self.report_error(e.message)
      print(u"You can install it with command 'pip install python-redmine'")
      exit(1)
    from urllib3 import disable_warnings
    disable_warnings()
    redmine = Redmine('https://redmine.swemel.grp', 
                      requests={'verify': False}, 
                      key=self.redmine_key)
    try:
      issue = redmine.issue.get(self.redmine_issue)
    except redminelib.exceptions.ResourceNotFoundError as e:
      print('Please specify Redmine issue ID')
      exit(1)
    if self.deploy_OK:
      issue.notes = u"OK"
    else: 
      issue.notes = u"Problems:\n"
      issue.notes += "\n".join(p for p in self.problems)
    if self.screenshots != []:
      issue.notes += "\n"
      issue.notes += "\n".join("!http://10.1.48.29/{0}!".format(s) for s in self.screenshots)
    issue.save()


  def check_dbms_availability(self):
    out, err = self.run_ssh_command("nc -z {0} 5432".format(self.db_host))
    if out.channel.recv_exit_status() == 0:
      self.report_info("DBMS is available")
    else:
      self.report_error("DBMS on {0} is unavailable".format(self.db_host))

  
  def execute_query(self, query):
    out, err = self.run_ssh_command("export PGPASSWORD={0}; psql -h {1} -d {2} -U {3} -q -A -X -t -c \"{4}\"".format(self.db_password, self.db_host, self.db_name, self.db_username, query))
    rc = out.channel.recv_exit_status()
    out = "".join(re.sub('\n', '', l) for l in out.readlines())
    return out, rc

  
  def execute_query_raw(self, query):
    out, err = self.run_ssh_command("export PGPASSWORD={0}; psql -h {1} -d {2} -U {3} -q -A -X -t -c \"{4}\"".format(self.db_password, self.db_host, self.db_name, self.db_username, query))
    out = [re.sub('\n', '', l) for l in out.readlines()]
    return out


  def get_remote_date(self):
    out, err = self.run_ssh_command("date -u +'%Y-%m-%d %H:%M:%S'")
    stdout = out.readlines()
    return datetime.strptime(re.sub('\n', '', stdout[0]), "%Y-%m-%d %H:%M:%S")


  def check_date(self):
    current_time = get_current_time()
    real_time = datetime.utcfromtimestamp(current_time.tx_time)
    remote_date = self.get_remote_date()
    delta = real_time - remote_date
    delta_sec = delta.total_seconds()
    if delta_sec > 60 or delta_sec < -60:
      self.report_error("Date is incorrect. Diff in seconds: {0}".format(delta_sec))
    elif delta_sec > 5 or delta_sec < -5:
      self.report_warning("Date is incorrect. Diff in seconds: {0}".format(delta_sec))
    else:
      self.report_info("Date is correct, diff less than 5 seconds")


  def __del__(self):
    try:
      self.session.close()
    except:
      pass
    try:
      self.ssh_client.close()
    except:
      pass
    try:
      self.browser.quit()
    except:
      pass
    try:
      self.display.stop()
    except:
      pass
    


class flygres(SPO):
  def __init__(self, **kwargs):
    self.qtransport_port = 8001
    self.host = kwargs.get('host')
    self.base_url = "http://{0}/".format(self.host)
    # GIS URL is not affected by 'remote' parameter, we always get tiles from local GIS
    self.gis_url = self.base_url + 'gis/tms/1.0.0'
    self.deployments_path = '/opt/wildfly10/standalone/deployments/'
    self.userzone_rpms =   ['wildfly10',
                          'spo-web-arctic-chat',
                          'spo-reports-generator',
                          'flygres-app-min']
    self.globalzone_rpms = ['wildfly10',
                          'spo-flygres-db-update']
    self.basic_requests = [
      { 'url': 'arctic/api/resources' },
      { 'url': 'arctic/api/attributes' },
      { 'url': 'arctic/api/dom/elements' }
    ]
    super(flygres, self).__init__(**kwargs)
    self.necessary_processes.append("java.*wildfly10")
    if 'user' in self.zones:
      self.necessary_processes.append("mono.*FastReport")
      self.necessary_processes.append("Xvfb")
    if self.sync and ('global' in self.zones):
      self.necessary_processes.append("qtransport")
      if self.closed_contour or self.have_closed_contour:
        self.necessary_processes.append("owt-server")
    if self.remote:
      self.base_url += 'remote/'
    self.login_url = self.base_url + 'arctic/api/identity/login'
    self.version_url = 'arctic/api/info/version'
    # Check ZK portal in closed contour or VS portal in open contour or MVS if we do remote check
    if self.closed_contour:
      self.web_login_url = self.base_url + 'zk'
      try:
        self.rpms.remove('spo-web-arctic-chat')
      except ValueError:
        pass
    elif self.remote or self.web:
      self.web_login_url = self.base_url + 'mvs'
    else:
      self.web_login_url = self.base_url + 'vs'

    # Add sync-transport packages to check if special arguments were specified
    if self.sync and ('global' in self.zones):
      self.rpms.append("qtransport")
      if self.closed_contour or self.have_closed_contour:
        self.rpms.append("owt-server")
    for plugin in self.plugins:
      self.rpms.append('flygres-plugin-' + plugin)


  def auth_api(self):
    self.get_cookies_url = self.base_url + 'arctic/api/identity/getUser'
    self.login_data = u'{{"username":"{0}","password":"{1}"}}'.format(self.web_user, self.web_pass)
    return super(flygres, self).auth_api()


  def check_version(self):
    super(flygres, self).check_version()
    frontend_version_url = 'static/version.json'
    res = self.do_API_request(frontend_version_url)
    try:
      self.version.update(res.json())
      self.report_info(u"""Frontend version: {0}   Backend version: {1}   DB version: {2}""".format(self.version['version'], self.version['project-version'], self.version['db-version']))
    except AttributeError:
      self.report_error(u"Can't get software version")


  def get_db_info(self):
    out, err = self.run_ssh_command("egrep '(connection-url|user-name|password)' {0}flygres-ds.xml".format(self.deployments_path))
    if out.channel.recv_exit_status() == 0:
      conf = out.readlines()
      self.db_host = re.search('.*postgresql:\/\/(.*):5432.*', conf[0]).group(1)
      self.db_name = re.search('.*:5432\/(.*)<\/connection-url>', conf[0]).group(1)
      self.db_username = re.search('.*<user-name>(.*)<\/user-name>.*', conf[1]).group(1)
      self.db_password = re.search('.*<password>(.*)<\/password>.*', conf[2]).group(1)
    else:
      self.report_error("Can't get DB info from flygres-ds.xml")


  def check_db_version(self):
    out, rc = self.execute_query("SELECT max(id) from liquibase.databasechangelog;")
    if rc == 0:
      self.report_info("DB version based on databasechangelog: {0}".format(out))
    else:
      self.report_error("Can't get DB version. RC: {0}".format(rc))


  def check_sync(self):
    self.check_sync_version()
    self.check_sync_ip_constant()
    self.check_sync_nodes_count()
    self.check_sync_nodes_availability()
    self.check_sync_in_count()
    self.check_sync_out_count()
    self.check_sync_out_not_validated_count()
    self.check_sync_queues()
    self.check_sync_limit_rows()
    self.check_sync_reglaments_count()
    self.check_sync_null_mask_reglaments_count()
    self.check_sync_long_queries()
    # Do this check for every host to be sure that sending data to owt-server
    # was set only on right hosts
    self.check_sync_reglaments_owt()
    self.check_sync_qtransport_configuration()
    self.check_sync_qtransport_status()
    self.check_sync_qtransport_port()
    if self.closed_contour or self.have_closed_contour:
      self.check_sync_owt_nodes_count()
      self.check_sync_owt_configuration()

  
  def check_sync_version(self):
    out, rc = self.execute_query("SELECT obj_description('sync.constants'::regclass, 'pg_class');")
    self.report_info("Sync version based on sync.constants: {0}".format(out))

  
  def check_sync_ip_constant(self):
    ip_const, rc = self.execute_query("SELECT sync.func_get_constant('current_ip');")
    if ip_const in [ '', '127.0.0.1', 'localhost' ]:
      self.report_error("Sync current IP constant incorrect: {0}".format(ip_const))
    else:
      self.report_info("Sync current IP constant: {0}".format(ip_const))

  
  def check_sync_owt_nodes_count(self):
    owt_nodes_count, rc = self.execute_query("SELECT count(*) FROM sync.sync_node WHERE node_bit = 0 AND id = 1 AND ip IS NULL AND is_closed = TRUE;")
    if len(owt_nodes_count) > 0 and int(owt_nodes_count) == 1:
      self.report_info("Sync OWT node specified")
    else:
      self.report_error("Sync OWT nodes specified: {0}".format(owt_nodes_count))

  
  def check_sync_reglaments_count(self):
    reglaments_count, rc = self.execute_query("SELECT COUNT(*) FROM sync.sync_reglament;")
    if len(reglaments_count) > 0 and int(reglaments_count) == 0:
      self.report_error("Sync reglaments are not specified")
    else:
      self.report_info("Sync reglaments count: {0}".format(reglaments_count))

  
  def check_sync_null_mask_reglaments_count(self):
    null_mask_reglaments_count, rc = self.execute_query("SELECT COUNT(*) reglament_without_mask FROM sync.sync_reglament where out_node_mask = '0'::bit(128);")
    if int(null_mask_reglaments_count) == 0:
      self.report_info("Sync null-mask reglaments are not specified")
    else:
      self.report_warning("Sync null-mask reglaments count: {0}".format(null_mask_reglaments_count))

  
  def check_sync_out_count(self):
    outlines = self.execute_query_raw("SELECT info.node_id, count(*) FROM sync.sync_out out INNER JOIN sync.sync_out_info info on out.id = info.sync_out_id and info.status_id = 1 group by node_id;")
    for l in outlines:
      node, count = l.split('|')
      self.report_info("Sync entries in sync_out. node: {0} count: {1}".format(node, count))

  
  def check_sync_out_not_validated_count(self):
    not_validated_count, rc = self.execute_query("SELECT count(*) FROM sync.sync_out WHERE is_validate=false;")
    self.report_info("Sync entries in sync_out. not validated: {0}".format(not_validated_count))


  def check_sync_queues(self):
    out, rc = self.execute_query("""select (select count(*) from 
    sync.sync_out out inner join sync.sync_out_info info on out.id = info.sync_out_id 
    where status_id != 2) out_count, (select count(*) from sync.sync_in) in_count, 
    (select max(create_date) from sync.sync_log where description like '%func_sync_in%') 
    last_sync_time""")
    out_count, in_count, last_sync_time = out.split('|')
    self.report_info("Sync queue in: {0}".format(in_count))
    self.report_info("Sync queue out: {0}".format(out_count))
    self.report_info("Sync last time: {0}".format(last_sync_time))

  
  def check_sync_in_count(self):
    in_count, rc = self.execute_query("SELECT count(*) FROM sync.sync_in;")
    self.report_info("Sync entries in sync_in count: {0}".format(in_count))

  
  def check_sync_limit_rows(self):
    limit_rows = self.execute_query_raw("SELECT ip,limit_rows FROM sync.sync_node WHERE ip IS NOT NULL OR is_closed = TRUE;")
    if len(limit_rows) == 0:
      self.report_warning("Sync limit_rows are not specified")
    else:
      self.report_info("Sync limit_rows entries count: {0}".format(len(limit_rows)))

  
  def check_sync_nodes_count(self):
    sync_nodes_count = self.execute_query_raw("SELECT ip FROM sync.sync_node WHERE ip IS NOT NULL AND group_type IS NOT NULL;")
    if len(sync_nodes_count) == 0:
      self.report_error("Sync nodes are not specified")
    else:
      self.report_info("Sync nodes:\n{0}".format(", ".join(re.sub('\n', '', l) for l in sync_nodes_count)))


  # Check if there are reglaments with user-related resources.
  # Check will be failed when they are specified because we don't send user info
  # from open contour the a closed one
  def check_sync_reglaments_owt(self):
    reglaments = self.execute_query_raw("select out_node_mask from sync.sync_reglament where table_name in ('3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2', '6d13d494-7bf5-4d11-9f32-424c4f8b6450');")
    reglaments_with_zero_bit = False
    for reg in reglaments:
      if reg[0] == '1':
        reglaments_with_zero_bit = True
    if reglaments_with_zero_bit: 
      if self.have_closed_contour:
        self.report_error("Sync users reglaments are marked to be send to owt")
      else:
        self.report_warning("Sync users reglaments are marked to be send to owt but fortunately we don't have ZK")
    else:
      self.report_info("Sync users reglaments aren't marked to be send to owt")


  def check_sync_nodes_availability(self):
    ips = self.execute_query_raw("SELECT ip FROM sync.sync_node where not ip='';")
    for ip in ips:
      out, err = self.run_ssh_command("nc -z {0} {1}".format(ip, self.qtransport_port))
      _availability = True
      if out.channel.recv_exit_status() != 0:
        self.report_error("Sync qtransport node {0} is unavailable from {1}".format(ip, self.host))
        _availability = False
    else:
      return
    if _availability:
      self.report_info("Sync qtransport nodes are available")

  
  def check_sync_owt_configuration(self):
    out, err = self.run_ssh_command("egrep -v '(^#|^\s*$)' /etc/sysconfig/owt-server")
    config = out.readlines()
    self.report_info("Sync owt config:\n{0}".format(''.join(l for l in config)))
    
    out, err = self.run_ssh_command("egrep -v '(^#|^\s*$)' /opt/wildfly10/standalone/configuration/owt.properties")
    properties = out.readlines()
    self.report_info("Sync owt properties:\n{0}".format(''.join(l for l in properties)))


  def check_sync_qtransport_status(self):
    o,e = self.run_ssh_command("service qtransport status")
    if not o.channel.recv_exit_status() == 0:
      self.report_error(u'Sync qtransport service is down: {0}'.format(re.sub('\n', '', o.readline())))
    else:
      self.report_info(u'Sync qtransport service is up: {0}'.format(re.sub('\n', '', o.readline())))


  def check_sync_qtransport_port(self):
    out, err = self.run_ssh_command("netstat -ntulp | grep 'qtransport' | awk '{print $4}' | cut -d: -f2")
    port = "".join(l for l in out.readlines())
    if len(port) > 0 and int(port) == self.qtransport_port:
      self.report_info("Sync qtransport is listening on {0} port".format(re.sub('\n', '', port)))
    else:
      self.report_error("Sync qtransport not running on {0} port".format(self.qtransport_port))

  
  def check_sync_qtransport_configuration(self):
    out, err = self.run_ssh_command("egrep -v '(^#|^\s*$)' /etc/qtransport/qtransport.ini")
    config = [re.sub('\n', '', l) for l in out.readlines()]
    result = True
    keys = ['host', 'port', 'database', 'role', 'password', 'receive_port']
    values = [ self.db_host, '5432', self.db_name, self.db_username, self.db_password, str(self.qtransport_port) ]
    for l in config:
      key_value = l.split('=')
      for k, v in zip(keys, values):
        if (key_value[0] == k and not key_value[1] == v):
          self.report_error("Sync qtransport config is incorrent. These is a problem:\n{0}".format(l))
          result = False
          continue
    if result:
      self.report_info("Sync qtransport config is correct")

  
  # 'g' in this query means 'granted lock' and when some query waiting for the end
  # of execution of another query then there is a problem
  def check_sync_long_queries(self):
    executing_queries = self.execute_query_raw("""select distinct query, query_start, state, g from 
      (select
        substring(nsp.nspname, 0, 10) as schemn,
        substring(r.relname, 0, 20) as tablen,
        l.mode, l.granted g,
        a.query_start, a.state,
        replace(substring(replace(replace(replace(a.query, ' ', ''), 'schema_name', ''), 'table_name', ''), 15, 100), '_id::text,json_data::text,order_num,status_id,row_info::text', '') as query
      from pg_locks l
        join pg_class r on l.relation = r.oid
        join pg_namespace nsp on r.relnamespace = nsp.oid
        join pg_stat_activity a on l.pid = a.pid where a.query like '%%sync%%' and a.pid <> pg_backend_pid()) a;""")
    local_date = self.get_remote_date()
    max_delta = timedelta(minutes=15)
    result = True
    for q in executing_queries:
      entry = q.split('|')
      query_start_date = datetime.strptime(entry[1][:19], "%Y-%m-%d %H:%M:%S")
      if (local_date - query_start_date) > max_delta:
        if entry[3] == 'f':
          self.report_error("Sync query started more than 15 minutes ago and still waiting for granted lock: {0}".format(entry[0]))
          result = False
        else:
          self.report_warning("Sync query started more than 15 minutes ago: {0}".format(entry[0]))
          result = False
    if result:
      self.report_info("Sync queries are executing less than 15 minutes")



class frontier(SPO):
  def __init__(self, **kwargs):
    self.host = kwargs.get('host')
    self.base_url = "http://{0}/frontier/".format(self.host)
    self.web_login_url = self.base_url + 'auth.html'
    self.get_gis_url = self.base_url + 'api/properties/string/key/gis.url'
    self.version_url = 'universal?qualifier=build_number'
    self.deployments_path = '/opt/wildfly8/standalone/deployments/'
    self.userzone_rpms =   ['wildfly8',
                          'frontier-user-app',
                          'classifier-provider-app']
    self.globalzone_rpms = ['wildfly8',
                          'swm_spo_db_update_frontier_app']
    self.basic_requests = [
      { 'url': 'api/bell/counts' }
    ]
    super(frontier, self).__init__(**kwargs)
    self.necessary_processes.append("java.*wildfly8")
    if self.sync:
      self.rpms.append('data-exchange-global-app')
    

  def auth_api(self):
    self.get_cookies_url = self.base_url + 'universal?qualifier=authenticator'
    self.login_url = self.base_url + 'universal?action=login&qualifier=authenticator'
    self.login_data = '{"uid":"c28ff541-f6d6-4b96-bff5-09ca4d201fac"}'
    return super(frontier, self).auth_api()


  def check_gis_availability(self):
    try:
      res = self.session.get(self.get_gis_url)
      res.raise_for_status()
      self.gis_url = res.json()['value'] + '/tms/data'
      if self.gis_url== 'http://127.0.0.1/giscalc/tms/data':
        self.gis_url = "http://{0}/giscalc".format(self.host) + '/tms/data'
    except requests.exceptions.RequestException as e:
      self.report_error(u"""Can't get gis.url value. Request: {0}""".format(self.get_gis_url))
      return False
    super(frontier, self).check_gis_availability()

  
  def check_version(self):
    super(frontier, self).check_version()
    try: 
      self.report_info(u"SPO version: {0}.{1}".format(self.version['gitVersion'], 
                                                self.version['gitRelease']))
    except AttributeError:
      self.report_error(u"Can't get software answer")



# Differ from 'frontier' only in rpms
class frontierArctic(frontier):
  def __init__(self, **kwargs):
    super(frontierArctic, self).__init__(**kwargs)
    self.userzone_rpms =   ['wildfly8',
                          'frontier-user-arctic',]
    self.globalzone_rpms = ['wildfly8',
                          'swm_spo_db_update_frontier_arctic']
    if 'user' in self.zones:
      self.rpms = self.userzone_rpms
    if 'global' in self.zones:
      self.rpms = self.globalzone_rpms
    if self.sync:
      self.rpms.append('data-exchange-global-arctic')
