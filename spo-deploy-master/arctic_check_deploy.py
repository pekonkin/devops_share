import subprocess
import logging
import os
import argparse


parser = argparse.ArgumentParser()
parser.add_argument("-a", "--api", dest="full_check", default=False, 
                    help="Must be specified to do all checks including api checks. Default: False", action="store_true")
parser.add_argument("--mvs", dest="mvs_only", default=False, 
                    help="Must be specified to do check of Murmansk MVS portal availability. Default: False", action="store_true")
parser.add_argument("-s", "--sync", "--sync_only", dest="sync_only", default=False, 
                    help="Must be specified to do only sync checks. Default: False", action="store_true")
parser.add_argument("--sync_out", "--sync_out_only", dest="sync_out_only", default=False, 
                    help="Must be specified to do only sync_out checks. Default: False", action="store_true")
parser.add_argument("--sync_brief", dest="sync_brief", default=False, 
                    help="Must be specified to do only brief sync checks. Default: False", action="store_true")
args = parser.parse_args()


script_path = os.path.dirname(os.path.abspath( __file__ )) + '/spo_deploy_checker.py'
proxy_server = '10.1.48.19'
web_pass = '12345678'
logfile = 'arctic_deploy.log'


logging.basicConfig( \
    format = u'%(asctime)s %(levelname)s - %(message)s', \
    filename = logfile, \
    level = logging.INFO)


destinations_ok = [
  {
    "location": "PPK",
    "proxy_port": "5172",
    "proxy_port_term": "51721",
    "plugins": [ 'sdno', 'breeze', 'esimo', 'osm-far' ], 
    "host": "192.168.172.131"
  },
  {
    "location": "Magadan",
    "proxy_port": "5175",
    "proxy_port_term": "51751",
    "plugins": [ 'sdno' ], 
    "host": "192.168.175.3"
  },
  {
    "location": "Anadyr",
    "proxy_port": "5178",
    "proxy_port_term": "51781",
    "plugins": [ 'sdno' ], 
    "host": "192.168.178.3"
  },
  {
    "location": "Elizovo",
    "proxy_port": "5179",
    "proxy_port_term": "51791",
    "plugins": [ 'sdno' ], 
    "host": "192.168.179.1"
  },
  {
    "location": "Lavrentia",
    "proxy_port": "5169",
    "plugins": [ 'sdno' ], 
    "host": "192.168.169.1"
  },
  {
    "location": "Provideniya",
    "proxy_port": "5168",
    "plugins": [ 'sdno' ], 
    "host": "192.168.168.1"
  },
  {
    "location": "Oktyabrskiy",
    "proxy_port": "5167",
    "plugins": [ 'sdno' ], 
    "host": "192.168.167.1"
  },
  {
    "location": "Kopi",
    "proxy_port": "5166",
    "plugins": [ 'sdno' ], 
    "host": "192.168.166.1",
    "only_ok": True
  },
  {
    "location": "Pevek",
    "proxy_port": "5165",
    "plugins": [ 'sdno' ], 
    "host": "192.168.165.1",
    "only_ok": True
  },
  {
    "location": "Murmansk",
    "proxy_port": "5072",
    "plugins": [ 'sdno', 'breeze', 'esimo', 'osm-far' ], 
    "host": "192.168.72.90"
  },
  {
    "location": "Arhangelsk",
    "proxy_port": "5075",
    "plugins": [ 'sdno' ], 
    "host": "192.168.75.3"
  },
  {
    "location": "Salehard",
    "proxy_port": "5076",
    "plugins": [ 'sdno' ], 
    "host": "192.168.76.1"
  },
  {
    "location": "Dudinka",
    "proxy_port": "5077",
    "plugins": [ 'sdno' ], 
    "host": "192.168.77.1"
  },
  {
    "location": "Liinahamari",
    "proxy_port": "5078",
    "plugins": [ 'sdno' ], 
    "host": "192.168.78.1"
  },
  {
    "location": "Teriberka",
    "proxy_port": "5079",
    "plugins": [ 'sdno' ], 
    "host": "192.168.79.1"
  },
  {
    "location": "NaryanMar",
    "proxy_port": "5069",
    "plugins": [ 'sdno' ], 
    "host": "192.168.69.1"
  },
  {
    "location": "Sabetta",
    "proxy_port": "5068",
    "plugins": [ 'sdno' ], 
    "host": "192.168.68.1"
  },
  {
    "location": "Polyarniy",
    "proxy_port": "5067",
    "plugins": [ 'sdno' ], 
    "host": "192.168.67.1",
    "only_ok": True
  },
  {
    "location": "Nagurskoe",
    "proxy_port": "5066",
    "plugins": [ 'sdno' ], 
    "host": "192.168.66.1",
    "only_ok": True
  },
  {
    "location": "Sredniy",
    "proxy_port": "5065",
    "plugins": [ 'sdno' ], 
    "host": "192.168.65.1",
    "only_ok": True
  },
  {
    "location": "PSKR_Mukmansk",
    "proxy_port": "5064",
    "plugins": [ 'sdno' ], 
    "host": "192.168.64.1",
    "only_ok": True
  },
  {
    "location": "Dikson",
    "proxy_port": "5063",
    "plugins": [ 'sdno' ], 
    "host": "192.168.63.1",
    "only_ok": True
  }
]

# We don't have an access to some ZK so I've commented entries which describe them
destinations_zk = [
  # {
  #   "location": "Murmansk",
  #   "proxy_port": "5034",
  #   "plugins": [ 'sdno' ], 
  #   "host": "192.168.22.8",
  #   "host_gz": "192.168.34.12"
  # },
  # {
  #   "location": "Arhangelsk",
  #   "proxy_port": "5090",
  #   "plugins": [ 'sdno' ], 
  #   "host": "192.168.75.3",
  #   "host_gz": "192.168.90.5"
  # },
  # {
  #   "location": "Salehard",
  #   "proxy_port": "5095",
  #   "plugins": [ 'sdno' ], 
  #   "host": "192.168.76.1",
  #   "host_gz": "192.168.95.4"
  # },
  # {
  #   "location": "Dudinka",
  #   "proxy_port": "5102",
  #   "plugins": [ 'sdno' ], 
  #   "host": "192.168.77.1",
  #   "host_gz": "192.168.102.4"
  # },
  # {
  #   "location": "Liinahamari",
  #   "proxy_port": "5106",
  #   "plugins": [ 'sdno' ], 
  #   "host": "192.168.78.1",
  #   "host_gz": "192.168.106.4"
  # },
  # {
  #   "location": "Teriberka",
  #   "proxy_port": "5110",
  #   "plugins": [ 'sdno' ], 
  #   "host": "192.168.79.1",
  #   "host_gz": "192.168.110.4"
  # },
  # {
  #   "location": "NaryanMar",
  #   "proxy_port": "5114",
  #   "plugins": [ 'sdno' ], 
  #   "host": "192.168.69.1",
  #   "host_gz": "192.168.114.4"
  # },
  # {
  #   "location": "Sabetta",
  #   "proxy_port": "5118",
  #   "plugins": [ 'sdno' ], 
  #   "host": "192.168.68.1",
  #   "host_gz": "192.168.118.4"
  # },
  {
    "location": "PPK",
    "proxy_port": "5134",
    "plugins": [ 'sdno' ], 
    "host": "192.168.126.8",
    "host_gz": "192.168.134.12"
  },
  {
    "location": "Magadan",
    "proxy_port": "5190",
    "plugins": [ 'sdno' ], 
    "host": "192.168.182.3",
    "host_gz": "192.168.190.5"
  },
  {
    "location": "Anadyr",
    "proxy_port": "5205",
    "plugins": [ 'sdno' ], 
    "host": "192.168.197.3",
    "host_gz": "192.168.205.5"
  },
  {
    "location": "Elizovo",
    "proxy_port": "5212",
    "plugins": [ 'sdno' ], 
    "host": "192.168.208.1",
    "host_gz": "192.168.212.4"
  },
  {
    "location": "Oktyabrskiy",
    "proxy_port": "5230",
    "plugins": [ 'sdno' ], 
    "host": "192.168.229.1",
    "host_gz": "192.168.230.3"
  },
  {
    "location": "Lavrentia",
    "proxy_port": "5219",
    "plugins": [ 'sdno' ], 
    "host": "192.168.215.1",
    "host_gz": "192.168.219.4"
  },
  {
    "location": "Provideniya",
    "proxy_port": "5226",
    "plugins": [ 'sdno' ], 
    "host": "192.168.222.1",
    "host_gz": "192.168.226.4"
  }
]

def test_sync(msg):
  try:
    test = SPO.flygres(**test_arguments)
    test.report_info(msg)
    # When sync_out_only specified then we just check that we can connect
    # via SSH, have access to DBMS and make a few checks related to sync_out
    test.auth_ssh()
    test.get_db_info()
    test.check_dbms_availability()
    if args.sync_out_only:
      test.check_sync_out_count()
      test.check_sync_out_not_validated_count()
    elif args.sync_brief:
      test.check_sync_out_count()
      test.check_sync_out_not_validated_count()
      test.check_sync_in_count()
      test.check_sync_qtransport_status()
      test.check_sync_qtransport_port()
    else:
      test.check_db_version()
      test.check_sync()
    print("----------")
  except KeyboardInterrupt:
    logging.info("Interrupted")
    print("Interrupted")
    exit(0)
  except SystemExit as e:
    pass
  except Exception as e:
    try:
      print(e.message)
    except AttributeError:
      print(e)

### Tests of sync without API/OS tests
if args.sync_only or args.sync_out_only or args.sync_brief:
  import SPO
  # SPO subclasses get these arguments in their constructors
  default_test_arguments = {
    'zones': ['global'],
    'ssh_user': 'root',
    'ssh_pass': '12345678',
    'noapi': True,
    'sync': True,
    'plugins': []
  }
  for dest in destinations_ok:
    # Copy default_test_arguments dict and add necessary key-values:
    # have_closed_contour - when ZK is present;
    # host with global and user zone in OK is the same.
    test_arguments = {}
    test_arguments.update(default_test_arguments)
    test_arguments['have_closed_contour'] = False if dest.get('only_ok') else True
    test_arguments['host'] = dest.get('host')
    test_arguments['proxy'] = proxy_server+':'+dest.get('proxy_port')
    test_sync("Start testing sync for OK {0}".format(dest.get('location')))
  for dest in destinations_zk:
    # Copy default_test_arguments dict and add necessary key-values:
    # have_closed_contour - when ZK is present;
    # host with global and user zone in OK is the same.
    test_arguments = {}
    test_arguments.update(default_test_arguments)
    test_arguments['closed_contour'] = True
    test_arguments['host'] = dest.get('host_gz')
    test_arguments['proxy'] = proxy_server+':'+dest.get('proxy_port')
    test_sync("Start testing sync for ZK {0}".format(dest.get('location')))
  exit(0)


### Tests including API testing
for dest in destinations_ok:
  if (args.full_check or args.mvs_only) and dest.get('location') != 'Murmansk':
    logging.info("Start testing remote MVS portal for {0}".format(dest.get('location')))

    subprocess.call("""python {0} \
    --location {1} \
    --proxy {2}:{3} \
    --dest_host {4} \
    --web_pass {5} \
    --logfile {6} \
    --remote""".format(script_path, dest.get('location'), 
                    proxy_server, dest.get('proxy_port'),
                    dest.get('host'), web_pass, logfile), shell=True)
    logging.info("----------------------------------------------\n")
  if not args.mvs_only:
    logging.info("Start testing local VS portal for {0}".format(dest.get('location')))
    plugins = " ".join(p for p in dest.get('plugins'))
    subprocess.call("""python {0} \
    --location {1} \
    --proxy {2}:{3} \
    --dest_host {4} \
    --web_pass {5} \
    --logfile {6} \
    --plugins {7} \
    --sync {8} {9} --zones global user""".format(script_path, dest.get('location'), 
                    proxy_server, dest.get('proxy_port'),
                    dest.get('host'), web_pass, logfile, plugins, '' if dest.get('only_ok') else '--okzk', '' if args.full_check else '--noapi'), shell=True)
    logging.info("----------------------------------------------\n")


if not args.mvs_only:
  for dest in destinations_zk:
    logging.info("Start testing ZK portal for {0}".format(dest.get('location')))
    plugins = " ".join(p for p in dest.get('plugins'))
    subprocess.call("""python {0} \
    --location {1} \
    --proxy {2}:{3} \
    --dest_host {4} \
    --web_pass {5} \
    --logfile {6} \
    --zk {7}""".format(script_path, dest.get('location'), 
                    proxy_server, dest.get('proxy_port'),
                    dest.get('host'), web_pass, logfile, '' if args.full_check else '--noapi'), shell=True)
    logging.info("----------------------------------------------\n")

    logging.info("Start testing ZK global zone for {0}".format(dest.get('location')))
    subprocess.call("""python {0} \
    --location {1} \
    --proxy {2}:{3} \
    --dest_host {4} \
    --logfile {5} \
    --plugins {6} \
    --sync --zk --noapi --zones global""".format(script_path, dest.get('location'), 
                    proxy_server, dest.get('proxy_port'),
                    dest.get('host_gz'), logfile, plugins), shell=True)
    logging.info("----------------------------------------------\n")
logging.info("\n\n\n")
