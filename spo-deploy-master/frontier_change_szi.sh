#!/bin/bash

# lowercase for all variables
ZONE=$(echo $1 | tr A-Z a-z)

help() {
  echo "Usage:
	bash $0 [zone]
Info:
	https://redmine.swemel.grp/projects/spo/wiki/HOWTO"
  exit
}

fail() {
  echo FAIL
  [ "$1" != "" ] && echo $1
  exit 1
}

[ "$1" == "" ] && help


printf "Check if RPM package orm-${ZONE} present in our repo....."
yum clean all -q
yum list orm-${ZONE} &>/dev/null && echo OK || fail "Package doesn't exists, check zone name or repo conf"


OLD_ORM=$(rpm -qa | grep "^orm-" | sed -E "s/(orm-\w+).*/\\1/g")
printf "Remove RPM package ${OLD_ORM}..... "
if [ "${OLD_ORM}" != "" ]; then
  yum remove -y -q ${OLD_ORM} && echo OK
else
  echo SKIP
fi


printf "Install RPM package orm-${ZONE}....."
yum install -y -q orm-${ZONE} && echo OK || fail


printf "Change DB user and password in frontier-ds.xml....."
sed -i -E -e "s/(.*user-name>).*(<.*)/\1user_${ZONE}\2/" \
  -e "s/(.*password>).*(<.*)/\112345678\2/" \
    /opt/wildfly8/standalone/deployments/frontier-ds.xml && echo OK || fail


printf "Set zone ${ZONE} in standalone.properties....."
WF8_CONF=/etc/wildfly8/standalone/standalone.properties
grep -q "current\.zone" ${WF8_CONF} || fail
if [ $? -eq 0 ]; then
  sed -i -E "s/(current\.zone=).*/\1${ZONE^^}/" ${WF8_CONF} && echo OK || fail
fi


printf "Restart wildfly8....."
service wildfly8 restart &>/dev/null && echo OK || fail


printf "Check if there any failed deployment of EARs....."
for F in /opt/wildfly8/standalone/deployments/*isdeploying; do 
  while [ -f "$F" ]; do sleep 1; done
done
FAILED_EARS=$(ls /opt/wildfly8/standalone/deployments/ | grep failed)
[ "${FAILED_EARS}" != "" ] && fail "${FAILED_EARS}" || echo OK
