#!/bin/bash

set -x

Usage(){
	exec >&2
	echo "usage: $0 "
	echo "Либо указываем в качестве параметра тип"
	echo "     -t (front-smoke|front-full|front-develop|fly-smoke|fly-master|fly-dev|fly-sync)"
	echo
	echo "Либо все необходимые параметры"
	echo "     -p <frontier|flygres>"
    echo "     -b <BRANCH>"
    echo "     -a <PABOT_ARGS>"
    echo "     -s <SUITESFROM.xml>"
    echo
    echo "Имя хоста указывается обязательно"
    echo "-h <addres>"
	echo "[-m '<список емейлов, разделенных пробелами>']"
	echo
	Die 1 "прост"
}


#TESTPROJ=frontier-testing; BRANCH=master ; PABOT_ARGS='-i smoke test'     ; SUITESFROM=smokerange.xml
#TESTPROJ=frontier-testing; BRANCH=master ; PABOT_ARGS='test'              ; SUITESFROM=fullrange.xml
#TESTPROJ=frontier-testing; BRANCH=develop; PABOT_ARGS='test'              ; SUITESFROM=fullrange.xml
#TESTPROJ=flygres-ui      ; BRANCH=master ; PABOT_ARGS='-i smoke -esync* .'; SUITESFROM=''
#TESTPROJ=flygres-ui      ; BRANCH=master ; PABOT_ARGS='-esync* .'         ; SUITESFROM=''
#TESTPROJ=flygres-ui      ; BRANCH=dev    ; PABOT_ARGS='-esync* .'         ; SUITESFROM=''
#TESTPROJ=flygres-ui      ; BRANCH=dev    ; PABOT_ARGS='-i sync .'         ; SUITESFROM=''

#ADDR=fly155.local

emails=''
emails="$emails zorin@swemel.ru"
emails="$emails zhelanov@swemel.ru"
emails="$emails o.boriskina@swemel.ru"
emails="$emails s.boiko@swemel.ru"
emails="$emails titov@swemel.ru"
emails="$emails dulceva@swemel.ru"
emails="$emails s.lukyanov@swemel.ru"
emails="$emails n.saveleva@swemel.ru"
emails="$emails pekonkin@swemel.ru"
emails="$emails o.duda@swemel.ru"
emails="$emails potokin@swemel.ru"


Log(){
	echo "$(date +%Y%m%d-%H%M%S): $@"
}
Err(){
	Log "ERROR: $@" >&2
}
Die(){
	ECOD=$1; shift
	Err "$@. Exiting"
	exit $ECOD
}

[ -z "${TESTPROJ}" ] && {
	while getopts :t:p:b:a:s:h:m: option; do
		case $option in
			m)	emails="$OPTARG"
				;;
			t)	# type of execution
				case "$OPTARG" in
					front-smoke)
						TESTPROJ=frontier-testing; BRANCH=master ; PABOT_ARGS='-i smoke test'     ; SUITESFROM=smokerange.xml
						;;
					front-master|front-full)
						TESTPROJ=frontier-testing; BRANCH=master ; PABOT_ARGS='test'              ; SUITESFROM=fullrange.xml
						;;
					front-dev|front-develop)
						TESTPROJ=frontier-testing; BRANCH=develop; PABOT_ARGS='test'              ; SUITESFROM=fullrange.xml
						;;
					fly-smoke)
						TESTPROJ=flygres-ui      ; BRANCH=master ; PABOT_ARGS='-i smoke -e sync .'; SUITESFROM=''
						;;
					fly-master)
						TESTPROJ=flygres-ui      ; BRANCH=master ; PABOT_ARGS='-e sync .'         ; SUITESFROM=''
						;;
					fly-dev|fly-develop)
						TESTPROJ=flygres-ui      ; BRANCH=dev    ; PABOT_ARGS='-e sync .'         ; SUITESFROM=''
						;;
					fly-sync)
						TESTPROJ=flygres-ui      ; BRANCH=dev    ; PABOT_ARGS='-i sync .'         ; SUITESFROM=''
						;;
					*)
						Die 3 "Wrong execution type"
						;;
				esac
				;;
			p)	# test program type
				case "$OPTARG" in
					front|frontier)
						TESTPROJ=frontier-testing
						;;
					fly|flygres)
						TESTPROJ=flygres-ui
						;;
					*) Die 2 "Не понятно, для какого ПО выполняются тесты"
						;;
				esac
				;;
			a)	PABOT_ARGS="$OPTARG"
				;;
			b)  BRANCH="$OPTARG"
				;;
			s)	SUITESFROM="$OPTARG"
				;;
			h)	ADDR="$OPTARG"
				;;
			*)	Die 5 "Неправильная опция -${option}"
				;;
		esac
	done
}


[ -z "${TESTPROJ}" ] && Die 4 "Не указано имя хоста"
HSTNAME=`ssh root@${ADDR} hostname` || Die 6 "Невозможно соединиться с тестируемым хостом ${ADDR}"

WORKDIR=/var/tmp/${ADDR}-${TESTPROJ}-${BRANCH}      # было что-то вроде fly155smoke

PABOT_SERVER=10.1.47.241
        ip a | grep $PABOT_SERVER || Die 8 "Запуск должен производиться на сервере с pabot"
WWW_ROOT=/opt/nginx/html/report

FROMUSER=bamboo@swemel.ru
SMTP=10.1.10.173
MAIL_ARGS='-o message-content-type=text -o message-charset=UTF-8'

[ -z "$WORKDIR" ] && Die 9 "Не установлена переменная с рабочей директорией"
[ -d "$WORKDIR/${TESTPROJ}" ] && rm -rf $WORKDIR
mkdir -p  $WORKDIR && cd  $WORKDIR || Die 10 "Не получилось зайти в рабочую директорию $WORKDIR/${TESTPROJ}"

git clone -b $BRANCH git@git.swemel.grp:test/${TESTPROJ}.git && cd ${TESTPROJ} || Die 12 "Не склонировался исходный текст тестов"

DATADIR="$(date +%Y%m%d-%H%M)-${ADDR}-${BRANCH}-$(git describe --always)"

if [ "$TESTPROJ" =  frontier-testing ]; then
	sed -i "s%^\${HOST}.*$%\${HOST}  ${ADDR}%" environment.robot || Die 14 "Не получилось поменять адрес тестирования для Фронтира"
fi

[ "$SUITESFROM" = 'fullrange.xml' ] && {
	gunzip fullrange.xml.gz || Die 16 "Не распаковался fullrange.xml"
}
[ ! -z "$SUITESFROM" ] && SUITESFROM_ARGS="--suitesfrom $SUITESFROM"
pabot ${SUITESFROM_ARGS} --verbose --processes 6 --pythonpath ./ -d report -L DEBUG ${PABOT_ARGS}

[ -d report ] || Die 17 "Нет директории с отчетом. Отчет не выполнился?"

RESULT="$(python utils/statistic.py -i report/output.xml)"

if [ "$TESTPROJ" =  frontier-testing ]; then PACKET_NAME=frontier-user; fi
if [ "$TESTPROJ" =  flygres-ui ];       then PACKET_NAME='spo-web-arctic-portal|flygres-app-min'; fi

VERSIONS=`ssh root@${ADDR} 'rpm -qa' | egrep "$PACKET_NAME"`


SUBJECT="Автотест ${ADDR}:${TESTPROJ}/${BRANCH} ${SUITESFROM}. $RESULT"
MSG="
Тесты:    ${TESTPROJ}/${BRANCH} ${SUITESFROM}
Дата:     $(date)
Стенд:    $HSTNAME (${ADDR})

Версия пакетов на сервере:
$VERSIONS

Результат выполнения тестов: $RESULT

http://$PABOT_SERVER/report/$DATADIR
"

mkdir -p ${WWW_ROOT}/$DATADIR &&
    cp -arpf report/* ${WWW_ROOT}/$DATADIR ||
    Die 18 "Не получилось скопировать результаты на вэб-сервер"
chcon -R -u system_u -r object_r -t httpd_sys_content_t ${WWW_ROOT}/$DATADIR/*

sendemail -f "$FROMUSER" -t ${emails} -u "${SUBJECT}" -m "${MSG}" -s "$SMTP" $MAIL_ARGS || Die 20 "Письмо не отправилось"
sleep 5m
