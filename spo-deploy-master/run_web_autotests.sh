#!/bin/bash

help() {
  echo "Usage: $0 AUTOTEST_BRANCH TEST_TYPE SOFTWARE STEND
  AUTOTEST_BRANCH = dev|develop|master
  TEST_TYPE= full|smoke
  SOFTWARE = flygres|frontier|frontierArctic
  STEND = fly145.local|10.1.47.153|{any_resolvable_address}
Examples:
  $0 dev smoke flygres fly145.local
  $0 master full frontier spo145.local
  $0 develop smoke frontierArctic spo153.local
  "
  exit 1
}

[ ${#} -ne 4 ] && help

Log(){
	echo "$(date +%Y%m%d-%H%M%S): $@"
}
Err(){
	Log "ERROR: $@" >&2
}
Die(){
	ECOD=$1; shift
	Err "$@"
	exit $ECOD
}

AUTOTEST_BRANCH=$1
TEST_TYPE=$2
SOFTWARE=$3
STEND=$4

STEND_SHORT=$(sed 's%\.local%%' <<< ${STEND})

HTTP_DIR=/opt/nginx/html/report
LOCAL_ADDR=10.1.47.241
SMTP=10.1.10.173 

RECIPIENTS="zorin@swemel.ru dulceva@swemel.ru o.boriskina@swemel.ru s.boiko@swemel.ru s.lukyanov@swemel.ru pekonkin@swemel.ru o.duda@swemel.ru"
[[ ${AUTOTEST_BRANCH} == "master" ]] && RECIPIENTS="${RECIPIENTS} titov@swemel.ru zhelanov@swemel.ru n.saveleva@swemel.ru agafonov@swemel.ru"


if [[ ${SOFTWARE} == "flygres" ]]; then
  AUTOTEST_DIR=flygres-testing-${AUTOTEST_BRANCH}
else
  AUTOTEST_DIR=frontier-testing-${AUTOTEST_BRANCH}
fi

for DIR in ${AUTOTEST_DIR} spo-deploy; do
  Log "Reset ${DIR} local Git repo - "
  git --git-dir=${DIR}/.git reset --hard --quiet && Log OK
  Log "Pull ${DIR} local Git repo - "
  git --git-dir=${DIR}/.git pull --quiet && Log OK
done


Log "Get SPO versions - "
SPO_VERSIONS=$(python3 spo-deploy/spo_get_versions.py -s ${SOFTWARE} -d ${STEND} | sed -e "s%OK - %%" -e "s% {3}%\n%g") && 
    Log "OK" || Err "Getting versions failure"

REPORT_DIR="$(date +%d-%m-%Y)-$(git --git-dir=${AUTOTEST_DIR}/.git describe --always)-${SOFTWARE}-${STEND_SHORT}-${TEST_TYPE}"
REPORT_DIR_PATH=${HTTP_DIR}/${REPORT_DIR}
mkdir -p ${REPORT_DIR_PATH}

[[ ${TEST_TYPE} == "smoke" ]] && OPTS="-i smoke"
[[ ${SOFTWARE} == "flygres" ]] && TEST_DIR="." || TEST_DIR="test"
pabot --verbose --processes 6 --pythonpath ${AUTOTEST_DIR} --outputdir ${REPORT_DIR_PATH} --loglevel DEBUG -variable IP:${STEND} ${OPTS} ${AUTOTEST_DIR}/${TEST_DIR}
[ $? -eq 0 ] && TEST_RESULT="OK" || TEST_RESULT="FAILED"

chcon -R -u system_u -r object_r -t httpd_sys_content_t ${REPORT_DIR_PATH}

Log "Send notifications - "
MSG="Date: $(date '+%F %R')\n
Software: ${SOFTWARE} \n
Test Environment: ${STEND} \n
Test Type: ${TEST_TYPE} \n\n
Result: <bold>${TEST_RESULT}</bold> \n\n
Report: http://${LOCAL_ADDR}/report/${REPORT_DIR} \n\n
${SPO_VERSIONS} \n"

Log $MSG

sendemail \
    -f bamboo@swemel.ru \
    -t "${RECIPIENTS}" \
    -u "Autotest Report ${SOFTWARE}-${STEND_SHORT}-${TEST_TYPE}. Result: ${TEST_RESULT}" \
    -m "${MSG}" \
    -s "${SMTP}" \
    -o message-content-type="text/enriched" \
    -o message-charset=UTF-8 && 
        Log OK || Err "Send mail failure"
