#!/usr/bin/python2

import SPO
import logging
import argparse
import sys

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--software", dest="software", default="flygres",
                    help="Software type: flygres, frontier, frontierArctic. Default: flygres",
                    choices=['flygres', 'frontier', 'frontierArctic'])
parser.add_argument("-d", "--dest_host", dest="host", default="fly145.local",
                    help="IP or DNS name of host. Default: fly145.local")
parser.add_argument("-z", "--zones", nargs="+", dest="zones", default="user",
                    help="Zone to test - user of global. Default: user. Example: '--zones user global'")
parser.add_argument("--plugins", nargs="+", dest="plugins", default="",
                    help="Flygres plugins to check to. Default: ''. Example: '--plugins osm-far sdno pvdnp esimo sync-frontier'")
parser.add_argument("-u", "--ssh_user", dest="ssh_user", default="root",
                    help="SSH user. Default: root")
parser.add_argument("-p", "--ssh_pass", dest="ssh_pass", default="12345678",
                    help="SSH password. Default: 12345678")
parser.add_argument("--web_user", dest="web_user", default="flyadmin",
                    help="Web SPO user. Default: flyadmin")
parser.add_argument("--web_pass", dest="web_pass", default="1",
                    help="Web SPO password. Default: 1")
parser.add_argument("--proxy", dest="proxy", default=None,
                    help="SOCKS proxy server's IP address and port. Example: '10.1.102.16:5072'. Default: None")
parser.add_argument("--sync", dest="sync", default=False, 
                    help="Must be specified if sync present. Default: False", action="store_true")
parser.add_argument("--location", "--region", dest="location", default=None,
                    help="Location where software is installed. For now used only for screenshot dirs naming. Default: not specified")
parser.add_argument("--remote", dest="remote", default=False, 
                    help="Must be specified if you need to check $HOST/remote/$PORTAL instead of local. It disables ssh checks. Default: False", action="store_true")
parser.add_argument("--zk", "--closed_contour", dest="closed_contour", default=False, 
                    help="Must be specified if dest machine IS IN closed contour. Default: False", action="store_true")
parser.add_argument("--okzk", "--have_closed_contour", dest="have_closed_contour",
                    help="Must be specified if dest machine IS IN open contour, but have a machine with closed contour (for sync checks). Default: False", 
                    default=False, action="store_true")
parser.add_argument("--web", dest="web", default=False, 
                    help="Test via web interface. Default: False", action="store_true")
parser.add_argument("--nossh", dest="nossh", default=False, 
                    help="Don't test via ssh. Default: False", action="store_true")
parser.add_argument("--noapi", dest="noapi", default=False, 
                    help="Don't test via API interface. Default: False", action="store_true")
parser.add_argument("-l", "--logfile", dest="logfile", default="deploy.log" if sys.platform.startswith('win') else "/dev/null",
                    help="Path to the log file. Default: /dev/null or deploy.log")
parser.add_argument("-i", "--redmine_issue", "--issue", 
                    dest="redmine_issue", default=None,
                    help="Issue to post result to. Default: None")
parser.add_argument("-k", "--redmine_key",  
                    dest="redmine_key", default=None,
                    help="Key to post result. Default: None")
parser.add_argument("-f", "--requests_file", dest="requests_file",
                    help="Path to the requests.yaml/json with API requests")
args = parser.parse_args()
logging.basicConfig( \
    format = u'%(asctime)s %(levelname)s - %(message)s', \
    filename = args.logfile, \
    level = logging.INFO)

arguments = { 
  'nossh':args.nossh, 
  'noapi':args.noapi, 
  'web':args.web,
  'host': args.host, 
  'sync': args.sync, 
  'location': args.location, 
  'remote': args.remote, 
  'closed_contour': args.closed_contour, 
  'have_closed_contour':args.have_closed_contour,
  'zones': args.zones, 
  'plugins': args.plugins, 
  'proxy': args.proxy,
  'ssh_user':args.ssh_user, 
  'ssh_pass':args.ssh_pass, 
  'web_user':args.web_user, 
  'web_pass':args.web_pass, 
  'redmine_issue':args.redmine_issue,
  'redmine_key':args.redmine_key
  }

if args.software == 'frontier':
  test = SPO.frontier(**arguments)
elif args.software == 'frontierArctic':
  test = SPO.frontierArctic(**arguments)
elif args.software == 'flygres':
  test = SPO.flygres(**arguments)

test_result = test.check_deploy()
if args.redmine_issue:
  test.post_result_to_redmine()
if not test_result:
  exit(1)
