#!/usr/bin/python2

import SPO
import logging
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--software", dest="software", default="flygres",
                    help="Software type: flygres, frontier, frontierArctic. Default: flygres")
parser.add_argument("-d", "--dest_host", dest="host", default="fly145.local",
                    help="IP or DNS name of host. Default: fly145.local")
parser.add_argument("-l", "--logfile", dest="logfile", default="results.log",
                    help="Path to the log file. Default: results.log")
args = parser.parse_args()
logging.basicConfig( \
    format = u'%(asctime)s %(levelname)s - %(message)s', \
    filename = args.logfile, \
    level = logging.INFO)


if args.software == 'frontier':
  test = SPO.frontier(args.host)
elif args.software == 'frontierArctic':
  test = SPO.frontierArctic(args.host)
elif args.software == 'flygres':
  test = SPO.flygres(args.host)

test.checkVersion()