#!/bin/bash

DB_UPDATE_DIR=/opt/spo/flygres/db_update/
STATUS=0

check_status() {
    if [ $? -eq 0 ]; then
        echo OK
    else
        echo FAIL
        exit 1
    fi
}

check_services() {
    for SERVICE in postgresql-9.5; do
        printf "Check ${SERVICE} status....."
        service ${SERVICE} status >/dev/null
        check_status
    done
}

check_rpms() {
    for RPM in wildfly10 liquibase; do
        printf "Check that RPM ${RPM} was installed....."
        rpm -q ${RPM} >/dev/null
        check_status
    done
}

update_db_update_package() {
    printf "Update spo-flygres-db-update package....."
    yum clean all -q && yum update spo-flygres-db-update -y -q
    check_status
}


cd ${DB_UPDATE_DIR}
PGOLD=${PGOPTIONS}
export PGOPTIONS='--client-min-messages=warning'

# Preparations
check_services
check_rpms
update_db_update_package
echo "DB UPDATE VERSION: $(rpm -qi spo-flygres-db-update | grep Version | awk '{print $3}')"

# Test
DATABASES='
    flygres_arctic_production
    flygres_arctic_etalon
    flygres_arctic_previous
    flygres_astrakhan_production
    flygres_astrakhan_etalon
    flygres_astrakhan_previous
    flygres_analysts_002_007_298
'
for DB in $DATABASES; do
    TEST_DB="${DB}_test"

    printf "Create database ${TEST_DB} from ${DB}....."
    psql -q -U postgres -c "DROP DATABASE IF EXISTS ${TEST_DB};"
    psql -q -U postgres -c "CREATE DATABASE ${TEST_DB} WITH TEMPLATE ${DB} OWNER flygres;"
    check_status

    printf "Update main.conf for DB ${TEST_DB}....."
    sed -i -e "s/TARGET_PASSWORD=.*/TARGET_PASSWORD=\'flygres\'/" \
           -e "s/TARGET_DATABASE=.*/TARGET_DATABASE=\'${TEST_DB}\'/" \
                   main.conf
    check_status

    echo "Update DB ${TEST_DB}....."
    ./run.sh
    if [ $? -eq 0 ]; then
        echo "OK"

        # Was database (not) ethalon?
        if [[ "${DB}" = *"previous"* ]]; then
            # not ethalon -- drop previous database and set tested database as new previous
            psql -q -U postgres -c "DROP DATABASE IF EXISTS ${DB};"
            PGOPTIONS=$PGOLD psql -q -U postgres -c "ALTER DATABASE ${TEST_DB} RENAME TO ${DB};"
        else
            # ethalon -- drop tested database because  we going to test etalon database next time again
            psql -q -U postgres -c "DROP DATABASE IF EXISTS ${TEST_DB};"
        fi
    else
        echo "TEST FAILED"
        STATUS=1
    fi
    echo -e "\n------------------------------\n"
done

exit ${STATUS}