#!/bin/bash

echo "Find and kill zombie process"
if [ $(ps -A -ostat,ppid | awk '/[zZ]/' | wc -l ) -gt 0 ]
    then
        for i in $(ps -A -ostat,ppid | awk '/[zZ]/ && !a[$2]++ {print $2}')
        do
            kill -9 ${i}
        done
fi

echo "Kill sync-transport"
if [ $(ps aux | grep sync-transport2 | grep -v grep | awk {'print$2'} | wc -l ) -gt 0 ]
    then
    ps aux | grep sync-transport2 | grep -v grep | awk {'print$2'} | xargs -I {} kill -9 {}
fi


#echo "Find deadth ansible"
#if [ $( ps aux | grep -v grep | egrep -c 'root/.ansible' ) -ge 1 ]
# then
#    echo "Kill now"
#    ps aux | egrep 'root/.ansible' | grep -v grep | awk {'print$2'} | xargs kill -1
#    rm -rf /root/.ansible/*
#    rm -rf /root/.ansible_async/*
#fi



