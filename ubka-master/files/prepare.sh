#!/bin/bash -x

if [ `id -u` -eq 0 ]
then
    if [ ! $(rpm -qa sshpass) ]
       then 
           yum -y localinstall files/sshpass-1.06-1.el6.x86_64.rpm
    fi
else
echo "Need run this megasuperpuper script root"
fi
if [ ! -e files/repo.sh ]
 then
    echo "`pwd` files/repo.sh not found"
    exit 1
fi

if [ ! -e files/libselinux-python-2.0.94-5.3.z36c.1.x86_64.rpm ]
 then
   echo "`pwd` files/libselinux-python-2.0.94-5.3.z36c.1.x86_64.rpm not found"
   exit 1
fi

for i in "$1"
do
  SSHPASS='12345678' sshpass -e scp -o StrictHostKeyChecking=no files/repo.sh root@${i}:/opt/
  SSHPASS='12345678' sshpass -e scp -o StrictHostKeyChecking=no files/libselinux-python-2.0.94-5.3.z36c.1.x86_64.rpm root@${i}:
  SSHPASS='12345678' sshpass -e ssh -A -o StrictHostKeyChecking=no root@$i 'bash /opt/repo.sh; rm -f /opt/repo.sh' 
done
