#!/bin/bash

if [ $( rpm -qa | grep -c libvirtd ) -eq 0 -a $( ls -1 /etc/sysconfig/network-scripts/ifcfg-* | grep -c eth ) -ge 1 ]
 then
    sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
    setenforce 0
fi

echo "Run -->> install libselinux-python"
yum -y install at rsync pciutils dmidecode

rpm -Uivh libselinux-python-*.rpm
rm -f libselinux-python-*.rpm

echo "End -->> install libselinux-python"


