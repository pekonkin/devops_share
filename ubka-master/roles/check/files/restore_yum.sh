#!/usr/bin/env bash


if [ -e /etc/yum.repos.d/update.repo ]; then
    rm -f /etc/yum.repos.d/update.repo
fi

if [ -e /etc/yum.repos.d/updates_spo.repo.old ]; then
    rm -f /etc/yum.repos.d/updates_spo.repo.old
fi


if [ -e /etc/yum.repos.d/update-spo.repo ]; then
   rm -f /etc/yum.repos.d/update-spo.repo
fi

if [ -e /etc/yum.repos.d/updates_spo.repo ]; then
   rm -f /etc/yum.repos.d/updates_spo.repo
fi

if [ -d /etc/yum.repos.d.old ]; then

find /etc/ -name 'Zircon36C-Media.repo.old' -exec mv {} /etc/yum.repos.d/ \;

if [ -e /etc/yum.repos.d/Zircon36C-Media.repo.old ]; then
    mv /etc/yum.repos.d/Zircon36C-Media.repo.old /etc/yum.repos.d/Zircon36C-Media.repo
fi

rm -rf /etc/yum.repos.d.old
fi