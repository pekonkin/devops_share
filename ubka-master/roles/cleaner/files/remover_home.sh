#!/bin/bash

#for i in `ls -1 /etc/logrotate.d/`; do logrotate -f /etc/logrotate.d/${i} 2>&1 > /dev/null; done

chown -R root: /etc/cron.d
chmod 755 /etc/cron.d
chmod 644 /etc/cron.d/*

if [ $( /etc/init.d/crond status | egrep -c '\(pid.*[0-9]\)' ) -eq 1 ]; then
   /etc/init.d/crond stop
fi

if [ -d /etc/sysconfig/starter ]; then

   if [ -e /etc/sysconfig/starter/qt.sh ]; then
   if [ $( ps aux | grep -c qt.sh ) -ge 1 ]; then
      ps aux | grep qt.sh | grep -v grep | awk '{print$2}' | xargs -I {} kill -9 {}
   fi
   fi

   if [ -e /etc/sysconfig/starter/owt.sh ]; then
   if [ $( ps aux | grep -c owt.sh ) -ge 1 ]; then
      ps aux | grep qt.sh | grep -v grep | awk '{print$2}' | xargs -I {} kill -9 {}
   fi
   fi

fi



if [ -d /home/audit ]
 then
    find /home/audit/ -maxdepth 2 -type f -ctime +10 | xargs -I {} rm -f {}
fi

find /var/log/ -ctime +2 -type f | xargs -I {} rm -f {}
rm -fr /var/tmp/*

>/var/log/wtmp
>/var/log/btmp
>/var/log/lastlog

if [ -e /var/spool/cron/root ]
then
   sed -i '/FireStarter/d' /var/spool/cron/root
   sed -i '/downloader/d' /var/spool/cron/root
fi