#!/bin/bash

echo 3 > /proc/sys/vm/drop_caches
swapoff -a
swapon -a
sync


if [ $(ps -A -ostat,ppid | awk '/[zZ]/' | wc -l ) -gt 0 ]; then
    for i in $(ps -A -ostat,ppid | awk '/[zZ]/ && !a[$2]++ {print $2}'); do
        kill -9 ${i}
    done
fi
