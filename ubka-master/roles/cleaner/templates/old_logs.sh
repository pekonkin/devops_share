#!/bin/bash

find /var/log -ctime +3 ! -type d | xargs -I {} rm -f {}

if [ -d /home/audit ]
 then
    find /home/audit/ -maxdepth 2 -type f -ctime +10 | xargs -I {} rm -f {}
fi
