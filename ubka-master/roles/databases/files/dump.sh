#!/bin/bash


echo "Prepare database"
_dir="/var/lib/pgsql/9.5/backups/"

[ -d ${_dir} -o $(ps aux | grep postgres | wc -l) -ge 2 ] || exit

if [ ! -d ${_dir} ]
 then
    mkdir -p ${_dir}
fi

if [ $(ps aux | grep flygres | wc -l) -ge 2 ]; then

	if [ $( find ${_dir} -name "flygres_*.dump" | wc -l ) -ge 2 ]; then
   		logger "Remove old database flygres backup"
   		cd ${_dir}
   		ls -tr flygres_*.dump | head -n -2 | xargs rm -f
	fi

	if [ ! -e ${_dir}flygres_`date +%F-%s`.dump ]; then
    		logger "Run backup in ${_dir}flygres_`date +%F-%s`.dump"
    		if [ -e /usr/pgsql-9.5/bin/pg_dump ]; then
        		/usr/pgsql-9.5/bin/pg_dump flygres -U postgres -f ${_dir}flygres_`date +%F-%s`.dump -Fc -Z 9 -v > /dev/null 2>&1
     		else
        		/usr/bin/pg_dump flygres -U postgres -f ${_dir}flygres_`date +%F-%s`.dump -Fc -Z 9 -v > /dev/null 2>&1
    		fi
	logger "Backup flygres database is complited"
	fi

if [ $(date +%u) -eq 3 ]; then
    	if [ $( psql -U postgres -t -l | awk -F \| {'print$1'} | egrep -c ^"[[:space:]]zabbix[[:space:]]" ) -ge 1 ]; then
        	psql -t -U postgres -d zabbix -c "delete FROM alerts where age(to_timestamp(alerts.clock)) > interval '180 days';" > /dev/null 2>&1
        	psql -t -U postgres -d zabbix -c "delete FROM acknowledges where age(to_timestamp(acknowledges.clock)) > interval '180 days';" > /dev/null 2>&1
        	psql -t -U postgres -d zabbix -c "delete FROM events where age(to_timestamp(events.clock)) > interval '180 days';" > /dev/null 2>&1
        	psql -t -U postgres -d zabbix -c "delete FROM history where age(to_timestamp(history.clock)) > interval '365 days' ;" > /dev/null 2>&1
        	psql -t -U postgres -d zabbix -c "delete FROM history_uint where age(to_timestamp(history_uint.clock)) > interval '365 days' ;" > /dev/null 2>&1
        	psql -t -U postgres -d zabbix -c "delete FROM history_str where age(to_timestamp(history_str.clock)) > interval '365 days' ;" > /dev/null 2>&1
        	psql -t -U postgres -d zabbix -c "delete FROM history_text where age(to_timestamp(history_text.clock)) > interval '365 days' ;" > /dev/null 2>&1
        	psql -t -U postgres -d zabbix -c "delete FROM history_log where age(to_timestamp(history_log.clock)) > interval '365 days' ;" > /dev/null 2>&1
        	psql -t -U postgres -d zabbix -c "delete FROM trends_uint where age(to_timestamp(trends_uint.clock)) > interval '365 days' ;" > /dev/null 2>&1
        	psql -t -U postgres -d zabbix -c "delete FROM trends where age(to_timestamp(trends.clock)) > interval '365 days';" > /dev/null 2>&1
        	psql -t -U postgres -d zabbix -c 'REINDEX DATABASE zabbix;' > /dev/null 2>&1
        	psql -t -U postgres -d zabbix -c 'VACUUM (FULL, ANALYZE);' > /dev/null 2>&1
    	fi
	if [ $( psql -U postgres -t -l | awk -F \| {'print$1'} | egrep -c ^"[[:space:]]roundcube[[:space:]]" ) -ge 1 ]; then
        	psql -U postgres -d roundcube -c 'REINDEX DATABASE roundcube;' > /dev/null 2>&1
        	psql -U postgres -d roundcube -c 'VACUUM (FULL, ANALYZE);' > /dev/null 2>&1
    	fi

fi

    	if [ $( psql -U postgres -t -l | awk -F \| {'print$1'} | egrep -c ^"[[:space:]]flygres[[:space:]]" ) -ge 1 ]; then
        	psql -U postgres -d flygres -c 'REINDEX DATABASE flygres;' > /dev/null 2>&1
        	psql -U postgres -d flygres -c 'VACUUM (FULL, ANALYZE);' > /dev/null 2>&1
        	logger "REINDEX DATABASE and VACUUM analize Complited for flygres"
    	fi
        if [ $( psql -U postgres -t -l | awk -F \| {'print$1'} | egrep -c ^"[[:space:]]astk[[:space:]]" ) -ge 1 ]; then
            psql -U postgres -d postgres -c "select pg_terminate_backend(pid) from pg_stat_activity where datname ='astk';"
            psql -U postgres -d postgres -c 'ALTER DATABASE astk WITH ALLOW_CONNECTIONS false;'
            psql -U postgres -d postgres -c "select pg_terminate_backend(pid) from pg_stat_activity where datname ='astk';"
            psql -U postgres -d astk -c 'TRUNCATE srv_targetinfo4arm CASCADE;' > /dev/null 2>&1
            logger "START | REINDEX DATABASE astk сomplited"
            psql -U postgres -d astk -c 'REINDEX DATABASE astk;' > /dev/null 2>&1
            logger "END | REINDEX DATABASE astk сomplited"
            logger "START | VACUUM and ANALYZE for astk database"
            psql -U postgres -d astk -c 'VACUUM (FULL, ANALYZE);' > /dev/null 2>&1
            psql -U postgres -d postgres -c 'ALTER DATABASE astk WITH ALLOW_CONNECTIONS true;'
            logger "END | VACUUM and ANALYZE complited for astk database"
        fi
        logger "REINDEX DATABASE and VACUUM analize Complited"

fi
