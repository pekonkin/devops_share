#!/bin/bash


echo "Prepare database"
_dir="/var/lib/pgsql/9.5/backups/"

[ -d ${_dir} -o $(ps aux | grep postgres | wc -l) -ge 1 ] || exit

if [ ! -d ${_dir} ]
 then
    mkdir -p ${_dir}
fi

if [ $(ps aux | grep flygres | wc -l) -ge 2 ]; then

	if [ $( find ${_dir} -name "flygres_*.dump" | wc -l ) -ge 2 ]; then
   		logger "Remove old database flygres backup"
   		cd ${_dir}
   		ls -tr flygres_*.dump | head -n -2 | xargs rm -f
	fi

	if [ ! -e ${_dir}flygres_`date +%F-%s`.dump ]; then
    		logger "Run backup in ${_dir}flygres_`date +%F-%s`.dump"
    		if [ -e /usr/pgsql-9.5/bin/pg_dump ]; then
        		/usr/pgsql-9.5/bin/pg_dump flygres -U postgres -f ${_dir}flygres_`date +%F-%s`.dump -Fc -Z 9 -v > /dev/null 2>&1
     		else
        		/usr/bin/pg_dump flygres -U postgres -f ${_dir}flygres_`date +%F-%s`.dump -Fc -Z 9 -v > /dev/null 2>&1
    		fi
	logger "Backup flygres database is complited"
	fi

if [ $(ps aux | grep frontier | wc -l) -ge 2 ]; then

	if [ $( find ${_dir} -name "frontier_*.dump" | wc -l ) -ge 2 ]; then
   		logger "Remove old database frontier backup"
   		cd ${_dir}
   		ls -tr frontier_*.dump | head -n -2 | xargs rm -f
	fi

	if [ ! -e ${_dir}frontier_`date +%F-%s`.dump ]; then
    		logger "Run backup in ${_dir}frontier_`date +%F-%s`.dump"
    		if [ -e /usr/pgsql-9.5/bin/pg_dump ]; then
        		/usr/pgsql-9.5/bin/pg_dump frontier -U postgres -f ${_dir}frontier_`date +%F-%s`.dump -Fc -Z 9 -v > /dev/null 2>&1
     		else
        		/usr/bin/pg_dump frontier -U postgres -f ${_dir}frontier_`date +%F-%s`.dump -Fc -Z 9 -v > /dev/null 2>&1
    		fi
	logger "Backup frontier database is complited"
	fi
fi

if [ $(date +%u) -eq 3 ]; then
	if [ $( psql -U postgres -t -l | awk -F \| {'print$1'} | egrep -c ^"[[:space:]]roundcube[[:space:]]" ) -ge 1 ]; then
        	psql -U postgres -d roundcube -c 'REINDEX DATABASE roundcube;'
        	psql -U postgres -d roundcube -c 'VACUUM (FULL, ANALYZE);' > /dev/null 2>&1
    	fi

fi

    	if [ $( psql -U postgres -t -l | awk -F \| {'print$1'} | egrep -c ^"[[:space:]]flygres[[:space:]]" ) -ge 1 ]; then
        	psql -U postgres -d flygres -c 'REINDEX DATABASE flygres;'
        	psql -U postgres -d flygres -c 'VACUUM (FULL, ANALYZE);' > /dev/null 2>&1
        	logger "REINDEX DATABASE and VACUUM analize Complited for flygres"
    	fi
    	if [ $( psql -U postgres -t -l | awk -F \| {'print$1'} | egrep -c ^"[[:space:]]frontier[[:space:]]" ) -ge 1 ]; then
        	psql -U postgres -d frontier -c 'REINDEX DATABASE frontier;'
        	psql -U postgres -d frontier -c 'VACUUM (FULL, ANALYZE);' > /dev/null 2>&1
        	logger "REINDEX DATABASE and VACUUM analize Complited for frontier"
    	fi
    	if [ $( psql -U postgres -t -l | awk -F \| {'print$1'} | egrep -c ^"[[:space:]]classifier[[:space:]]" ) -ge 1 ]; then
        	psql -U postgres -d classifier -c 'REINDEX DATABASE classifier;'
       		psql -U postgres -d classifier -c 'VACUUM (FULL, ANALYZE);' > /dev/null 2>&1
		logger "REINDEX DATABASE and VACUUM analize Complited for classifier"
    	fi
        if [ $( psql -U postgres -t -l | awk -F \| {'print$1'} | egrep -c ^"[[:space:]]astk[[:space:]]" ) -ge 1 ]; then
            psql -U postgres -d postgres -c "select pg_terminate_backend(pid) from pg_stat_activity where datname ='astk';"
            psql -U postgres -d postgres -c 'ALTER DATABASE astk WITH ALLOW_CONNECTIONS false;'
            psql -U postgres -d postgres -c "select pg_terminate_backend(pid) from pg_stat_activity where datname ='astk';"
            psql -U postgres -d astk -c 'TRUNCATE srv_targetinfo4arm CASCADE;' > /dev/null 2>&1
            logger "START | REINDEX DATABASE astk сomplited"
            psql -U postgres -d astk -c 'REINDEX DATABASE astk;' > /dev/null 2>&1
            logger "END | REINDEX DATABASE astk сomplited"
            logger "START | VACUUM and ANALYZE for astk database"
            psql -U postgres -d astk -c 'VACUUM (FULL, ANALYZE);' > /dev/null 2>&1
            psql -U postgres -d postgres -c 'ALTER DATABASE astk WITH ALLOW_CONNECTIONS true;'
            logger "END | VACUUM and ANALYZE complited for astk database"
        fi
        logger "REINDEX DATABASE and VACUUM analize Complited"
fi
