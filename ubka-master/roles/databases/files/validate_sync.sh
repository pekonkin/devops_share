#!/bin/bash

if
psql -U postgres -c "select query from pg_stat_activity" | grep -e 'func_validate_sync'
then
logger -t postgres "`basename $0` | validate is running!"
elif
psql -U flygres -d flygres -c "select distinct is_validate from sync.sync_out" | grep f
then
psql -U flygres -d flygres -c "select sync.func_validate_sync();";
logger -t postgres "`basename $0` | validate vas started"
fi