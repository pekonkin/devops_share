#!/usr/bin/env bash

_sync2="{{ _sync2 }}"
_sync_group="{{ _sync_group }}"
_sync_mask="{{ _sync_mask }}"

if [ -e /etc/init.d/zabbix-agent ]; then
   /etc/init.d/zabbix-agent stop
fi

if [ -e /etc/init.d/zabbix-server ]; then
   /etc/init.d/zabbix-server stop
fi

if [ -e /etc/init.d/iptables ]; then
   if [ $( /etc/init.d/iptables status | egrep -ic 'state' ) -eq 0 ]; then
      /etc/init.d/iptables restart
   fi
fi

if [ -e /sbin/iptables ]; then
   if [ $( /sbin/iptables --line-number -nvL INPUT | egrep -c 'DROP.*5432' ) -eq 0 ]; then
      /sbin/iptables -I INPUT 1 -p tcp -m tcp --dport 5432 -j DROP
   fi
fi

logger "Check if pid is exits for select pg_terminate_backend | if find kill it"
if [ $(ps aux | egrep 'psql -U postgres -c select pg_terminate_backend' | grep -v grep | wc -l) -ge 1 ]; then
  kill -9 `ps aux | egrep 'psql -U postgres -c select pg_terminate_backend' | awk {'print$2'}`
  logger "kill pg_terminate_backend"
fi

psql -U flygres -d flygres -c "alter function service.set_state_localship_triggers(boolean) owner to flygres;"
psql -U flygres -d flygres -c "alter function service.set_state_constraint_triggers(boolean) owner to flygres;"
psql -U flygres -d flygres -c "alter function service.set_state_notification_triggers(boolean) owner to flygres;"


logger "Select and drop connections"
psql -U flygres -c "select pg_terminate_backend(pid) from pg_stat_activity where not datname='postgres';" 2>&1 > /dev/null
psql -U flygres -d flygres -c "select sync.func_set_state_trigger(false);"
psql -U flygres -d flygres -c "truncate sync.sync_in, sync.sync_out, sync.sync_out_info, sync.sync_out_resend, sync.sync_log, sync.sync_in_log, sync.sync_tree_aud;"
psql -U flygres -d flygres -c "select sync.func_set_state_trigger(true);"

logger "pg_terminate_backend is killed"
psql -U flygres -d flygres -f $_sync2
logger "$_sync2 is inserting"
psql -U flygres -d flygres -f $_sync_group
logger "$_sync_group is inserting"
psql -U flygres -d flygres -f $_sync_mask
logger "$_sync_mask is inserting"

if [ -e /tmp/${_sync_group} ]; then
 rm -f /tmp/${_sync_group}
fi

if [ -e /tmp/${_sync_mask} ]; then
 rm -f /tmp/${_sync_mask}
fi

if [ -e /sbin/iptables ]; then
   if [ $( /sbin/iptables --line-number -nvL INPUT | egrep -c 'DROP.*5432' ) -ge 1 ]; then
      for _delete in `iptables --line-number -nvL INPUT | egrep 'DROP.*5432' | awk {'print$1'} | sort -r`; do
         /sbin/iptables -D INPUT ${_delete}
      done
   fi
fi

if [ -e /etc/init.d/zabbix-server ]; then
   /etc/init.d/zabbix-server start
fi

if [ -e /etc/init.d/zabbix-agent ]; then
   /etc/init.d/zabbix-agent start
fi