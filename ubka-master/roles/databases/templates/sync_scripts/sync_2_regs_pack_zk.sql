-- Удаление триггеров sync


DO $$
declare
  r record;
begin
	for r in select t.tgname, n.nspname, substring(t.tgname from 5 for 36) as table_name from pg_trigger t
		inner join pg_class p ON t.tgrelid = p.oid
		inner join pg_namespace n ON n.oid = p.relnamespace
		where n.nspname = any(sync.func_get_constant('allow_schema_audit_array')::text[])
		and t.tgname like 'trg_%' loop
			execute 'drop trigger "'||r.tgname||'" on '||r.nspname||'."'||r.table_name||'";';
	end loop;
END $$;  


DELETE FROM sync.sync_tree;  -- Удаление кустов
DELETE FROM sync.sync_reglament; -- Удаление регламентов




 -- Добавление регламентов
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (32,'node','053ebb98-e741-42dd-aa4c-4c1e8d69591c','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["callsignsOfficialsMainForm"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (33,'node','5d70df1e-aef4-4cee-b3d3-083321a33d6a','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["negotiationTableJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (1,'node','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["eventMainFrontier"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (2,'node','e2276a2f-9a58-46c4-b0c8-87cd18eb17ee','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["csOnDutyJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (3,'node','84bca5a4-3ebd-4ee3-b19f-57056f505abc','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["targetsLand"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (4,'node','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["alarmsLand"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (5,'node','ea076d36-1053-47fb-9bfc-4a75090e6721','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["aviaMovingJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (6,'node','d453ccd4-ff88-4001-923c-8d37e88319b3','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["aviaFlightJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (7,'node','4fba4935-b935-4b81-b17b-57c1d0a1d0e7','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["airSituationJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (10,'node','01e60aa5-1f85-4102-b526-28287dd5bf9f','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["logisticsAvailabilityJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (11,'node','7f7a1963-8971-43b5-a9a5-e83260048e69','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["logisticsSystemStatusJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (12,'node','87ed5dc7-e8b1-4f67-b288-12aab1626350','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["csInSeaJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (13,'node','dc7fe95b-679f-41fc-9228-aabbfc252d26','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["automobileChecklistJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (15,'node','92623287-26fa-42bf-8fec-0ab86d80cf89','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["tspkCatalogJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (16,'node','f805daff-04fc-4e4f-b3bc-6b1d5a87ec43','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["structuresRegistrListJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (17,'node','7a4fab53-5014-48a8-88ab-41bcd1626a57','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["meansOfComunicationMainJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (18,'node','16ce53a5-a172-49d2-97e6-69d026e24616','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["frontierGuardComunicationsForm"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (19,'node','8c45e434-bc21-4337-b406-00d50eba9950','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["callsignsCommunicationCenterMainJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (20,'node','de108002-4ff6-4be1-ad5a-60059be841c3','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["pathOfFrontierGuardJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (21,'node','43cfecae-33f8-4663-a87d-a0c6951249d9','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["pdAnnualPlanningJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (22,'node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["zkPUOrgstructureForm", "zkPUOrgstructurePersonalSquadForm"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (23,'node','da8ac0d4-4cbc-4875-abd2-3ea83eac1c88','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["zkPUOrgstructSubdivisionForm", "zkPUOrgstructSubdivisionPersonnelForm"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (24,'node','ab722707-0ffa-4113-89e8-a4ced25cd4d4','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["frontierGuardPlanJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (14,'node','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["tsogRegistrListJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (8,'node','c086f87e-c53b-452c-a1f4-c392e70de1db','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["foivPersonnelZKVSJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (9,'node','3415687c-4b36-45d7-ab0e-7619560167df','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["ks"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (25,'node','3c695dae-4b39-41f3-98bc-4ddb3aa21664','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["frontierDuty", "planElementNoLegendForm", "planElementForm"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (26,'node','691e35a6-e4ca-4d1b-8114-7d65a10f8abf','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["placesOfFrontierGuardJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (27,'node','67a36e83-a3ab-4c51-bac5-ae4a2fd0c653','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["frontierGuardAutomobileUsejournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (28,'node','f3605afe-86ac-46d5-b978-024132ae11e9','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["automobileTripMainJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (29,'node','f04190e4-9f58-41fa-a36a-7972f9acebad','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["eventMainArrestedLoadJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (30,'node','1b80db34-9947-44a6-8a7d-2ec385848873','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["eventMainTraitorsJournal"]'::json,null,'[]'::json,null,null);
insert into sync.sync_reglament(id,schema_name,table_name,out_node_mask,is_private,is_create_tree,journal_label,out_node_where,out_node_order,in_node_priority,out_time_out_sec) 
values (31,'node','7fae9564-b0f2-4296-86e3-7d7e6f2124aa','00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000'::bit(128),false,true,'["callsignFrontierGuardMainJournal"]'::json,null,'[]'::json,null,null);


-- Добавление кустов

insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),31,'rel','7fae9564-b0f2-4296-86e3-7d7e6f2124aa','to_id','node','1cf1f968-302a-4fb8-86e3-31560532f4f8','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),31,'node','1cf1f968-302a-4fb8-86e3-31560532f4f8','id','rel','1cf1f968-302a-4fb8-86e3-31560532f4f8','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),31,'rel','7fae9564-b0f2-4296-86e3-7d7e6f2124aa','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),31,'node','7fae9564-b0f2-4296-86e3-7d7e6f2124aa','id','rel','7fae9564-b0f2-4296-86e3-7d7e6f2124aa','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),32,'rel','053ebb98-e741-42dd-aa4c-4c1e8d69591c','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),32,'node','1cf1f968-302a-4fb8-86e3-31560532f4f8','id','rel','1cf1f968-302a-4fb8-86e3-31560532f4f8','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),32,'rel','053ebb98-e741-42dd-aa4c-4c1e8d69591c','to_id','node','1cf1f968-302a-4fb8-86e3-31560532f4f8','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),32,'node','053ebb98-e741-42dd-aa4c-4c1e8d69591c','id','rel','053ebb98-e741-42dd-aa4c-4c1e8d69591c','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),32,'rel','053ebb98-e741-42dd-aa4c-4c1e8d69591c','to_id','node','b4a038ff-dd42-4686-b85d-51be1fd3faca','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),33,'rel','5d70df1e-aef4-4cee-b3d3-083321a33d6a','to_id','node','aa9a41d3-208e-4a53-a22b-21fe88990ac4','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),33,'node','5d70df1e-aef4-4cee-b3d3-083321a33d6a','id','rel','5d70df1e-aef4-4cee-b3d3-083321a33d6a','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),33,'rel','5d70df1e-aef4-4cee-b3d3-083321a33d6a','to_id','node','e3d85e95-dbfd-417a-b576-0d808781a911','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),2,'node','e2276a2f-9a58-46c4-b0c8-87cd18eb17ee','id','rel','e2276a2f-9a58-46c4-b0c8-87cd18eb17ee','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),2,'rel','e2276a2f-9a58-46c4-b0c8-87cd18eb17ee','to_id','node','bbd4ff84-d62b-4b70-ab3e-6395bfa28e44','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),2,'rel','e2276a2f-9a58-46c4-b0c8-87cd18eb17ee','to_id','node','3415687c-4b36-45d7-ab0e-7619560167df','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),2,'rel','e2276a2f-9a58-46c4-b0c8-87cd18eb17ee','to_id','node','bbbfeb4c-aa05-4d48-8b3a-f1948564fea2','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),2,'rel','bbbfeb4c-aa05-4d48-8b3a-f1948564fea2','to_id','node','063b2fde-8353-4092-95ce-3418c80ee3af','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),2,'rel','e2276a2f-9a58-46c4-b0c8-87cd18eb17ee','to_id','node','af8d7cfe-8b03-452d-87b3-2f0c8d593387','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),2,'node','bbbfeb4c-aa05-4d48-8b3a-f1948564fea2','id','rel','bbbfeb4c-aa05-4d48-8b3a-f1948564fea2','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),4,'rel','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),4,'rel','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','to_id','node','fe9021c6-3a6e-418f-97e4-7e305e275804','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),4,'node','84bca5a4-3ebd-4ee3-b19f-57056f505abc','id','rel','84bca5a4-3ebd-4ee3-b19f-57056f505abc','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),4,'node','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','id','rel','e36b3b7b-ee2f-401c-bf20-29a5eb04881e','to_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),4,'node','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','id','rel','84bca5a4-3ebd-4ee3-b19f-57056f505abc','to_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),4,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','from_id','node','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),4,'node','e36b3b7b-ee2f-401c-bf20-29a5eb04881e','id','rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),4,'rel','84bca5a4-3ebd-4ee3-b19f-57056f505abc','from_id','node','84bca5a4-3ebd-4ee3-b19f-57056f505abc','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),4,'node','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','id','rel','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),4,'rel','e36b3b7b-ee2f-401c-bf20-29a5eb04881e','from_id','node','e36b3b7b-ee2f-401c-bf20-29a5eb04881e','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),4,'rel','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','to_id','node','1ddc9a28-b1f5-4836-bf54-5fa401ce1305','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),7,'rel','4fba4935-b935-4b81-b17b-57c1d0a1d0e7','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),7,'node','4fba4935-b935-4b81-b17b-57c1d0a1d0e7','id','rel','4fba4935-b935-4b81-b17b-57c1d0a1d0e7','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),10,'node','01e60aa5-1f85-4102-b526-28287dd5bf9f','id','rel','01e60aa5-1f85-4102-b526-28287dd5bf9f','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),10,'node','258cb9aa-1969-4af4-9847-1a5913312c38','id','rel','258cb9aa-1969-4af4-9847-1a5913312c38','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),10,'rel','01e60aa5-1f85-4102-b526-28287dd5bf9f','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),10,'rel','01e60aa5-1f85-4102-b526-28287dd5bf9f','to_id','node','258cb9aa-1969-4af4-9847-1a5913312c38','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),11,'node','7f7a1963-8971-43b5-a9a5-e83260048e69','id','rel','7f7a1963-8971-43b5-a9a5-e83260048e69','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),11,'node','80b18b71-38f2-43a4-aed5-94c0d43fd387','id','rel','80b18b71-38f2-43a4-aed5-94c0d43fd387','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),11,'rel','7f7a1963-8971-43b5-a9a5-e83260048e69','to_id','node','80b18b71-38f2-43a4-aed5-94c0d43fd387','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),11,'rel','7f7a1963-8971-43b5-a9a5-e83260048e69','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),12,'node','87ed5dc7-e8b1-4f67-b288-12aab1626350','id','rel','87ed5dc7-e8b1-4f67-b288-12aab1626350','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),12,'rel','87ed5dc7-e8b1-4f67-b288-12aab1626350','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),12,'rel','87ed5dc7-e8b1-4f67-b288-12aab1626350','to_id','node','3415687c-4b36-45d7-ab0e-7619560167df','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),12,'node','3415687c-4b36-45d7-ab0e-7619560167df','id','rel','3415687c-4b36-45d7-ab0e-7619560167df','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),13,'node','dc7fe95b-679f-41fc-9228-aabbfc252d26','id','rel','dc7fe95b-679f-41fc-9228-aabbfc252d26','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),13,'rel','dc7fe95b-679f-41fc-9228-aabbfc252d26','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),13,'rel','dc7fe95b-679f-41fc-9228-aabbfc252d26','to_id','node','ec529fb1-87cd-430d-a322-fa086ad8bb2b','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),13,'rel','64c650a2-4d67-4977-9370-a3dcfbcf89db','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),13,'rel','dc7fe95b-679f-41fc-9228-aabbfc252d26','to_id','node','64c650a2-4d67-4977-9370-a3dcfbcf89db','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),13,'node','64c650a2-4d67-4977-9370-a3dcfbcf89db','id','rel','64c650a2-4d67-4977-9370-a3dcfbcf89db','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),15,'rel','92623287-26fa-42bf-8fec-0ab86d80cf89','to_id','node','ec529fb1-87cd-430d-a322-fa086ad8bb2b','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),15,'rel','92623287-26fa-42bf-8fec-0ab86d80cf89','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),15,'rel','64c650a2-4d67-4977-9370-a3dcfbcf89db','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),15,'node','92623287-26fa-42bf-8fec-0ab86d80cf89','id','rel','92623287-26fa-42bf-8fec-0ab86d80cf89','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),15,'node','a2811725-20e7-43cb-b238-43c9a4e3de3c','id','rel','a2811725-20e7-43cb-b238-43c9a4e3de3c','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),15,'rel','92623287-26fa-42bf-8fec-0ab86d80cf89','to_id','node','a2811725-20e7-43cb-b238-43c9a4e3de3c','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),15,'rel','92623287-26fa-42bf-8fec-0ab86d80cf89','to_id','node','64c650a2-4d67-4977-9370-a3dcfbcf89db','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),15,'rel','a2811725-20e7-43cb-b238-43c9a4e3de3c','to_id','node','53c7e37c-bbf3-47d0-ad69-c907c5158728','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),15,'node','64c650a2-4d67-4977-9370-a3dcfbcf89db','id','rel','64c650a2-4d67-4977-9370-a3dcfbcf89db','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),16,'rel','cb2ce019-a2ac-45ac-977e-52a9e8e2e8cc','to_id','node','513843f4-6dde-45c2-935c-9a904e494997','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),16,'rel','f805daff-04fc-4e4f-b3bc-6b1d5a87ec43','to_id','node','64c650a2-4d67-4977-9370-a3dcfbcf89db','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),16,'rel','f805daff-04fc-4e4f-b3bc-6b1d5a87ec43','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),16,'rel','cb2ce019-a2ac-45ac-977e-52a9e8e2e8cc','to_id','node','22f8b0ac-6545-4358-81b2-21476ac95a54','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),16,'node','22f8b0ac-6545-4358-81b2-21476ac95a54','id','rel','22f8b0ac-6545-4358-81b2-21476ac95a54','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),16,'rel','6d5001d0-0e66-489f-b8d8-c697bc8f62bd','to_id','node','3915d667-098e-4245-b641-1cfbf8e9e182','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),16,'node','3915d667-098e-4245-b641-1cfbf8e9e182','id','rel','3915d667-098e-4245-b641-1cfbf8e9e182','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),16,'rel','64c650a2-4d67-4977-9370-a3dcfbcf89db','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),16,'rel','f805daff-04fc-4e4f-b3bc-6b1d5a87ec43','to_id','node','cb2ce019-a2ac-45ac-977e-52a9e8e2e8cc','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),16,'node','11337db8-9012-43f2-a311-c16923cd802d','id','rel','11337db8-9012-43f2-a311-c16923cd802d','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),16,'node','f805daff-04fc-4e4f-b3bc-6b1d5a87ec43','id','rel','f805daff-04fc-4e4f-b3bc-6b1d5a87ec43','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),16,'rel','6d5001d0-0e66-489f-b8d8-c697bc8f62bd','to_id','node','25075024-75ed-4eca-ba73-54fc10dd533a','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),16,'rel','f805daff-04fc-4e4f-b3bc-6b1d5a87ec43','to_id','node','6d5001d0-0e66-489f-b8d8-c697bc8f62bd','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),16,'rel','513843f4-6dde-45c2-935c-9a904e494997','to_id','node','b4a038ff-dd42-4686-b85d-51be1fd3faca','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),16,'rel','22f8b0ac-6545-4358-81b2-21476ac95a54','to_id','node','b4a038ff-dd42-4686-b85d-51be1fd3faca','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),16,'node','6d5001d0-0e66-489f-b8d8-c697bc8f62bd','id','rel','6d5001d0-0e66-489f-b8d8-c697bc8f62bd','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),16,'rel','6d5001d0-0e66-489f-b8d8-c697bc8f62bd','to_id','node','37ce622a-45cd-4e4d-a027-2e880eb2e957','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),16,'rel','f805daff-04fc-4e4f-b3bc-6b1d5a87ec43','to_id','node','ec529fb1-87cd-430d-a322-fa086ad8bb2b','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),16,'rel','f805daff-04fc-4e4f-b3bc-6b1d5a87ec43','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),16,'node','cb2ce019-a2ac-45ac-977e-52a9e8e2e8cc','id','rel','cb2ce019-a2ac-45ac-977e-52a9e8e2e8cc','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),16,'node','513843f4-6dde-45c2-935c-9a904e494997','id','rel','513843f4-6dde-45c2-935c-9a904e494997','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),16,'node','64c650a2-4d67-4977-9370-a3dcfbcf89db','id','rel','64c650a2-4d67-4977-9370-a3dcfbcf89db','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),17,'node','7a4fab53-5014-48a8-88ab-41bcd1626a57','id','rel','7a4fab53-5014-48a8-88ab-41bcd1626a57','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),17,'rel','7a4fab53-5014-48a8-88ab-41bcd1626a57','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),17,'rel','64c650a2-4d67-4977-9370-a3dcfbcf89db','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),17,'rel','7a4fab53-5014-48a8-88ab-41bcd1626a57','to_id','node','ec529fb1-87cd-430d-a322-fa086ad8bb2b','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),17,'rel','7a4fab53-5014-48a8-88ab-41bcd1626a57','to_id','node','8c02785c-a94a-44c7-a1fa-0c42aa265375','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),17,'node','64c650a2-4d67-4977-9370-a3dcfbcf89db','id','rel','64c650a2-4d67-4977-9370-a3dcfbcf89db','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),17,'rel','7a4fab53-5014-48a8-88ab-41bcd1626a57','to_id','node','64c650a2-4d67-4977-9370-a3dcfbcf89db','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),19,'rel','8c45e434-bc21-4337-b406-00d50eba9950','to_id','node','1cf1f968-302a-4fb8-86e3-31560532f4f8','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),19,'node','1cf1f968-302a-4fb8-86e3-31560532f4f8','id','rel','1cf1f968-302a-4fb8-86e3-31560532f4f8','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),19,'rel','8c45e434-bc21-4337-b406-00d50eba9950','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),19,'node','8c45e434-bc21-4337-b406-00d50eba9950','id','rel','8c45e434-bc21-4337-b406-00d50eba9950','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),20,'rel','de108002-4ff6-4be1-ad5a-60059be841c3','to_id','node','a0d589cf-ea6f-4da6-b499-26618b84883b','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),20,'rel','de108002-4ff6-4be1-ad5a-60059be841c3','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),20,'node','de108002-4ff6-4be1-ad5a-60059be841c3','id','rel','de108002-4ff6-4be1-ad5a-60059be841c3','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),23,'node','da8ac0d4-4cbc-4875-abd2-3ea83eac1c88','id','rel','da8ac0d4-4cbc-4875-abd2-3ea83eac1c88','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),18,'node','ccb13fa4-411f-4916-9d4e-290ad476c846','id','rel','ccb13fa4-411f-4916-9d4e-290ad476c846','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),18,'rel','16ce53a5-a172-49d2-97e6-69d026e24616','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),18,'node','16ce53a5-a172-49d2-97e6-69d026e24616','id','rel','16ce53a5-a172-49d2-97e6-69d026e24616','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),18,'rel','16ce53a5-a172-49d2-97e6-69d026e24616','to_id','node','ccb13fa4-411f-4916-9d4e-290ad476c846','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),18,'node','166ff927-dbe1-4a1d-ada3-d0a29171f46a','id','rel','166ff927-dbe1-4a1d-ada3-d0a29171f46a','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),18,'rel','ccb13fa4-411f-4916-9d4e-290ad476c846','to_id','node','5d70df1e-aef4-4cee-b3d3-083321a33d6a','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),18,'rel','16ce53a5-a172-49d2-97e6-69d026e24616','to_id','node','166ff927-dbe1-4a1d-ada3-d0a29171f46a','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),23,'node','da8ac0d4-4cbc-4875-abd2-3ea83eac1c88','id','dataset','tree_ds','node_id',3,3);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),16,'rel','f805daff-04fc-4e4f-b3bc-6b1d5a87ec43','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),3,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','397d7b11-3bf4-44fb-be5e-d1d77ea07543','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),3,'rel','84bca5a4-3ebd-4ee3-b19f-57056f505abc','to_id','node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),3,'rel','84bca5a4-3ebd-4ee3-b19f-57056f505abc','to_id','node','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),3,'node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id','rel','e08e0f7c-85f7-44af-9afe-aeedb38260af','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),3,'node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id','rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),3,'rel','84bca5a4-3ebd-4ee3-b19f-57056f505abc','to_id','node','d13d2d68-f07f-429d-9427-ce9801b63b82','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),3,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),3,'rel','84bca5a4-3ebd-4ee3-b19f-57056f505abc','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),3,'node','84bca5a4-3ebd-4ee3-b19f-57056f505abc','id','rel','84bca5a4-3ebd-4ee3-b19f-57056f505abc','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),3,'rel','84bca5a4-3ebd-4ee3-b19f-57056f505abc','to_id','node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),3,'rel','84bca5a4-3ebd-4ee3-b19f-57056f505abc','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),3,'rel','84bca5a4-3ebd-4ee3-b19f-57056f505abc','to_id','node','d943c4a4-239c-4b6e-aa50-0da53cf712e9','id',1,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'node','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','id','rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'node','022d3cf7-6c3e-4e79-88e2-c9612df1f801','id','rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','1b80db34-9947-44a6-8a7d-2ec385848873','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','e36b3b7b-ee2f-401c-bf20-29a5eb04881e','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','5d3272d2-a131-4c89-a0b5-de0be03ae5ca','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','f04190e4-9f58-41fa-a36a-7972f9acebad','to_id','node','21edea70-88a6-48a1-9ce6-65b8791589ef','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','to_id','node','257d48a9-d4e4-43ab-91ca-b90ab5fd4a40','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id','rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','from_id',7,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'node','f04190e4-9f58-41fa-a36a-7972f9acebad','id','rel','f04190e4-9f58-41fa-a36a-7972f9acebad','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'node','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','id','rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','from_id',7,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'node','abbd736e-4d87-4525-8a3e-e7f66d436568','id','rel','abbd736e-4d87-4525-8a3e-e7f66d436568','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','to_id','node','0fc0c943-8d0b-4ce4-9c89-a1b774be873c','id',8,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'node','be0ef829-5bb8-4f21-8fa6-d969c21115af','id','rel','be0ef829-5bb8-4f21-8fa6-d969c21115af','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','1e987810-6ade-4b6b-b79f-b452d77ff289','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','a23493b0-6a3b-4654-940f-d575014d389e','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','e08e0f7c-85f7-44af-9afe-aeedb38260af','to_id','node','924c1085-530f-45a6-9d3c-d84b1839eba3','id',8,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','74ad9486-21e5-4df8-9217-d3d38332ad36','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','be0ef829-5bb8-4f21-8fa6-d969c21115af','to_id','node','fa134406-d856-4f4e-bbad-30fbbfe49648','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','abbd736e-4d87-4525-8a3e-e7f66d436568','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'node','d943c4a4-239c-4b6e-aa50-0da53cf712e9','id','rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','to_id','node','fc511fe9-713d-4741-936c-dbcf47117566','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','3efd6007-ed54-47c9-9b50-963ed44377ab','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','abbd736e-4d87-4525-8a3e-e7f66d436568','to_id','node','336ce6ea-a0c6-40fc-af7a-d410c41fedc5','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','232b1704-f28e-424c-960a-a83e9e228ce0','to_id','node','74ad9486-21e5-4df8-9217-d3d38332ad36','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','f04190e4-9f58-41fa-a36a-7972f9acebad','to_id','node','af4e93f5-5ffb-11e4-9803-0800200c9a66','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'node','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','id','rel','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'node','1b80db34-9947-44a6-8a7d-2ec385848873','id','rel','1b80db34-9947-44a6-8a7d-2ec385848873','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','384b717e-acd6-4570-a8a8-d2291664955f','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','62a015af-7c63-4678-9f72-e117b9f4326e','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','62a015af-7c63-4678-9f72-e117b9f4326e','to_id','node','edabd97d-aac4-4326-818f-6ce16f81010d','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','f04190e4-9f58-41fa-a36a-7972f9acebad','to_id','node','be0ef829-5bb8-4f21-8fa6-d969c21115af','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'node','5d3272d2-a131-4c89-a0b5-de0be03ae5ca','id','rel','0f516482-6869-4115-b458-c4eeed1e6a50','to_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'node','7fbd40a8-c0db-4269-99ff-622e7752a3f1','id','rel','7fbd40a8-c0db-4269-99ff-622e7752a3f1','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'node','232b1704-f28e-424c-960a-a83e9e228ce0','id','rel','232b1704-f28e-424c-960a-a83e9e228ce0','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','to_id','node','6a2e0e1c-817c-4445-b6d0-28f1b7dbb13a','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id','rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','from_id',7,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','91d5f27c-a87e-4038-a1d8-da53a2588b5e','id',8,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','232b1704-f28e-424c-960a-a83e9e228ce0','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','af4e93f5-5ffb-11e4-9803-0800200c9a66','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'node','266f85da-4aa7-4b8d-8455-a92b2398861f','id','rel','266f85da-4aa7-4b8d-8455-a92b2398861f','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','fe9021c6-3a6e-418f-97e4-7e305e275804','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'node','e36b3b7b-ee2f-401c-bf20-29a5eb04881e','id','rel','e36b3b7b-ee2f-401c-bf20-29a5eb04881e','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','7fbd40a8-c0db-4269-99ff-622e7752a3f1','to_id','node','ab2a7d5f-dfaa-4011-85ce-e80fcb610cb1','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'node','62a015af-7c63-4678-9f72-e117b9f4326e','id','rel','62a015af-7c63-4678-9f72-e117b9f4326e','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','232b1704-f28e-424c-960a-a83e9e228ce0','to_id','node','af556ac6-f34a-4fdf-ade9-d84db16731e4','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','to_id','node','fe9021c6-3a6e-418f-97e4-7e305e275804','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','to_id','node','d0d3d766-360c-4f51-a0d1-78145f40c9b4','id',8,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','e36b3b7b-ee2f-401c-bf20-29a5eb04881e','to_id','node','d943c4a4-239c-4b6e-aa50-0da53cf712e9','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','62a015af-7c63-4678-9f72-e117b9f4326e','to_id','node','36b7b2f7-4cb0-455d-8f44-98aedfbd4135','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','f04190e4-9f58-41fa-a36a-7972f9acebad','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','266f85da-4aa7-4b8d-8455-a92b2398861f','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id','rel','e08e0f7c-85f7-44af-9afe-aeedb38260af','from_id',7,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','022d3cf7-6c3e-4e79-88e2-c9612df1f801','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','0f516482-6869-4115-b458-c4eeed1e6a50','from_id','node','0f516482-6869-4115-b458-c4eeed1e6a50','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','e36b3b7b-ee2f-401c-bf20-29a5eb04881e','to_id','node','68c48e9f-7e6e-4da2-9da2-7028ad7d8e35','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','266f85da-4aa7-4b8d-8455-a92b2398861f','to_id','node','edd94aa2-433f-4eea-a80b-3082083092f6','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','af556ac6-f34a-4fdf-ade9-d84db16731e4','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','to_id','node','3aca4bf0-b0e4-46f3-9370-263d88559a19','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','266f85da-4aa7-4b8d-8455-a92b2398861f','to_id','node','198a2cca-afe7-4f72-9c7e-51ac96ab8510','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','d943c4a4-239c-4b6e-aa50-0da53cf712e9','to_id','node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','to_id','node','70709cc4-f6a5-4e8e-9f6d-07c710a460df','id',8,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','7fbd40a8-c0db-4269-99ff-622e7752a3f1','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id','rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','abbd736e-4d87-4525-8a3e-e7f66d436568','to_id','node','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','022d3cf7-6c3e-4e79-88e2-c9612df1f801','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','397d7b11-3bf4-44fb-be5e-d1d77ea07543','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),6,'rel','d453ccd4-ff88-4001-923c-8d37e88319b3','to_id','node','448f35fa-aa85-4dd1-ab0d-68770bf22950','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),6,'rel','d453ccd4-ff88-4001-923c-8d37e88319b3','to_id','node','b2b5d855-91c8-47e3-954e-bffb3071dd76','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),6,'rel','d453ccd4-ff88-4001-923c-8d37e88319b3','to_id','node','ea076d36-1053-47fb-9bfc-4a75090e6721','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),6,'node','d453ccd4-ff88-4001-923c-8d37e88319b3','id','rel','d453ccd4-ff88-4001-923c-8d37e88319b3','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),22,'rel','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),22,'node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id','rel','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),22,'node','a6d00127-e1ff-41dc-ac24-749ef8146a5e','id','rel','a6d00127-e1ff-41dc-ac24-749ef8146a5e','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),22,'rel','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','to_id','node','a6d00127-e1ff-41dc-ac24-749ef8146a5e','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),22,'node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id','dataset','tree_ds','node_id',3,3);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),1,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),14,'rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','to_id','node','7e820077-7d8c-4c73-9564-596b2914bf97','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),14,'rel','8426e2e7-0068-4f2f-acd1-7099c2622f13','to_id','node','024b7e52-56dc-4152-9629-de0f619df67c','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),14,'rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','to_id','node','b2a5331a-a7a8-4dcb-9308-bf207ca38c49','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),14,'rel','23a10797-7d81-4303-9228-f51625c7a8de','to_id','node','fe5770c4-49f6-4e97-a299-04db5ccdf75d','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),14,'node','8426e2e7-0068-4f2f-acd1-7099c2622f13','id','rel','8426e2e7-0068-4f2f-acd1-7099c2622f13','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),14,'node','ac7314eb-148d-4476-abed-ba7fc679a817','id','rel','ac7314eb-148d-4476-abed-ba7fc679a817','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),14,'rel','64c650a2-4d67-4977-9370-a3dcfbcf89db','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),14,'rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','to_id','node','ec529fb1-87cd-430d-a322-fa086ad8bb2b','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),14,'node','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','id','rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),14,'rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),14,'rel','8426e2e7-0068-4f2f-acd1-7099c2622f13','to_id','node','fe5770c4-49f6-4e97-a299-04db5ccdf75d','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),14,'rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','to_id','node','8426e2e7-0068-4f2f-acd1-7099c2622f13','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),14,'rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','to_id','node','23a10797-7d81-4303-9228-f51625c7a8de','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),14,'rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','to_id','node','64c650a2-4d67-4977-9370-a3dcfbcf89db','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),14,'rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','to_id','node','9c306e89-f472-40b8-9cd7-c1f125a54c42','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),14,'node','23a10797-7d81-4303-9228-f51625c7a8de','id','rel','23a10797-7d81-4303-9228-f51625c7a8de','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),14,'node','64c650a2-4d67-4977-9370-a3dcfbcf89db','id','rel','64c650a2-4d67-4977-9370-a3dcfbcf89db','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),14,'rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','to_id','node','ac7314eb-148d-4476-abed-ba7fc679a817','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),9,'rel','3415687c-4b36-45d7-ab0e-7619560167df','to_id','node','3c695dae-4b39-41f3-98bc-4ddb3aa21664','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),9,'rel','513a2be6-efac-4f19-aa71-e3ec8f47fc91','to_id','node','ad7bab38-8a99-46d5-ad11-06ca00431ea4','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),9,'rel','3415687c-4b36-45d7-ab0e-7619560167df','to_id','node','48cedebe-45eb-4c1f-b2fa-ee7b410c8920','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),9,'node','3c695dae-4b39-41f3-98bc-4ddb3aa21664','id','rel','3c695dae-4b39-41f3-98bc-4ddb3aa21664','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),9,'rel','3415687c-4b36-45d7-ab0e-7619560167df','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),9,'rel','3415687c-4b36-45d7-ab0e-7619560167df','to_id','node','4673d875-5627-4769-aa6f-22b1029942de','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),9,'node','3415687c-4b36-45d7-ab0e-7619560167df','id','rel','3415687c-4b36-45d7-ab0e-7619560167df','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),9,'node','513a2be6-efac-4f19-aa71-e3ec8f47fc91','id','rel','513a2be6-efac-4f19-aa71-e3ec8f47fc91','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),9,'rel','3415687c-4b36-45d7-ab0e-7619560167df','to_id','node','c24346f6-5bb4-4865-bb68-4810dcff2eeb','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),9,'rel','3c695dae-4b39-41f3-98bc-4ddb3aa21664','to_id','node','6c2fab16-1b58-418d-b06e-0c5cb0252067','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),9,'rel','3c695dae-4b39-41f3-98bc-4ddb3aa21664','to_id','node','513a2be6-efac-4f19-aa71-e3ec8f47fc91','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),25,'node','560ea664-be2c-47b9-b602-9150f09f191e','id','rel','560ea664-be2c-47b9-b602-9150f09f191e','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),25,'rel','560ea664-be2c-47b9-b602-9150f09f191e','from_id','node','560ea664-be2c-47b9-b602-9150f09f191e','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),25,'node','3c695dae-4b39-41f3-98bc-4ddb3aa21664','id','rel','560ea664-be2c-47b9-b602-9150f09f191e','to_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),25,'rel','3c695dae-4b39-41f3-98bc-4ddb3aa21664','to_id','node','6c2fab16-1b58-418d-b06e-0c5cb0252067','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),25,'rel','3c695dae-4b39-41f3-98bc-4ddb3aa21664','to_id','node','513a2be6-efac-4f19-aa71-e3ec8f47fc91','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),25,'rel','560ea664-be2c-47b9-b602-9150f09f191e','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),25,'node','3c695dae-4b39-41f3-98bc-4ddb3aa21664','id','rel','3c695dae-4b39-41f3-98bc-4ddb3aa21664','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),8,'rel','c086f87e-c53b-452c-a1f4-c392e70de1db','to_id','node','3c695dae-4b39-41f3-98bc-4ddb3aa21664','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),2,'rel','e2276a2f-9a58-46c4-b0c8-87cd18eb17ee','to_id','node','4521425f-eb43-4a05-a464-1dfd8d1b1536','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'rel','ab722707-0ffa-4113-89e8-a4ced25cd4d4','to_id','node','09dd043e-813a-4835-a7ac-1b2c84d035cd','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'rel','09dd043e-813a-4835-a7ac-1b2c84d035cd','to_id','node','16ce53a5-a172-49d2-97e6-69d026e24616','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'rel','ab722707-0ffa-4113-89e8-a4ced25cd4d4','to_id','node','3c695dae-4b39-41f3-98bc-4ddb3aa21664','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'rel','ccb13fa4-411f-4916-9d4e-290ad476c846','to_id','node','5d70df1e-aef4-4cee-b3d3-083321a33d6a','id',8,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'node','16ce53a5-a172-49d2-97e6-69d026e24616','id','rel','16ce53a5-a172-49d2-97e6-69d026e24616','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'rel','43cfecae-33f8-4663-a87d-a0c6951249d9','from_id','node','43cfecae-33f8-4663-a87d-a0c6951249d9','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'node','3c695dae-4b39-41f3-98bc-4ddb3aa21664','id','rel','c086f87e-c53b-452c-a1f4-c392e70de1db','to_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'rel','43cfecae-33f8-4663-a87d-a0c6951249d9','to_id','node','691e35a6-e4ca-4d1b-8114-7d65a10f8abf','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'rel','7fae9564-b0f2-4296-86e3-7d7e6f2124aa','to_id','node','1cf1f968-302a-4fb8-86e3-31560532f4f8','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'node','ccb13fa4-411f-4916-9d4e-290ad476c846','id','rel','ccb13fa4-411f-4916-9d4e-290ad476c846','from_id',7,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'rel','ab722707-0ffa-4113-89e8-a4ced25cd4d4','to_id','node','687f084d-3978-4812-8f91-90b9bd3e06bd','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'rel','16ce53a5-a172-49d2-97e6-69d026e24616','to_id','node','ccb13fa4-411f-4916-9d4e-290ad476c846','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'rel','ad5afedb-5580-4d17-8488-1006b9d4ba40','from_id','node','ad5afedb-5580-4d17-8488-1006b9d4ba40','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'rel','16ce53a5-a172-49d2-97e6-69d026e24616','to_id','node','166ff927-dbe1-4a1d-ada3-d0a29171f46a','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'node','3c695dae-4b39-41f3-98bc-4ddb3aa21664','id','rel','43cfecae-33f8-4663-a87d-a0c6951249d9','to_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'rel','43cfecae-33f8-4663-a87d-a0c6951249d9','to_id','node','de108002-4ff6-4be1-ad5a-60059be841c3','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'rel','ab722707-0ffa-4113-89e8-a4ced25cd4d4','to_id','node','7fae9564-b0f2-4296-86e3-7d7e6f2124aa','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'node','7fae9564-b0f2-4296-86e3-7d7e6f2124aa','id','rel','7fae9564-b0f2-4296-86e3-7d7e6f2124aa','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'node','166ff927-dbe1-4a1d-ada3-d0a29171f46a','id','rel','166ff927-dbe1-4a1d-ada3-d0a29171f46a','from_id',7,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'node','09dd043e-813a-4835-a7ac-1b2c84d035cd','id','rel','09dd043e-813a-4835-a7ac-1b2c84d035cd','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'rel','43cfecae-33f8-4663-a87d-a0c6951249d9','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'node','43cfecae-33f8-4663-a87d-a0c6951249d9','id','rel','43cfecae-33f8-4663-a87d-a0c6951249d9','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'node','ab722707-0ffa-4113-89e8-a4ced25cd4d4','id','rel','ab722707-0ffa-4113-89e8-a4ced25cd4d4','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'node','c086f87e-c53b-452c-a1f4-c392e70de1db','id','rel','ad5afedb-5580-4d17-8488-1006b9d4ba40','to_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),24,'rel','c086f87e-c53b-452c-a1f4-c392e70de1db','from_id','node','c086f87e-c53b-452c-a1f4-c392e70de1db','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),21,'rel','43cfecae-33f8-4663-a87d-a0c6951249d9','to_id','node','de108002-4ff6-4be1-ad5a-60059be841c3','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),21,'rel','513a2be6-efac-4f19-aa71-e3ec8f47fc91','to_id','node','ad7bab38-8a99-46d5-ad11-06ca00431ea4','id',6,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),21,'rel','43cfecae-33f8-4663-a87d-a0c6951249d9','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),21,'node','43cfecae-33f8-4663-a87d-a0c6951249d9','id','rel','43cfecae-33f8-4663-a87d-a0c6951249d9','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),21,'node','3c695dae-4b39-41f3-98bc-4ddb3aa21664','id','rel','3c695dae-4b39-41f3-98bc-4ddb3aa21664','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),21,'rel','43cfecae-33f8-4663-a87d-a0c6951249d9','to_id','node','3c695dae-4b39-41f3-98bc-4ddb3aa21664','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),21,'node','513a2be6-efac-4f19-aa71-e3ec8f47fc91','id','rel','513a2be6-efac-4f19-aa71-e3ec8f47fc91','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),21,'rel','43cfecae-33f8-4663-a87d-a0c6951249d9','to_id','node','691e35a6-e4ca-4d1b-8114-7d65a10f8abf','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),21,'rel','3c695dae-4b39-41f3-98bc-4ddb3aa21664','to_id','node','6c2fab16-1b58-418d-b06e-0c5cb0252067','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),21,'rel','3c695dae-4b39-41f3-98bc-4ddb3aa21664','to_id','node','513a2be6-efac-4f19-aa71-e3ec8f47fc91','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),8,'rel','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','from_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),8,'node','2b14f1c4-6b1e-4895-84e4-3ede9e367c0b','id','rel','2b14f1c4-6b1e-4895-84e4-3ede9e367c0b','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),8,'node','c086f87e-c53b-452c-a1f4-c392e70de1db','id','rel','ad5afedb-5580-4d17-8488-1006b9d4ba40','to_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),8,'rel','c086f87e-c53b-452c-a1f4-c392e70de1db','to_id','node','2b14f1c4-6b1e-4895-84e4-3ede9e367c0b','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),8,'node','ad5afedb-5580-4d17-8488-1006b9d4ba40','id','rel','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','to_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),8,'rel','c086f87e-c53b-452c-a1f4-c392e70de1db','to_id','node','659c8886-09fc-4e87-b986-ebd7192ef4f5','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),8,'rel','ad5afedb-5580-4d17-8488-1006b9d4ba40','to_id','node','b4a038ff-dd42-4686-b85d-51be1fd3faca','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),8,'node','c086f87e-c53b-452c-a1f4-c392e70de1db','id','rel','c086f87e-c53b-452c-a1f4-c392e70de1db','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),8,'node','ad5afedb-5580-4d17-8488-1006b9d4ba40','id','rel','ad5afedb-5580-4d17-8488-1006b9d4ba40','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),8,'rel','ad5afedb-5580-4d17-8488-1006b9d4ba40','from_id','node','ad5afedb-5580-4d17-8488-1006b9d4ba40','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),8,'node','c086f87e-c53b-452c-a1f4-c392e70de1db','id','files','c086f87e-c53b-452c-a1f4-c392e70de1db','node_id',3,3);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),14,'rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','to_id','files','9c306e89-f472-40b8-9cd7-c1f125a54c42','node_id',3,3);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),26,'rel','691e35a6-e4ca-4d1b-8114-7d65a10f8abf','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),26,'node','691e35a6-e4ca-4d1b-8114-7d65a10f8abf','id','rel','691e35a6-e4ca-4d1b-8114-7d65a10f8abf','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),26,'rel','691e35a6-e4ca-4d1b-8114-7d65a10f8abf','to_id','node','a0d589cf-ea6f-4da6-b499-26618b84883b','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),27,'rel','debc2b0d-3744-4e27-a9aa-99daa086b91a','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),27,'node','debc2b0d-3744-4e27-a9aa-99daa086b91a','id','rel','debc2b0d-3744-4e27-a9aa-99daa086b91a','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),27,'rel','debc2b0d-3744-4e27-a9aa-99daa086b91a','to_id','node','dc7fe95b-679f-41fc-9228-aabbfc252d26','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),27,'rel','67a36e83-a3ab-4c51-bac5-ae4a2fd0c653','to_id','node','debc2b0d-3744-4e27-a9aa-99daa086b91a','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),27,'node','dc7fe95b-679f-41fc-9228-aabbfc252d26','id','rel','dc7fe95b-679f-41fc-9228-aabbfc252d26','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),27,'node','67a36e83-a3ab-4c51-bac5-ae4a2fd0c653','id','rel','67a36e83-a3ab-4c51-bac5-ae4a2fd0c653','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),28,'rel','debc2b0d-3744-4e27-a9aa-99daa086b91a','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),28,'node','debc2b0d-3744-4e27-a9aa-99daa086b91a','id','rel','debc2b0d-3744-4e27-a9aa-99daa086b91a','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),28,'rel','debc2b0d-3744-4e27-a9aa-99daa086b91a','to_id','node','dc7fe95b-679f-41fc-9228-aabbfc252d26','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),28,'rel','f3605afe-86ac-46d5-b978-024132ae11e9','to_id','node','debc2b0d-3744-4e27-a9aa-99daa086b91a','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),28,'node','dc7fe95b-679f-41fc-9228-aabbfc252d26','id','rel','dc7fe95b-679f-41fc-9228-aabbfc252d26','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),28,'node','f3605afe-86ac-46d5-b978-024132ae11e9','id','rel','f3605afe-86ac-46d5-b978-024132ae11e9','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),28,'rel','f3605afe-86ac-46d5-b978-024132ae11e9','to_id','node','c9adafcf-61bd-4d60-8405-17e56a5f3a95','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),29,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','from_id','node','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),29,'node','f04190e4-9f58-41fa-a36a-7972f9acebad','id','rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),29,'node','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','id','rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),29,'node','f04190e4-9f58-41fa-a36a-7972f9acebad','id','rel','f04190e4-9f58-41fa-a36a-7972f9acebad','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),29,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),29,'node','be0ef829-5bb8-4f21-8fa6-d969c21115af','id','rel','be0ef829-5bb8-4f21-8fa6-d969c21115af','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','from_id','node','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','22223124-80ba-4136-82c5-fc1bbf35d400','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','397d7b11-3bf4-44fb-be5e-d1d77ea07543','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'node','1b80db34-9947-44a6-8a7d-2ec385848873','id','rel','1b80db34-9947-44a6-8a7d-2ec385848873','from_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'rel','abbd736e-4d87-4525-8a3e-e7f66d436568','to_id','node','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','232b1704-f28e-424c-960a-a83e9e228ce0','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id','rel','e08e0f7c-85f7-44af-9afe-aeedb38260af','from_id',5,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id','rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'rel','266f85da-4aa7-4b8d-8455-a92b2398861f','to_id','node','edd94aa2-433f-4eea-a80b-3082083092f6','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'rel','0f516482-6869-4115-b458-c4eeed1e6a50','from_id','node','0f516482-6869-4115-b458-c4eeed1e6a50','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'node','266f85da-4aa7-4b8d-8455-a92b2398861f','id','rel','266f85da-4aa7-4b8d-8455-a92b2398861f','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'node','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','id','rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','abbd736e-4d87-4525-8a3e-e7f66d436568','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'rel','8ad01266-66fa-405a-a38c-5f1bd2f618ab','to_id','node','e08e0f7c-85f7-44af-9afe-aeedb38260af','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','8ad01266-66fa-405a-a38c-5f1bd2f618ab','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','5d3272d2-a131-4c89-a0b5-de0be03ae5ca','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'node','232b1704-f28e-424c-960a-a83e9e228ce0','id','rel','232b1704-f28e-424c-960a-a83e9e228ce0','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'node','5d3272d2-a131-4c89-a0b5-de0be03ae5ca','id','rel','0f516482-6869-4115-b458-c4eeed1e6a50','to_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','18177e57-bcc9-4a9b-b389-cf794ac3c727','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id','node','2848c2b1-b653-4f8e-84e8-4ab1c4adab68','id',4,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','266f85da-4aa7-4b8d-8455-a92b2398861f','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'node','1b80db34-9947-44a6-8a7d-2ec385848873','id','rel','8dbf1d7b-43bc-47bf-9ca6-55e67dcf6dbc','to_id',1,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'rel','1b80db34-9947-44a6-8a7d-2ec385848873','to_id','node','c03a8ce7-6bc6-4bda-a179-64ca6ab028db','id',2,1);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'node','abbd736e-4d87-4525-8a3e-e7f66d436568','id','rel','abbd736e-4d87-4525-8a3e-e7f66d436568','from_id',3,2);
insert into sync.sync_tree(id,reglament_id,from_schema_name,from_table_name,from_column_name,to_schema_name,to_table_name,to_column_name,layer_num,order_num)
values(nextval('sync.sync_tree_id_seq'),30,'node','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','id','rel','f5945f1e-c791-45c5-b5e0-d79fa1b0da76','from_id',5,2);
