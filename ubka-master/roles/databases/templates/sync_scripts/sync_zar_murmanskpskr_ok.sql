DO
$DO$
BEGIN

/*Global Zone IP*/
IF (EXISTS (SELECT * FROM sync.constants WHERE key='current_ip')) THEN
  UPDATE sync.constants SET value = '192.168.64.1' where key = 'current_ip';
ELSE
  INSERT INTO sync.constants (key, value) values ('current_ip', '192.168.64.1');
END IF;

/*OWT*/
DELETE FROM sync.sync_node WHERE id = 1;

/*2-ой узел, о нем ничего не знаем*/
IF (EXISTS (SELECT id -- 
	FROM sync.sync_node
	WHERE id = 2)) then
 UPDATE sync.sync_node
 set 
  ip = null,
  period = null,
  send_port = null,
  recv_port = null,
  node_bit = 1,
  limit_rows = 200,
  is_closed = false,
  group_type = null
 where id = 2;
ELSE
	INSERT INTO sync.sync_node(id, ip, period, send_port, recv_port, node_bit, limit_rows, is_closed, group_type)
	VALUES (2, null, null, null, null, 1, 200, false, null);
end if;

/*ПУ ОК*/
IF (EXISTS (SELECT id -- 
	FROM sync.sync_node
	WHERE id = 3)) then
 UPDATE sync.sync_node
 set 
  ip = '192.168.72.90',
  period = '1',
  recv_port = '8001',
  send_port = '8001',
  node_bit = 2,
  limit_rows = 200,
  is_closed = false,
  group_type = 'pu'
 where id = 3;
ELSE
	INSERT INTO sync.sync_node(id, ip, period, send_port, recv_port, node_bit, limit_rows, is_closed, group_type)
	VALUES (3, '192.168.72.90', '1', '8001', '8001', 2, 200, false, 'pu');
end if;

/*4-ой узел, о нем ничего не знаем*/
IF (EXISTS (SELECT id -- 
	FROM sync.sync_node
	WHERE id = 4)) then
 UPDATE sync.sync_node
 set 
  ip = null,
  period = null,
  send_port = null,
  recv_port = null,
  node_bit = 3,
  limit_rows = 200,
  is_closed = false,
  group_type = null
 where id = 4;
ELSE
	INSERT INTO sync.sync_node(id, ip, period, send_port, recv_port, node_bit, limit_rows, is_closed, group_type)
	VALUES (4, null, null, null, null, 3, 200, false, null);
end if;

/*5-ой узел, о нем ничего не знаем*/
IF (EXISTS (SELECT id -- 
	FROM sync.sync_node
	WHERE id = 5)) then
 UPDATE sync.sync_node
 set 
  ip = null,
  period = null,
  send_port = null,
  recv_port = null,
  node_bit = 4,
  limit_rows = 200,
  is_closed = false,
  group_type = null
 where id = 5;
ELSE
	INSERT INTO sync.sync_node(id, ip, period, send_port, recv_port, node_bit, limit_rows, is_closed, group_type)
	VALUES (5, null, null, null, null, 4, 200, false, null);
end if;

/*6-ой узел, о нем ничего не знаем*/
IF (EXISTS (SELECT id -- 
	FROM sync.sync_node
	WHERE id = 6)) then
 UPDATE sync.sync_node
 set 
  ip = null,
  period = null,
  send_port = null,
  recv_port = null,
  node_bit = 5,
  limit_rows = 200,
  is_closed = false,
  group_type = null
 where id = 6;
ELSE
	INSERT INTO sync.sync_node(id, ip, period, send_port, recv_port, node_bit, limit_rows, is_closed, group_type)
	VALUES (6, null, null, null, null, 5, 200, false, null);
end if;

/*7-ой узел, о нем ничего не знаем*/
IF (EXISTS (SELECT id -- 
	FROM sync.sync_node
	WHERE id = 7)) then
 UPDATE sync.sync_node
 set 
  ip = null,
  period = null,
  send_port = null,
  recv_port = null,
  node_bit = 6,
  limit_rows = 200,
  is_closed = false,
  group_type = null
 where id = 7;
ELSE
	INSERT INTO sync.sync_node(id, ip, period, send_port, recv_port, node_bit, limit_rows, is_closed, group_type)
	VALUES (7, null, null, null, null, 6, 200, false, null);
end if;

/*8-ой узел, о нем ничего не знаем*/
IF (EXISTS (SELECT id -- 
	FROM sync.sync_node
	WHERE id = 8)) then
 UPDATE sync.sync_node
 set 
  ip = null,
  period = null,
  send_port = null,
  recv_port = null,
  node_bit = 7,
  limit_rows = 200,
  is_closed = false,
  group_type = null
 where id = 8;
ELSE
	INSERT INTO sync.sync_node(id, ip, period, send_port, recv_port, node_bit, limit_rows, is_closed, group_type)
	VALUES (8, null, null, null, null, 7, 200, false, null);
end if;

/*9-ой узел, о нем ничего не знаем*/
IF (EXISTS (SELECT id -- 
	FROM sync.sync_node
	WHERE id = 9)) then
 UPDATE sync.sync_node
 set 
  ip = null,
  period = null,
  send_port = null,
  recv_port = null,
  node_bit = 8,
  limit_rows = 200,
  is_closed = false,
  group_type = null
 where id = 9;
ELSE
	INSERT INTO sync.sync_node(id, ip, period, send_port, recv_port, node_bit, limit_rows, is_closed, group_type)
	VALUES (9, null, null, null, null, 8, 200, false, null);
end if;

/*10-ой узел, о нем ничего не знаем*/
IF (EXISTS (SELECT id -- 
	FROM sync.sync_node
	WHERE id = 10)) then
 UPDATE sync.sync_node
 set 
  ip = null,
  period = null,
  send_port = null,
  recv_port = null,
  node_bit = 9,
  limit_rows = 200,
  is_closed = false,
  group_type = null
 where id = 10;
ELSE
	INSERT INTO sync.sync_node(id, ip, period, send_port, recv_port, node_bit, limit_rows, is_closed, group_type)
	VALUES (10, null, null, null, null, 9, 200, false, null);
end if;

/*11-ой узел, о нем ничего не знаем*/
IF (EXISTS (SELECT id -- 
	FROM sync.sync_node
	WHERE id = 11)) then
 UPDATE sync.sync_node
 set 
  ip = null,
  period = null,
  send_port = null,
  recv_port = null,
  node_bit = 10,
  limit_rows = 200,
  is_closed = false,
  group_type = null
 where id = 11;
ELSE
	INSERT INTO sync.sync_node(id, ip, period, send_port, recv_port, node_bit, limit_rows, is_closed, group_type)
	VALUES (11, null, null, null, null, 10, 200, false, null);
end if;

/*12-ой узел, о нем ничего не знаем*/
IF (EXISTS (SELECT id -- 
	FROM sync.sync_node
	WHERE id = 12)) then
 UPDATE sync.sync_node
 set 
  ip = null,
  period = null,
  send_port = null,
  recv_port = null,
  node_bit = 11,
  limit_rows = 200,
  is_closed = false,
  group_type = null
 where id = 12;
ELSE
	INSERT INTO sync.sync_node(id, ip, period, send_port, recv_port, node_bit, limit_rows, is_closed, group_type)
	VALUES (12, null, null, null, null, 11, 200, false, null);
end if;

/*13-ой узел, о нем ничего не знаем*/
IF (EXISTS (SELECT id -- 
	FROM sync.sync_node
	WHERE id = 13)) then
 UPDATE sync.sync_node
 set 
  ip = null,
  period = null,
  send_port = null,
  recv_port = null,
  node_bit = 12,
  limit_rows = 200,
  is_closed = false,
  group_type = null
 where id = 13;
ELSE
	INSERT INTO sync.sync_node(id, ip, period, send_port, recv_port, node_bit, limit_rows, is_closed, group_type)
	VALUES (13, null, null, null, null, 12, 200, false, null);
end if;

/*14-ой узел, о нем ничего не знаем*/
IF (EXISTS (SELECT id -- 
	FROM sync.sync_node
	WHERE id = 14)) then
 UPDATE sync.sync_node
 set 
  ip = null,
  period = null,
  send_port = null,
  recv_port = null,
  node_bit = 13,
  limit_rows = 200,
  is_closed = false,
  group_type = null
 where id = 14;
ELSE
	INSERT INTO sync.sync_node(id, ip, period, send_port, recv_port, node_bit, limit_rows, is_closed, group_type)
	VALUES (14, null, null, null, null, 13, 200, false, null);
end if;

/*15-ой узел, о нем ничего не знаем*/
IF (EXISTS (SELECT id -- 
	FROM sync.sync_node
	WHERE id = 15)) then
 UPDATE sync.sync_node
 set 
  ip = null,
  period = null,
  send_port = null,
  recv_port = null,
  node_bit = 14,
  limit_rows = 200,
  is_closed = false,
  group_type = null
 where id = 15;
ELSE
	INSERT INTO sync.sync_node(id, ip, period, send_port, recv_port, node_bit, limit_rows, is_closed, group_type)
	VALUES (15, null, null, null, null, 14, 200, false, null);
end if;

/*16-ой узел, о нем ничего не знаем*/
IF (EXISTS (SELECT id -- 
	FROM sync.sync_node
	WHERE id = 16)) then
 UPDATE sync.sync_node
 set 
  ip = null,
  period = null,
  send_port = null,
  recv_port = null,
  node_bit = 15,
  limit_rows = 200,
  is_closed = false,
  group_type = null
 where id = 16;
ELSE
	INSERT INTO sync.sync_node(id, ip, period, send_port, recv_port, node_bit, limit_rows, is_closed, group_type)
	VALUES (16, null, null, null, null, 15, 200, false, null);
end if;

/*17-ой узел, о нем ничего не знаем*/
IF (EXISTS (SELECT id -- 
	FROM sync.sync_node
	WHERE id = 17)) then
 UPDATE sync.sync_node
 set 
  ip = null,
  period = null,
  send_port = null,
  recv_port = null,
  node_bit = 16,
  limit_rows = 200,
  is_closed = false,
  group_type = null
 where id = 17;
ELSE
	INSERT INTO sync.sync_node(id, ip, period, send_port, recv_port, node_bit, limit_rows, is_closed, group_type)
	VALUES (17, null, null, null, null, 16, 200, false, null);
end if;

/*18-ой узел, о нем ничего не знаем*/
IF (EXISTS (SELECT id -- 
	FROM sync.sync_node
	WHERE id = 18)) then
 UPDATE sync.sync_node
 set 
  ip = null,
  period = null,
  send_port = null,
  recv_port = null,
  node_bit = 17,
  limit_rows = 200,
  is_closed = false,
  group_type = null
 where id = 18;
ELSE
	INSERT INTO sync.sync_node(id, ip, period, send_port, recv_port, node_bit, limit_rows, is_closed, group_type)
	VALUES (18, null, null, null, null, 17, 200, false, null);
end if;

/*19-ой узел, о нем ничего не знаем*/
IF (EXISTS (SELECT id -- 
	FROM sync.sync_node
	WHERE id = 19)) then
 UPDATE sync.sync_node
 set 
  ip = null,
  period = null,
  send_port = null,
  recv_port = null,
  node_bit = 18,
  limit_rows = 200,
  is_closed = false,
  group_type = null
 where id = 19;
ELSE
	INSERT INTO sync.sync_node(id, ip, period, send_port, recv_port, node_bit, limit_rows, is_closed, group_type)
	VALUES (19, null, null, null, null, 18, 200, false, null);
end if;

/*20-ой узел, о нем ничего не знаем*/
IF (EXISTS (SELECT id -- 
	FROM sync.sync_node
	WHERE id = 20)) then
 UPDATE sync.sync_node
 set 
  ip = null,
  period = null,
  send_port = null,
  recv_port = null,
  node_bit = 19,
  limit_rows = 200,
  is_closed = false,
  group_type = null
 where id = 20;
ELSE
	INSERT INTO sync.sync_node(id, ip, period, send_port, recv_port, node_bit, limit_rows, is_closed, group_type)
	VALUES (20, null, null, null, null, 19, 200, false, null);
end if;

/*21-ой узел, о нем ничего не знаем*/
IF (EXISTS (SELECT id -- 
	FROM sync.sync_node
	WHERE id = 21)) then
 UPDATE sync.sync_node
 set 
  ip = null,
  period = null,
  send_port = null,
  recv_port = null,
  node_bit = 20,
  limit_rows = 200,
  is_closed = false,
  group_type = null
 where id = 21;
ELSE
	INSERT INTO sync.sync_node(id, ip, period, send_port, recv_port, node_bit, limit_rows, is_closed, group_type)
	VALUES (21, null, null, null, null, 20, 200, false, null);
end if;

/*22-ой узел, о нем ничего не знаем*/
IF (EXISTS (SELECT id -- 
	FROM sync.sync_node
	WHERE id = 22)) then
 UPDATE sync.sync_node
 set 
  ip = null,
  period = null,
  send_port = null,
  recv_port = null,
  node_bit = 21,
  limit_rows = 200,
  is_closed = false,
  group_type = null
 where id = 22;
ELSE
	INSERT INTO sync.sync_node(id, ip, period, send_port, recv_port, node_bit, limit_rows, is_closed, group_type)
	VALUES (22, null, null, null, null, 21, 200, false, null);
end if;


END
$DO$