#!/bin/bash

_pass="12345678"
_dc="$1"
_host="$2"

_get_dn=`ldapsearch -x -D 'cn=Manager,dc='${_dc}',dc=local' -w${_pass} -b 'dc='${_dc}',dc=local' -h ${_host} | egrep 'dn:[[:space:]]ou=People.*ou=L[0-9]{1,2}C[0-9]{1,9}.*' | sed 's/^dn\:[[:space:]]//g'`
for i in ${_get_dn}; do

if [ $( ldapsearch -x -D 'cn=Manager,dc='${_dc}',dc=local' -w${_pass} -b ''${i}'' -h ${_host} uid=readwf | egrep -c 'dn:' ) -eq 0 ]; then

OU=`echo ${i} | egrep -o 'L[0-9]{1,2}C[0-9]{1,9}'`
SUM=`ldapsearch -x -D 'cn=Manager,dc='${_dc}',dc=local' -w${_pass} -b ''${i}'' -h ${_host} | egrep 'uidNumber' | sort -n -k2 | tail -n1 | cut -d ' ' -f 2`

echo -e "
dn: uid=readwf,$i
objectClass: top
objectClass: posixAccount
objectClass: shadowAccount
objectClass: organizationalPerson
objectClass: inetOrgPerson
objectClass: person
objectClass: Security
objectClass: orgstruct
objectClass: mail
objectClass: Kiosk
cn: readwf
sn: $(uuidgen)
uid: readwf
uidNumber: $(( $SUM+1 ))
gidNumber: 10000
homeDirectory: /home/readwf
loginShell: /sbin/nologin
gecos: readwf
userPassword: f081bd91-6b22-46d6-b3e3-0d953dc3361c
PersonLabels: $OU
" > ${OU}.ldif

ldapadd -x -w${_pass} -h ${_host} -D 'cn=Manager,dc='${_dc}',dc=local' -f ${OU}.ldif


else
echo "readwf account have been add to ${i}"
   fi

done

rm -f L*.ldif
 
