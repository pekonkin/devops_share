#!/bin/bash

_pass="12345678"
_dc="$1"
_host="$2"

_get_dn=`ldapsearch -x -D 'cn=Manager,dc='${_dc}',dc=local' -w${_pass} -b 'dc='${_dc}',dc=local' -h ${_host} | egrep 'dn:[[:space:]]ou=People.*ou=L[0-9]{1,2}C[0-9]{1,9}.*' | sed 's/^dn\:[[:space:]]//g'`
for i in ${_get_dn}; do

if [ $( ldapsearch -x -D 'cn=Manager,dc='${_dc}',dc=local' -w${_pass} -b ''${i}'' -h ${_host} uid=flyadmin | egrep -c 'dn:' ) -eq 0 ]; then

OU=`echo ${i} | egrep -o 'L[0-9]{1,2}C[0-9]{1,9}'`
SUM=`ldapsearch -x -D 'cn=Manager,dc='${_dc}',dc=local' -w${_pass} -b ''${i}'' -h ${_host} | egrep 'uidNumber' | sort -n -k2 | tail -n1 | cut -d ' ' -f 2`

KEY='ZeNrtXcm24ygS3edX1PG6Vr3sv8EQlkmJIQH5lV+e/PcmkGRLNvJsWaGuOrkp6wnFhSCImd8//or/rZhQUksfHAtyB6v//vU7/d48c0HyCvzg1/YtEX8Mroa/hw9AyJB/4kCZ9IHMs52Er/bJ4cGf49+sOPMzoELXag3uNXRc+5aDX7X0MuRg90bNvhucqdcVKFZmqc1++kdvkBXTrNp7OXx3JZhiBWTQ5/769GsbVnkYPP1zNiOmijxodH6cZo7TMH+fP22nZOzxYdXH/mCcyuwMC6YLcGcru4Id6ODPf/dbaVV81J/2brK5UarWkp9jj2xfVV4W+uOcX9TMCQXe5xlgAmJOpkxHHq9OJ8uJj0+UcSLxxWep8MAdBOYkC3NYLSfjEcPOlutjIn1IXe2DUfI7v/14HWUSiMB8eask7YuDf/gWJcX9bxZRhFu/V+sTLv8EN1Vs/6pT7ykyDBOeb0HdI4Ge2svrn8DD5zfzB7lgsFM2xvGTLbuKB9TGyJ1xRdQgax5q9wCzMy0V+zyfMxfFVPF5XXdUH2io4dzUOuQovUjtZYqvUT1KeU6Tc1CgQbGfD4F56awLqQHcqxYd/gFeX1Ng57AwF7Ts20d6UplEjXgSQ0oFPwOtsBWPd1AipK8MZ7Pmp/HZGp+rZ+bRRj3EaA25s2LtQXOY7VQ9wHG6Vm/QJkzt+ANndJQa2lvjciy8Y7Jia1nJsJ/t/AuwzGXVk9kI5/pJmVx7mPmJG7y556hNjqUguR95fJXvnpdj8zgxg7fl+bxdp+1fXfA6gcHMcdMMDC+pBZoD5sQIH/5+dF0eX4sWm8r4NQSuiMxpI9cYMX6xxtUcM42vKmPMoj9T8pxlc/Vl3OnM7QM49flYgBX3IzBB3v9SVHqU9D6uok+mdrl7wBOVP7mvKgsKHdBZTf26u0DquFCMn7Nf4ZjdSi4Mr1XrLr8XTRWVkbHxb5Ooews4Sm+Q5Lt/BGhk6hOniBDJf3ceB2AOWP7X+I9vj4fZyV/gjmWNenvyJBlT8WMh8ww9ZjuoqoweufJsBw3hmQdjLzUhGuP63xtOhdEyPj+1qFdMupFHH3H07CSbBRHdPE7hzeSZTRw5DpkfXGSvRs3KHITcZiXATQfl9cPylgPz4qGZOdNbhd9zJ+2ocjRn2pXJb5RX0n0vTSOu98fHA7eTHIUwh3kv0EX1kdskgV+uQ0Z574IPxr7AS5M92Oaif0cVY1NrPm7E8Mp4eJTKW6loPEvPWXieGwdr8xg3TILSW+CSVXEn89J/cLY7PSJPwqQxgLzS95YTeCPjQZpTf37VJoAfe/wAqqjY5lIhKnBhxg66APwjrgwHrXW0aDk7+2MqjlTAI/MXzR/76dmdj0Qz8Sg7T+VsQ8fx91k76bu0ujmTd1O649Wxxpxz8/LdZt1GDzClh5zBXTGnfOGkeG4qOVubwArQ6c9/L1WAN7ml6AJ8DuV2HwWmiqeteZKDx6Kttw7w09Qu7iU/2xkvrXo5bTcbJAwzXEc9L5OQ4KTVbCfVs4EvlCJPissoQC7pB/9vh/woJW9L/kNn+dCnu8mI9AtKpmB7gn44HbbkqN4Dc5Q8aiPGMlQPRNhSjurnyyWaCNcDad/9aM6Hc9YUmyQ/z/oyI8bGd96LDrfRTfLo+Bfl9DyW9NtoeCTAief3aflGEzNNgdhWj8uEl1w8teFehLmoZKeP6HcE4lqrOLkytAlRr6WOCEs6rTPBlKaij8VsNpja6bFiiDoa6bwMdTL7qUM5WO/fJW0MeW/5dAiuU3lDjc2LyX1qVq2tuDPer1M1JLLIf8hySMTSluKo0i4AxTGbjPa6eGV7iXFZJyIVKHUwyqxlBSkuW0kfZnA03E51cNIqJsmfZzvJNpUstmEBQOLbUhfEgVzy65OB0FSQRxnVVM+u9wGY8uRXpqq2wOLhfnHjTyuRumYZG2d0kFHvSD0r6AumDpcfFAZzwMzlJcGLZp7E7CS/CFAeCrkD3bQMIo8mcExN2q3dUeda76NEA1y4rbTkBRpqXtbEsYcp0eRhyUqso31bNqaYZd4vAtIBjQMsRPppyEPCWq6dFDWrlrNUpqowpW8HC2PBI64lQ+q2FnFgSoFDtaJNr8V6VEM+ZtG11LroEp9Y/fZSe2DUJ9YbLeqwpw6jdi4aCLXesF1EwtYVFKomDqpJvzS1t1vQRoFm4KUymeDxG9hbsP1ZCgyluWtLvkfL8Yng6Ho2NeFn6mK8Q4O9XlN/0IXAwkzV5J/YJ5vKsyiNfACBBckTHVcvpT84hrXLnhDtHenoTOk8YqT5CaO0FPkI6abGP8g0FOd6McyOQJBxFgOG4g74ihPsFrECPqAFoHKNYwihSOtBUSwlwintgH9kcBtti4Iqv7TOnhS4yOeGvMNG7H/1cJvBTNwzLW3D9J9Jp2X46fh/PFt6/0YKDqmXY1mLs2PgWqOcw/zr/HJ9hIcaojDsNwuijNwd+sJ+lzvqxjNGVTnzQTBZ7Sd0cHXfDVsHINjUn24DyQ4spo6irKCc09chCpHWTcWKePinDUMeVz+35pgCWM8iMf8u4rk55tKQFxl9YOilJo4n6QfZhpXvYAv82oVGl+PT8sQ3jzX4lKWB5DBQqcjiqCoommAqdVFwTCMhnRM/7DNMGwk2XYuGf6poXYBwPgDywkRjYCblCq9B9KtGYyvKNPrleS35zW1BreHWmL07/2mTrSz9YTfobEc8GnMccWAZOeW8gFJpHTWtLXFuL63CDtp0K1EjAFQ/CQPwczb9kn4H0UgKe8rKBPrdAKPJHP2pzJKvLKhM0bQe79/lshRMfu8DKPQB1dTNCsWwAAQWoOzFcyIC8VyCduDj6c231J1yChhWT/U9WjMpobp6iQrbjQURaEy9hsIEmaY8hdiJbw4dOQdDdLoQwGWq8jKmJBCxPhK+gOLBI5i2X3vb0neyWPJzxGMDpebaa2W9YIGRXw5EhOXbeANPlS5CHFRCLgJfUbhNatpCv29AAwjTVxZRwtTAObJbAwrEImAFk9qutT3YJHn/u7aCsj5jRD1N0M2EbVPEgZ33FtF0L2JoimuajldkYdSic8sSMCB61GKXQOebdFcSlId0Pd/aDRopEN8Elrky6a0mkEfifa9tGuUtjVCasm5sAEsbSdjGfd5PdyGw1a2IEqpm1Yzk6lOLIBYRvDsq1FH8dkYd9aVZRkfEHoxS6eWsiXVGdU7nBaBqDNHWfUCb3Zrs8XUkt2RfmYsqpj4x8FJUT/Cku3ovNxGOqFjA67kKVaMDcxqTuPuoT3mhGGqb6LuOdgzG1t/lBhVcIu2xM/QeJNA9gufad/BWUea3zJF3ql2+H/Utm2LwyelK5dr2LMp6poXk++AX4RuLsA4Z9uR7RTZqDxrV9FWf9m62wpnaBgeQzw59DMtzdDVacjQxTa0P0Q/ysTa3XX8vIEKNFXpOOiM0E9/GUU+gSdlAQW4kP/DdkiITqVJ3Sk22Kw2eQ6450lKZxomLIWHqa9nVWwnwki+gdvGgF7wYx7vJPb3G6f3tBK6SFgmhL6u8mE7bTuXZ2HYvmJT6UhTLcQV7LDn7Yg5d9BOW8h9MWt+aMkSus/G1t6B9e0gs4thPCamhuTWZsnbWIJj9LW1RkmSqx58Z0JuC1iYK3pZR02KVoZA7EvYW0GHd81en7BGq+6Rpy7NmvKSNoM2GoQoiqg6pAdiS0noipviuw9VZxNH4BcxG02kBnpiIBDMpeTQIU9U66bh/C6brREUWhtSiwzCh6v0lw1Y49sWqRexRdB6xdXw9dc/8WKe5wUXorbt8eBM6t/bGS9UH9d7ijuvgmbVudCLf44E36wyqMZa6SP1lIq8ROkrsCcG99cmTNzrGSCJ8xnPZ1mHtoG3TP9t58DsjSnnnPNzLIF7cz/Ye2I0vDfZdW0kNw6VdoV+3Spfdn03ZSmNfyP6o3Vj1Zsi//ZZJ9wPqNRa4/2WU05uuB939r2MIMiNEjknL/jkOOLlq7cnBMjdMvZdBe4l3Gbsc07u1uPQ3dxEzkuX3BoQNM//48+N/jJh0/Q=='

echo -e "
dn: uid=flyadmin,$i
objectClass: top
objectClass: posixAccount
objectClass: shadowAccount
objectClass: organizationalPerson
objectClass: inetOrgPerson
objectClass: person
objectClass: Security
objectClass: orgstruct
objectClass: mail
objectClass: Kiosk
cn: flyadmin
sn: $(uuidgen)
uid: flyadmin
uidNumber: $(( $SUM+1 ))
gidNumber: 10000
homeDirectory: /home/flyadmin
loginShell: /sbin/nologin
gecos: flyadmin
userPassword: 12345678
PersonLabels: $OU
permission: $(echo ${KEY})
" > ${OU}.ldif

ldapadd -x -w${_pass} -h ${_host} -D 'cn=Manager,dc='${_dc}',dc=local' -f ${OU}.ldif


else
echo "flyadmin account have been add to ${i}"
   fi

done

rm -f L*.ldif
