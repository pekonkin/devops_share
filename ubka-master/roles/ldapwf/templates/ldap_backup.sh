#!/bin/sh

_dir_ldap='/var/lib/ldap'
_dir_back='/var/lib/backup_ldap'

if [ -e /etc/init.d/slapd ]; then
 _slap_status=`/etc/init.d/slapd status | egrep -c '\(pid.*[0-9].+\)'`
 if [ $_slap_status -eq 1 ]; then
    /etc/init.d/slapd stop
 fi

fi

if [ -d /var/lib/ldap ]; then
   if [ ! -d ${_dir_back} ]; then
      mkdir -p ${_dir_back}
   fi
   	if [ $( find ${_dir_back} -name "ldap_*.tar.gz" | wc -l ) -ge 30 ]; then
      		logger "Find backup files up 30 days"
      		cd ${_dir_back}
      		ls -tr ldap_*.tar.gz | head -n -30 | xargs rm -f
   	fi
   	tar -czf ${_dir_back}/ldap_`date +%Y-%m-%d`.tar.gz ${_dir_ldap}
	logger "Directory ${_dir_ldap} have been backuped"
fi

 if [ $_slap_status -eq 1 ]; then
    /etc/init.d/slapd start
 fi
