#!/bin/bash

if [ -e /etc/init.d/vsmserver ]
 then
    /etc/init.d/vsmserver stop
fi
if [ -e /etc/init.d/vsmagent ]
 then
    /etc/init.d/vsmagent stop
fi

if [ $(ps aux | grep chrome | grep -v grep |awk {'print$2'} | wc -l) -gt 0 ]
 then
   for a in `ps aux | grep chrome | grep -v grep | awk {'print$2'}`
     do
       kill -9 ${a}
   done
fi

for i in `ls -1 /home/`
  do
  if [ -d /home/${i}/.cache ]
   then
      rm -rf /home/${i}/.cache/*
  fi
done

if [ -e /opt/Label/startApp.sh ]; then 
   if [ $( egrep -c 'http://192.168.72.90/mvs' /opt/Label/startApp.sh ) -ge 1 ]; then
     sed -i 's/192.168.72.90\/mvs/arctic.local\/remote\/mvs/g' /opt/Label/startApp.sh
   fi
   if [ $( egrep -c 'spo.local/remote/mvs' /opt/Label/startApp.sh ) -ge 1 ]; then
     sed -i 's/spo.local\/remote\/mvs/arctic.local\/remote\/mvs/g' /opt/Label/startApp.sh
   fi
   if [ $( egrep -c 'http://mail.local:88' /opt/Label/startApp.sh ) -ge 1 ]; then
     sed -i 's/mail.local:88/mail.local\/mail/g' /opt/Label/startApp.sh
   fi
   if [ $( egrep -c '192.168.172.141' /opt/Label/startApp.sh ) -ge 1 ]; then
        sed -i 's/192.168.172.141/arctic.local/g' /opt/Label/startApp.sh
   fi
   if [ $( egrep -c '192.168.172.131' /opt/Label/startApp.sh ) -ge 1 ]; then
        sed -i 's/192.168.172.131/arctic.local/g' /opt/Label/startApp.sh
   fi
   if [ $( egrep -c '192.168.172.140' /opt/Label/startApp.sh ) -ge 1 ]; then
        sed -i 's/192.168.172.140/arctic.local/g' /opt/Label/startApp.sh
   fi
fi

if [ -e /etc/init.d/vsmserver ]
 then
    /etc/init.d/vsmserver start
fi
if [ -e /etc/init.d/vsmagent ]
 then
    /etc/init.d/vsmagent start
fi
