#!/bin/bash

if [ -e /etc/init.d/spo-reports-generator ]; then
   if [ $(ps aux | grep -c 'spo-reports') -ge 3 ]; then
      ps aux | grep 'spo-reports' | awk {'print$2'} | xargs -I {} kill -9
   fi

   nohup /opt/swemel/spo-reports/generator/FastReport.Asp.Net/spo-reports-generator-daemon &
   sleep 3s;

   if [ $(ps aux | grep -c 'spo-reports') -ge 3 ]; then
      ps aux | grep 'spo-reports' | awk {'print$2'} | xargs -I {} kill -9
      /etc/init.d/spo-reports-generator start
      sleep 1s;
   fi


fi