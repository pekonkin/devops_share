#!/usr/bin/env bash

function _default_modified {

if [ -d /var/log/audit ]
 then
    cd /var/log/audit

    if [ -e /var/log/audit/audit.log ]; then
     if [ `ls -1 /var/log/audit/audit.log* | wc -l` -ge 5 ]; then
      ls -tr audit.log* | head -n -3 | xargs rm -f
     fi
    fi
    if [ -e /var/log/audit/etc.log ]; then
     if [ `ls -1 /var/log/audit/etc.log* | wc -l` -ge 5 ]; then
      ls -tr etc.log* | head -n -3 | xargs rm -f
     fi
    fi
    if [ -e /var/log/audit/tmp_audit.log ]; then
     if [ `ls -1 /var/log/audit/tmp_audit.log* | wc -l` -ge 5 ]; then
      ls -tr tmp_audit.log* | head -n -3 | xargs rm -f
     fi
    fi
fi

echo "===================== Disable SELINUX $i only VM=========================="
if [ $( rpm -qa | grep -c libvirtd ) -eq 0 -a $( ls -1 /etc/sysconfig/network-scripts/ifcfg-* | grep -c eth ) -ge 1 ]
 then
    sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config
    setenforce 0
fi

echo "===================== Add deadline online $i =========================="
for i in $(lsblk | grep disk | awk {'print$1'})
do
echo "===================== Edit deadline $i =========================="
echo 'deadline' > /sys/block/${i}/queue/scheduler
done

echo "===================== Edit network-scripts $i =========================="

_net="/sys/class/net"
_net_conf="/etc/sysconfig/network-scripts/ifcfg"
_net_tune="/etc/udev/rules.d/70-persistent-net.rules"

if [ -d /sys/class/net ]
then
        for _interface in `ls -1 ${_net} | grep -v lo`
          do

            /sbin/ifconfig ${_interface} txqueuelen 5000

            if [ -e ${_net_conf}-${_interface} ]
             then
               if [ $( egrep -c 'IPADDR' ${_net_conf}-${_interface} ) -eq 1 ]
               then
                  sed -i 's/BOOTPROTO=none/BOOTPROTO=static/g' ${_net_conf}-${_interface}
               fi
               if [ $(egrep -c 'eth' ${_net_conf}-${_interface}) -ge 1 -a $(egrep -c 'ETHTOOL_OPTS' ${_net_conf}-${_interface}) -ge 1 ]
               then
                  sed -i '/^ETHTOOL_OPTS/d' ${_net_conf}-${_interface}
               fi
               if [ $(egrep -ci 'bridge' ${_net_conf}-${_interface} ) -eq 1 -a $( ip a | egrep -c 'vnet') -ge 2 ]
               then
                  if [ $( egrep -c 'rx 4096' ${_net_conf}-${_interface} ) -eq 0 ]; then
                  _line=`wc -l ${_net_conf}-${_interface} | awk {'print$1'}`
                  sed -i ''${_line}' i\ETHTOOL_OPTS="-G '${_interface}' rx 4096 tx 4096"' ${_net_conf}-${_interface}
                  fi
               fi

            fi
        done
               if [ -e ${_net_tune} ]
                then
                      sed -i '/^KERNEL==/d' ${_net_tune}
                      for i in $(ls -1 ${_net} | grep -v lo | sed 's/[[:digit:]]/*/g' | sed -e '/\*\*/d' -e '/virbr/d' | sort -u )
                      do
                      sed -i '1 i\KERNEL=="'${i}'", RUN+="/sbin/ip link set %k txqueuelen 5000"' ${_net_tune}
                      done
               fi
else
        exit 1
fi


echo "===================== Edit modules $i =========================="
#### Add modules tcp_westwood

if [ ! -e /etc/modules ]
 then
  > /etc/modules
  echo "tcp_westwood" >> /etc/modules
  echo "tcp_htcp" >> /etc/modules
 else
        if [ $(egrep -c 'tcp_westwood' /etc/modules ) -eq 0  ]
        then
           echo "tcp_westwood" >> /etc/modules
        fi
        if [ $(egrep -c 'tcp_htcp' /etc/modules ) -eq 0  ]
        then
           echo "tcp_htcp" >> /etc/modules
        fi
fi

modprobe tcp_westwood
modprobe tcp_htcp

echo "Module `lsmod | grep tcp_westwood | awk {'print$1'}` upload"

echo "===================== Modified mtab $i =========================="

rm -f /etc/mtab
ln -sf /proc/mounts /etc/mtab

echo "===================== Modified sysctl.conf $i =========================="
cat <<OEF > /etc/sysctl.conf
kernel.printk=3 4 1 3

vm.swappiness = 20
vm.dirty_background_ratio = 5
vm.dirty_ratio = 10
vm.dirty_bytes = 8388608
vm.dirty_background_bytes = 8388608
vm.dirty_writeback_centisecs = 1500
vm.overcommit_memory = 2
vm.overcommit_ratio = 100
fs.inotify.max_user_watches = 33554432

net.core.somaxconn = 65535
net.core.rmem_default = 65536
net.core.wmem_default = 65536
net.core.rmem_max = 33554432
net.core.wmem_max = 33554432
net.ipv4.tcp_max_orphans = 65536
net.ipv4.tcp_rmem = 4096 87380 33554432
net.ipv4.tcp_wmem = 4096 65536 33554432
net.ipv4.tcp_window_scaling = 1
net.ipv4.tcp_fin_timeout = 30
net.ipv4.tcp_keepalive_intvl = 30
net.ipv4.tcp_tw_recycle = 1
net.ipv4.tcp_tw_reuse = 1
net.ipv4.tcp_congestion_control = htcp
net.ipv4.tcp_max_syn_backlog = 16384
net.ipv4.tcp_max_tw_buckets = 16384
net.ipv4.tcp_synack_retries = 1

net.ipv4.icmp_echo_ignore_all = 0
net.ipv4.icmp_echo_ignore_broadcasts = 0
net.ipv4.icmp_ignore_bogus_error_responses = 1
net.ipv4.ip_forward = 0
net.ipv4.ip_local_port_range = 1024 65535
net.ipv4.neigh.default.gc_stale_time = 7200
net.ipv4.neigh.default.gc_thresh1 = 225280
net.ipv4.neigh.default.gc_thresh2 = 450560
net.ipv4.neigh.default.gc_thresh3 = 450560
net.ipv4.route.flush = 1

net.ipv4.tcp_keepalive_time = 1800

net.ipv4.tcp_sack = 1
net.ipv4.tcp_slow_start_after_idle = 0
net.ipv4.tcp_syncookies = 1
net.ipv4.tcp_syn_retries = 3
net.ipv4.tcp_timestamps = 1
net.ipv4.tcp_rfc1337 = 1
net.ipv4.ip_nonlocal_bind = 1
net.ipv4.tcp_keepalive_probes = 5
net.ipv4.tcp_orphan_retries = 0
net.ipv4.tcp_no_metrics_save = 1
net.core.netdev_max_backlog = 1000
net.core.default_qdisc = pfifo_fast
net.ipv4.igmp_max_memberships = 50
net.ipv4.conf.all.accept_redirects = 0
net.ipv4.conf.default.accept_redirects = 0
net.ipv4.conf.all.accept_source_route = 0
net.ipv4.conf.default.accept_source_route = 0
net.ipv4.conf.all.secure_redirects = 0
net.ipv4.conf.default.secure_redirects = 0
net.ipv4.conf.all.send_redirects = 0
net.ipv4.conf.default.send_redirects = 0
net.ipv4.conf.all.arp_filter = 0
net.ipv4.conf.default.arp_filter = 0
net.ipv4.conf.all.rp_filter = 0
net.ipv4.conf.default.rp_filter = 0
net.ipv4.conf.all.log_martians = 0
net.ipv4.conf.lo.accept_source_route = 0
net.ipv4.conf.lo.rp_filter = 0
net.ipv4.tcp_dsack = 0
net.ipv4.tcp_fack = 0

OEF

if [ $(sysctl -a | grep -c net.ipv6.conf.all.disable_ipv6 ) -eq 1 ]
 then
    echo "net.ipv6.conf.all.disable_ipv6 = 1" | tee -a /etc/sysctl.conf
fi

if [ $(sysctl -a | grep -c net.netfilter.nf_conntrack_max ) -eq 1 ]
 then
   echo "net.netfilter.nf_conntrack_max = 65536" | tee -a /etc/sysctl.conf
fi

if [ $(sysctl -a | grep -c net.netfilter.nf_conntrack_generic_timeout ) -eq 1 ]
 then
   echo "net.netfilter.nf_conntrack_generic_timeout = 120" | tee -a /etc/sysctl.conf
fi

if [ $(sysctl -a | grep -c net.netfilter.nf_conntrack_tcp_timeout_established ) -eq 1 ]
 then
   echo "net.netfilter.nf_conntrack_tcp_timeout_established = 120" | tee -a /etc/sysctl.conf
fi

if [ $(sysctl -a | grep -c net.netfilter.nf_conntrack_tcp_timeout_max_retrans ) -eq 1 ]
 then
   echo "net.netfilter.nf_conntrack_tcp_timeout_max_retrans = 120" | tee -a /etc/sysctl.conf
fi

if [ $(sysctl -a | grep -c net.netfilter.nf_conntrack_tcp_timeout_unacknowledged ) -eq 1 ]
 then
   echo "net.netfilter.nf_conntrack_tcp_timeout_unacknowledged = 120" | tee -a /etc/sysctl.conf
fi

if [ $(sysctl -a | grep -c net.netfilter.nf_conntrack_udp_timeout_stream ) -eq 1 ]
 then
   echo "net.netfilter.nf_conntrack_udp_timeout_stream = 60" | tee -a /etc/sysctl.conf
fi

if [ $(sysctl -a | grep -c net.bridge.bridge-nf-call-ip6tables ) -eq 1 ]
 then
   echo "net.bridge.bridge-nf-call-ip6tables = 0" | tee -a /etc/sysctl.conf
fi

if [ $(sysctl -a | grep -c net.bridge.bridge-nf-call-iptables ) -eq 1 ]
 then
   echo "net.bridge.bridge-nf-call-iptables = 0" | tee -a /etc/sysctl.conf
fi

if [ $(sysctl -a | grep -c net.bridge.bridge-nf-call-arptables ) -eq 1 ]
 then
   echo "net.bridge.bridge-nf-call-arptables = 0" | tee -a /etc/sysctl.conf
fi

if [ $( egrep -c 'madvise' /etc/rc.local ) -ge 1 ]
   then
     sed -i '/madvise/d' /etc/rc.local
fi

if [ $( egrep -c 'always' /etc/rc.local ) -ge 1 ]
   then
     sed -i '/always/d' /etc/rc.local
fi

if [ $( egrep -c '4096' /etc/rc.local ) -ge 1 ]
   then
     sed -i '/4096/d' /etc/rc.local
fi

if [ $( egrep -c '1000' /etc/rc.local ) -ge 1 ]
   then
     sed -i '/1000/d' /etc/rc.local
fi

if [ $( egrep -c 'echo 1' /etc/rc.local ) -ge 1 ]
   then
     sed -i '/echo 1/d' /etc/rc.local
fi


echo always > /sys/kernel/mm/redhat_transparent_hugepage/enabled
echo always > /sys/kernel/mm/redhat_transparent_hugepage/defrag
echo always > /sys/kernel/mm/transparent_hugepage/enabled
echo always > /sys/kernel/mm/transparent_hugepage/defrag



echo "===================== Add Modified shmax|min in sysctl.conf $i =========================="
page_size=`getconf PAGE_SIZE`
phys_pages=`getconf _PHYS_PAGES`
shmall=`expr $phys_pages / 2`
shmmax=`expr $shmall \* $page_size`
echo "kernel.shmmax = $shmmax" | tee -a /etc/sysctl.conf
echo "kernel.shmall = $shmall" | tee -a /etc/sysctl.conf

echo "===================== Add Semaphore Limits in sysctl.conf $i =========================="
echo "kernel.sem = `LANG=C ipcs -l | egrep -A5 "Semaphore Limits" | awk -F\= {'print$2'} | tr -s "\n" " " | awk {'print$2,$3,$4,$1'}`" | tee -a /etc/sysctl.conf

sysctl -p /etc/sysctl.conf > /dev/null 2>&1

}

function _grub_default {
echo "===================== Edit grub $i =========================="

_grub="/boot/grub/grub.conf"

if [ -e ${_grub} ]
then
 if [ $( egrep -c 'deadline' ${_grub} ) -eq 0 ]
  then
     sed -i.original '/kernel \/vmlinuz-2.6/ s|$| elevator=deadline rd.fstab=no acpi=noirq noapic|' ${_grub}
 fi
fi

}

function _grub_kvm {
echo "===================== Edit grub $i =========================="

_grub="/boot/grub/grub.conf"

if [ -e ${_grub} ]
then
 if [ $( egrep -c 'hugepagesz' ${_grub} ) -eq 0 ]
  then
     sed -i.original '/kernel \/vmlinuz-2.6/ s|$| elevator=deadline rd.fstab=no acpi=noirq noapic swapaccount=1 hugepagesz=1GB hugepages=1|' ${_grub}
 fi
fi
}


function _tune_kvm {
echo "===================== Enable HugePage $i =========================="

if [ $( grep -c '^vm.nr_hugepages' /etc/sysctl.conf ) -eq 0 ]
 then
    if [ "$(( `free -m | grep 'Mem:' | awk {'print$4'}` /2 ))" -gt "15000" ]
     then
       echo "vm.nr_hugepages = 15000" | tee -a /etc/sysctl.conf
     else
       echo "vm.nr_hugepages = $(( `free -m | grep 'Mem:' | awk {'print$4'}` /2 ))" | tee -a /etc/sysctl.conf
    fi
fi

echo "===================== Enable Libvirtd HugePage $i =========================="
if [ $( egrep -c '^#hugetlbfs_mount' /etc/libvirt/qemu.conf ) -eq 1 ]
 then
    sed -i 's/#hugetlbfs_mount =/hugetlbfs_mount =/g' /etc/libvirt/qemu.conf
fi

echo "===================== Add fstab HugePage $i =========================="
if [ $( egrep -c '^hugetlbfs' /etc/fstab ) -eq 0 ]
 then
    echo 'hugetlbfs               /dev/hugepages              hugetlbfs defaults      0 0' | tee -a /etc/fstab
fi

echo "===================== Add HugePage for POSTGRES VM $i =========================="
if [ -d /etc/libvirt/qemu ]; then
    _calculate=`egrep -rc 'memoryBacking' /etc/libvirt/qemu/*.xml | awk -F\: {'print$2'} | tr -ts '\n' '\+' | sed -e 's/\+$//' | xargs -I {} echo {}`

    if [ $(echo "print ${_calculate}" | python) -eq 0 ]; then
    find /etc/libvirt/qemu/ -maxdepth 1 -name "*psql*" -exec sed -i '/currentMemory\>/ s|$| \n  <memoryBacking>\n    <hugepages/>\n  </memoryBacking> |' {} \;
    fi

#    if [ $(echo /etc/libvirt/qemu/*sq*.xml | tr -ts ' ' '\n' | wc -l) -ge '1' ]; then
#       for i in `echo /etc/libvirt/qemu/*sq*.xml`; do
#         if [ `egrep -c '<memoryBacking>' ${i}` -ge 2 ]; then
#            sed -i '/memoryBacking/d' ${i}
#            sed -i '/hugepages/d' ${i}
#            sed -i '/currentMemory\>/ s|$| \n  <memoryBacking>\n    <hugepages/>\n  </memoryBacking> |' ${i}
#         fi
#       done
#    fi
fi
sysctl -p /etc/sysctl.conf 2>&1 > /dev/null
}

function _tune_station {

#if [ $( egrep -c '/sys/kernel/mm/ksm/run' /etc/rc.local ) -eq 0 ]; then
# echo 'echo 1 > /sys/kernel/mm/ksm/run' >> /etc/rc.local
#fi
#if [ $( egrep -c 'sleep_millisecs' /etc/rc.local ) -eq 0 ]; then
# echo 'echo 1000 > /sys/kernel/mm/ksm/sleep_millisecs' >> /etc/rc.local
#fi

sysctl -p /etc/sysctl.conf 2>&1 > /dev/null
}

function _tune_postgres {

echo "===================== RPM check postgres $i =========================="

if [ `rpm -qa | grep -c postgres` -gt 0 ]
  then
    test -e /etc/init.d/postgresql-9.5 && if [ -d /usr/pgsql-9.5/bin ]; then echo "PATH=$PATH:/usr/pgsql-9.5/bin" > /etc/profile.d/postgres.sh && chmod +x /etc/profile.d/postgres.sh; fi
    test -e /postgresql-9.5 && if [ -d /usr/pgsql-9.5/bin ]; then echo "PATH=$PATH:/usr/pgsql-9.5/bin" > /etc/profile.d/postgres.sh && chmod +x /etc/profile.d/postgres.sh; fi
  else
    echo "Postgres is not install"
    echo "You can install and re-run it is script"
    exit 1
fi

echo "===================== Add HugePage to POSTGRES sysctl $i =========================="
if [ $(grep -c 'vm.nr_hugepages' /etc/sysctl.conf ) -eq 0 ]
 then
    echo "vm.nr_hugepages = $(( `egrep ^VmPeak /proc/$( head -1 /var/lib/pgsql/9.5/data/postmaster.pid )/status | awk {'print$2'}` / 2048 + 1 ))" | tee -a /etc/sysctl.conf
fi

#if [ $( egrep -c '/sys/kernel/mm/ksm/run' /etc/rc.local ) -eq 0 ]; then
#echo 'echo 1 > /sys/kernel/mm/ksm/run' >> /etc/rc.local
#echo 'echo 1000 > /sys/kernel/mm/ksm/sleep_millisecs' >> /etc/rc.local
#fi

if [ -e /var/lib/pgsql/9.5/data/postgresql.conf ]
 then
    echo "===================== Add postgres.conf to $i server =========================="
    _mem=`free -m | grep 'Mem:.*' | awk {'print$2'}`

    echo -e "
listen_addresses='*'
port=5432
max_connections=500
shared_buffers=$(echo print "int (${_mem}*0.15)" | python)MB
effective_cache_size=$(echo print "int (${_mem}*0.45)" | python)MB
maintenance_work_mem=512MB
min_wal_size=$(echo print "int (${_mem}*0.03)" | python)MB
max_wal_size=$(echo print "int (${_mem}*0.7)" | python)MB
checkpoint_completion_target=0.9
wal_buffers=16MB
default_statistics_target=100
random_page_cost=4
effective_io_concurrency=2
max_worker_processes=`nproc`
work_mem=$(echo print "int (${_mem}*0.02)" | python)MB
max_prepared_transactions=100
dynamic_shared_memory_type=posix
wal_level=hot_standby
max_wal_senders=2
wal_keep_segments=16
log_destination='syslog'
syslog_facility='LOCAL7'
log_lock_waits=on

log_timezone='Europe/Moscow'
log_min_messages=warning
log_min_error_statement=error
datestyle='iso,dmy'
timezone='Europe/Moscow'
lc_messages='ru_RU.UTF-8'
lc_monetary='ru_RU.UTF-8'
lc_numeric='ru_RU.UTF-8'
lc_time='ru_RU.UTF-8'
default_text_search_config='pg_catalog.russian'
max_locks_per_transaction=256


huge_pages=try
autovacuum=on
autovacuum_vacuum_scale_factor = 0
autovacuum_vacuum_threshold = 10000
autovacuum_vacuum_cost_delay = 20ms
autovacuum_vacuum_cost_limit = 200
autovacuum_max_workers = 6
autovacuum_naptime = 10
vacuum_cost_page_hit = 1
vacuum_cost_page_miss = 10
vacuum_cost_page_dirty = 20
log_autovacuum_min_duration=-1



max_stack_depth=6MB
temp_buffers=128MB

cpu_tuple_cost=0.0030
cpu_index_tuple_cost=0.0010
cpu_operator_cost=0.0005

" > /var/lib/pgsql/9.5/data/postgresql.conf

test -e /etc/rsyslog.d/postgres.conf || rm -f /etc/logrotate.d/postgresql*
test -e /etc/logrotate.d/postgresql-common || rm -f /etc/logrotate.d/postgresql*

    if [ ! -e /etc/rsyslog.d/postgres.conf ]
        then
        cat << POSTGRESLOG >  /etc/rsyslog.d/postgres.conf
:syslogtag, startswith, "postgres" -/var/log/postgres/postgresql.log
:syslogtag, startswith, "postgres" ~
POSTGRESLOG
    fi

    if [ ! -e /etc/logrotate.d/postgres ]
     then
cat << POSTGRESROTATE >  /etc/logrotate.d/postgres
/var/log/postgres/*.log {
    daily
    nodateext
    size 200M
    rotate 10
    missingok
    delaycompress
    compress
    su postgres postgres
}
POSTGRESROTATE
    fi


fi

    test -e /etc/init.d/postgresql-9.5 && /etc/init.d/postgresql-9.5 restart 2>&1 > /dev/null
    test -e /postgresql-9.5 && /postgresql-9.5 restart 2>&1 > /dev/null
    test -e /etc/init.d/rsyslog && /etc/init.d/rsyslog restart 2>&1 > /dev/null


}


function _tune_router {
sed -i 's/net.ipv4.ip_forward = 0/net.ipv4.ip_forward = 1/g' /etc/sysctl.conf
sysctl -p /etc/sysctl.conf 2>&1 > /dev/null
}

function _all_in_one {
 _default_modified
 _grub_default
 _tune_station
 _tune_postgres
 _tune_router


}

i=$1
case "$i" in
  [Kk][Vv][Mm]|[kK])
        echo -e "Starting TUNING: $i\n"
        _default_modified
        _grub_kvm
        _tune_kvm
        echo "Bye bye !!!!!"
        ;;
  [Ss][tT][aA][tT][iI][oO][nN]|[sS])
        echo -e "Starting TUNING: $i\n"
        _default_modified
        _grub_default
        _tune_station
        echo "Bye bye !!!!!"
        ;;
  [Pp][oO][sS][tT][gG][rR][Ee][Ss]|[pP])
        echo -e "Starting TUNING: $i\n"
        _default_modified
        _grub_default
        _tune_postgres
        echo "Bye bye !!!!!"
        ;;

  [Rr][oO][uU][tT][eE][rR]|[rR])
        echo -e "Starting TUNING: $i\n"
        _default_modified
        _grub_default
        _tune_router
        echo "Bye bye !!!!!"
        ;;

  [Aa][Ll][Ll]|[a])
        echo -e "Starting TUNING: $i\n"
        _all_in_one
        echo "Bye bye !!!!!"
        ;;


  *)
    clear
        echo -e "\n\n\tUsage: {KVM|STATION|POSTGRES|ROUTER|ALL}\n\n"
        echo -e "\tKVM ( quick key -> k ) -------> tuning hardware KVM server"
        echo -e "\tSTATION ( quick key -> s ) ---> tuning all KVM virtual machine, exclude postgres server"
        echo -e "\tPOSTGRES ( quick key -> p )--> tuning only POSTGRES KVM virtual machine"
        echo -e "\tROUTER ( quick key -> r )----> tuning only ROUTER KVM virtual machine\n\n"
        echo -e "\tALL ( quick key -> a )----> tuning all in one service in KVM virtual machine\n\n"
        exit 1
esac
