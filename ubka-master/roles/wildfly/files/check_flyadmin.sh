#!/bin/bash

myid=`psql -t -U flygres -d flygres -h $1 -c "select id from metamodel.resource where label='systemUsers';"`
_user=`psql -t -U postgres -d flygres -h $1 -c 'select * from node."'"${myid:1}"'" where props :: text like '"'%flyadmin%'"';' | grep -c flyadmin`

if [ ${_user} -eq 0 ]
then
curl 'http://localhost:8090/arctic/inner-api/nodes?type=foivPersonnel' \
-H 'Content-Type: application/json;charset=UTF-8' \
-H 'Accept: application/json, text/plain, */*' \
-H 'X-Requested-With: XMLHttpRequest' \
--data-binary '{"type":"3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2","birthDate":"1999-01-01","orgFoivDepartment":
{"id":"6469e77f-005f-4e89-a8be-f9f52f689ade","type":"257d48a9-d4e4-43ab-91ca-b90ab5fd4a40"},"lastName":"systemAccount","firstName":"flyadmin","email":"flyadmin@swemel.ru"}' --compressed | tee flyadmin.txt

_sn=`cat flyadmin.txt | tr -s ',' '\n' | head -n1 | awk -F\: {'print$2'}`

curl 'http://localhost:8090/arctic/inner-api/nodes?type=systemUsers' \
-H 'Content-Type: application/json;charset=UTF-8' \
-H 'Accept: application/json, text/plain, */*' \
-H 'X-Requested-With: XMLHttpRequest' \
--data-binary '{"type":"6d13d494-7bf5-4d11-9f32-424c4f8b6450","login":"flyadmin","_foivPersonnel_systemUsersLink":{"id":"${_sn}","type":"3f68e2b8-e95b-4bd6-918b-f5e3b8a285f2"}}' --compressed
fi

rm -f flyadmin.txt