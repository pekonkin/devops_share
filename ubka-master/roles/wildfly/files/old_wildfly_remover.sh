#!/bin/bash

sed -i '/export JAVA_HOME/d' /etc/profile
sed -i '/export PATH/d' /etc/profile

	echo "Find ansible temp and remove it"
	find /{opt,tmp,root} -maxdepth 1 -type d \( -iname "ansible" -o -iname ".ansible" -o -iname "ansible-tmp-*" \) -exec rm -rf {} \;

	echo "Find opt service and remove old integration-platform-configuration and jetty"
	find /etc/init.d/ -iname "jetty*" -exec {} stop \;
	find /etc/init.d/ -iname "jetty*" -exec rm -f {} \;
	find /opt/ -maxdepth 1 -iname "integration*" -exec rm -rf {} \;
	find /opt/ -maxdepth 1 -iname "jetty*" -exec rm -rf {} \; 

if [ $( rpm -qa | grep -c wildfly8 ) -eq 0 ]
  then
      
	echo "Find initd service and stop"
	find /etc/init.d/ \( -iname wildfly -o -iname wildfly8 \) -exec {} stop \; 

	echo "Find initd service and remove"
	find /etc/init.d/ \( -iname wildfly -o -iname wildfly8 \) -exec rm -f {} \;

	echo "Find opt service and remove old wildfly"
	find /opt/ -maxdepth 1 ! -path "/opt/wildfly10*" -type d -iname "wildfly*" -exec rm -rf {} \; 

fi

for _service in Java WildFly Postgres qt3
    do
        if [ ! -z $(rpm -qa | grep proton_${_service}) ]
        then
            yum -y remove proton_${_service}
        fi
done

_directory="/opt/gradle-.*/bin
/opt/apache-maven-*/bin
/opt/gradle-*/bin:
/usr/lib64/qt-*/bin
/usr/lib64/ccache
/opt/obuildfactory/jdk-1.8.0-openjdk-x86_64/bin
"

for _directory in ${_directory}
    do
        if [ -d ${_directory} ]
            then
            rpm -qf ${_directory} | xargs yum -y remove
        fi
        if [ -d ${_directory} ]
            then
            rm -rf ${_directory}
        fi
done

if [ -d /opt/obuildfactory ]
    then
    find /etc/sysconfig -name "j*-open*-x86_64" -exec rm -f {} \;
    rm -rf /opt/obuildfactory
fi

export PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin:/root/bin




