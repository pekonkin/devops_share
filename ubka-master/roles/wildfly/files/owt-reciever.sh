#!/usr/bin/env bash

_get_db=`egrep -o '\/.*owtdb.db3' /etc/sysconfig/owt-server`

if [ $( ps aux | grep owt-server | grep -v grep | wc -l ) -ge 1 ]; then
   ps aux | grep owt-server | grep -v grep | awk {'print$2'} | xargs -I {} kill -9 {}
fi

if [ ! -e ${_get_db} ]; then
 if [ -e /opt/owt-server/owt-server-init-db ]; then
    /opt/owt-server/owt-server-init-db receiver

 fi
fi
if [ $( sqlite3 ${_get_db} 'SELECT * FROM channels_tbl' | wc -l ) -eq 0 ]; then
logger -t owt-server 'owt-server | Upload tables for chanel 1'
sqlite3 ${_get_db} 'INSERT INTO channels_tbl (nrep,sz_chunk,tm_stor,tm_retr,enabled) VALUES (10,1404,259200,5,1)'
fi

if [ $( sqlite3 ${_get_db} 'SELECT * FROM channels_tbl' | wc -l ) -eq 1 ]; then
logger -t owt-server 'owt-server | Upload tables for chanel 2'
sqlite3 ${_get_db} 'INSERT INTO channels_tbl (nrep,sz_chunk,tm_stor,tm_retr,enabled) VALUES (1,1404,259200,10,1)'
fi
logger -t owt-server 'owt-server | Update tables for all chanel'
sqlite3 ${_get_db} 'UPDATE channels_tbl SET sz_chunk="1404"'
sqlite3 ${_get_db} 'UPDATE channels_tbl SET nrep="10" where id = "1"'
sqlite3 ${_get_db} 'UPDATE channels_tbl SET tm_retr="5" where id = "1"'
sqlite3 ${_get_db} 'UPDATE channels_tbl SET nrep="1" where id = "2"'
sqlite3 ${_get_db} 'UPDATE channels_tbl SET tm_retr="10" where id = "2"'

