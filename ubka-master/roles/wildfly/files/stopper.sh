#!/bin/bash

if [ $( /etc/init.d/crond status | egrep -c '\(pid.*[0-9]\)' ) -eq 1 ]; then
   /etc/init.d/crond stop
fi

if [ -d /etc/sysconfig/starter ]; then

   if [ -e /etc/sysconfig/starter/qt.sh ]; then
   if [ $( pgrep  | grep -c qt.sh ) -ge 1 ]; then
      pkill -s 9 qt.sh
   fi
   fi

   if [ -e /etc/sysconfig/starter/owt-checker.sh ]; then
   if [ $( ps aux | grep -c owt-checker.sh ) -ge 1 ]; then
      pkill -s 9 owt-checker.sh
   fi
   fi

fi

if [ $( /etc/init.d/crond status | egrep -c '\(pid.*[0-9]\)' ) -eq 0 ]; then
   /etc/init.d/crond start
fi
