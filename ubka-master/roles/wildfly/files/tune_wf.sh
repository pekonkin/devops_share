#!/usr/bin/env bash

if [ $( egrep -c 'madvise' /etc/rc.local ) -ge 1 ]
   then
     sed -i '/madvise/d' /etc/rc.local
fi

if [ $( egrep -c 'always' /etc/rc.local ) -ge 1 ]
   then
     sed -i '/always/d' /etc/rc.local
fi

if [ $( egrep -c 'transparent_hugepag' /etc/rc.local ) -eq 0 ]; then
  if [ -e /sys/kernel/mm/transparent_hugepage/enabled ]; then
  echo always > /sys/kernel/mm/transparent_hugepage/enabled
  echo 'echo always > /sys/kernel/mm/transparent_hugepage/enabled' | tee -a /etc/rc.local
  fi
  if [ -e /sys/kernel/mm/transparent_hugepage/defrag ]; then
  echo always > /sys/kernel/mm/transparent_hugepage/defrag
  echo 'echo always > /sys/kernel/mm/transparent_hugepage/defrag' | tee -a /etc/rc.local
  fi
fi

if [ $( egrep -c '^hugetlbfs' /etc/fstab ) -eq 0 ]
 then
    echo 'hugetlbfs               /dev/hugepages              hugetlbfs defaults      0 0' | tee -a /etc/fstab
    mount -a
fi



