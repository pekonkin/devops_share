        location ~ /chat {
                location ~ /chat/share {
                        proxy_pass http://szi_chat;
                        proxy_cache szi_cache;

                        proxy_ignore_headers Expires;
                        proxy_ignore_headers X-Accel-Expires;
                        proxy_ignore_headers Cache-Control;
                        proxy_ignore_headers Set-Cookie;
                        proxy_hide_header Set-Cookie;
                        proxy_hide_header X-Accel-Expires;
                        proxy_hide_header Expires;
                        proxy_hide_header Cache-Control;
                        proxy_hide_header Pragma;
                        proxy_cache_use_stale error timeout updating http_500 http_502
                        http_503 http_504;

                        proxy_cache_valid 200 301 302 1y;
                        proxy_cache_key "$scheme$request_method$host$request_uri";
                }

                proxy_pass http://szi_chat;
                proxy_set_header                Upgrade                 $http_upgrade;
                proxy_set_header                Connection              "upgrade";
        }
