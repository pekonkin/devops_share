#!/bin/bash

if [ $(ps aux | egrep $0 | grep -v grep | wc -l ) -gt 2 ]; then

   exit 1

fi

for RPMPACK in gzip strace; do

    if ! rpm -qa ${RPMPACK} 2>&1 >/dev/null
    then

       yum -y install ${RPMPACK}; 2>&1 >/dev/null

    fi
done

LANG=C
SERVICE='/etc/init.d/owt-server'

if [ -e /etc/init.d/owt-server ]; then

   if [ -e /etc/sysconfig/owt-server ]; then

      if [  $(egrep -c 'OWT_HTTPSA=10007' /etc/sysconfig/owt-server ) -eq 1 ]; then

         IPADDR='127.0.0.1'

         else

         IPADDR=`egrep -o "OWT_HTTPSA=192.168.[0-9]{1,3}\.[0-9]{1,3}" /etc/sysconfig/owt-server  | awk -F\= {'print$2'}`

      fi

   fi

    while :
    do

        OWT=`basename $SERVICE`
        PID="/var/run/owt-server/owt-server.pid"
        DIR_LOG="/var/log"
        LOG="owt-server.log"
        TCPPORT='10007'
        URL='/login.html'
        RESULT=`pgrep ${OWT}`

        function STARTDIOD
        {
            ${SERVICE} start
            sleep 5s
            continue
        }

        function KILLEMALL
        {

        if [ ! -e ${PID} ]; then

            if [ $( ps aux | grep ${OWT} | egrep -v "grep|$LOG|$PID|$SERVICE|vi|tail|less|cat|tac|more|mc|service" | wc -l ) -ge 1 ]; then

                ps aux | grep ${OWT} | egrep -v "grep|$LOG|$PID|$SERVICE|vi|tail|less|cat|tac|more|mc|service" | awk {'print$2'} | xargs -I {} kill -9 {}

            fi

        fi

        RESULT=`pgrep ${OWT}`
        if [ "${RESULT:-zero}" = zero ]; then

           continue

        else

           ${SERVICE} stop

           sleep 1s

           if [ `ps aux | grep ${OWT} | egrep -v "grep|$LOG|$SERVICE|$PID|vi|tail|less|cat|tac|more|mc|service" | wc -l` -gt 1 ]; then
                ps aux | grep ${OWT} | egrep -v "grep|$LOG|$SERVICE|$PID|vi|tail|less|cat|tac|more|mc|service" | awk {'print$2'} | xargs -I {} kill -9 {}
           fi

           if [ -e ${PID} ]; then
              > ${PID}
           fi

        fi

        sleep 2s;
        STARTDIOD

        }

            if [ "${RESULT:-zero}" = zero ]; then

                logger -t $OWT "`date +%c` -- Service has been starting"
                STARTDIOD

            else
                if [ `ps aux | grep ${OWT} | egrep -v "grep|$LOG|$SERVICE|$PID|vi|tail|less|cat|tac|more|mc|service" | wc -l` -gt 1 ]; then

                logger -t $OWT "`date +%c` -- Need kill -> Run several services"
                KILLEMALL

                fi
            fi

            if [ ! -e ${PID} ]; then

                logger -t $OWT "`date +%c` -- ${PID} not found, restart service"
                KILLEMALL

            fi


            if netstat -anlpt | grep ":${TCPPORT}" > /dev/null 2>&1;
            then

                if nc -vz ${IPADDR} ${TCPPORT} > /dev/null 2>&1;
                then

                   logger -t $OWT "`date +%c` -- NETCAT | TCP PORT ${TCPPORT} -> OPEN"

                            if curl http://${IPADDR}:${TCPPORT}${URL} |& grep 'Connection refused' > /dev/null 2>&1
                            then

                                logger -t $OWT "`date +%c` -- Connection refused -> http://${IPADDR}:${TCPPORT}${URL}"
                                KILLEMALL

                            fi

                            if curl http://${IPADDR}:${TCPPORT}${URL} |& grep "Couldn\'t connect to host" > /dev/null 2>&1
                            then

                                logger -t $OWT "`date +%c` -- Couldn\'t connect to host -> http://${IPADDR}:${TCPPORT}${URL}"
                                KILLEMALL

                            fi

                            if [ $(curl -I http://${IPADDR}:${TCPPORT}${URL} |& grep -c "HTTP/1.1 200 OK") -eq 0 ] > /dev/null 2>&1
                            then

                                logger -t $OWT "`date +%c` -- Web host can\`t get http request HTTP/1.1 200 OK  -> http://${IPADDR}:${TCPPORT}${URL}"
                                KILLEMALL

                            fi
                else

                    logger -t $OWT "`date +%c` -- NETCAT | TCP PORT ${TCPPORT} -> UNREACH"
                    KILLEMALL

                fi

            else

                logger -t $OWT "`date +%c` -- TCP PORT ${TCPPORT} -> NOT LISTEN"
                KILLEMALL

            fi

            sleep 5s

    done

fi
