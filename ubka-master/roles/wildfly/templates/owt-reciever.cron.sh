#!/bin/bash

if [ -d /etc/sysconfig/starter ]; then

    if [ $( /etc/init.d/crond status | egrep -c '\(pid.*[0-9]\)' ) -eq 1 ]; then

       /etc/init.d/crond stop
       STOPPED=1

    fi

    if [ -e /etc/sysconfig/starter/owt-checker.sh ]; then

        if [ $( ps aux | grep -c owt-checker.sh ) -ge 1 ]; then

            ps aux | grep owt-checker.sh | grep -v grep | awk '{print$2}' | xargs -I {} kill -9 {}
        fi

    fi

fi

if [ -e /etc/sysconfig/owt-server ]; then

    _get_db=`egrep -o '\/.*owtdb.db3' /etc/sysconfig/owt-server`

        if [ -e /etc/init.d/rgmanager ]; then

            _status_owt=`/etc/init.d/owt-server status | egrep -c '\(pid.*[0-9]\)'`

            if [ ${_status_owt} -eq 1 ]; then

                if [ -e /etc/init.d/owt-server ]; then

                    logger -t owt-server 'owt-server | stop service'
                    /etc/init.d/owt-server stop

                fi
            fi

        else

            if [ -e /etc/init.d/owt-server ]; then

                logger -t owt-server 'owt-server | stop service'
                /etc/init.d/owt-server stop

            fi
        fi


        if [ -e ${_get_db} ]; then

            logger -t owt-server "owt-server | Remove database ${_get_db}"
            rm -f ${_get_db}

                if [ -d ${_get_db}.storage ]; then

                    logger -t owt-server "owt-server | Remove ${_get_db}.storage directory"
                    rm -rf ${_get_db}.storage/*

                fi

        fi

        if [ ! -e ${_get_db} ]; then

            if [ -e /opt/owt-server/owt-server-init-db ]; then

                logger -t owt-server 'owt-server | reinitialization owt-server-init-db receiver'
                /opt/owt-server/owt-server-init-db receiver

            fi
        fi

        if [ $( sqlite3 ${_get_db} 'SELECT * FROM channels_tbl' | wc -l ) -eq 0 ]; then

            logger -t owt-server 'owt-server | Upload tables for chanel 1'
            sqlite3 ${_get_db} 'INSERT INTO channels_tbl (nrep,sz_chunk,tm_stor,tm_retr,enabled) VALUES (10,1404,259200,5,1)'

        fi

        if [ $( sqlite3 ${_get_db} 'SELECT * FROM channels_tbl' | wc -l ) -eq 1 ]; then

            logger -t owt-server 'owt-server | Upload tables for chanel 2'
            sqlite3 ${_get_db} 'INSERT INTO channels_tbl (nrep,sz_chunk,tm_stor,tm_retr,enabled) VALUES (1,1404,259200,10,1)'

        fi

                logger -t owt-server 'owt-server | Update tables for all chanel'
                sqlite3 ${_get_db} 'UPDATE channels_tbl SET sz_chunk="1404"'
                sqlite3 ${_get_db} 'UPDATE channels_tbl SET nrep="10" where id = "1"'
                sqlite3 ${_get_db} 'UPDATE channels_tbl SET tm_retr="5" where id = "1"'
                sqlite3 ${_get_db} 'UPDATE channels_tbl SET nrep="1" where id = "2"'
                sqlite3 ${_get_db} 'UPDATE channels_tbl SET tm_retr="10" where id = "2"'


        if [ -e ${_get_db} ]; then

            logger -t owt-server "owt-server | Set user for new directory"
            chown -R owt-server: `dirname ${_get_db}`

        fi

        if [ -e /etc/init.d/rgmanager ]; then

            if [ ${_status_owt} -eq 1 ]; then

                if [ -e /etc/init.d/owt-server ]; then

                    logger -t owt-server 'owt-server | Start service'
                    /etc/init.d/owt-server start

                fi
            fi

        else

            if [ -e /etc/init.d/owt-server ]; then

                logger -t owt-server 'owt-server | Start service'
                /etc/init.d/owt-server start

            fi
        fi

fi


if [ ${STOPPED:-zero} != zero ]; then

   /etc/init.d/crond start

fi
