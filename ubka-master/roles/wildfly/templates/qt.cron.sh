#!/bin/bash

if [ $(ps aux | egrep $0 | grep -v grep | wc -l ) -gt 2 ]; then

   exit 1

fi

for RPMPACK in gzip strace; do
    if ! rpm -qa ${RPMPACK} 2>&1 >/dev/null
    then
        yum -y install ${RPMPACK}; 2>&1 >/dev/null
    fi
done

LANG=C
SERVICE='/etc/init.d/qtransport'

if [ -e $SERVICE ]; then

   while :
   do

        QTR=`basename $SERVICE`
        DIR_LOG="/var/log"
        LOG="qtransport-server.log"
        TCPPORT='8001'
        IPADDR='127.0.0.1'
        RESULT=`pgrep ${QTR}`
        PID='/var/run/qtransport.pid'

        function STARTQTRANSPORT
        {
                ${SERVICE} start
                sleep 5s
                continue
        }

        function KILLEMALL
        {

        RESULT=`pgrep ${QTR}`

        if [ ! -e $PID ]; then

            if [ $( ps aux | grep ${QTR} | egrep -v "grep|$LOG|$PID|$SERVICE|vi|tail|less|cat|tac|more|mc|service" | wc -l ) -gt 1 ]; then

                ps aux | grep ${QTR} | egrep -v "grep|$LOG|$PID|$SERVICE|vi|tail|less|cat|tac|more|mc|service" | awk {'print$2'} | xargs -I {} kill -9 {}

            fi

        fi


        # If PID not found, try start funtion STARTFIRESTARTER
        if [ "${RESULT:-zero}" = zero ]; then

            continue

        else

        # If find Qtransport kill service and STARTQTRANSPORT
            if [ $( ps aux | grep ${QTR} | egrep -v "grep|$LOG|$PID|$SERVICE|vi|tail|less|cat|tac|more|mc|service" | wc -l ) -gt 1 ]; then

                ps aux | grep ${QTR} | egrep -v "grep|$LOG|$PID|$SERVICE|vi|tail|less|cat|tac|more|mc|service" | awk {'print$2'} | xargs -I {} kill -9 {}

            fi

        fi

        sleep 1s;
        STARTQTRANSPORT

        }



        if [ "${RESULT:-zero}" = zero ]; then

            logger -t $QTR "`date +%c` -- Service has been starting"
            STARTQTRANSPORT


        else

            if [ `ps aux | grep ${QTR} | egrep -v "grep|$LOG|$PID|$SERVICE|vi|tail|less|cat|tac|more|mc|service" | wc -l` -ge 2 ]; then

                logger -t $QTR "`date +%c` -- Need kill -> Run several services"
                KILLEMALL

            fi
        fi

        if [ ! -e ${PID} ]; then

            logger -t $QTR "`date +%c` -- ${PID} not found, restart service"
            KILLEMALL

        fi


        if netstat -anlp | grep ":${TCPPORT}" > /dev/null 2>&1;

        then

            if nc -vz ${IPADDR} ${TCPPORT} > /dev/null 2>&1;

                then

                    logger -t $QTR "`date +%c` -- NETCAT | TCP PORT ${TCPPORT} -> OPEN"

            else

                    logger -t $QTR "`date +%c` -- NETCAT | TCP PORT ${TCPPORT} -> UNREACH"
                    KILLEMALL

            fi

        else

            logger -t $QTR "`date +%c` -- TCP PORT ${TCPPORT} -> NOT LISTEN"
            KILLEMALL

        fi

        sleep 5s

   done

fi
