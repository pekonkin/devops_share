#!/bin/bash

if [ $( /etc/init.d/crond status | egrep -c '\(pid.*[0-9]\)' ) -eq 1 ]; then
   /etc/init.d/crond stop
fi

if [ -d /etc/sysconfig/starter ]; then

   if [ -e /etc/sysconfig/starter/qt.sh ]; then
   if [ $( pgrep  | grep -c qt.sh ) -ge 1 ]; then
      ps aux | grep qt.sh | grep -v grep | awk {'print$2'} | xargs kill -9
   fi
   fi

   if [ -e /etc/sysconfig/starter/owt-checker.sh ]; then
   if [ $( ps aux | grep -c owt-checker.sh ) -ge 1 ]; then
      ps aux | grep owt-checker.sh | grep -v grep | awk {'print$2'} | xargs kill -9
   fi
   fi

fi

if [ $( /etc/init.d/crond status | egrep -c '\(pid.*[0-9]\)' ) -eq 1 ]; then
   /etc/init.d/crond start
fi
