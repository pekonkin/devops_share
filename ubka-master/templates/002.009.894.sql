--liquibase formatted sql

--changeset yankovskiy:002.009.894 splitStatements:false stripComments:false logicalFilePath:002.000.000/002.009.000/002.009.894.sql
-- --------
--Created: 2019-03-22T15:48:28.112
--comment #16906 Исправление  бага неправильного вызова функциии (исправлено 2 функции)
--LiquibaseChangesetType: CUSTOM_SQL

-- --------

-- DROP FUNCTION service.Import_OSM_Tabs_fly(xml_file text, xml_dic text, name_tab text)
CREATE OR REPLACE FUNCTION service.Import_OSM_Tabs_fly(xml_file text, xml_dic text, name_tab text)
RETURNS int
LANGUAGE plpgsql
as
$$
declare
	lb int = 100000; -- ? это значение длины буфера ускоряет разбор файла 2.4Мб с 27 секунд до 1.5
	lsd int = 1;
	service text = 'Загрузка ресурсов из сервисов ОСМ ФАР';
	il int;
	in_file text;
	in_dic xml;
	tab text;
	step int = 1;
	reltab text;
	lreltab int;
	posreltab int;
	psrl varchar(256);
	lid int = 20; -- ограничение длины идентификатора + 1
	pid varchar(20); -- папа
	cid varchar(20); -- дитё
	relrect text;
	num_tab int;
	num_rec int;
	relteg text;
	np1 int;
	p1 int;
	p2 int;
	pp2 int;
	fsname varchar(255) = 'ОСМ ФАР';
	fsdescription varchar(255) = 'Отраслевая система мониторинга Федерального агентства по рыболовству';
	fslabel varchar(255) = 'osmfar';
	tabid uuid; -- (n3)
	datatop text = '<rows>';
	datastop text = REPLACE(datatop, '<', '</');
	dic_path text = '/result/^dic^/rows';
	path text;
	rowtag text = 'row';
	value_path text = '/' || rowtag || '/';
	datapath text = '/' || rowtag;
	rowstart text = '<' || rowtag || '>';
	rowstop text = '</' || rowtag || '>';
	code_att_name varchar(255);

	collect int; -- устанавливается в 1 если текущий атрибут связи "COLLECTION" (имя узла внешнего ключа начинается с @) или в 2 если это ещё и обратная связь (имя узла внешнего ключа начинается с %)
	last_version int; -- устанавливается в 1 если по текущему атрибуту связи проверяется последняя версия (метка атрибута внешнего ключа начинается с @, при этом
	last_version_block int; --устанавливается в 1 если получили не последнюю версию контента объекта, и можно только добавить к существующему объекту связь на объект связи, по которой проверялась версия
	back int; --устанавливается в 1 если братная связь VALUE или -1, если прямой COLLECTION без привязки к истории
	back_dop int; --устанавливается в 1 если back IN(1, -1) и в другом ресурсе может быть значение int, float... (в настройке ?? или !?)';
	dpust int;
	vers_fk text;
	vers_fk_attribute_idt text;
	vers_uk text;
	vers_osmcode text;
	vers_foreignres_id text;
	_cash_ bool;
	cash text = '';
	cash_label varchar(255);
	cash_id uuid;
	cash_in int = 0;
	cash_out int = 0;
	cash_pos int;
	cash_f text = '';
	cash_f_label varchar(255);
	cash_f_id uuid;
	cash_f_uk varchar(255);
	cash_f_in int = 0;
	cash_f_out int = 0;
	rec record;
	n_rec int;
	child text;
	resatt text;
	att text;
	typeatt varchar(255);
	typevers int;
	vers_att varchar(255); -- по этому атрибуту только типа #t0 (datetime) определяются более свежие версии записи, чтобы не переписывать их более старыми. Если есть версионная связь, то не применяется.
	vers_val varchar(1024);
	vers_val1 varchar(1024);
	valatt text;
	attlab text;
	attnam text;
	i int;
	j int;
	ie text;
	id_name varchar(8000);
	term varchar(8000);
	longterm varchar(8000);
	hitag varchar(255);
	hichild varchar(255);
	hiparent varchar(255);
	hidstart varchar(255);
	hidstop varchar(255);
	hiderror text;
	foreignkeys text;
	foreignkey text;
	foreignres_id uuid;
	foreignres_idt text;
	fs_res_idt text;
	fk_attribute varchar(8000);
	fk_attribute_id uuid;
	fk_attribute_idt text;
	fk_res_label varchar(255);
	fk_unique varchar(255);
	fk_n int;
	fk text;
	uk text;
	ni int;
	att_list text;
	fk_list text;
	fk_parent_id uuid;
	fk_parent_old_id uuid;
	fk_rel_id uuid;
	cross_id uuid;
	fk_cross_tab text;
	fk_cross_tab_res uuid;
	fk_cross_tab_id uuid;
	fk_cross_att text;
	fk_cross_att_id uuid;
	fk_cross_att_idt uuid;
	fk_cross_att_name text;
	fk_cross_parent_old_id uuid;
	val_cross_term text;
	tablabel varchar(255);
	tablabel_r varchar(255);
	tabname varchar(8000);
	tabname_r varchar(8000);
	val_id varchar(8000);
	val_term varchar(8000);
	val_longterm text;
	val_json jsonb;
	tval_json text;
	old_props jsonb;
	val_coordinate varchar(255);
	res_tab uuid;
	res_simple_set uuid;
	res_tree_set uuid = '0c902b81-392d-4e02-a95a-3082bb90f896';
	topreslabel varchar(255) = 'fishing';
	parentres uuid = '546dc864-9cca-44d8-98c5-1af984a2364e';
	foreign_system_id0 uuid = 'a4a28d9e-2553-4063-980e-3b46d0ec2022';
	test_last text = 'SELECT n.foreign_code
FROM rel."*res" r
JOIN node."*to_res" n ON n.id = to_id
WHERE r.from_id = ''*tabid'' AND attribute_id = ''*fk_attribute_idt'' AND foreign_system_id = ''' || foreign_system_id0::text || '''::uuid AND foreign_code > ''*osmcode''
LIMIT 1';
	att_classifier uuid;
	att_coordinate uuid;
	att_string uuid = '7ff694a8-bf36-4a30-9b4b-71a40192c73a';
	att_int uuid = 'dd8ad359-e1b1-48fd-ac50-bcc9e85f2488';
	att_long uuid = '2517d0c8-7dbc-4f1e-9e76-596dac0434c7';
	att_float uuid = 'abb3b23c-56a8-4dd6-96e9-a392661ab792'; -- real
	att_double uuid = 'f051b1ee-34c9-4979-aebf-4df818d531f8';
	att_date uuid = 'e3c0bbb8-fb46-4570-a6bb-9aed55ecacf0';
	att_datetime uuid = '1e902bd2-c3ac-4198-9451-df67f3ffd8dd';
	att_guid uuid = '5e7c250b-84f3-42eb-a7cd-1ef6a7ef5668';
	att_point uuid = '06a91963-490b-43d4-8899-580cd8797677';
	nodeid uuid;
	node_data_set uuid;
	inode_data_set uuid;
	oldname text;
	p_id uuid;
	c_id uuid;
	p_code varchar(8000);
	c_code varchar(8000);
	recset uuid;
	doplabel varchar(8000);
	fordeletefromtree text;
	id_code varchar(255);
	id_label varchar(255) = '80a283b1-6e04-40c3-a85f-f9adc52c7333';
	view_tab text;
	attname text;
	resname text;
	joins text;
	foreigns text;
	osmcode text;
	v_returned_sqlstate text;
	v_column_name text;
	v_constraint_name text;
	v_pg_datatype_name text;
	v_message_text text;
	v_table_name text;
	v_schema_name text;
	v_pg_exception_detail text;
	v_pg_exception_hint text;
	v_pg_exception_context text;
	tbegin timestamp;
	tend timestamp;
	prepareXML interval = 0;
	selectForeign interval = 0;
	insertForeign interval = 0;
	prepareRecord interval = 0;
	selectOldRecord interval = 0;
	identification interval = 0;
	insertRecord interval = 0;
	test_make_Mapping interval = 0;
	selectOldRelParent interval = 0;
	selectNewParent interval = 0;
	delupinsRelParent interval = 0;
	attr_comments text = '';
	create_view int;
	vfk varchar(8000);
	id_obj int = 0;
	id_obj0 int = 0;
	ins_obj int = 0;
	upd_obj int = 0;
	p_new bool = false;
	ins_sv int = 0;
	upd_sv int = 0;
	del_sv int = 0;
	ins2_sv int = 0;
	upd2_sv int = 0;
	del2_sv int = 0;
	struct_type varchar(255);
	attr_type uuid;
	attr_type_name text;
	voice varchar(512);
	collect_id text = '';
	collect_del_ins text;
	collect_count text;
	ndel int;
	nins int;
	nnis int;
	kv text;
	collect_type text = 'COLLECTION';
	value_type text = 'VALUE';
	l int;
	nnew1 int;
	nall1 int;
	nnew0 int;
	nall0 int;
	p_j jsonb = '{}'; -- пустой jsonb
	pt bool;
	delim varchar(2); -- разделитель формирования атрибута из разных тегов: при первом сиволе списка их названий ","  это ", ", иначе "*"
	x1a char = E'\x1a'; --''; - сбойный символ x1A в теге <responsible> для id_ves 20487 SELECT E'\x1a', '', CASE WHEN E'\x1a' = '' THEN '=' END, E'\x1a'::bytea, ''::bytea;
	map bool;
	e_command text;
	relos xml;
	da varchar = E'\012';
	da2 varchar(2) = E'\015\012';
	fsr_id uuid;
	bad_parent_code uuid;
	upos text = 'узел привязки приёмных папаш company вместо неполученных от ОСМ ФАР';
	ppos text = ' - код приёмного папаши company, неполученного из ОСМ ФАР';
	r_ship uuid = 'dac4c525-cbd4-4942-ad89-5aefb6e8a6f9';
	local_id uuid;
/* Коментарии к relos, который читается из таблицы foreign_system
--#d - дата, #t datetime
-- В атрибутe id и term, а также FK_XML значение начинающееся с ! указывает константу RIGHT(значение,-1), или если после ! идёт цифра n, то к ней примыкает название тега, от которого берутся n первых символов
-- MAPING отличается от описания других связей тем, что 1-ый элемент начинается с точки:
--   1 - '.'||label_атрибута, 2 - id атрибута, 3 - FK_XML для сравнения с fs_value мапинга, 4 - ID ресурса классификатора мапинга, 5 - ID внешнего ресурса fs.fs_id мапинга
-- ПРЯМАЯ и ОБРАТНАЯ связь:
-- label_атрибута, 2 - id атрибута, 3 - FK_XML для сравнения с полем или атрибутом, 4 - ID_связного_ресурса, 5 - метка_атрибута_UK, 6 - ID_атрибута_UK (если используются идентификатор и код внешней систеы, то 5 и 6 пусты)
-- @ перед меткой своего атрибута - признак связи с проверкой версионности (для не последней версии от основного объекта добавляется только эта связь и больше ничего не изменяется)
-- @ перед FK_XML - это COLLECTION на одного конкретного за раз (синхронизируется с последней версией, если установлена проверка на версионность)
-- % перед FK_XML - это COLLECTION в обратную сторону от одного конкретного за раз
-- ! перед FK_XML - это VALUE или COLLECTION в обратную сторону от многих сразу
-- !? перед FK_XML - то же самое, что !, но с учётом что в jsonb значение без кавычек
-- ? перед FK_XML - это прямой COLLECTION без привязки к истории и ко многим сразу
-- ?? перед FK_XML - то же самое, что ?, но с учётом что в jsonb значение без кавычек
-- ID объекта ОСМ в атрибуте resatt всегда сразу после лейбла и имени ресурса, т.е. после первой звезды *
-- Цифры в последних элементах resatt не используются, это осталось для указания весов при идентификации, которая здесь больше не нужна
resource_id IN ('22223124-80ba-4136-82c5-fc1bbf35d400','dac4c525-cbd4-4942-ad89-5aefb6e8a6f9','3217e8c2-4ab5-4d09-89e0-8367bd0de08f','268f0266-ad7d-4c1e-9ffb-1bb0b45c9383','cb672aba-4318-47d3-814c-815930e2da76','b3f757b3-58b6-46e8-9ad2-5da39dc0d5a1'
,'a264bf75-34c6-4acc-a56b-a9790e440eb1','9d393143-8199-47e5-a798-e5f49385f863','30549635-03b8-4f78-9d09-178c8184f7ff','da8ac0d4-4cbc-4875-abd2-3ea83eac1c88','d14be1bc-9c56-4570-9fa1-f9eab6cb1bf3'
,'5c8db12a-8d2c-4fbe-b376-5c124548e845','15b0e47c-a215-471a-ae09-b1e8c0dd91e9','f6fd4eb7-3eb5-4b2a-88af-2b5dbb8bffe7','57ec70e6-23f7-4576-9a64-353b9a2adbc3','84c3d1fe-19b6-40ce-b0c6-448418c73613')
*/
BEGIN
	RAISE NOTICE 'Начало транзакции загрузки %', now();
	SELECT processing_rules_xml[2] INTO relos FROM fs.foreign_system WHERE id = foreign_system_id0;
	IF relos is NULL THEN
	  -- il = service.import_log_write(service, 'ERROR', 'xml настройки импорта ресурсов ОСМ ФАР отсутствует в таблице foreign_system');
	  RAISE EXCEPTION 'Не удалось получить XML настройки из таблицы foreign_system для обработки потока данных от источника ОСМ ФАР';
	END IF;
	IF LEFT(name_tab, 1) = '-' THEN
	  RAISE NOTICE 'Запуск только для построения вьюхи при пустом теге %', datatop;
	  create_view = 1;
	  name_tab = RIGHT(name_tab, -1);
	END IF;
	voice = split_part(name_tab, '*', 2);
	name_tab = split_part(name_tab, '*', 1);
	IF LENGTH(voice) = 0 THEN voice = name_tab; END IF;

-- убиваем заглавный тег и xdms: (или другие префиксы перед названиями и атрибутами тегов), поскольку с этими элементами ничего не парсится
	relos = REPLACE(REPLACE(REPLACE(relos::text, da2, da), da || ' *', '*'), da, ' ');
	relteg = ((xpath('/tabs/' || name_tab, relos))[1])::text;  -- узел в xml настроики импорта
	relos = NULL;
--RAISE NOTICE '%', relteg;
	IF relteg is NULL THEN
	  -- il = service.import_log_write(service, 'ERROR', 'Таблица '||name_tab||' отсутствует в настройке импорта из ОСМ ФАР');
	  RAISE EXCEPTION 'Таблица % отсутствует в настройке импорта из ОСМ ФАР', name_tab;
	END IF;
	service = (xpath('/' || name_tab || '/@service', relteg::xml))[1];  -- имя сервиса
	-- il = service.import_log_write(service, 'MESSAGE', 'Вызвана функция загрузки контента '||name_tab||' '||clock_timestamp()::text);
	child = (xpath('/' || name_tab || '/@child', relteg::xml))[1];  -- связанные таблицы, которые надо загрузить предварительно
	i = 0;
--RAISE NOTICE 'child %', child;
	WHILE child is NOT NULL LOOP
	  i = i + 1;
	  ie = split_part(child, '*', i);
	  IF COALESCE(ie, '') = '' THEN EXIT; END IF;
	  -- il = service.import_log_write(service, 'MESSAGE', 'Запуск импорта дочерней таблицы '||ie::text||' '||clock_timestamp()::text);
	  RAISE NOTICE 'Запуск импорта дочерней таблицы %, Начало контента "%" %', ie, LEFT(xml_file, 10), SUBSTRING(clock_timestamp()::text, 12,12);
	  step = service.Import_OSM_Tabs_fly(xml_file, NULL, CASE WHEN create_view = 1 THEN '-' ELSE '' END || ie || '*' || name_tab); -- с учётом вьюхи, и чтобы не было лишних сообщений от загрузок дочерних записей
	  -- il = service.import_log_write(service, 'MESSAGE', 'Импорт '||ie::text||' завершён с кодом '||step::text||' '||clock_timestamp()::text);
	  RAISE NOTICE 'Импорт % завершён с кодом %, %', ie, step, SUBSTRING(clock_timestamp()::text, 12,12);
	END LOOP;
	IF xml_file is NULL THEN
	  xml_file = '';
	  BEGIN
	    IF EXISTS (SELECT xml FROM service.service_reglament WHERE service_name = service AND xml is NOT NULL) THEN
	      pt = true;
	    END IF;
	  EXCEPTION WHEN OTHERS THEN
	    RAISE NOTICE 'Не удалось прочесть таблицу service_reglament, попробуем прочесть временную таблицу temp_osm_tab';
	  END;
	  IF pt is NULL THEN
	    RAISE NOTICE 'В таблице service_reglament контент сервиса "%" пуст, смотрим временную таблицу temp_osm_tab', service;
	    BEGIN
	      IF EXISTS (SELECT x_file FROM temp_osm_tab WHERE x_file is NOT NULL) THEN
	        pt = false;
	      END IF;
	    EXCEPTION WHEN OTHERS THEN
	      RAISE NOTICE 'Вызов без подготовленного в таблице XML загрузки';
	    END;
	  END IF;
	END IF;
raise notice 'pt %, LEFT(xml_file, 100) %', pt, LEFT(xml_file, 100);
	IF pt is NULL THEN
	  IF xml_file is NULL THEN
	    RAISE EXCEPTION 'Отсутствует входной XML загрузки';
	  ELSIF LENGTH(xml_file) < 20 THEN
	    RAISE EXCEPTION 'Инвалидный входной контент, взятый из % % %', CASE WHEN pt OR NOT pt THEN 'таблицы' ELSE 'параметра' END, da, COALESCE(xml_file, 'NULL');
	  END IF;
	END IF;
	path = datapath;
	_cash_ = false;
	res_tab = (xpath('/' || name_tab || '/@table', relteg::xml))[1];  -- ресурс и таблица
	id_name = (xpath('/' || name_tab || '/@id', relteg::xml))[1];  -- узел id
	term = (xpath('/' || name_tab || '/@term', relteg::xml))[1];  -- узел термина
	longterm = (xpath('/' || name_tab || '/@longterm', relteg::xml))[1];  -- узел полного термина
	hitag = (xpath('/' || name_tab || '/@hitag', relteg::xml))[1];  -- имя таблицы связей
	hichild = (xpath('/' || name_tab || '/@hichild', relteg::xml))[1]; -- child
	hiparent = (xpath('/' || name_tab || '/@hiparent', relteg::xml))[1]; -- parent
	hidstart = (xpath('/' || name_tab || '/@hidstart', relteg::xml))[1]; -- корневой узел
	hidstop = (xpath('/' || name_tab || '/@hidstop', relteg::xml))[1]; -- стоплист парентов
	hiderror = (xpath('/' || name_tab || '/@hiderror', relteg::xml))[1]; -- неправильные связи child->parent, которые надо игнорировать
	foreignkeys = (xpath('/' || name_tab || '/@foreignkeys', relteg::xml))[1]; -- метка_своего_атрибута~имя_своего_атрибута~узел_ссылки~метка_первичного_ресурса~атрибут_первичного_ресурса_для_построения_ссылки
	resatt = (xpath('/' || name_tab || '/@resatt', relteg::xml))[1];  -- набор создаваемых атрибутов основного ресурса
	cash_label = ''; --CASE WHEN _cash_ THEN (xpath('/' || name_tab || '/@cash', relteg::xml))[1] ELSE '' END;  -- метка основного кешируемого ресурса - это НЕ НАДО - тут индекс по полям внешних идентификаторов!
	cash_f_label = CASE WHEN _cash_ THEN (xpath('/' || name_tab || '/@cash_f', relteg::xml))[1] ELSE '' END;  -- метка кешируемого атрибута связи
	tablabel = split_part(split_part(resatt, '*', 1), '~', 1);  -- метка таблицы
	tabname = split_part(split_part(resatt, '*', 1), '~', 2);  -- имя таблицы
	reltab = '';
	lreltab = 0;
RAISE NOTICE '%
id_name = %, term = %, longterm = %, tablabel = %, tabname = %, cash %, cash_f %, hitag = %, hichild = %, hiparent = %, hidstart = %, hidstop = %, foreignkeys = %'
, SUBSTRING(clock_timestamp()::text, 12,12), id_name, term, longterm, tablabel, tabname, cash_label, cash_f_label, hitag, hichild, hiparent, hidstart, hidstop, foreignkeys;
	IF id_name || term is NULL THEN RAISE EXCEPTION 'В настройке таблицы % нет описания кода или термина', name_tab; END IF;

	IF res_tab is NULL THEN
	  -- il = service.import_log_write(service, 'ERROR', 'Не задан ID загружаемого ресурса c меткой '||tablabel);
	  RAISE EXCEPTION 'Не задан ID загружаемого ресурса %', tablabel;
	END IF;
RAISE NOTICE 'Готовим атрибуты для % % %', tablabel, tabname, res_tab;
	att_list = '';
--	select_identshab = NULL;
	ni = 0;
	i = 1;
	LOOP
	  i = i + 1;
	  att = split_part(resatt, '*', i);
	  IF COALESCE(att, '') = '' THEN EXIT; END IF;
	  attlab = split_part(att, '~', 2);
	  attnam = split_part(att, '~', 3); -- ATT_ID!
	  fk_attribute_id = attnam::uuid;
	  typeatt = split_part(att, '~', 4);
	  IF typeatt = '#t0' AND vers_att is NULL THEN
	    typevers = 1;
	    typeatt = LEFT(typeatt,2);
--RAISE NOTICE 'Атрибут проверки версии % %0', attlab, typeatt;
	  ELSIF typeatt = '#t0' AND vers_att is NOT NULL THEN
	    -- il = service.import_log_write(service, 'ERROR', 'Нельзя указывать более 1 атрибута проверки версии по datetime (тип "#t0"). Дубль в атрибуте '||attlab);
	    RAISE EXCEPTION 'Нельзя указывать более 1 атрибута проверки версии по datetime (тип "#t0"). Дубль в атрибуте %', attlab;
	  ELSE
	    typevers = 0;
	  END IF;

	  IF i = 2 THEN
	    id_code = attnam::uuid;
	    code_att_name = attlab;
	  ELSIF typevers = 1 THEN
	    vers_att = attnam;
	    RAISE NOTICE 'Атрибут проверки версии % %0 - %', attlab, typeatt, vers_att;
	  END IF; -- это ID ОСМ
	  att_list = att_list || '*' || split_part(att, '~', 1) || '~' || attnam || '~' || typeatt;
	END LOOP;
	att_list = RIGHT (att_list, -1);
	RAISE NOTICE 'Построенные атрибуты: %', att_list;

--FK
	vers_fk_attribute_idt = NULL;
	vers_uk = NULL;
	vers_foreignres_id = NULL;
	joins = '';
	foreigns = '';
	fk_list = '';
	fk_n = 0;
	LOOP
--RAISE NOTICE '%', -2;
	  fk_n = fk_n + 1;
	  foreignkey = split_part(foreignkeys, '*', fk_n);
RAISE NOTICE 'fk_n =%, foreignkey = "%"', fk_n, foreignkey;
	  IF COALESCE(foreignkey, '') = '' THEN EXIT; END IF;
-- 1                                     2                  3               4                                          5    							6
-- метка_своего_атрибута_внешней_ссылки~ID_своего_атрибута~узел_ссылки_XML~метка_первичного_ресурса_для_идентификации~атрибут_первичного_ресурса_для_идентификации_ссылки_от_FK~ресурс куда(откуда) ссылка
-- при LEFT(метка_своего_атрибута,1)=@ - атрибут кода группы для проверки последней версии
-- fk_list = fk_list || '*' || uk || '~' || foreignres_idt || '~' || fk || '~' || CASE WHEN last_version = 1 THEN '@' WHEN map THEN '.' ELSE '' END || fk_attribute_idt
	  map = CASE WHEN LEFT(foreignkey, 1) = '.' THEN true ELSE false END; -- mapping or not
	  fk_attribute = split_part(foreignkey, '~', 1);
	  fk_attribute_idt = split_part(foreignkey, '~', 2);
	  fk = split_part(foreignkey, '~', 3);
	  foreignres_idt = split_part(foreignkey, '~', 4);
	  uk = CASE WHEN map THEN split_part(foreignkey, '~', 5) ELSE split_part(foreignkey, '~', 6) END;
	  SELECT id INTO fsr_id FROM fs.foreign_system_resource WHERE label LIKE 'osm.%' AND classifier_type_id = foreignres_idt::uuid AND foreign_system_id = foreign_system_id0 LIMIT 1;
	  IF map AND fsr_id is NULL THEN
	    -- il = service.import_log_write(service, 'ERROR', 'Oтсутствует внешний ресурс источника ОСМ ФАР для мапига с label LIKE ''osm.%%'' и classifire_type_id = '||foreignres_idt);
	    RAISE EXCEPTION 'Oтсутствует внешний ресурс источника ОСМ ФАР для мапига с label LIKE ''osm.%%'' и classifire_type_id = %', foreignres_idt;
	  ELSIF map AND uk <> fsr_id::text THEN
	    RAISE NOTICE '!!! Заменяется неправильный идентификатор ресурса мапинга из настроечной таблицы % на правильный %', uk, fsr_id;
	    uk = fsr_id::text;
	  END IF;
	  IF foreignres_idt = '' OR fk_attribute_idt = '' OR fk = '' THEN
	    -- il = service.import_log_write(service, 'ERROR', 'Не полностью заданы элементы связи - '||foreignkey);
	    RAISE EXCEPTION '"%" - не полностью заданы элементы связи', foreignkey;
	  END IF;
	  IF LEFT(fk_attribute, 1) = '@' THEN
	    IF vers_fk_attribute_idt is NOT NULL THEN
	      -- il = service.import_log_write(service, 'ERROR', 'Не может быть другого ресурса для связи с проверкой версионности ('||::RIGHT(fk_attribute, -1)||'), кроме '||vers_fk_attribute_idt);
	      RAISE EXCEPTION 'Не может быть другого ресурса для связи с проверкой версионности (%), кроме %', RIGHT(fk_attribute, -1), vers_fk_attribute_idt;
	    END IF;
	    fk_attribute = RIGHT(fk_attribute, -1);
	    vers_fk_attribute_idt = fk_attribute_idt;
	    vers_foreignres_id = foreignres_idt;
	    vers_fk = RIGHT(fk, -1);
	    last_version = 1; -- проверка версии
	  ELSE
	    last_version = 0;
	    IF LEFT(fk, 1) = '@' THEN
	      collect_id = collect_id || '''' || fk_attribute_idt || ''','; -- список атрибутов прямого COLLECT для синхронизации с однотипными связями версионной записи
	    END IF;
	  END IF;
--                                    1            2                             3            4
	  fk_list = fk_list || '*' || uk || '~' || foreignres_idt || '~' || fk || '~' || CASE WHEN last_version = 1 THEN '@' WHEN map THEN '.' ELSE '' END || fk_attribute_idt;
RAISE NOTICE 'vers_foreignres_id %, fk_attribute-%, fk_list-%', vers_foreignres_id,fk_attribute, fk_list;
	END LOOP;

	IF LENGTH(collect_id) > 0 AND vers_fk_attribute_idt is NOT NULL THEN -- Заготовка чистки прежних связей и создания новых collect при смене версии
	  collect_id = LEFT(collect_id, -1);
	  collect_del_ins = 'DELETE FROM rel."' || res_tab::text || '" r1 WHERE r1.attribute_id IN(' || collect_id || ') AND r1.from_id = $1
  AND NOT EXISTS (SELECT id FROM rel."' || vers_foreignres_id::text || '" r2 WHERE r2.attribute_id = r1.attribute_id AND r2.from_id = $2 AND r2.to_id = r1.to_id);
INSERT INTO rel."' || res_tab::text || '" (from_id, to_id, attribute_id, from_type_id)
  SELECT $1, r2.to_id, r2.attribute_id, ''' || res_tab::text || ''' FROM rel."' || vers_foreignres_id::text || '" r2 WHERE r2.attribute_id IN(' || collect_id || ') AND r2.from_id = $2
  AND NOT EXISTS (SELECT id FROM rel."' || res_tab::text || '" r11 WHERE r11.attribute_id = r2.attribute_id AND r11.from_id = $1 AND r11.to_id = r2.to_id);';
	  RAISE NOTICE 'Синхронизация многозначных связей с версией: %', collect_del_ins;
	END IF;

	fk_list = RIGHT (fk_list, -1);
	relteg = NULL;
	IF hitag || hichild || hiparent is NOT NULL THEN -- в таблице есть (поли)иерархия
	  IF xml_dic is NULL THEN
	    BEGIN
	      SELECT xml::text INTO xml_dic FROM service.service_reglament WHERE service_name = 'all_stat_dicts';
	    EXCEPTION WHEN OTHERS THEN
	      RAISE NOTICE 'Не удалось прочесть таблицу service_reglament, попробуем прочесть временную таблицу temp_osm_dic';
	    END;
	  END IF;
	  IF xml_dic is NULL OR LENGTH(xml_dic) < 10 THEN
	    RAISE NOTICE 'В таблице service_reglament контент классификаторов пуст, смотрим временную таблицу temp_osm_dic';
	    BEGIN
	      SELECT x_file::text INTO xml_dic FROM temp_osm_dic LIMIT 1;
	    EXCEPTION WHEN OTHERS THEN
	      RAISE NOTICE 'Вызов без временной таблицы temp_osm_dic, поэтому без справочника связей';
	    END;
	  END IF;
	  in_dic = REPLACE(REPLACE(REPLACE(xml_dic, '<?', '<!-'||'-'), '?>', '-'||'->'),'xdms:', '')::xml;
	  path = REPLACE(dic_path, '^dic^', hitag);
	  relteg = ((xpath(path, in_dic))[1])::text;
	  xml_dic = NULL;
	END IF;
	IF relteg is NOT NULL THEN -- строим матрицу связей записей
	  RAISE NOTICE 'нашли справочник связей %', path;
	  relteg = REPLACE(relteg, da, '');
	  num_rec = 0;
	  pp2 = 0;
	  LOOP
--RAISE NOTICE '%', -4;
	    num_rec = num_rec + 1;
	    IF RIGHT(num_rec::text, 3) = '000' THEN RAISE NOTICE 'Найдено тысяч связей %', LEFT(num_rec::text, -3); END IF;
	    p1 = strpos(relteg, rowstart) - 1;
	    p2 = strpos(relteg, rowstop) + LENGTH(rowstop) - 1;
	    IF COALESCE(p1, 0) <= 0 THEN EXIT; END IF;
	    relrect = SUBSTRING(relteg, p1 + 1, p2 - p1);
	    relteg = LEFT(relteg, p1) || RIGHT(relteg, -p2);
	    IF relrect is NULL THEN EXIT; END IF;
	    pid = service.xspath(relrect, hiparent);
	    cid = service.xspath(relrect, hichild);
--RAISE NOTICE '%, pid = %, cid = %', '/row/' || hiparent || '/text()', pid,cid;
	    IF cid || pid is NOT NULL THEN
	      reltab = reltab || '@' || LPAD(cid, lid - 1) || RPAD('*', 37) || '@' || LPAD(pid, lid - 1) || RPAD('*', 37); -- пара кодов дитя+папа + место для их гуидов фиксированной длины в массиве
	      lreltab = lreltab + lid*2 + 37*2;
	    END IF;
	  END LOOP;
	  RAISE NOTICE 'Всего связей %', lreltab/(lid*2 + 37*2);
--RAISE NOTICE '%', -5;
	END IF;
-- Создание node для date_set
	IF name_tab IN ('own') OR lreltab > 0 THEN
	  SELECT id INTO node_data_set FROM node."0c902b81-392d-4e02-a95a-3082bb90f896" WHERE name = tabname;
	  IF node_data_set is NULL THEN
	    node_data_set = uuid_generate_v4();
RAISE NOTICE 'Строим node_data_set % "%" % %', node_data_set, tabname, '{"' || id_label || '": "' || tablabel || '"}', res_tree_set;
	    INSERT INTO node."0c902b81-392d-4e02-a95a-3082bb90f896" (id, name, props, type_id)
	    VALUES(node_data_set, LEFT(tabname, 512), ('{"' || id_label || '": "' || tablabel || '"}')::jsonb, res_tree_set);
	  ELSE
RAISE NOTICE 'node_data_set  id = %', node_data_set;
	  END IF;
	ELSE
	  node_data_set = NULL;
	END IF;
	fordeletefromtree = NULL;
	num_rec = 0;
	LOOP -- переносим таблицу в базу
	BEGIN
tbegin = clock_timestamp();
	  num_rec = num_rec + 1;
	  p1 = strpos(xml_file, rowstart) - 1;
	  p2 = strpos(xml_file, rowstop) + LENGTH(rowstop) - 1;
-- raise notice 'p1 %, p2 %, lsd %, LEFT(xml_file, 150) "%"', p1, p2, lsd, LEFT(xml_file, 150);
	  IF COALESCE(p1, 0) < 0 OR p2 <= LENGTH(rowstop) THEN
	    IF pt is NULL THEN
	      EXIT;
	    ELSIF pt THEN
	      xml_file = xml_file || (SELECT SUBSTRING(xml::text, lsd, lb) FROM service.service_reglament WHERE service_name = service);
	    ELSE
	      xml_file = xml_file || (SELECT SUBSTRING(x_file::text, lsd, lb) FROM temp_osm_tab LIMIT 1);
	    END IF;
	    IF lsd = 1 THEN
	      p1 = strpos(xml_file, datatop);
	      p2 = strpos(xml_file, rowstart);
	      IF p1 = 0 THEN
	        RAISE NOTICE 'Во входящем XML отсутствует или пуст головной тег %', datatop;
	        EXIT;
	      END IF;
	      IF p1 > 1 THEN xml_file = RIGHT(xml_file, -p1 + 1); END IF;
--raise notice 'Позиция головнрго тега %', p1;
	    END IF;
	    xml_file = REPLACE(REPLACE(xml_file, da2, ''), da, '');
	    lsd = lsd + lb;
	    p1 = strpos(xml_file, rowstart) - 1;
	    p2 = strpos(xml_file, rowstop) + LENGTH(rowstop) - 1;
	    IF COALESCE(p1, 0) < 0 OR p2 <= LENGTH(rowstop) THEN EXIT; END IF;
--raise notice 'LEFT(xml_file, 100) "%", RIGHT(xml_file, 30) "%"', LEFT(xml_file, 100), RIGHT(xml_file, 30);
	  END IF;
	  relrect = SUBSTRING(xml_file, p1 + 1, p2 - p1);
	  LOOP -- удаление лишних пробелов
	    IF STRPOS(relrect, '  ') = 0 THEN EXIT; END IF;
	    relrect = REPLACE(relrect, '  ', ' ');
	  END LOOP;
	  xml_file = LEFT(xml_file, p1) || RIGHT(xml_file, -p2);
--raise notice 'LEFT(relrect, 30) "%", RIGHT(relrect, 30) "%", LEFT(xml_file, 100) "%", RIGHT(xml_file, 30) "%"', LEFT(relrect, 30), RIGHT(relrect, 30), LEFT(xml_file, 100), RIGHT(xml_file, 30);
	  IF COALESCE(relrect, '') = '' THEN EXIT; END IF;
	  dpust = 0;
	  IF id_name LIKE '%*%' THEN --структура из последовательности элементов
	    i = 0;
	    val_id = '';
	    LOOP
	      i = i + 1;
	      ie = split_part(id_name, '*', i);
	      IF COALESCE(ie, '') = '' THEN EXIT; END IF;
	      dpust = dpust + CASE WHEN LEFT(ie, 1) <> '!' THEN 1 ELSE LENGTH(ie) END;
--RAISE NOTICE 'i = %, ie = %, relcect = %',i,ie,relrect;
	      val_id = val_id || '*' || CASE WHEN LEFT(ie, 1) <> '!' THEN  COALESCE(service.xspath(relrect, ie), '') ELSE RIGHT(ie, -1) END;
	    END LOOP;
	    val_id = RIGHT(val_id, -1);
	    dpust = dpust - 1; -- длина пустого идентификатора (число разделителей + длина констант)
	  ELSE
	    val_id = COALESCE(service.xspath(relrect, id_name), '');
	  END IF;
	  IF LENGTH(COALESCE(val_id, '')) <= dpust OR val_id = '0' THEN RAISE NOTICE '№% ОСМ код пуст или 0', num_rec; CONTINUE; END IF; -- с пустым идентификатором пропускаем
	  IF term LIKE '%*%' THEN --структура, с добавлением последоватеьности элементов
	    IF LEFT(term, 1) = '*' THEN  --структура, начинающаяся с val_id с добавлением последоватеьности элементов
	      i = 1;
	      val_term = val_id || '*';
	    ELSE
	      i = 0;
	      val_term = '';
	    END IF;
	    LOOP
	      i = i + 1;
	      ie = split_part(term, '*', i);
	      IF COALESCE(ie, '') = '' THEN EXIT; END IF;
	      val_term = val_term || COALESCE(service.xspath(relrect, ie), '') || '*';
	    END LOOP;
	    val_term = LEFT(val_term, -1);
	  ELSIF term LIKE '%~%' THEN -- (используется для owners) первый элемент - названия поля, значение которого надо подставить во второй элемент вместо __iD. Второй элемент - SELECT, в котором надо ещё заменить ($' || '1) на " и &gt; на >.
	    ie = split_part(term, '~', 1);
	    val_term = COALESCE(service.xspath(relrect, ie), '');
	    IF val_term is NOT NULL THEN
--raise notice 'EE0 %', REPLACE(REPLACE(split_part(term, '~', 2), '%%%', '"'), '($' || '1)', val_term);
	      EXECUTE REPLACE(REPLACE(split_part(term, '~', 2), '%%%', '"'), '($' || '1)', val_term) INTO val_term;
	    END IF;
	    val_term = COALESCE(val_term, 'не учтён');
	  ELSE
	    val_term = COALESCE(service.xspath(relrect, term), '');
	  END IF;
	  val_longterm = COALESCE(service.xspath(relrect, longterm), val_term, val_id || ' - без названия');
	  val_term = COALESCE(val_term, val_longterm); -- COALESCE(val_longterm, val_term);
	  IF COALESCE(val_term, '') = '' THEN RAISE NOTICE '№% ОСМ код % термин пуст', num_rec, val_id; CONTINUE; END IF; -- без термина пропускаем
	  IF voice = name_tab THEN
	    RAISE NOTICE '№% % ОСМ код % "%"', num_rec, SUBSTRING(clock_timestamp()::text, 12,12), val_id, CASE WHEN term LIKE '*%' THEN RIGHT(val_term, - LENGTH(val_id)) ELSE val_term END;
	  ELSIF RIGHT(num_rec::text, 3) IN ('333', '667', '000') OR num_rec = 1 THEN
	    RAISE NOTICE '% №% ОСМ код %', SUBSTRING(clock_timestamp()::text, 12,8), num_rec, val_id;
	  END IF;
	  IF lreltab > 0 AND val_id = hidstart THEN
	    RAISE NOTICE '%', 'Нашли вершину иерархии';
	  ELSE
-- запись в таблицу
	    tval_json = '{"';
--	    select_ident = select_identshab;
	    i = 0;
tbegin = clock_timestamp();
	    LOOP
	      i = i + 1;
	      ni = 0;
	      att = split_part(att_list, '*', i);
	      IF COALESCE(att, '') = '' THEN EXIT; END IF;
--RAISE NOTICE 'att = %, %', att, value_path || split_part(att, '~', 1) || '/text()';
	      typeatt = split_part(att, '~', 3);
	      attlab = split_part(att, '~', 2);
	      IF i = 1 THEN
	        attnam = val_id; -- ПЕРВЫЙ всегда идентификатор!
	      ELSE
	        osmcode = split_part(att, '~', 1);
--RAISE NOTICE 'osmcode = %', osmcode;
	        j = 0;
	        attnam = '';
	        LOOP
	          j = j + 1;
	          ie = split_part(osmcode, '`', j);
	          IF j = 1 THEN
	            delim = CASE WHEN LEFT(ie, 1) = ',' THEN ', ' ELSE '*' END; -- , ","(1 символ не буквенный) обозначает конкатенацию через ", ", иначе "*"
--RAISE NOTICE 'delim = %, ie=%', delim, ie;
	            IF delim <> '*' THEN ie = RIGHT(ie, -1); END IF;
	          END IF;
	          IF LEFT(ie, 1) BETWEEN '1' AND '9' THEN -- цифра не 0 перед названием тега означает длину обрезания этого тега, 0 обозначает конкатенацию через ', '
	            l = LEFT(ie, 1)::int;
	            ie = RIGHT(ie, -1);
	          ELSE
	            l = 1000000000;
	          END IF;
--RAISE NOTICE 'osmcode = %, j = %, ie=%', osmcode, j, ie;
	          IF COALESCE(ie, '') = '' THEN EXIT; END IF;
	          attnam = attnam || delim || CASE WHEN LEFT(ie, 1) <> '!' THEN LEFT(COALESCE(service.xspath(relrect, ie), ''), l) ELSE RIGHT(ie, -1) END; -- либо значение XML, либо константа
	        END LOOP;
	        IF LENGTH(attnam) = LENGTH(delim)*(j - 1) THEN CONTINUE; END IF; -- пустые и нули не заносим
--	        attnam = service.json_masc_simbols(attnam); -- маскировка спецсимволов json в значениях атрибутов
	        attnam = RIGHT(attnam, -LENGTH(delim));
	        IF delim <> '*' THEN
	          WHILE RIGHT(attnam, LENGTH(delim)) = delim LOOP attnam = LEFT(attnam, -LENGTH(delim)); END LOOP;
--raise notice 'составной не через * "%"', attnam;
	        END IF;
	      END IF;
	      IF attnam = '0' OR COALESCE(attnam, '') = '' THEN CONTINUE; END IF;
	      IF typeatt = '#t' THEN -- Это формат datetime
	        IF SUBSTRING(attnam, 11,1) = ' ' THEN
	          attnam = LEFT(attnam,10)  || 'T' || RIGHT(attnam, - 11);
	        ELSIF LENGTH(attnam) = 10 THEN
	          attnam = attnam || 'T00:00:00';
	        END IF;
	        IF vers_att = attlab THEN
	          vers_val = attnam;
	        END IF;
	      ELSIF typeatt = '#d' AND LENGTH(attnam) > 10 THEN -- Это формат date
	        attnam = LEFT(attnam,10);
	      END IF;
	      tval_json = tval_json || attlab || '": ' || CASE WHEN typeatt IN('#i', '#r') THEN '' ELSE '"' END || service.json_masc_simbols(attnam) || CASE WHEN typeatt IN('#i', '#r') THEN '' ELSE '"' END || ', "';
	    END LOOP;
	    val_json = (LEFT(tval_json, -3) || '}')::jsonb;
--RAISE NOTICE '%', val_json;
prepareRecord = prepareRecord + clock_timestamp() - tbegin;
--raise notice 'prepareRecord = %', prepareRecord;
tbegin = clock_timestamp();
	    tabid = NULL;
	    oldname = NULL;
	    e_command = 'SELECT id, name, props FROM node."' || res_tab::text || '" WHERE foreign_system_id = ''' || foreign_system_id0::text || ''' AND foreign_code = ''' || REPLACE(val_id, '''','''''') || ''' LIMIT 1';
--raise notice 'EE1 %', e_command;
	    EXECUTE e_command INTO tabid, oldname, old_props;
--raise notice 'EE1+ %', e_command;
selectOldRecord = selectOldRecord + clock_timestamp() - tbegin;
--raise notice 'selectOldRecord = %', selectOldRecord;
 	    IF tabid is NOT NULL THEN
 	      p_new = false;
  	      IF vers_att is NOT NULL THEN
 	        vers_val1 = old_props ->> vers_att;
 	        IF vers_val1 > vers_val THEN
	          IF voice = name_tab THEN RAISE NOTICE 'Не новый id = %. Прежняя версия % свежее пришедшей %', tabid, vers_val1, vers_val; END IF;
	          CONTINUE;
	        ELSE
 	          IF voice = name_tab THEN RAISE NOTICE 'Не новый id = %. Прежняя версия % обновляется на %', tabid, vers_val1, vers_val; END IF;
 	        END IF;
	      ELSE
	        IF voice = name_tab THEN RAISE NOTICE 'Не новый id = %', tabid; END IF;
	      END IF;
	    ELSE
 	      p_new = true;
 	    END IF;

tbegin = clock_timestamp();
	    fk_n = 0;
	    last_version_block = 0;
-- проверка последней версии если требуется
	    IF NOT p_new AND vers_fk_attribute_idt is NOT NULL THEN
--RAISE NOTICE '%, ', -7;
	      fk_attribute_idt = vers_fk_attribute_idt::uuid;
	      uk = vers_uk;
	      fk = vers_fk;
-- составной ключ - !!! Составной ключ группы с проверкой версии всегда должен начинаться с ключа версируемого объекта !!!
	      i = 0;
	      osmcode = val_id; -- он в этом случае всегда первый
	      LOOP
	        i = i + 1;
	        ie = split_part(vers_fk, '`', i);
	        IF COALESCE(ie, '') = '' THEN EXIT; END IF;
	        osmcode = osmcode || '*' || CASE WHEN LEFT(ie, 1) <> '!' THEN  COALESCE(service.xspath(relrect, ie), '') ELSE RIGHT(ie, -1) END;
	      END LOOP;
	      vers_osmcode = osmcode;
	      e_command = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(test_last, '*res', res_tab::text), '*tabid', tabid::text), '*fk_attribute_idt', fk_attribute_idt), '*to_res', vers_foreignres_id), '*osmcode', REPLACE(vers_osmcode, '''',''''''));
--raise notice 'EE2_1 Проверка версии% %', da, e_command;
	      EXECUTE e_command INTO e_command;
	      IF e_command is NOT NULL THEN
	        IF voice = name_tab THEN RAISE NOTICE 'Есть версия новее чем % (%)', vers_osmcode, e_command;  END IF;
	        last_version_block = 1;
	      END IF;
	    END IF;
	    IF p_new THEN -- не было ещё такого в таблице из данного источника (ОСМ ФАР)
	      tabid = uuid_generate_v4();
	      e_command = 'INSERT INTO node."' || res_tab::text || '" (id, name, props, type_id, foreign_system_id, foreign_code, version) VALUES ($' || '1, $2, $3, $4, $5, $6, $7)';
--	       (''' || tabid::text || ''', ''' || LEFT(val_term, 512) || ''', ''' || p_j::text || ''', ''' || res_tab::text || || ''', ''' || foreign_system_id0::text || ''', ''' || val_id || ''', now())';
-- спачала делаем пустой props, чтобы триггер не запускал идентификацию и интеграцию, после обработки связей подставим val_json
--raise notice 'EE3 %', e_command;
	      EXECUTE e_command USING tabid, LEFT(val_term, 512), p_j, res_tab, foreign_system_id0, val_id, now();
	      IF voice = name_tab THEN RAISE NOTICE 'Новый %', tabid;  END IF;
	      ins_obj = ins_obj + 1;
--RAISE NOTICE 'Новый объект id = %, name = %, props = %, type_id = %', tabid, val_term, val_json, res_tab;
--raise notice 'insertRecord = %', insertRecord;
	      old_props = p_j;
	    END IF;
	  END IF;

--                                    1            2                             3            4
--	  fk_list = fk_list || '*' || uk || '~' || foreignres_id::text || '~' || fk || '~' || fk_attribute_id::text
--                                                           5                              6                                7
--	   || CASE WHEN LENGTH(fk_cross_tab) > 0 THEN '~' || LEFT(fk_cross_tab,1) || '~' || fk_cross_tab_res::text || '~' || fk_cross_att_id::text ELSE '' END;
	  last_version = 0;
	  IF fk_list <> '' THEN
	    fk_n = 0;
	    LOOP
--RAISE NOTICE '%, ', -7;
	      fk_n = fk_n + 1;
	      map = false;
	      foreignkey = split_part(fk_list, '*', fk_n);
	      IF COALESCE(foreignkey, '') = '' THEN EXIT; END IF;
	      ie = split_part(foreignkey, '~', 4);
	      fk_attribute_idt = CASE WHEN ie = '' THEN NULL ELSE ie END;
	      IF LEFT(fk_attribute_idt, 1) = '@' THEN
	        last_version = 1;
	        fk_attribute_idt = RIGHT(fk_attribute_idt, -1);
	      ELSE
	        last_version = 0;
	        IF LEFT(fk_attribute_idt, 1) = '.' THEN
	          map = true;
--raise notice 'EE4_0 % %', fk_attribute_idt, CASE WHEN map THEN 'mapping' ELSE '' END;
	          fk_attribute_idt = RIGHT(fk_attribute_idt, -1);
	        END IF;
	      END IF;
	      IF last_version_block = 1 AND last_version = 0 THEN
-- Если не последняя версия, то пропускается обрабртка всех ссылок, кроме ссылки на проверку версий
	        CONTINUE;
	      ELSIF last_version_block = 1 AND last_version = 1 THEN
-- Установка ссылки на не последнюю версию и восстановление полученных ранее переменных по версионности для отработки привязки к прежней версии
	        fk_attribute_idt = vers_fk_attribute_idt;
	        fk_attribute_id = fk_attribute_idt::uuid;
	        uk = vers_uk;
	        fk = vers_fk;
	        osmcode = vers_osmcode; -- маскировка спецсимволов json в значениях атрибутов
	        foreignres_id = vers_foreignres_id;
	        RAISE NOTICE '%value = %, fk = %', fk_n, osmcode, fk;
	        collect = 1; -- Он всегда 1 для проверки версий, это проверяется при подготовке атрибутов в начале функции
	      ELSE
-- Обычная обработка связей
	        foreignres_id = split_part(foreignkey, '~', 2)::uuid;
	        fk_attribute_id = fk_attribute_idt::uuid;
	        uk = split_part(foreignkey, '~', 1);
	        fk = split_part(foreignkey, '~', 3);
	        IF LEFT(fk,1) IN ('@', '%') THEN
	          if map THEN
	            -- il = service.import_log_write(service, 'ERROR', 'В настройке загрузки таблицы "'||tabname::text||'" ('||res_tab::text||') указана установка множественной связи '||fk_attribute_idt||' на одну запись по мапингу. Это недопустимо.');
	            RAISE EXCEPTION 'В настройке загрузки таблицы "%" (%) указана установка множественной связи % на одну запись по мапингу. Это недопустимо.', tabname, res_tab, fk_attribute_idt;
	          END IF;
	          collect = CASE WHEN LEFT(fk,1) = '@' THEN 1 ELSE 2 END; -- прямой или обратный COLLECTION
	          back = 0;
	          fk = RIGHT(fk, -1);
	          IF vers_fk_attribute_idt is NOT NULL AND collect = 1 AND last_version <> 1 THEN
	            RAISE NOTICE 'Этот COLLECTION строится при синхронизации fk = %', fk;
	            CONTINUE; -- Связи collect = 1 в этом случае будут или уже были синхронизированы вместе с построением связи на версию.
	          END IF;
	        ELSIF LEFT(fk,1) = '!' THEN
		  if map THEN
	            -- il = service.import_log_write(service, 'ERROR', 'В настройке загрузки таблицы "'||tabname::text||'" ('||res_tab::text||') указана установка обратной связи '||fk_attribute_idt||' по мапингу. Это недопустимо.');
	            RAISE EXCEPTION 'В настройке загрузки таблицы "%" (%) указана установка обратной связи % по мапингу. Это недопустимо.', tabname, res_tab, fk_attribute_idt;
	          END IF;
	          collect = 0;
	          back = 1;
	          fk = RIGHT(fk, -1);
	        ELSIF LEFT(fk,1) = '?' THEN
		  if map THEN
	            -- il = service.import_log_write(service, 'ERROR', 'В настройке загрузки таблицы "'||tabname::text||'" ('||res_tab::text||') указана установка множественной связи '||fk_attribute_idt||' на множество записей по мапингу. Это недопустимо.');
	            RAISE EXCEPTION 'В настройке загрузки таблицы "%" (%) указана установка множественной связи % на множество записей по мапингу. Это недопустимо.', tabname, res_tab, fk_attribute_idt;
	          END IF;
	          collect = 0;
	          back = -1;
	          fk = RIGHT(fk, -1);
	        ELSE
	          collect = 0;
	          back = 0;
	        END IF;
	        IF back <> 0 AND LEFT(fk, 1) = '?'  THEN -- сравнение props без двойных кавычек
	          back_dop = 1;
	          fk = RIGHT(fk, -1);
	        ELSE
	          back_dop = 0;
	        END IF;
	        dpust = 0; -- вычисляемая минимальная длина не пустого ключа (константы + разделители + 1)
	        IF LEFT(uk, 1) = '!' THEN -- константа связи
	          osmcode = 0;
	        ELSIF fk NOT LIKE '%`%' THEN
	          osmcode = service.xspath(relrect, fk); -- для версионной связи такого не бывает
	        ELSE -- составной ключ
	          i = 0;
	          vfk = CASE WHEN last_version <> 1 THEN '' ELSE val_id END; -- для версионной связи составной ключ начинается с ОСМ кода оюъекта
	          LOOP
	            i = i + 1;
	            ie = split_part(fk, '`', i);
	            IF COALESCE(ie, '') = '' THEN EXIT; END IF;
	            IF LEFT(ie, 1) <> '!' THEN -- добавление в ключ значения поля
	              dpust = dpust + 1;
	              IF LEFT(ie, 1) BETWEEN '1' AND '9' THEN -- цифра перед названием тега означает длину обрезания этого тега
	                l = LEFT(ie, 1)::int;
	                ie = RIGHT(ie, -1);
	              ELSE
	                l = 1000000000;
	              END IF;
--RAISE NOTICE 'osmcode = %, j = %, ie=%', osmcode, j, ie;
	              vfk = vfk || '*' ||  LEFT(COALESCE(service.xspath(relrect, ie), ''), l);
	            ELSE  -- добавление в ключ константы
	              dpust = dpust + LENGTH(ie);
	              vfk = vfk || '*' || RIGHT(ie, -1);
	            END IF;
	          END LOOP;
	          osmcode =
	           CASE WHEN LENGTH(REPLACE(vfk,'0', '')) >= dpust THEN
	            CASE WHEN last_version <> 1 THEN RIGHT(vfk, -1) ELSE vfk END
	           ELSE NULL END;
	        END IF;
--raise notice 'osmcode = %', osmcode;
	        IF osmcode is NULL OR osmcode = '0' AND collect IN (1, 2) THEN CONTINUE; END IF; -- пустые связи структуры COLLECTION не обрабатываются
		IF voice = name_tab THEN RAISE NOTICE '% %value = %, fk = %, c%, b%d%'
		 , SUBSTRING(clock_timestamp()::text, 12,12), fk_n, osmcode, CASE WHEN uk = '' THEN 'внешние ID' WHEN LEFT(uk, 1) <> '!' THEN fk ELSE RIGHT(uk , -1) END, collect, back, back_dop; END IF;
	      END IF;
----	      IF uk <> '' AND NOT map THEN osmcode = REPLACE(REPLACE(REPLACE(osmcode, '\', '\\'), '"', '\"'), '/', '\/'); END IF; -- маскировка спецсимволов json в значениях атрибутов (кроме связи по мапу и внешним ID)
--raise notice 'selectOldRelParent time = %, SUM(time) = %, from_id = %, attribute_id = %', (tend - tbegin), selectOldRelParent, tabid, fk_attribute_id;
	      IF osmcode is NOT NULL AND osmcode <> '0' THEN
	        IF back <> 0 THEN
	          nnis = NULL;
	          kv = CASE WHEN back_dop = 0 THEN '"' ELSE '' END;
	          kv = '{"' || uk || '": ' || kv || service.json_masc_simbols(osmcode) || kv || '}';
	          IF voice = name_tab THEN RAISE NOTICE 'INS b% %', back, kv; END IF;
	          IF back = 1 THEN -- Удалить все ссылки на текущую запись, у кого их не должно быть и установит от тех, кому надо
	            e_command = 'WITH parents AS
	            (
	             SELECT r.id AS bad_parent
	             FROM node."' || foreignres_id::text || '" n
	             JOIN rel."' || foreignres_id::text || '" r ON to_id <> $3 AND from_id = n.id AND attribute_id = $2
	             WHERE n.props @> $4
	            )
	            DELETE FROM rel."' || foreignres_id::text || '" WHERE id IN (SELECT bad_parent FROM parents)';
-- raise notice 'EE4_del %', e_command;
	            EXECUTE e_command USING foreignres_id, fk_attribute_id, tabid, kv::jsonb;
	            e_command = 'INSERT INTO rel."' || foreignres_id::text || '" (from_type_id, attribute_id, from_id, to_id)
	            SELECT $1, $2, n.id, $3 FROM node."' || foreignres_id::text || '" n
	            WHERE props @> $4 AND NOT EXISTS (SELECT r.id FROM rel."' || foreignres_id::text || '" r WHERE to_id = $3 AND from_id = n.id AND attribute_id = $2)';
--raise notice 'EE4 %', e_command;
	            EXECUTE e_command USING foreignres_id, fk_attribute_id, tabid, kv::jsonb;
	          ELSE -- Установить все ссылки от этой записи, на кого их нет
	            e_command = 'INSERT INTO rel."' || res_tab::text || '" (from_type_id, attribute_id, from_id, to_id)
	            SELECT $1, $2, $3, n.id FROM node."' || foreignres_id::text || '" n
	            WHERE props @> $4 AND NOT EXISTS (SELECT r.id FROM rel."' || res_tab::text || '" r WHERE from_id = $3 AND to_id = n.id AND attribute_id = $2)';
--raise notice 'EE5 %', e_command;
	            EXECUTE e_command USING res_tab, fk_attribute_id, tabid, kv::jsonb;
	          END IF;
	          CONTINUE;
	        END IF;
tbegin = clock_timestamp();
	        cash_pos = -1;
	        fk_parent_id = NULL;
	        IF cash_f_id = foreignres_id AND cash_f_uk = uk THEN
	          cash_pos = strpos(cash_f, '[' || osmcode || ']');
--RAISE NOTICE 'fk_n % в кеш_f, osmcode % pos %', fk_n, osmcode, cash_pos;
	          IF cash_pos > 0 THEN
	            cash_pos = cash_pos + LENGTH(osmcode) + 2;
	            fk_parent_id = CASE WHEN SUBSTRING(cash_f, cash_pos, 4) = 'NULL' THEN NULL ELSE SUBSTRING(cash_f, cash_pos, 36)::uuid END;
	            IF fk_parent_id is NOT NULL THEN
	              cash_f_out = cash_f_out + 1;
--RAISE NOTICE '% Взято в кеш_f %/%, osmcode %', SUBSTRING(clock_timestamp()::text, 12, 12), cash_f_out, cash_f_in, osmcode;
	            END IF;
	          END IF;
	        END IF;
-- RAISE NOTICE 'fk_parent_id %, osmcode %, pos %, %/%',fk_parent_id, osmcode, cash_pos, cash_f_out, cash_f_in;
-- Идентификация новой прямой или конечной ссылки
	        IF fk_parent_id is NULL AND (cash_pos < 0 OR back <> 1) THEN -- для обратной связи используем только исходный кеш, во избежании конфликтов записи связей с двух сторон
--raise notice 'EE4_1 % %', fk_attribute_idt, CASE WHEN map THEN 'mapping' ELSE '' END;
	          e_command = 'SELECT ' || CASE WHEN map THEN 'classifier_node_id, fqn' ELSE 'id, name' END
	           || ' FROM ' || CASE WHEN map THEN 'fs.foreign_system_data_mapping JOIN node."' || foreignres_id::text || '" n ON n.id = classifier_node_id' ELSE 'node."' || foreignres_id::text || '"' END || da
	           || 'WHERE '
	           || CASE
	              WHEN map THEN 'fs_resource_id = ''' || uk || ''' AND fs_value = ''' || osmcode || ''''
	              WHEN uk <> '' THEN 'props @> (''{"' || uk || '": "' || service.json_masc_simbols(REPLACE(osmcode, '''', '''''')) || '"}'')::jsonb'
	              ELSE 'foreign_system_id = ''' || foreign_system_id0::text || ''' AND foreign_code = ''' || REPLACE(osmcode, '''', '''''') || ''''
	              END || ' LIMIT 1';
--RAISE NOTICE 'Идентификация новой прямой или конечной ссылки% %', da, e_command;
--IF osmcode LIKE '%''%' THEN raise notice 'EE6 %', e_command; END IF;
	          EXECUTE e_command INTO fk_parent_id, val_cross_term;
	          IF cash_pos = 0 AND fk_parent_id is NOT NULL THEN
	            cash_f_in = cash_f_in + 1;
-- RAISE NOTICE '% Вставлено в кеш_f %/%, osmcode %', SUBSTRING(clock_timestamp()::text, 12, 12), cash_f_out, cash_f_in, osmcode;
	            cash_f = cash_f || '[' || osmcode || ']' || COALESCE(tabid::text, 'NULL');
	          END IF;
	        END IF;
tend = clock_timestamp();
--RAISE NOTICE 'поиск % %', SUBSTRING(tbegin::text, 12,12), tend - tbegin;
selectNewParent = (selectNewParent + tend) - tbegin;
	        IF fk_parent_id is NULL THEN
	          RAISE NOTICE '!!! Объект ОСМ код % тип %(%) не найден по %', osmcode, foreignres_id, fk_cross_tab_res
	          , CASE WHEN map THEN 'мапингу внешнего ресурса ' || uk || ', или объект отсутствует в базе' WHEN uk <> '' THEN 'атрибуту ' || uk ELSE 'foreign_code' END;
	          IF last_version_block = 1 THEN EXIT; END IF; -- не нашли нужную старую версию, уходим
	          CONTINUE; -- переходим к следующей связи, эту не меняем
	        END IF;
	      ELSE
	        val_cross_term = NULL;
	        IF LEFT(uk, 1) <> '!' THEN
	          fk_parent_id = NULL;
	        ELSE
--RAISE NOTICE 'd-l %', uk;
	          fk_parent_id = RIGHT(uk, -1);
	        END IF;
	      END IF;
-- Старая прямая ссылка или промежуточная ссылка на старый кросс
tbegin = clock_timestamp();
	      IF collect NOT IN (1, 2) AND back = 0 THEN
	        e_command = 'SELECT id, to_id FROM rel."' || res_tab::text || '" WHERE from_id = ''' || tabid::text || ''' AND attribute_id = ''' || fk_attribute_id::text || ''' LIMIT 1';
--IF LEFT(uk, 1) = '!' THEN RAISE NOTICE 'fk_parent_old_id %', fk_parent_old_id; END IF;
	      ELSIF collect = 1 OR back = -1 THEN -- прямой COLLECTION
	        e_command = 'SELECT id, to_id FROM rel."' || res_tab::text || '" WHERE from_id = ''' || tabid::text || ''' AND attribute_id = ''' || fk_attribute_id::text || ''' AND to_id = ''' || fk_parent_id || ''' LIMIT 1';
	      ELSIF collect = 2 THEN -- back COLLECTION
	        e_command = 'SELECT id, to_id FROM rel."' || foreignres_id::text || '" WHERE to_id = ''' || tabid::text || ''' AND attribute_id = ''' || fk_attribute_id::text || ''' AND from_id = ''' || fk_parent_id || ''' LIMIT 1';
	      ELSE
	        e_command = 'SELECT id, to_id FROM rel."' || foreignres_id::text || '" WHERE to_id = ''' || tabid::text || ''' AND attribute_id = ''' || fk_attribute_id::text || ''' LIMIT 1';
	      END IF;
--raise notice 'EE7 %', e_command;
	      EXECUTE e_command INTO fk_rel_id, fk_parent_old_id;
tend = clock_timestamp();
selectOldRelParent = (selectOldRelParent + tend) - tbegin;
	      IF fk_cross_tab_res is NULL THEN -- прямая ссылка или обратная ссылка, а промежуточная вообще отменена со своим двойным проходом - всё покрыто мапом
tbegin = clock_timestamp();
	        IF fk_parent_id = fk_parent_old_id AND last_version <> 1 OR fk_parent_id is NULL AND fk_rel_id is NULL THEN CONTINUE; END IF;
	        IF fk_parent_id is NULL AND fk_rel_id is NOT NULL AND collect NOT IN (1, 2) AND back = 0 THEN
IF voice = name_tab THEN RAISE NOTICE 'Уд. связи % (%)', fk_attribute_id, fk_parent_old_id; END IF;
	          e_command = 'DELETE FROM rel."' || res_tab::text || '" WHERE id = ''' || fk_rel_id::text || '''';
--raise notice 'EE8 %', e_command;
	          EXECUTE e_command;
	          del_sv = del_sv + 1;
	        ELSIF fk_parent_id <> fk_parent_old_id AND collect NOT IN (1, 2) AND back = 0 THEN
IF voice = name_tab THEN RAISE NOTICE 'Смена связи % с % на %', fk_attribute_id, fk_parent_old_id, fk_parent_id; END IF;
	          e_command = 'UPDATE rel."' || res_tab::text || '" SET to_id = ''' || fk_parent_id::text || ''' WHERE id = ''' || fk_rel_id::text || '''';
--raise notice 'EE9 %', e_command;
	          EXECUTE e_command;
	          upd_sv = upd_sv + 1;
	        ELSIF fk_parent_id <> fk_parent_old_id AND back = 0 THEN
IF voice = name_tab THEN RAISE NOTICE 'Смена обр. связи % от % на %', fk_attribute_id, fk_parent_old_id, fk_parent_id; END IF;
	          e_command = 'UPDATE rel."' || foreignres_id::text || '" SET from_id = ''' || fk_parent_id::text || ''' WHERE id = ''' || fk_rel_id::text || '''';
--raise notice 'EE10 %', e_command;
	          EXECUTE e_command;
	          upd_sv = upd_sv + 1;
	        ELSIF fk_parent_old_id is NULL THEN
IF voice = name_tab THEN RAISE NOTICE 'Уст. %связи % %', CASE WHEN last_version = 1 THEN 'верс. ' ELSE '' END, fk_attribute_id, CASE WHEN collect <> 2 AND back <> 1 THEN 'на ' ELSE 'от ' END || fk_parent_id::text; END IF;
	          e_command = 'INSERT INTO rel."' || CASE WHEN collect <> 2 AND back <> 1 THEN res_tab::text ELSE foreignres_id::text END || '" (from_type_id, attribute_id, from_id, to_id) VALUES($' || '1, $2, $3, $4)';
--raise notice 'EE11 %', e_command;
	          EXECUTE e_command USING
	            CASE WHEN collect <> 2 AND back <> 1 THEN res_tab ELSE foreignres_id END,
	            fk_attribute_id,
	            CASE WHEN collect <> 2 AND back <> 1 THEN tabid ELSE fk_parent_id END,
	            CASE WHEN collect <> 2 AND back <> 1 THEN fk_parent_id ELSE tabid END;
	          ins_sv = ins_sv + 1;
--RAISE NOTICE 'q01';
	        END IF;
delupinsRelParent = delupinsRelParent + clock_timestamp() - tbegin;
--raise notice 'delupinsRelParent = %', delupinsRelParent;
--raise notice 'last_version_block = %, last_version %, collect %, collect_id = "%"', last_version_block, last_version, collect, collect_id;
	        IF last_version_block = 1 THEN
	          EXIT; -- установили связь на старую версию и уходим
	        ELSIF last_version = 1 AND LENGTH(collect_id) > 0 THEN
	          ndel = 0;
		  RAISE NOTICE '% Синхронизируем связи COLLECTION с новой версией tabid % fk_parent_id %', SUBSTRING(clock_timestamp()::text, 12,12), tabid, fk_parent_id;
	          EXECUTE collect_del_ins USING tabid, fk_parent_id;
	        END IF;
	      END IF;
	    END LOOP;
--RAISE NOTICE '%', -8;
--RAISE NOTICE 'q03';
	  END IF;
	  IF res_tab::uuid = r_ship THEN
	    SELECT "0c989a5a-1b16-45e9-be2d-cdb7b30791de" INTO local_id FROM node."dac4c525-cbd4-4942-ad89-5aefb6e8a6f9" WHERE id = tabid;
--last_version_block = 1 ?
	    i = service.local_ship_for_plagin(now()::timestamp, tabid
	    , CASE WHEN last_version_block = 1 THEN oldname ELSE LEFT(val_term, 512) END
	    , CASE WHEN last_version_block = 1 THEN old_props ELSE val_json END
	    , foreign_system_id0, val_id, local_id, true, true, old_props);
	  ELSIF (oldname is DISTINCT FROM val_term OR val_json is DISTINCT FROM old_props) AND last_version_block <> 1 THEN -- Изменение содержания только для более новой версии
	    IF NOT p_new AND voice = name_tab THEN RAISE NOTICE 'Нов. контент';  END IF;
/* IF voice <> name_tab THEN
	    RAISE NOTICE 'Изменение содержания №%
"%": %
"%": %', num_rec, oldname, service.view_props(old_props), val_term, service.view_props(val_json);
END IF;*/
	    e_command = 'UPDATE node."' || res_tab::text || '" SET name = $1, props = $2 WHERE id = $3';
--raise notice 'EE13 %', e_command;
	    EXECUTE e_command USING LEFT(val_term, 512), val_json, tabid;
	    IF NOT p_new THEN upd_obj = upd_obj + 1; END IF;
insertRecord = insertRecord + clock_timestamp() - tbegin;
--raise notice 'updateRecord = %', insertRecord;
	  END IF;
--RAISE NOTICE 'q1';


	  IF lreltab > 0 AND node_data_set is NOT NULL THEN
--RAISE NOTICE '%', 'Мапирование reltab';
	    reltab = REPLACE(reltab, '@' || LPAD(val_id, lid - 1) || RPAD('*', 37), '@' || LPAD(val_id, lid - 1) || '*'
		     || (CASE WHEN val_id = hidstart THEN node_data_set ELSE tabid END)::text);
--RAISE NOTICE 'пара связи %', '@' || LPAD(val_id, lid - 1) || '*' || (CASE WHEN val_id = hidstart THEN node_data_set ELSE tabid END)::text;
	  END IF;
--	  COMMIT;
	EXCEPTION WHEN OTHERS THEN
	  GET STACKED DIAGNOSTICS v_returned_sqlstate = RETURNED_SQLSTATE,
	    v_message_text = MESSAGE_TEXT,
	    v_column_name = COLUMN_NAME,
	    v_constraint_name = CONSTRAINT_NAME,
	    v_pg_datatype_name = PG_DATATYPE_NAME,
	    v_table_name = TABLE_NAME,
	    v_schema_name = SCHEMA_NAME,
	    v_pg_exception_detail = PG_EXCEPTION_DETAIL,
	    v_pg_exception_hint = PG_EXCEPTION_HINT,
	    v_pg_exception_context = PG_EXCEPTION_CONTEXT;
	  num_rec = num_rec;
/*	  il = service.import_log_write(service, 'MESSAGE', clock_timestamp()::text||' Обработано и отменено: '||num_rec::text||' записей, вставлено '||ins_obj::text||', изменено '||upd_obj::text||',
 нов. связей '||ins_sv::text||', изм. связей '||upd_sv::text||', уд связей '||del_sv::text||'');*/
	  RAISE NOTICE '%
До ошибки обработано % записей, вставлено %, изменено %, нов. связей %, изм. связей %, уд связей %'
	  , SUBSTRING(clock_timestamp()::text, 12,12), num_rec - 1, ins_obj, upd_obj, ins_sv, upd_sv, del_sv;
--	  RAISE NOTICE 'Взято/вставлено в кеш %/%, в кеш_f %/%', cash_out, cash_in, cash_f_out, cash_f_in;
	  /*il = service.import_log_write(service, 'ERROR', 'ERROR на записи №'||num_rec::text||', ОСМ код '||val_id::text||': returned_sqlstate="'||v_returned_sqlstate::text||'", message_text="'||v_message_text::text||'"
              , table_name="'||v_table_name::text||'", schema_name="'||v_schema_name::text||'", column_name="'||v_column_name::text||'", constraint_name="'||v_constraint_name::text||'", pg_datatype_name="'||v_pg_datatype_name::text||'"
              , pg_exception_detail="'||v_pg_exception_detail::text||'", pg_exception_hint="'||v_pg_exception_hint::text||'", pg_exception_context="'||v_pg_exception_context||'"');*/
          RAISE EXCEPTION 'ERROR на записи №%, ОСМ код %: returned_sqlstate="%", message_text="%"
              , table_name="%", schema_name="%", column_name="%", constraint_name="%", pg_datatype_name="%"
              , pg_exception_detail="%", pg_exception_hint="%", pg_exception_context="%"'
              , num_rec, val_id, v_returned_sqlstate, v_message_text
              , v_table_name, v_schema_name, v_column_name, v_constraint_name, v_pg_datatype_name
              , v_pg_exception_detail, v_pg_exception_hint, v_pg_exception_context;
 --         RETURN -1;
	END;
--RAISE NOTICE 'q2';
	END LOOP;
	IF lreltab > 0 THEN
--RAISE NOTICE '%', reltab;
	  posreltab = 1 - lid*2 - 37*2; -- -lid - 37*2 + 2;
	  -- il = service.import_log_write(service, 'MESSAGE', 'Проверка/установка иерархических связей. Это пара минут. '||clock_timestamp()::text);
	  RAISE NOTICE 'Проверка/установка иерархических связей company. Это пара минут.';
	  bad_parent_code = NULL;
	  LOOP
--RAISE NOTICE '%', -9;
	    posreltab = posreltab + lid*2 + 37*2;
	    IF posreltab >= lreltab THEN EXIT; END IF;
	    psrl = SUBSTRING(reltab, posreltab, lid*2 + 37*2);
--RAISE NOTICE '!!!! для теста "%"', psrl;
	    c_code = LTRIM(SUBSTRING(psrl, 2, lid-1));
	    c_id = CASE WHEN SUBSTRING(psrl, lid + 2, 1) <> ' ' THEN SUBSTRING(psrl, lid + 2, 36) ELSE NULL END;
	    p_code = LTRIM(SUBSTRING(psrl, lid + 37 + 2, lid-1));
	    p_id = CASE WHEN p_code = hidstart THEN node_data_set::text WHEN SUBSTRING(psrl, lid*2 + 37 + 2, 1) <> ' ' THEN SUBSTRING(psrl, lid*2 + 37 + 2, 36) ELSE NULL END;
	    IF c_id is NULL THEN CONTINUE; END IF; -- чайлд не участвует в текущей загрузке, а к новым вершинам не привязываем прежние объекты, поскольку полииерархия не позволяет сделать это коректно
	    IF p_id is NULL THEN -- тогда поищем парента в базе, если он не участвует в текущей загрузке
RAISE NOTICE '%', '{"' || id_code || '": "' || REPLACE(REPLACE(REPLACE(p_code, '\', '\\'), '"', '\"'), '/', '\/') || '"}';
	      e_command = 'SELECT id FROM node."' || res_tab::text || '" WHERE foreign_system_id = ''' || foreign_system_id0 || ''' AND foreign_code = ''' || p_code || ''' LIMIT 1';
--raise notice 'EE14 %', e_command;
	      EXECUTE e_command INTO p_id;
RAISE NOTICE 'Parent для ОСМ кода % с ID % взят из БД по parent code % Parent_ID %', c_code, c_id, p_code, p_id;
	    END IF;
	    IF p_id is NULL THEN
	      -- il = service.import_log_write(service, 'WARNING', 'Отсутствие ID парента в связи child_code '||c_code::text||', id '||c_id::text||' -> parent_code '||p_code::text||' id '||p_id::text);
		      IF bad_parent_code is NULL THEN
-- надо завести вершину для привязки приёмных папаш вместо отсутствующих парентов
		        e_command = 'SELECT id FROM node."' || res_tab::text || '" WHERE foreign_system_id = ''' || foreign_system_id0 || ''' AND foreign_code = ''' || upos || ''' LIMIT 1';
		        EXECUTE e_command INTO bad_parent_code;
		        IF bad_parent_code is NULL THEN -- надо завести вершину для привязки отсутствующих парентов
		          RAISE NOTICE 'Строится вершина для кодов, неполученных из ОСМ ФАР узлов иерархии company';
		          bad_parent_code = uuid_generate_v4();
		          e_command = 'INSERT INTO node."' || res_tab::text || '" (id, name, props, foreign_system_id, foreign_code, type_id)
		          VALUES($' || '1, ''Коды неполученных из ОСМ ФАР узлов иерархии'', ''{}'', ''' || foreign_system_id0 || ''', ''' || upos || ''', $2)';
		          EXECUTE e_command USING bad_parent_code, res_tab;
		          IF NOT EXISTS(SELECT parent_id FROM dataset.tree_ds WHERE data_set_id = node_data_set::uuid AND parent_id = p_id AND node_id = c_id) THEN -- запись в дерево вершины для потерянных кодов
		            INSERT INTO dataset.tree_ds(data_set_id, node_id, parent_id, ds_type_id, type_id, parent_type_id, position_num)
		            VALUES (node_data_set::uuid, bad_parent_code, node_data_set::uuid, res_tree_set, res_tab::uuid, res_tree_set, 0);
		          END IF;
		        END IF;
		      END IF;
-- Теперь надо записать приёмного папашу в классификатор
		      e_command = 'SELECT id FROM node."' || res_tab::text || '" WHERE foreign_system_id = ''' || foreign_system_id0 || ''' AND foreign_code = ''' || p_code::text || ppos || ''' LIMIT 1';
		      RAISE NOTICE 'Строится вершина для кода, неполученного из ОСМ ФАР узла иерархии company - %', p_code;
		      EXECUTE e_command INTO p_id;
		      IF p_id is NULL THEN
		        p_id = uuid_generate_v4();
		        RAISE NOTICE 'Запись в company с id - %', p_id;
		        e_command = 'INSERT INTO node."' || res_tab::text || '" (id, name, props, foreign_system_id, foreign_code, type_id)
		        VALUES($' || '1, ''' || p_code::text || ' - код неполученного из ОСМ ФАР узла иерархии company'', ''{}'', ''' || foreign_system_id0 || ''', ''' || p_code::text || ppos || ''', $2)';
		        EXECUTE e_command USING p_id, res_tab;
		      END IF;
		      RAISE NOTICE 'Для child_code %, id % вместо неполученного папаши parent_code % предоставляется приёмный папаша %', c_code, c_id, p_code, p_id;
		      INSERT INTO dataset.tree_ds(data_set_id, node_id, parent_id, ds_type_id, type_id, parent_type_id, position_num) -- привязка приёмного папаши к bad_parent_code
		      VALUES (node_data_set::uuid, p_id, bad_parent_code, res_tree_set, res_tab::uuid, res_tab::uuid , 0) ON CONFLICT DO NOTHING;
	    ELSIF hiderror LIKE '%*' || c_code || '~' || p_code || '*%' THEN
	      -- il = service.import_log_write(service, 'WARNING', 'Игнорируем ошибочную связь в классификаторе от кода '||c_code::text||', id '||c_id::text||' к коду '||p_code::text);
	      RAISE NOTICE 'Игнорируем ошибочную связь в company от кода % id % к коду %', c_code, c_id, p_code;
	      CONTINUE;
	    END IF;
--RAISE NOTICE 'запись в graph.tree_data_set % -> %', c_id, p_id;
	      IF NOT EXISTS (SELECT parent_id FROM dataset.tree_ds WHERE data_set_id = node_data_set AND node_id = c_id AND parent_id = p_id) THEN
--RAISE NOTICE 'Установка связи oт % к % (% -> %)', LPAD(c_code,6), LPAD(p_code,6), c_id, p_id;
	        IF dataset.is_it_child_of_parent_in_tree(p_id, c_id, node_data_set::uuid) THEN
	          RAISE NOTICE 'Запись иерархического набора данных data_set_id %, parent_node_id %, node_id % отброшена, чтобы не было зацикливания', node_data_set, p_id, c_id;
	        ELSE
--RAISE NOTICE 'Установка связи oт % к % (% -> %)', LPAD(c_code,6), LPAD(p_code,6), c_id, p_id;
		  ALTER TABLE dataset.tree_ds DISABLE TRIGGER dataset_tree_ds_tr;
	          INSERT INTO dataset.tree_ds(data_set_id, ds_type_id, node_id, type_id, parent_id, parent_type_id, position_num)
	          VALUES (node_data_set, res_tree_set, c_id, res_tab, p_id, CASE WHEN p_id <> node_data_set THEN res_tab ELSE res_tree_set END, 0);
		  ALTER TABLE dataset.tree_ds ENABLE TRIGGER dataset_tree_ds_tr;
	        END IF;
	      END IF;
	  END LOOP;
--RAISE NOTICE '%', -10;
	END IF;
	num_rec = num_rec - 1;
/*	il = service.import_log_write(service, 'MESSAGE', clock_timestamp()::text||' Обработано записей '||num_rec::text||', вставлено '||ins_obj::text||', изменено '||upd_obj::text||',
 нов. связей '||ins_sv::text||', изм. связей '||upd_sv::text||', уд связей '||del_sv::text||'');*/
	RAISE NOTICE '%
Обработано записей %, вставлено %, изменено %, нов. связей %, изм. связей %, уд связей %'
, SUBSTRING(clock_timestamp()::text, 12,12), num_rec, ins_obj, upd_obj, ins_sv, upd_sv, del_sv;
-- RAISE NOTICE 'Взято/вставлено в кеш_f %/%', cash_f_out, cash_f_in;
--IF voice = name_tab THEN raise exception 'Отладочный эксепшн'; END IF;
	RETURN 0;
END;
/*
-- запуск
SELECT service.Import_OSM_Tabs_fly(
'<?xml version="1.0" encoding="WINDOWS-1251"?>
<rows>
<row><OKDP></OKDP><telex></telex><phone></phone><e_mail></e_mail><id_information_source>301</id_information_source><sec_sert_date_issue></sec_sert_date_issue><sec_sert_period></sec_sert_period><state_registrator></state_registrator><registration_date></registration_date><fax_resp_person></fax_resp_person><postal_address></postal_address><id_own>0</id_own><kpp></kpp><act_stamp>2016-12-05 04:19:03</act_stamp><responsible></responsible><own>не определено</own><director></director><phone_resp_person></phone_resp_person><KOPF></KOPF><e_mail_resp_person></e_mail_resp_person><action>ii</action><note></note><bank></bank><id_country>0</id_country><sec_sert_number></sec_sert_number><SOATO></SOATO><reg_number></reg_number><fax></fax><id_basik>0</id_basik><SOOGU></SOOGU><sec_sert_issuer></sec_sert_issuer><id_recd>14981</id_recd><resp_person></resp_person><OKPO></OKPO><prev_id></prev_id><timestamp>2016-12-05 01:17:00</timestamp><own_full></own_full><inn></inn><OKONH></OKONH><address></address><KFS></KFS><examination_date></examination_date><log_usr>fisher@localhost</log_usr><examination_type></examination_type></row>
<row><fax></fax><SOOGU></SOOGU><id_basik>1</id_basik><SOATO></SOATO><sec_sert_number></sec_sert_number><id_country>643</id_country><reg_number></reg_number><timestamp>2016-12-06 08:15:00</timestamp><own_full>Общество с ограниченной ответственностью "Протей"</own_full><id_recd>14982</id_recd><sec_sert_issuer></sec_sert_issuer><resp_person></resp_person><prev_id></prev_id><OKPO>35289818</OKPO><address>680000,Хабаровский край,г.Хабаровск,ул.Пушкина,15а</address><OKONH></OKONH><inn>2721221320</inn><KFS></KFS><examination_date></examination_date><examination_type></examination_type><log_usr>fisher@localhost</log_usr><registration_date></registration_date><state_registrator></state_registrator><sec_sert_date_issue></sec_sert_date_issue><sec_sert_period></sec_sert_period><OKDP></OKDP><id_information_source>301</id_information_source><e_mail></e_mail><telex></telex><phone>(423)2650426</phone><postal_address></postal_address><fax_resp_person></fax_resp_person><id_own>11758</id_own><phone_resp_person></phone_resp_person><director>Мячин Олег Владимирович учредитель</director><responsible>Гончар В.А.</responsible><kpp>272101001</kpp><act_stamp>2016-12-05 23:26:03</act_stamp><own>ООО "Протей"</own><note>05.12.16рег:ОГРН1152724013727 uraudit@mail.ru</note><action>ii</action><bank></bank><KOPF></KOPF><e_mail_resp_person></e_mail_resp_person></row>
<row><bank></bank><action>ii</action><note>29.11.16рег:ОГРН1092700000920 ooo-sts@mail.ru</note><e_mail_resp_person></e_mail_resp_person><KOPF>89</KOPF><director>Киле Александр Михайлович Председатель</director><phone_resp_person></phone_resp_person><own>СРО КМНС "Бихан"</own><act_stamp>2016-12-06 02:40:02</act_stamp><kpp>271401001</kpp><responsible>Гончар В.А.</responsible><id_own>11757</id_own><fax_resp_person></fax_resp_person><postal_address></postal_address><sec_sert_date_issue></sec_sert_date_issue><sec_sert_period></sec_sert_period><registration_date></registration_date><state_registrator></state_registrator><telex></telex><phone>8(924)3074483</phone><id_information_source>301</id_information_source><e_mail></e_mail><OKDP></OKDP><log_usr>fisher@localhost</log_usr><examination_type></examination_type><KFS>16</KFS><examination_date></examination_date><inn>2714010054</inn><OKONH></OKONH><address>682375 Хабаровский край,Нанайский район,с.Даерга,ул.Заводская,3</address><own_full>Семейная (родовая) община коренных малочисленных народов Севера "Бихан"</own_full><timestamp>2016-12-06 11:36:00</timestamp><OKPO>62206433</OKPO><prev_id></prev_id><sec_sert_issuer></sec_sert_issuer><id_recd>14983</id_recd><resp_person></resp_person><id_basik>1</id_basik><SOOGU>49013</SOOGU><fax></fax><reg_number></reg_number><id_country>643</id_country><sec_sert_number></sec_sert_number><SOATO>08228000009</SOATO></row>
</rows>'
,NULL
,'own'--'ship'--'permissionHead'--'-permissionHistory'--'-permissionConditions'--'-permissionPeriods'--'-shipregistr'--'-ownership'--'-shipowner'--'catch'--'unload'--'quota'--'ssd'--'shiptth'--'odu'--
);

SELECT service.Import_OSM_Tabs_fly(
'<?xml version="1.0" encoding="WINDOWS-1251"?>
<rows>
<row><refrigerant></refrigerant><id_ves>22381</id_ves><responsible>MF-ALENA\Alena</responsible><engine_type></engine_type><id_information_source>202</id_information_source><registration_number></registration_number><id_country>578</id_country><freezer_output></freezer_output><moduling></moduling><freezer_quantity></freezer_quantity><ves>Peder</ves><id_type_ves>0</id_type_ves><freezer_type></freezer_type><date_registration_ves>2016-12-12</date_registration_ves><country_building></country_building><cargo_size></cargo_size><board_number>N210VV</board_number><id_port>2999</id_port><engine_power>596</engine_power><action>ii</action><max_velocity></max_velocity><year_building></year_building><prev_id></prev_id><setting></setting><engine_quantity></engine_quantity><freezer_volume></freezer_volume><id_ves_right_limit></id_ves_right_limit><sailing_region></sailing_region><cargo_volume></cargo_volume><registration_criterion></registration_criterion><width>8</width><cargo_quantity></cargo_quantity><id_own>7003</id_own><date_expiration_ves></date_expiration_ves><main_prod></main_prod><ice_type></ice_type><town_building></town_building><id_rmc>202</id_rmc><water_volume></water_volume><length>25</length><registration_port_date></registration_port_date><freezer_temp></freezer_temp><generator_power></generator_power><height_board></height_board><id_owner>7003</id_owner><id_MMSI>259263000</id_MMSI><note>14.01.16 стар.назв.Fugloeyfjell, борт.T110L; 11.11.16 стар.назв.Atloy Viking, борт.SF22A; 12.12.16 стар.борт.N210MS</note><radio_sign>LHTT</radio_sign><id_IMO>7905247</id_IMO><id_purpose>0</id_purpose><main_prod_output></main_prod_output><timestamp>2016-12-12 07:48:31</timestamp><id_recd>38199</id_recd><log_usr>fisher@localhost</log_usr><act_stamp>2016-12-13 09:52:01</act_stamp><volume></volume><crew></crew><boiler_quantity></boiler_quantity><ves_lat>PEDER</ves_lat><dedveit></dedveit><automatization_degree></automatization_degree><project_autonomy></project_autonomy><engine_year_place></engine_year_place><freezer_size></freezer_size><total_volume></total_volume></row>
<row><id_ves_right_limit></id_ves_right_limit><freezer_volume></freezer_volume><sailing_region></sailing_region><engine_quantity></engine_quantity><setting></setting><id_port>2999</id_port><prev_id></prev_id><max_velocity></max_velocity><year_building>1985</year_building><engine_power>790</engine_power><action>dd</action><cargo_size></cargo_size><board_number>T251T</board_number><main_prod></main_prod><town_building></town_building><ice_type></ice_type><id_rmc>202</id_rmc><water_volume></water_volume><cargo_quantity></cargo_quantity><date_expiration_ves></date_expiration_ves><id_own>7003</id_own><registration_criterion></registration_criterion><width>8</width><cargo_volume></cargo_volume><registration_number></registration_number><id_country>578</id_country><freezer_output></freezer_output><id_information_source>202</id_information_source><engine_type></engine_type><responsible>MF-ALENA\Alena</responsible><id_ves>22383</id_ves><refrigerant></refrigerant><freezer_type></freezer_type><date_registration_ves>2016-06-06</date_registration_ves><country_building></country_building><moduling></moduling><freezer_quantity></freezer_quantity><id_type_ves>0</id_type_ves><ves>Johannes</ves><act_stamp>2016-12-13 09:52:01</act_stamp><volume></volume><log_usr>fisher@localhost</log_usr><timestamp>2016-06-06 07:01:34</timestamp><id_recd>38200</id_recd><main_prod_output></main_prod_output><engine_year_place></engine_year_place><freezer_size></freezer_size><total_volume></total_volume><dedveit></dedveit><project_autonomy></project_autonomy><automatization_degree></automatization_degree><boiler_quantity></boiler_quantity><ves_lat>JOHANNES</ves_lat><crew></crew><id_owner>7003</id_owner><height_board></height_board><generator_power></generator_power><freezer_temp></freezer_temp><length>27</length><registration_port_date></registration_port_date><id_purpose>0</id_purpose><id_IMO>8514514</id_IMO><note>06.06.16 стар.назв.Solvaerskjaer, борт.F250H</note><id_MMSI>257435500</id_MMSI><radio_sign>LNYZ</radio_sign></row>
<row><freezer_type></freezer_type><date_registration_ves>2016-06-06</date_registration_ves><country_building></country_building><moduling></moduling><freezer_quantity></freezer_quantity><id_type_ves>0</id_type_ves><ves>Johannes</ves><responsible>MF-ALENA\Alena</responsible><id_ves>22383</id_ves><refrigerant></refrigerant><registration_number></registration_number><id_country>578</id_country><freezer_output></freezer_output><id_information_source>202</id_information_source><engine_type></engine_type><registration_criterion></registration_criterion><width>8</width><cargo_volume></cargo_volume><ice_type></ice_type><main_prod></main_prod><town_building></town_building><id_rmc>202</id_rmc><water_volume></water_volume><cargo_quantity></cargo_quantity><date_expiration_ves>2016-12-13</date_expiration_ves><id_own>7003</id_own><id_port>2999</id_port><prev_id></prev_id><max_velocity></max_velocity><year_building>1985</year_building><engine_power>790</engine_power><action>ii</action><board_number>T251T</board_number><cargo_size></cargo_size><id_ves_right_limit></id_ves_right_limit><freezer_volume></freezer_volume><sailing_region></sailing_region><engine_quantity></engine_quantity><setting></setting><id_purpose>0</id_purpose><id_IMO>8514514</id_IMO><id_MMSI>257435500</id_MMSI><note>06.06.16 стар.назв.Solvaerskjaer, борт.F250H</note><radio_sign>LNYZ</radio_sign><generator_power></generator_power><height_board></height_board><freezer_temp></freezer_temp><length>27</length><registration_port_date></registration_port_date><id_owner>7003</id_owner><ves_lat>JOHANNES</ves_lat><boiler_quantity></boiler_quantity><crew></crew><engine_year_place></engine_year_place><freezer_size></freezer_size><total_volume></total_volume><dedveit></dedveit><project_autonomy></project_autonomy><automatization_degree></automatization_degree><log_usr>fisher@localhost</log_usr><id_recd>38201</id_recd><timestamp>2016-12-13 06:43:12</timestamp><main_prod_output></main_prod_output><act_stamp>2016-12-13 09:52:01</act_stamp><volume></volume></row>
<row><freezer_size></freezer_size><total_volume></total_volume><engine_year_place></engine_year_place><project_autonomy></project_autonomy><automatization_degree></automatization_degree><dedveit></dedveit><boiler_quantity></boiler_quantity><ves_lat>BENJACO</ves_lat><crew></crew><volume></volume><act_stamp>2016-12-13 09:52:01</act_stamp><log_usr>fisher@localhost</log_usr><timestamp>2016-12-13 06:43:12</timestamp><id_recd>38202</id_recd><main_prod_output></main_prod_output><id_purpose>0</id_purpose><id_IMO>8514514</id_IMO><radio_sign>LNYZ</radio_sign><note>06.06.16 стар.назв.Solvaerskjaer, борт.F250H; 13.12.16 стар.назв.Johannes, борт.T251T</note><id_MMSI>257435500</id_MMSI><id_owner>7003</id_owner><height_board></height_board><generator_power></generator_power><freezer_temp></freezer_temp><registration_port_date></registration_port_date><length>27</length><id_rmc>202</id_rmc><water_volume></water_volume><main_prod></main_prod><town_building></town_building><ice_type></ice_type><date_expiration_ves></date_expiration_ves><id_own>7003</id_own><cargo_quantity></cargo_quantity><registration_criterion></registration_criterion><width>8</width><cargo_volume></cargo_volume><sailing_region></sailing_region><id_ves_right_limit></id_ves_right_limit><freezer_volume></freezer_volume><engine_quantity></engine_quantity><setting></setting><max_velocity></max_velocity><prev_id></prev_id><year_building>1985</year_building><engine_power>790</engine_power><action>ii</action><id_port>2999</id_port><board_number>H51K</board_number><cargo_size></cargo_size><country_building></country_building><freezer_type></freezer_type><date_registration_ves>2016-12-13</date_registration_ves><id_type_ves>0</id_type_ves><ves>Benjaco</ves><moduling></moduling><freezer_quantity></freezer_quantity><id_country>578</id_country><freezer_output></freezer_output><registration_number></registration_number><id_information_source>202</id_information_source><engine_type></engine_type><responsible>MF-ALENA\Alena</responsible><id_ves>22383</id_ves><refrigerant></refrigerant></row>
</rows>'
,NULL
,'ship'--'own'--'permissionHead'--'-permissionHistory'--'-permissionConditions'--'-permissionPeriods'--'-shipregistr'--'-ownership'--'-shipowner'--'catch'--'unload'--'quota'--'ssd'--'shiptth'--'odu'--
);

*/
$$; -- Функция загрузки ресурсов при импорте из ОСМ ФАР

CREATE OR REPLACE FUNCTION service.local_ship_for_plagin
(now_time timestamp, ship_id uuid, ship_name text, ship_props jsonb, fs_id uuid, fs_code text, local_id uuid, new_ship_props bool, new_ship_rels bool, ship_props_old jsonb DEFAULT '{}'::jsonb)
RETURNS int
LANGUAGE plpgsql
AS
$$
DECLARE
osm uuid = 'a4a28d9e-2553-4063-980e-3b46d0ec2022';
gsr uuid = 'ac8f45b6-c7f7-4e6d-85ef-931396884dce';
gpk uuid = '6ab12659-c346-4b25-a339-d813a3747c90';
sdno uuid = '65bd076f-9808-41fe-bfa5-0b3d498b1ea5';

n_imo text = '3bb2cca2-3b1e-4fb8-82a0-719b49df3799';
n_mmsi text = '960b80a6-daf0-4426-984f-091ca72012ee';
n_callsign text = '80d4e6d5-4dbc-4e77-a960-defb3ae7c9b9';
n_ruscallsign text = 'ec182f67-22d1-49e7-acca-724fd17d2ded';
n_boardNumber text = 'b647e04e-2e6f-4de9-a2ba-5c79a076fe7c';
n_buildYear text = 'bf861948-b7ff-47da-b635-0b98e7ea0513';
n_length text = '63769ca8-39b8-4ab8-b7a1-356cf1515c8d';
n_width text = '920bf296-c4f9-4ac1-852a-9afaef7bc88f';
n_marineReg text = 'a1420296-2b09-4ab8-a7fa-6e0b864705d8';
n_shipnamerus text = 'b7034ce2-4752-4788-8ffd-26fc6720b125';
n_shipnamelat text = 'd49562c3-1de5-4209-b9da-df1eca3c1836';
n_osmShipCode text = '1a97d73d-0bb2-4c55-bca4-19a939fc4127';
n_osmShipForeignCode text = '1caea3b9-4853-407c-b9b5-d238e16bb59c';

ndr int;
kod int;
i int = 0;
BEGIN
--	ship_props = COALESCE(ship_props, '{}'::jsonb) = '{}'::jsonb;
	IF COALESCE(ship_props, '{}'::jsonb) = '{}'::jsonb OR ship_id is NULL OR fs_id is NULL THEN RAISE EXCEPTION 'Пустые значения в ship_id или ship_props или fs_id'; END IF;
	BEGIN
	  SELECT droper INTO ndr FROM ident_droper LIMIT 1;
	EXCEPTION WHEN OTHERS THEN
	  CREATE TEMP TABLE ident_droper (droper int not NULL DEFAULT 0);
	  ndr = 0;
	  INSERT INTO ident_droper VALUES (0);
	END;
	IF ndr >= 1000 THEN
	  ndr = 0;
	  DROP TABLE ident_droper;
	  DROP TABLE ident_ship_set;
	  CREATE TEMP TABLE ident_droper (droper int not NULL DEFAULT 0);
	  INSERT INTO ident_droper VALUES (ndr);
	END IF;

	IF ship_props is DISTINCT FROM ship_props_old THEN
raise notice 'делается UPDATE name, props, version';
	  UPDATE ident_droper SET droper = -1; -- !!! Запрет работы integration_local_ship_ftr !!!
	  UPDATE node."dac4c525-cbd4-4942-ad89-5aefb6e8a6f9" SET name = ship_name, props = ship_props, version = now() WHERE id = ship_id;
	  UPDATE ident_droper SET droper = ndr; -- теперь пусть работает
	END IF;

	IF new_ship_props AND (ship_props_old ->> n_imo is DISTINCT FROM ship_props ->> n_imo
	        OR ship_props_old ->> n_mmsi is DISTINCT FROM ship_props ->> n_mmsi
	        OR ship_props_old ->> n_callsign is DISTINCT FROM ship_props ->> n_callsign
	        OR ship_props_old ->> n_ruscallsign is DISTINCT FROM ship_props ->> n_ruscallsign
	        OR ship_props_old ->> n_boardNumber is DISTINCT FROM ship_props ->> n_boardNumber
	        OR ship_props_old ->> n_buildYear is DISTINCT FROM ship_props ->> n_buildYear
	        OR ship_props_old ->> n_length is DISTINCT FROM ship_props ->> n_length
	        OR ship_props_old ->> n_width is DISTINCT FROM ship_props ->> n_width
	        OR ship_props_old ->> n_marineReg is DISTINCT FROM ship_props ->> n_marineReg
	        OR ship_props_old ->> n_shipnamerus is DISTINCT FROM ship_props ->> n_shipnamerus
	        OR ship_props_old ->> n_shipnamelat is DISTINCT FROM ship_props ->> n_shipnamelat
	        OR ship_props_old ->> n_osmShipForeignCode is DISTINCT FROM ship_props ->> n_osmShipForeignCode)
	THEN
raise notice 'вызвана идентиф';
	  kod = service.ident_ship_fly(fs_id, false, ship_id, ship_props, ship_name, CASE WHEN fs_id = osm THEN fs_code ELSE ship_props ->> n_osmShipForeignCode END, local_id); -- идентификация
	ELSIF NOT new_ship_rels AND local_id is NOT NULL THEN
raise notice 'ничего не надо';
	  RETURN 1; -- ничего не менялось
	ELSE
	  kod = 0; -- создание временных таблиц и заполнение её записями группы без идентификации
raise notice 'только связи';
	  BEGIN
	    DELETE FROM ident_ship_set;
	  EXCEPTION WHEN OTHERS THEN
	    CREATE TEMP TABLE IF NOT EXISTS ident_ship_set -- ident_id: идентификатор в системе
	     (ord int, xy int, group_id uuid, ident_id uuid, info_source_id uuid, sum1 int -- ord:1-судно, 3-исх.судно
	     ,imo text, mmsi text, callsign text, ruscallsign text, boardNumber text, buildYear text, length text, width text, marineReg text, namerus text, namelat text, osmShipCode text, shipType text, buildPlace text);
	  END;

	  INSERT INTO ident_ship_set -- принятая запись
	  SELECT 3, NULL::int, local_id, ship_id, fs_id, NULL::int, ship_props ->> n_imo, ship_props ->> n_mmsi, ship_props ->> n_callsign, ship_props ->> n_ruscallsign, ship_props ->> n_boardNumber
	  , ship_props ->> n_buildYear, ship_props ->> n_length, ship_props ->> n_width, ship_props ->> n_marineReg, ship_props ->> n_shipnamerus, ship_props ->> n_shipnamelat
	  , COALESCE(ship_props ->> n_osmShipForeignCode, ship_props ->> n_osmShipCode), NULL::uuid, NULL::text;

	 IF local_id is NOT NULL THEN -- группа локального
	  INSERT INTO ident_ship_set
	  SELECT 1, 0, local_id, id, foreign_system_id, NULL::int, props ->> n_imo, props ->> n_mmsi, props ->> n_callsign, props ->> n_ruscallsign, props ->> n_boardNumber
	  , props ->> n_buildYear, props ->> n_length, props ->> n_width, props ->> n_marineReg, props ->> n_shipnamerus, props ->> n_shipnamelat
	  , COALESCE(props ->> n_osmShipForeignCode, props ->> n_osmShipCode), NULL::uuid, NULL::text
	  FROM node."dac4c525-cbd4-4942-ad89-5aefb6e8a6f9" WHERE "0c989a5a-1b16-45e9-be2d-cdb7b30791de" = local_id;
	 END IF;
	END IF;

	ndr = ndr + 1;
	UPDATE ident_droper SET droper = ndr;
-- Выполнение интеграции записи в нужное локальное судно
raise notice 'вызвана интеграция';
	kod = service.integrate_ship_fly_local_ship2();
	RETURN kod;
/*
WITH a AS (
  SELECT now()::timestamp AS ts, id, name, props, foreign_system_id, foreign_code, "0c989a5a-1b16-45e9-be2d-cdb7b30791de" AS loc_id, false AS n_p, false AS n_r, props AS old_props FROM node."dac4c525-cbd4-4942-ad89-5aefb6e8a6f9"
  WHERE id = '76425b57-8ba0-4d58-b7ba-805c819cea98')
SELECT service.local_ship_for_plagin(ts, id, name, props, foreign_system_id, foreign_code, loc_id, n_p, n_r, old_props) FROM a; SELECT 1/0
*/
END; $$;


