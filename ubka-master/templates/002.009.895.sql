--liquibase formatted sql

--changeset yankovskiy:002.009.895 splitStatements:false stripComments:false logicalFilePath:002.000.000/002.009.000/002.009.895.sql
-- --------
--Created: 2019-03-22T15:51:45.527
--comment #16906-- Исправление в БД последствий бага неправильного вызова функции для плагинов (время работы 30 сек.)
--LiquibaseChangesetType: CUSTOM_SQL

-- --------

DROP TABLE IF EXISTS stt_m;
CREATE TEMP TABLE IF NOT EXISTS stt_m(stt_id uuid, last_reg_id text); DELETE FROM stt_m;
INSERT INTO stt_m
SELECT ship.id, MAX(reg.foreign_code) AS f_code
FROM node."dac4c525-cbd4-4942-ad89-5aefb6e8a6f9" ship
JOIN node."3217e8c2-4ab5-4d09-89e0-8367bd0de08f" reg ON reg.foreign_system_id = ship.foreign_system_id AND reg.foreign_code LIKE ship.foreign_code || '*%'
WHERE ship.foreign_system_id = 'a4a28d9e-2553-4063-980e-3b46d0ec2022' --AND foreign_code = ANY('{10912,12522,20948,20949,13813,46281,18775,18776,22867,46308}'::char(5)[])
GROUP BY ship.id;--, ship.foreign_system_id

--SELECT * FROM stt_m ORDER BY last_reg_id

DROP TABLE IF EXISTS modified_ships;
CREATE TEMP TABLE modified_ships AS
WITH ship AS
(SELECT id AS ship_id, name AS ship_name, foreign_system_id AS fs_id, foreign_code AS f_code, last_reg_id, dbcreated AS s_created, dbmodified AS s_modified, props AS s_props
 FROM node."dac4c525-cbd4-4942-ad89-5aefb6e8a6f9"
 JOIN stt_m ON stt_id = id
)
SELECT ship_id, ship_name, f_code, reg.foreign_code AS reg_last, reg.name AS reg_name, s_props, s_created, s_modified
FROM ship s
 JOIN node."3217e8c2-4ab5-4d09-89e0-8367bd0de08f" reg ON reg.foreign_system_id = fs_id AND reg.foreign_code = last_reg_id
 AND (s_props ->> '3bb2cca2-3b1e-4fb8-82a0-719b49df3799' is DISTINCT FROM reg.props ->> '3bb2cca2-3b1e-4fb8-82a0-719b49df3799' OR --num
 s_props ->> '960b80a6-daf0-4426-984f-091ca72012ee' is DISTINCT FROM reg.props ->> '960b80a6-daf0-4426-984f-091ca72012ee' OR
 s_props ->> '80d4e6d5-4dbc-4e77-a960-defb3ae7c9b9' is DISTINCT FROM reg.props ->> '80d4e6d5-4dbc-4e77-a960-defb3ae7c9b9' OR
 s_props ->> 'b647e04e-2e6f-4de9-a2ba-5c79a076fe7c' is DISTINCT FROM reg.props ->> 'b647e04e-2e6f-4de9-a2ba-5c79a076fe7c' OR
 s_props ->> '1a97d73d-0bb2-4c55-bca4-19a939fc4127' is DISTINCT FROM reg.props ->> '9eb7155b-d4ab-4a03-8673-4284b12e860e' OR --osmShipCode
 (s_props ->> '1a97d73d-0bb2-4c55-bca4-19a939fc4127' is DISTINCT FROM f_code OR reg.props ->> '9eb7155b-d4ab-4a03-8673-4284b12e860e' is DISTINCT FROM f_code) OR
 s_props ->> '63769ca8-39b8-4ab8-b7a1-356cf1515c8d' is DISTINCT FROM reg.props ->> '63769ca8-39b8-4ab8-b7a1-356cf1515c8d' OR --num
 s_props ->> '920bf296-c4f9-4ac1-852a-9afaef7bc88f' is DISTINCT FROM reg.props ->> '920bf296-c4f9-4ac1-852a-9afaef7bc88f' OR --num
 s_props ->> 'b7034ce2-4752-4788-8ffd-26fc6720b125' is DISTINCT FROM reg.props ->> 'b7034ce2-4752-4788-8ffd-26fc6720b125' OR
 s_props ->> 'd49562c3-1de5-4209-b9da-df1eca3c1836' is DISTINCT FROM reg.props ->> 'd49562c3-1de5-4209-b9da-df1eca3c1836');
-- SELECT * FROM modified_ships ORDER BY f_code
/*
SELECT ship_id, ship_name, f_code, CASE WHEN s_modified::date = '2018-06-01' THEN '' ELSE (s_modified::date)::text END AS s_modified, CASE WHEN last.dbmodified::date = '2018-06-01' THEN '' ELSE (last.dbmodified::date)::text END AS r_modified
, CASE WHEN s_modified > last.dbmodified THEN '!!!' WHEN s_modified < last.dbmodified THEN '___???' ELSE '' END AS "!!??",
CASE WHEN s_props ->> '3bb2cca2-3b1e-4fb8-82a0-719b49df3799' is DISTINCT FROM last.props ->> '3bb2cca2-3b1e-4fb8-82a0-719b49df3799' THEN --num
 COALESCE(s_props ->> '3bb2cca2-3b1e-4fb8-82a0-719b49df3799', 'NULL') || '<>' || COALESCE(last.props ->> '3bb2cca2-3b1e-4fb8-82a0-719b49df3799', 'NULL') ELSE '=' END AS imo,
CASE WHEN s_props ->> '960b80a6-daf0-4426-984f-091ca72012ee' is DISTINCT FROM last.props ->> '960b80a6-daf0-4426-984f-091ca72012ee' THEN
 COALESCE(s_props ->> '960b80a6-daf0-4426-984f-091ca72012ee', 'NULL') || '<>' || COALESCE(last.props ->> '960b80a6-daf0-4426-984f-091ca72012ee', 'NULL') ELSE '=' END AS mmsi,
CASE WHEN s_props ->> '80d4e6d5-4dbc-4e77-a960-defb3ae7c9b9' is DISTINCT FROM last.props ->> '80d4e6d5-4dbc-4e77-a960-defb3ae7c9b9' THEN
 COALESCE(s_props ->> '80d4e6d5-4dbc-4e77-a960-defb3ae7c9b9', 'NULL') || '<>' || COALESCE(last.props ->> '80d4e6d5-4dbc-4e77-a960-defb3ae7c9b9', 'NULL') ELSE '=' END AS callsign,
CASE WHEN s_props ->> 'b647e04e-2e6f-4de9-a2ba-5c79a076fe7c' is DISTINCT FROM last.props ->> 'b647e04e-2e6f-4de9-a2ba-5c79a076fe7c' THEN
 COALESCE(s_props ->> 'b647e04e-2e6f-4de9-a2ba-5c79a076fe7c', 'NULL') || '<>' || COALESCE(last.props ->> 'b647e04e-2e6f-4de9-a2ba-5c79a076fe7c', 'NULL') ELSE '=' END AS boardNumber,
CASE WHEN s_props ->> '1a97d73d-0bb2-4c55-bca4-19a939fc4127' is DISTINCT FROM last.props ->> '9eb7155b-d4ab-4a03-8673-4284b12e860e' OR last.props ->> '9eb7155b-d4ab-4a03-8673-4284b12e860e' is NULL THEN
 COALESCE(s_props ->> '1a97d73d-0bb2-4c55-bca4-19a939fc4127', 'NULL') || '<>' || COALESCE(last.props ->> '9eb7155b-d4ab-4a03-8673-4284b12e860e', 'NULL') ELSE '=' END AS osmShipCode,
CASE WHEN s_props ->> '63769ca8-39b8-4ab8-b7a1-356cf1515c8d' is DISTINCT FROM last.props ->> '63769ca8-39b8-4ab8-b7a1-356cf1515c8d' THEN
 COALESCE(s_props ->> '63769ca8-39b8-4ab8-b7a1-356cf1515c8d', 'NULL') || '<>' || COALESCE(last.props ->> '63769ca8-39b8-4ab8-b7a1-356cf1515c8d', 'NULL') ELSE '=' END AS length, --num
CASE WHEN s_props ->> '920bf296-c4f9-4ac1-852a-9afaef7bc88f' is DISTINCT FROM last.props ->> '920bf296-c4f9-4ac1-852a-9afaef7bc88f' THEN
 COALESCE(s_props ->> '920bf296-c4f9-4ac1-852a-9afaef7bc88f', 'NULL') || '<>' || COALESCE(last.props ->> '920bf296-c4f9-4ac1-852a-9afaef7bc88f', 'NULL') ELSE '=' END AS width, --num
CASE WHEN s_props ->> 'b7034ce2-4752-4788-8ffd-26fc6720b125' is DISTINCT FROM last.props ->> 'b7034ce2-4752-4788-8ffd-26fc6720b125' THEN
 COALESCE(s_props ->> 'b7034ce2-4752-4788-8ffd-26fc6720b125', 'NULL') || '<>' || COALESCE(last.props ->> 'b7034ce2-4752-4788-8ffd-26fc6720b125', 'NULL') ELSE '=' END AS namerus,
CASE WHEN s_props ->> 'd49562c3-1de5-4209-b9da-df1eca3c1836' is DISTINCT FROM last.props ->> 'd49562c3-1de5-4209-b9da-df1eca3c1836' THEN
 COALESCE(s_props ->> 'd49562c3-1de5-4209-b9da-df1eca3c1836', 'NULL') || '<>' || COALESCE(last.props ->> 'd49562c3-1de5-4209-b9da-df1eca3c1836', 'NULL') ELSE '=' END AS namelat
FROM modified_ships
JOIN node."3217e8c2-4ab5-4d09-89e0-8367bd0de08f" last ON last.foreign_system_id = 'a4a28d9e-2553-4063-980e-3b46d0ec2022' AND last.foreign_code = reg_last
ORDER BY f_code, osmShipCode, imo
*/

CREATE TEMP TABLE IF NOT EXISTS id_name_props(inp_id uuid, inp_name text, f_code text, new_name text, new_props jsonb); DELETE FROM id_name_props;
INSERT INTO id_name_props
---- DROP TABLE IF EXISTS id_name_props
SELECT ship_id, ship_name, f_code,
 COALESCE(last.props ->> 'b7034ce2-4752-4788-8ffd-26fc6720b125', last.props ->> 'd49562c3-1de5-4209-b9da-df1eca3c1836'),
-- props = (props - '1caea3b9-4853-407c-b9b5-d238e16bb59c') ||
   REPLACE('{'
   || CASE WHEN last.props ->> '3bb2cca2-3b1e-4fb8-82a0-719b49df3799' is NOT NULL THEN '"3bb2cca2-3b1e-4fb8-82a0-719b49df3799": ' || (last.props ->> '3bb2cca2-3b1e-4fb8-82a0-719b49df3799') ELSE '' END
   || CASE WHEN last.props ->> '960b80a6-daf0-4426-984f-091ca72012ee' is NOT NULL THEN ', "960b80a6-daf0-4426-984f-091ca72012ee": "' || service.json_masc_simbols(last.props ->> '960b80a6-daf0-4426-984f-091ca72012ee') || '"' ELSE '' END
   || CASE WHEN last.props ->> '80d4e6d5-4dbc-4e77-a960-defb3ae7c9b9' is NOT NULL THEN ', "80d4e6d5-4dbc-4e77-a960-defb3ae7c9b9": "' || service.json_masc_simbols(last.props ->> '80d4e6d5-4dbc-4e77-a960-defb3ae7c9b9') || '"' ELSE '' END
   || CASE WHEN last.props ->> 'b647e04e-2e6f-4de9-a2ba-5c79a076fe7c' is NOT NULL THEN ', "b647e04e-2e6f-4de9-a2ba-5c79a076fe7c": "' || service.json_masc_simbols(last.props ->> 'b647e04e-2e6f-4de9-a2ba-5c79a076fe7c') || '"' ELSE '' END
   || CASE WHEN last.props ->> '63769ca8-39b8-4ab8-b7a1-356cf1515c8d' is NOT NULL THEN ', "63769ca8-39b8-4ab8-b7a1-356cf1515c8d": ' || (last.props ->> '63769ca8-39b8-4ab8-b7a1-356cf1515c8d') ELSE '' END
   || CASE WHEN last.props ->> '920bf296-c4f9-4ac1-852a-9afaef7bc88f' is NOT NULL THEN ', "920bf296-c4f9-4ac1-852a-9afaef7bc88f": ' || (last.props ->> '920bf296-c4f9-4ac1-852a-9afaef7bc88f') ELSE '' END
   || CASE WHEN last.props ->> 'b7034ce2-4752-4788-8ffd-26fc6720b125' is NOT NULL THEN ', "b7034ce2-4752-4788-8ffd-26fc6720b125": "' || service.json_masc_simbols(last.props ->> 'b7034ce2-4752-4788-8ffd-26fc6720b125') || '"' ELSE '' END
   || CASE WHEN last.props ->> 'd49562c3-1de5-4209-b9da-df1eca3c1836' is NOT NULL THEN ', "d49562c3-1de5-4209-b9da-df1eca3c1836": "' || service.json_masc_simbols(last.props ->> 'd49562c3-1de5-4209-b9da-df1eca3c1836') || '"' ELSE '' END
   || '}'
   ,'{,', '{')::jsonb
FROM modified_ships
JOIN node."3217e8c2-4ab5-4d09-89e0-8367bd0de08f" last ON last.foreign_system_id = 'a4a28d9e-2553-4063-980e-3b46d0ec2022' AND last.foreign_code = reg_last;

-- Убираем так же атрибуты Источник данных(1c9eebfb-c75b-4154-94cb-926e0c74a68a),Внешний ОСМ код судна(1caea3b9-4853-407c-b9b5-d238e16bb59c),"Код Российского морского регистра судоходства (РМРС)"(a1420296-2b09-4ab8-a7fa-6e0b864705d8)
--SELECT * FROM id_name_props ORDER BY inp_name
UPDATE node."dac4c525-cbd4-4942-ad89-5aefb6e8a6f9" SET name = new_name, props =
--SELECT foreign_code, name,new_name,service.view_props(
(((((((((((props - '3bb2cca2-3b1e-4fb8-82a0-719b49df3799') - '960b80a6-daf0-4426-984f-091ca72012ee') - '80d4e6d5-4dbc-4e77-a960-defb3ae7c9b9') - 'b647e04e-2e6f-4de9-a2ba-5c79a076fe7c') - '63769ca8-39b8-4ab8-b7a1-356cf1515c8d') - '920bf296-c4f9-4ac1-852a-9afaef7bc88f') - 'b7034ce2-4752-4788-8ffd-26fc6720b125') - 'd49562c3-1de5-4209-b9da-df1eca3c1836')-'1c9eebfb-c75b-4154-94cb-926e0c74a68a')-'1caea3b9-4853-407c-b9b5-d238e16bb59c')-'a1420296-2b09-4ab8-a7fa-6e0b864705d8')
 || new_props
--)
FROM id_name_props
--JOIN  node."dac4c525-cbd4-4942-ad89-5aefb6e8a6f9" ON id = inp_id
--ORDER BY foreign_code
WHERE id = inp_id;
DROP TABLE IF EXISTS stt_m;
DROP TABLE IF EXISTS modified_ships;
DROP TABLE IF EXISTS id_name_props;

