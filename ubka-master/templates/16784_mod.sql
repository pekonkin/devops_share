comment on table sync.constants is 'Version 25.03.2019 16784';

DO $$
declare
  l_cnt integer;
begin
	select count(*) into l_cnt from information_schema.columns
	where table_schema = 'sync'
	and table_name = 'sync_log'
	and column_name = 'duration';

	if l_cnt = 0 then
		execute 'ALTER TABLE sync.sync_log RENAME err_message  TO description';
		execute 'ALTER TABLE sync.sync_log ADD COLUMN duration interval';
		execute 'DROP FUNCTION IF EXISTS sync.func_add_sync_tree_aud(text, text, uuid)';
		insert into sync.constants (key, value) values ('validate_limit_row', '1000');
	end if;
END $$;

CREATE OR REPLACE FUNCTION sync.func_sync_out(
    p_node_id integer DEFAULT NULL::integer,
    p_limit integer DEFAULT NULL::integer,
    p_validate boolean DEFAULT NULL::boolean,
    p_resync boolean DEFAULT NULL::boolean)
  RETURNS SETOF sync.sync_return_table AS
$BODY$
DECLARE
  l_rec_node record;
  l_rec_sync record;
  l_rec_tree record;
  l_tree record;
  l_out_column record;
  l_sql text;
  l_cnt integer;
  l_group_sql text;
  l_order_num integer;
  l_status_id integer;
  l_pr_key_name text;
  l_result boolean;
  l_text_msg_err text;
  l_text_detail_err text;
  l_text_exc_context text;
BEGIN
	create temporary table out_sync (id bigint, schema_name text, table_name text, row_id uuid, json_data json, reglament_id bigint, out_node_order json, is_create_tree boolean, lim integer, status_id integer, create_date timestamp without time zone) on commit drop;
	create temporary table column_sync (schema_name text, table_name text, column_name text) on commit drop;
	create temporary table info_sync (sync_out_id bigint, node_id integer, create_date timestamp without time zone) on commit drop;
	create temporary table del_info_sync (sync_out_id bigint) on commit drop;
	create temporary table aud_sync (schema_name text, table_name text, row_id uuid) on commit drop;

	for l_rec_node in select id, node_bit, limit_rows, is_closed from sync.sync_node
			where coalesce(p_node_id, id) = id
		loop

		l_order_num := 1;

		for l_rec_sync in
				select s.id, s.schema_name, s.table_name, s.row_id, s.json_data, null as reglament_id, null as out_node_order, null as is_create_tree,
				coalesce(p_limit, sync.func_get_constant('limit_send_sync_out')::integer) as lim, 1 as status_id, s.create_date
				from sync.sync_out s
				inner join sync.sync_out_info si on s.id = si.sync_out_id and si.node_id = l_rec_node.id
				where s.schema_name in (sync.func_get_constant('transport_complited'), sync.func_get_constant('transport_tree'))
				and s.table_name = sync.func_get_constant('direct_back')
				union all
				select id, schema_name, table_name, row_id, json_data, reglament_id, out_node_order,
				case when schema_name = 'rel' then false else is_create_tree end,
				lim,
				case when schema_name = 'node' and table_name = any(sync.func_get_constant('ship_resources')::text[]) then 2 else 1 end as status_id,
				create_date
				from (
					select s.id, s.schema_name, s.table_name, s.row_id, s.json_data, r.id as reglament_id, r.out_node_order, r.is_create_tree,
					coalesce(coalesce(p_limit, l_rec_node.limit_rows), sync.func_get_constant('limit_send_sync_out')::integer) as lim, s.create_date
					from sync.sync_out s
					inner join sync.sync_out_info si on s.id = si.sync_out_id and si.node_id = l_rec_node.id
					left join sync.sync_reglament r on s.schema_name in ('rel', r.schema_name) and s.table_name = r.table_name
					where (si.status_id = 1 or coalesce(p_resync, false) is true)
					and s.is_validate = true
					order by coalesce(si.order_num, 1), s.create_date
					limit coalesce(coalesce(p_limit, l_rec_node.limit_rows), sync.func_get_constant('limit_send_sync_out')::integer)
				) as sel
			loop
				insert into out_sync (id, schema_name, table_name, row_id, json_data, reglament_id, out_node_order, is_create_tree, lim, status_id, create_date)
				values (l_rec_sync.id, l_rec_sync.schema_name, l_rec_sync.table_name, l_rec_sync.row_id, l_rec_sync.json_data, l_rec_sync.reglament_id, l_rec_sync.out_node_order, l_rec_sync.is_create_tree, l_rec_sync.lim, l_rec_sync.status_id, l_rec_sync.create_date);
		end loop;

		insert into column_sync (schema_name, table_name, column_name)
		select sel.schema_name, sel.table_name, c.column_name
		from information_schema.columns c
		inner join (select distinct m.table_schema as schema_name, m.table_name from out_sync os
				inner join sync.sync_tree_all_mv m on m.reglament_id = os.reglament_id
				where coalesce(os.is_create_tree, false) is true) sel on sel.schema_name = c.table_schema and sel.table_name = c.table_name;

		for l_rec_sync in select id, schema_name, table_name, row_id, json_data, reglament_id, out_node_order, is_create_tree, lim, status_id, create_date from out_sync
		loop
			if l_order_num > l_rec_sync.lim then
				exit;
			end if;

			return next sync.func_sync_add_return_table(
				p_id := l_rec_sync.id,
				p_schema_name := l_rec_sync.schema_name,
				p_table_name := l_rec_sync.table_name,
				p_row_id := l_rec_sync.row_id,
				p_json_data := l_rec_sync.json_data,
				p_node_id := l_rec_node.id,
				p_status_id := l_rec_sync.status_id,
				p_row_info := ('{'||case when l_rec_sync.reglament_id is not null then '"reglament_id" : "'||l_rec_sync.reglament_id||'", ' else '' end||'"row_id" : "'||l_rec_sync.row_id||'"}')::json,
				p_order_num := case when l_rec_sync.schema_name = 'rel' and coalesce((l_rec_sync.json_data->0->>sync.func_get_constant('delete_data'))::boolean, false) is true then 0 else l_order_num end
			);

			l_order_num := l_order_num + 1;

			if coalesce(l_rec_sync.is_create_tree, false) is true then
				for l_tree in select table_schema, table_name, tree from sync.sync_tree_all_mv
						where reglament_id = l_rec_sync.reglament_id
						group by table_schema, table_name, tree, order_num
						order by order_num
				loop
					l_sql := 'select json_agg(sel) as rjson from (select ';
					l_group_sql := '';
					for l_out_column in select column_name from column_sync
								where schema_name = l_tree.table_schema
								and table_name = l_tree.table_name
								order by column_name loop

								l_sql := l_sql||'out."'||l_out_column.column_name||'",';
								l_group_sql := l_group_sql||'sel."'||l_out_column.column_name||'",';
					end loop;

					l_sql := substring(l_sql from 1 for length(l_sql)-1)||' from '||l_rec_sync.schema_name||'."'||l_rec_sync.table_name||'" src '||l_tree.tree||' where src.id = '''||l_rec_sync.row_id||''') as sel group by '||substring(l_group_sql from 1 for length(l_group_sql)-1)||';';

					for l_rec_tree in execute l_sql loop
						if l_rec_tree.rjson is not null
						then
							select count(*) into l_cnt from sync.sync_tree_aud
							where schema_name = l_tree.table_schema
							and table_name = l_tree.table_name
							and row_id = (l_rec_tree.rjson->0->>'id')::uuid
							and get_bit(out_node_mask, l_rec_node.node_bit) = 1;

							if l_cnt = 0 then
								if l_tree.table_schema = any(sync.func_get_constant('allow_schema_audit_array')::text[]) then
									insert into aud_sync (schema_name, table_name, row_id)
									values (l_tree.table_schema, l_tree.table_name, (l_rec_tree.rjson->0->>'id')::uuid);
								end if;

								select case when
									l_tree.table_name = any(sync.func_get_constant('ship_resources')::text[]) or
									(l_tree.table_schema = 'rel' and l_rec_tree.rjson->0->>'attribute_id' = any(sync.func_get_constant('ship_tables')::text[]))
									then 2 else 1 end into l_status_id;

								return next sync.func_sync_add_return_table(
									p_schema_name := l_tree.table_schema,
									p_table_name := l_tree.table_name,
									p_row_id := (l_rec_tree.rjson->0->>'id')::uuid,
									p_json_data := l_rec_tree.rjson,
									p_node_id := l_rec_node.id,
									p_status_id := l_status_id,
									p_row_info := ('{'||case when l_rec_sync.reglament_id is not null then '"reglament_id" : "'||l_rec_sync.reglament_id||'", ' else '' end||'"row_id" : "'||l_rec_sync.row_id||'"}')::json,
									p_order_num := l_order_num
								);
								l_order_num := l_order_num + 1;
							end if;
						end if;
					end loop;
				end loop;

			end if;

			if l_rec_sync.schema_name in (sync.func_get_constant('transport_complited'), sync.func_get_constant('transport_tree'), sync.func_get_constant('transport_error'))
			and l_rec_sync.table_name = sync.func_get_constant('direct_back') then
				insert into del_info_sync (sync_out_id) values (l_rec_sync.id);
			else
				insert into info_sync (sync_out_id, node_id, create_date) values (l_rec_sync.id, l_rec_node.id, l_rec_sync.create_date);
			end if;
		end loop;
	end loop;

	update sync.sync_out_info soi set status_id = 2
	from info_sync as i
	where soi.sync_out_id = i.sync_out_id
	and soi.node_id = i.node_id;

	delete from sync.sync_out_info
	where sync_out_id in (select dis.sync_out_id from del_info_sync dis);

	insert into sync.sync_tree_aud(id,schema_name,table_name,row_id,out_node_mask)
	select nextval('sync.sync_tree_aud_id_seq'), sel.schema_name, sel.table_name, sel.row_id, 0::bit(128)
	from (select distinct schema_name, table_name, row_id from aud_sync
		except
		select schema_name, table_name, row_id from sync.sync_tree_aud) sel;

	if sync.func_check_is_loging(p_object_name := 'func_sync_out') is true then
		insert into sync.sync_log(id, description, duration, create_date)
		values (nextval('sync.sync_log_id_seq'), 'func_sync_out(p_node_id := '||coalesce(p_node_id::text, 'null')||',p_limit := '||coalesce(p_limit::text, 'null')||',p_validate := '||coalesce(p_validate::text, 'null')||',p_resync := '||coalesce(p_resync::text, 'null')||'): rows '||l_order_num-1, clock_timestamp()::time(2)-now()::time(2), now());
	end if;

 	EXCEPTION WHEN OTHERS THEN
		GET STACKED DIAGNOSTICS
			l_text_msg_err := MESSAGE_TEXT,
                        l_text_detail_err := PG_EXCEPTION_DETAIL,
                        l_text_exc_context := PG_EXCEPTION_CONTEXT;

		return next sync.func_sync_add_return_table(
			p_id := -1,
			p_schema_name := 'Error',
			p_table_name := l_text_msg_err||' - '||l_text_detail_err||' - '||l_text_exc_context
		);

		if sync.func_check_is_loging(p_object_name := 'func_sync_out') is true then

		insert into sync.sync_log(id, description, duration, create_date)
		values (nextval('sync.sync_log_id_seq'), 'func_sync_out(p_node_id := '||coalesce(p_node_id::text, 'null')||',p_limit := '||coalesce(p_limit::text, 'null')||',p_validate := '||coalesce(p_validate::text, 'null')||',p_resync := '||coalesce(p_resync::text, 'null')||'): rows '||l_order_num-1||'; error - '||l_text_msg_err||' - '||l_text_detail_err||' - '||l_text_exc_context, clock_timestamp()::time(2)-now()::time(2), now());
	end if;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100
  ROWS 1000;

CREATE OR REPLACE FUNCTION sync.func_sync_in(p_node_id integer DEFAULT NULL::integer)
  RETURNS boolean AS
$BODY$
DECLARE
  l_rec record;
  l_cnt integer := 0;
  l_result boolean;
  l_text_msg_err text;
  l_text_detail_err text;
  l_text_exc_context text;
BEGIN
	insert into sync.sync_in_log(id, sync_in_id, out_id, node_id, schema_name, table_name, row_id, json_data, create_date, err_message_text)
	select nextval('sync.sync_in_log_id_seq'), id, out_id, node_id, schema_name, table_name, row_id, json_data, now(), 'node_id not found in table synd_node'
	from sync.sync_in
	where node_id not in (select id from sync.sync_node);

	delete from sync.sync_in where node_id not in (select id from sync.sync_node);

	select * from sync.func_check_ship_existence() into l_result;

	create temporary table column_sync (table_schema text, table_name text, column_name text, data_type text, is_nullable boolean, trg_table text, trg_column text, is_pk boolean) on commit drop;
	create temporary table notify_sync (node_id integer, node_bit integer, in_id bigint, out_id bigint, schema_name text, table_name text, row_id uuid, status_id integer) on commit drop;
	create temporary table unique_rel (table_name text) on commit drop;
	create temporary table unique_attribute (attribute_id uuid) on commit drop;
	create temporary table import_tbl (id bigint, schema_name text, table_name text) on commit drop;

	insert into unique_rel
	select distinct resource_id from metamodel.resource_attributes ra inner join metamodel.attribute a on ra.attribute_id = a.id and a.unique_relation = true;

	insert into unique_attribute
	select id from metamodel.attribute where unique_relation = true;

	insert into import_tbl select s.id, s.schema_name, s.table_name from sync.sync_in s
	where coalesce(p_node_id, node_id) = node_id
	and s.status_id in (1, 2)
	order by package_id, order_num
	limit sync.func_get_constant('func_sync_in_limit')::integer;

	for l_rec in select distinct schema_name, table_name from import_tbl
	loop
		insert into column_sync (table_schema, table_name, column_name, data_type, is_nullable, trg_table, trg_column, is_pk)
		select	c.table_schema,
			c.table_name,
			c.column_name,
			case when c.data_type = 'USER-DEFINED' then c.udt_name else c.data_type end||case when c.character_maximum_length is not null then '('||c.character_maximum_length||')' else '' end as data_type,
			case when c.is_nullable = 'YES' then true else false end,
			sel.trg_table,
			sel.trg_column,
			case when sel3.column_name is not null then true else false end
		from information_schema.columns c
		left join (select sel2.conrelid::regclass as src_table, src_attr.attname as src_column,sel2.confrelid::regclass as trg_table, trg_attr.attname as trg_column
					from (select conrelid, confrelid, conkey[sel1.i] conkey, confkey[sel1.i] as confkey from
						(select conrelid, confrelid, conkey, confkey, generate_series(1, array_upper(conkey, 1)) as i
      						from pg_constraint
      						where contype = 'f'
  						and conrelid::regclass = (l_rec.schema_name||'."'||l_rec.table_name||'"')::regclass
						) sel1
					) sel2
					inner join pg_attribute trg_attr on trg_attr.attnum = sel2.confkey and trg_attr.attrelid = sel2.confrelid
					inner join pg_attribute src_attr on src_attr.attnum = sel2.conkey and src_attr.attrelid = sel2.conrelid
			) sel on c.column_name = sel.src_column
		left join (select kcu.column_name from information_schema.tables t
				inner join information_schema.table_constraints tc on tc.table_catalog = t.table_catalog and tc.table_schema = t.table_schema and tc.table_name = t.table_name and tc.constraint_type = 'PRIMARY KEY'
				inner join information_schema.key_column_usage kcu on kcu.table_catalog = tc.table_catalog and kcu.table_schema = tc.table_schema and kcu.table_name = tc.table_name and kcu.constraint_name = tc.constraint_name
			where t.table_schema = l_rec.schema_name
			and t.table_name = l_rec.table_name)sel3 on sel3.column_name = c.column_name
		where c.table_schema = l_rec.schema_name
		and c.table_name = l_rec.table_name;
	end loop;

	for l_rec in select id from import_tbl
	loop
		select sync.func_sync_in_row(p_id := l_rec.id) into l_result;

		l_cnt := l_cnt + 1;
	end loop;

	if l_cnt > 0 then
		select sync.func_notify_sync() into l_result;

		if sync.func_check_is_loging(p_object_name := 'func_sync_in') is true then
			insert into sync.sync_log(id, description, duration, create_date)
			values (nextval('sync.sync_log_id_seq'), 'func_sync_in(p_node_id := '||coalesce(p_node_id::text, 'null')||'): rows '||l_cnt, clock_timestamp()::time(2)-now()::time(2), now());
		end if;
	end if;

	if l_cnt = sync.func_get_constant('func_sync_in_limit')::integer then
		return true;
	else
		return false;
	end if;

	EXCEPTION WHEN OTHERS THEN
		GET STACKED DIAGNOSTICS
			l_text_msg_err := MESSAGE_TEXT,
                        l_text_detail_err := PG_EXCEPTION_DETAIL,
                        l_text_exc_context := PG_EXCEPTION_CONTEXT;

		insert into sync.sync_log(id, description, duration, create_date)
		values (nextval('sync.sync_log_id_seq'), 'func_sync_in(p_node_id := '||coalesce(p_node_id::text, 'null')||'): Exception: '||l_text_msg_err||' '||l_text_detail_err||' '||l_text_exc_context||' rows '||l_cnt, clock_timestamp()::time(2)-now()::time(2), now());

		return false;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

CREATE OR REPLACE FUNCTION sync.func_validate_sync()
  RETURNS boolean AS
$BODY$
DECLARE
  l_rec_sync record;
  l_rec_order record;
  l_cnt integer;
  l_schema_name text;
  l_where text;
  l_id text;
  l_sql text;
BEGIN
	create temporary table notify_sync (node_id integer, node_bit integer, in_id bigint, out_id bigint, schema_name text, table_name text, row_id uuid) on commit drop;

	for l_rec_sync in select (so.json_data->0->>'reglament_array')::bigint[] as reglament_array from sync.sync_out so
				where so.schema_name = 'rel'
				and so.json_data->0->>'reglament_array' is not null
				and so.is_validate = false
				group by so.json_data->0->>'reglament_array'
	loop
		for l_rec_order in select sn.id as node_id, min(sel.jo->>1) as order_num from (select json_array_elements(out_node_order) as jo
					from sync.sync_reglament sr
					where id = any(l_rec_sync.reglament_array)) sel
					inner join sync.sync_node sn on (sel.jo->>0)::integer = sn.node_bit
					group by sn.id loop
			update sync.sync_out_info set order_num = (l_rec_order.order_num)::integer - 0.5
			where sync_out_id in (select id from sync.sync_out so
					where so.schema_name = 'rel'
					and so.json_data->0->>'reglament_array' = l_rec_sync.reglament_array::text
					and so.is_validate = false)
			and node_id = l_rec_order.node_id::integer;
		end loop;
	end loop;

	for l_rec_sync in select so.id, so.schema_name, so.table_name, so.row_id, so.json_data, sr.id as reglament_id, snr.id as node_id,
			snr.node_bit, sr.out_node_where, snr.node_bit
			from sync.sync_out so
			inner join sync.sync_out_info si on si.sync_out_id = so.id
			inner join sync.sync_node snr on si.node_id = snr.id
			left join sync.sync_reglament sr on sr.schema_name = case when so.schema_name = 'rel' then 'node' else so.schema_name end and sr.table_name = so.table_name
			where so.is_validate = false
			limit sync.func_get_constant('validate_limit_row')::integer
	loop
		if l_rec_sync.out_node_where is not null then
			for i in 0..json_array_length(l_rec_sync.out_node_where)-1 loop
				for j in 0..json_array_length((l_rec_sync.out_node_where->i->>'node_bit')::json)-1 loop
					if l_rec_sync.node_bit = ((l_rec_sync.out_node_where->i->>'node_bit')::json->>j)::integer then
						if l_rec_sync.schema_name = 'rel' then
							l_schema_name := 'node';

							select 'select ''''''''||id||'''''''' from node."'||l_rec_sync.table_name||'" where '||st.from_column_name||' = '''||(l_rec_sync.json_data->0->>st.to_column_name)::text||'''',
							l_rec_sync.json_data->0->>st.to_column_name
							into l_sql, l_id
							from sync.sync_tree st
							where st.from_schema_name = 'node'
							and st.from_table_name = l_rec_sync.table_name
							and st.to_schema_name = l_rec_sync.schema_name
							and st.to_table_name = l_rec_sync.table_name
							and st.reglament_id = l_rec_sync.reglament_id
							limit 1;

							if l_sql is not null then
								execute l_sql into l_id;
							end if;

							if l_sql is null or l_id is null then
								l_id = 'null';
							end if;
						else
							l_schema_name := l_rec_sync.schema_name;
							l_id := ''''||l_rec_sync.row_id||'''';
						end if;

						l_where := (l_rec_sync.out_node_where->i->>'where')::text;
						l_where := replace(l_where, '<l_rec_sync.node_id>', l_rec_sync.node_id::text);

						execute 'select count(*) from '||l_schema_name||'."'||l_rec_sync.table_name||'" src where src.id = '||l_id||' and '||l_where into l_cnt;

						if l_cnt = 0 then
							delete from sync.sync_out_info where sync_out_id = l_rec_sync.id and node_id = l_rec_sync.node_id;
						end if;
					end if;
				end loop;
			end loop;
		end if;

		if l_rec_sync.id is not null then
			update sync.sync_out_info so set order_num = (select (sr.out_node_order->l_rec_sync.node_bit->>1)::real from sync.sync_reglament sr where sr.table_name = l_rec_sync.table_name)
			where sync_out_id = l_rec_sync.id
			and order_num is null
			and node_id = l_rec_sync.node_id;

			update sync.sync_out set is_validate = true
			where id = l_rec_sync.id;
		end if;
	end loop;

	for l_rec_sync in select id, node_bit, is_closed from sync.sync_node
	loop
		delete from sync.sync_out_info
		where node_id = l_rec_sync.id
		and l_rec_sync.is_closed is true
		and sync_out_id in (select so.id from sync.sync_out so
			where now() - so.create_date > interval '1 sec' * sync.func_get_constant('out_time_is_closed_sync_out')::integer);



		delete from sync.sync_out so
		where not exists (select 1 from sync.sync_out_info si where si.sync_out_id = so.id)
		or (now() - so.create_date > interval '1 sec' * (select coalesce(sr.out_time_out_sec, 99999999)::integer
									from sync.sync_reglament sr
									where sr.schema_name = so.schema_name
									and sr.table_name = so.table_name
														  )
		);
	end loop;

	return true;

  	EXCEPTION WHEN OTHERS THEN
		return false;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;