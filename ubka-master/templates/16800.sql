

insert into sync.constants (key, value) values ('validate_limit_row', '1000');

CREATE OR REPLACE FUNCTION sync.func_validate_sync()
  RETURNS boolean AS
$BODY$
DECLARE
  l_rec_sync record;
  l_rec_order record;
  l_cnt integer;
  l_schema_name text;
  l_where text;
  l_id text;
  l_sql text;
BEGIN
	create temporary table notify_sync (node_id integer, node_bit integer, in_id bigint, out_id bigint, schema_name text, table_name text, row_id uuid) on commit drop;

	for l_rec_sync in select (so.json_data->0->>'reglament_array')::bigint[] as reglament_array from sync.sync_out so
				where so.schema_name = 'rel'
				and so.json_data->0->>'reglament_array' is not null
				and so.is_validate = false
				group by so.json_data->0->>'reglament_array'
	loop
		for l_rec_order in select sn.id as node_id, min(sel.jo->>1) as order_num from (select json_array_elements(out_node_order) as jo
					from sync.sync_reglament sr
					where id = any(l_rec_sync.reglament_array)) sel
					inner join sync.sync_node sn on (sel.jo->>0)::integer = sn.node_bit
					group by sn.id loop
			update sync.sync_out_info set order_num = (l_rec_order.order_num)::integer - 0.5
			where sync_out_id in (select id from sync.sync_out so
					where so.schema_name = 'rel'
					and so.json_data->0->>'reglament_array' = l_rec_sync.reglament_array::text
					and so.is_validate = false)
			and node_id = l_rec_order.node_id::integer;
		end loop;
	end loop;

	for l_rec_sync in select so.id, so.schema_name, so.table_name, so.row_id, so.json_data, sr.id as reglament_id, snr.id as node_id,
			snr.node_bit, sr.out_node_where, snr.node_bit
			from sync.sync_out so
			inner join sync.sync_out_info si on si.sync_out_id = so.id
			inner join sync.sync_node snr on si.node_id = snr.id
			left join sync.sync_reglament sr on sr.schema_name = case when so.schema_name = 'rel' then 'node' else so.schema_name end and sr.table_name = so.table_name
			where so.is_validate = false
			limit sync.func_get_constant('validate_limit_row')::integer
	loop
		if l_rec_sync.out_node_where is not null then
			for i in 0..json_array_length(l_rec_sync.out_node_where)-1 loop
				for j in 0..json_array_length((l_rec_sync.out_node_where->i->>'node_bit')::json)-1 loop
					if l_rec_sync.node_bit = ((l_rec_sync.out_node_where->i->>'node_bit')::json->>j)::integer then
						if l_rec_sync.schema_name = 'rel' then
							l_schema_name := 'node';

							select 'select ''''''''||id||'''''''' from node."'||l_rec_sync.table_name||'" where '||st.from_column_name||' = '''||(l_rec_sync.json_data->0->>st.to_column_name)::text||'''',
							l_rec_sync.json_data->0->>st.to_column_name
							into l_sql, l_id
							from sync.sync_tree st
							where st.from_schema_name = 'node'
							and st.from_table_name = l_rec_sync.table_name
							and st.to_schema_name = l_rec_sync.schema_name
							and st.to_table_name = l_rec_sync.table_name
							and st.reglament_id = l_rec_sync.reglament_id
							limit 1;

							if l_sql is not null then
								execute l_sql into l_id;
							end if;

							if l_sql is null or l_id is null then
								l_id = 'null';
							end if;
						else
							l_schema_name := l_rec_sync.schema_name;
							l_id := ''''||l_rec_sync.row_id||'''';
						end if;

						l_where := (l_rec_sync.out_node_where->i->>'where')::text;
						l_where := replace(l_where, '<l_rec_sync.node_id>', l_rec_sync.node_id::text);

						execute 'select count(*) from '||l_schema_name||'."'||l_rec_sync.table_name||'" src where src.id = '||l_id||' and '||l_where into l_cnt;

						if l_cnt = 0 then
							delete from sync.sync_out_info where sync_out_id = l_rec_sync.id and node_id = l_rec_sync.node_id;
						end if;
					end if;
				end loop;
			end loop;
		end if;

		if l_rec_sync.id is not null then
			update sync.sync_out_info so set order_num = (select (sr.out_node_order->l_rec_sync.node_bit->>1)::real from sync.sync_reglament sr where sr.table_name = l_rec_sync.table_name)
			where sync_out_id = l_rec_sync.id
			and order_num is null
			and node_id = l_rec_sync.node_id;

			update sync.sync_out set is_validate = true
			where id = l_rec_sync.id;
		end if;
	end loop;

	for l_rec_sync in select id, node_bit, is_closed from sync.sync_node
	loop
		delete from sync.sync_out_info
		where node_id = l_rec_sync.id
		and l_rec_sync.is_closed is true
		and sync_out_id in (select so.id from sync.sync_out so
			where now() - so.create_date > interval '1 sec' * sync.func_get_constant('out_time_is_closed_sync_out')::integer);



		delete from sync.sync_out so
		where not exists (select 1 from sync.sync_out_info si where si.sync_out_id = so.id)
		or (now() - so.create_date > interval '1 sec' * (select coalesce(sr.out_time_out_sec, 99999999)::integer
									from sync.sync_reglament sr
									where sr.schema_name = so.schema_name
									and sr.table_name = so.table_name
														  )
		);
	end loop;

	return true;

  	EXCEPTION WHEN OTHERS THEN
		return false;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;