#!/bin/bash

SERVICE='/etc/init.d/postgresql-9.5'

if [ -e $SERVICE ]; then

    if [ $( $SERVICE status | grep -c '\(pid.*[0-9]\)' ) -eq 1 ]; then

        if [ $( psql -U postgres -t -l | awk -F \| {'print$1'} | egrep -c ^"[[:space:]]astk[[:space:]]" ) -ge 1 ]; then

            if [ $( /etc/init.d/crond status | egrep -c '\(pid.*[0-9]\)' ) -eq 1 ]; then
                /etc/init.d/crond stop
                STOPPED=1
            fi

            if [ -d /etc/sysconfig/starter ]; then

                if [ -e /etc/sysconfig/starter/firestarter_check.sh ]; then
                    if [ $( ps aux | grep -c firestarter_check.sh ) -ge 1 ]; then
                        ps aux | grep firestarter_check.sh | grep -v grep | awk '{print$2}' | xargs -I {} kill -9 {}
                    fi
                fi

            fi

            if [ -e /etc/init.d/FireStarter ]; then
                /etc/init.d/FireStarter stop
                RESULT=1
            fi

            psql -U postgres -d postgres -c "select pg_terminate_backend(pid) from pg_stat_activity where datname ='astk';"
            psql -U postgres -d astk -c 'TRUNCATE srv_targetinfo4arm CASCADE;' > /dev/null 2>&1

            logger "START | REINDEX DATABASE astk сomplited"
            psql -U postgres -d astk -c 'REINDEX DATABASE astk;' > /dev/null 2>&1
            logger "END | REINDEX DATABASE astk сomplited"

            logger "START | VACUUM and ANALYZE for astk database"
            psql -U postgres -d astk -c 'VACUUM (FULL, ANALYZE);' > /dev/null 2>&1
            logger "END | VACUUM and ANALYZE complited for astk database"

        fi

        if [ $RESULT -eq 1 ]; then
            /etc/init.d/FireStarter start
        fi

        if [ $STOPPED -eq 1 ]; then
            /etc/init.d/crond start
        fi

    fi

fi
