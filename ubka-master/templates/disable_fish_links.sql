update ui.dom_element
set props = jsonb_set(props, '{oldParentId}', to_jsonb(parent_id)), parent_id = null
where name = 'Журнал гидрометеорологической обстановки (ЕСИМО)'
      and parent_id in (select id from ui.dom_element where parent_id in (select id from ui.dom_element where parent_id = (select id from ui.dom_element where label = 'zkMenu')));

update ui.dom_element
set props = jsonb_set(props, '{oldParentId}', to_jsonb(parent_id :: text)), parent_id = null
where parent_id in (select id
                    from ui.dom_element
                    where name = 'Взаимодействие' and parent_id = (select id
                                                                   from ui.dom_element
                                                                   where label = 'zkMenu')) and
      name in ('Организационная структура ФОИВ', 'Типовые планы реализации событий', 'Планы взаимодействия');

update ui.dom_element
set props = jsonb_set(props, '{oldParentId}', to_jsonb(parent_id :: text)), parent_id = null
where name = 'Промысловая обстановка'
      and parent_id in (select id
                        from ui.dom_element
                        where parent_id = (select id
                                           from ui.dom_element
                                           where label = 'zkMenu'));

update ui.dom_element
set props = jsonb_set(props, '{oldParentId}', to_jsonb(parent_id)), parent_id = null
where name = 'Журнал КПМ'
      and parent_id in (select id
                        from ui.dom_element
                        where parent_id in (select id
                                            from ui.dom_element
                                            where parent_id = (select id
                                                               from ui.dom_element
                                                               where label = 'zkMenu')));

update ui.dom_element
set props = jsonb_set(props, '{oldParentId}', to_jsonb(parent_id :: text)), parent_id = null
where parent_id = (select id
                   from ui.dom_element
                   where parent_id = (select id
                                      from ui.dom_element
                                      where label = 'vsMenu') and name = 'Промысл. обстановка') and
      name = 'Анализ ущерба и результативности КПМ';

update ui.dom_element
set props = '{
  "tabs": [
    {
      "widget": "ZKVSabovewaterGrid"
    },
    {
      "widget": "smallShipSituationJournalZK"
    }
  ],
  "version": 7
}'
where label = 'abovewaterTabsZK';