update ui.dom_element
set props     = jsonb_set(props, '{oldParentId}', to_jsonb(parent_id :: text)),
    parent_id = null
where name = 'Взаимодействие'
  and parent_id = (select id
                   from ui.dom_element
                   where label = 'zkMenu');
