#!/bin/bash

if [ $(ps aux | egrep $0 | grep -v grep | wc -l ) -gt 2 ]; then
   exit 1
fi

for RPMPACK in gzip strace; do
    if ! rpm -qa ${RPMPACK} 2>&1 >/dev/null
    then
        yum -y install ${RPMPACK}; 2>&1 >/dev/null
    fi
done

LANG=C
SERVICE='/etc/init.d/FireStarter'


if [ -e $SERVICE ]; then

while :
do

FST=`basename $SERVICE`
PID="/var/run/$FST.pid"
DIR_LOG="/var/log"
LOG="FireStarter-counter.log"
TCPPORT='{{SDNOPORT}}'
URL='{{SDNOLINKS}}'
IPADDR='{{SDNOIP}}'

RESULT=`pgrep ${FST}`

function STARTFIRESTARTER
{
        ${SERVICE} start
        sleep 5s
        continue
}

function KILLEMALL
{
        RESULT=`pgrep ${FST}`

        # If PID not found, clean PID file and try start funtion STARTFIRESTARTER
        if [ "${RESULT:-zero}" = zero ]; then

            if [ -e ${PID} ]; then
                > ${PID}
            fi

            continue
        fi

        # If find FireStarter kill service and STARTFIRESTARTER
        if [ $( ps aux | grep ${FST} | egrep -v "grep|$LOG|$SERVICE|vi|tail|less|cat|tac|more|mc|service" | wc -l ) -ge 1 ]; then
           ps aux | grep ${FST} | egrep -v "grep|$LOG|$SERVICE|vi|tail|less|cat|tac|more|mc|service" | awk {'print$2'} | xargs -I {} kill -9 {}

            if [ -e ${PID} ]; then
               > ${PID}
            fi
        fi

        sleep 2s;
        STARTFIRESTARTER
}

    # Check service, if RESULT = zero, need start
    if [ "${RESULT:-zero}" = zero ]; then
        echo "`date +%c` -- Service has been starting" >> $DIR_LOG/$LOG 2>&1
        STARTFIRESTARTER
    else
        # If service have two or several runing PID, service need killed
        if [ `ps aux | grep ${FST} | egrep -v "grep|$LOG|$SERVICE" | wc -l` -ge 2 ]; then
            echo "`date +%c` -- Need kill -> Run several services" >> $DIR_LOG/$LOG 2>&1
            KILLEMALL
        fi
    fi

    # If PID not found need kill
    if [ ! -e ${PID} ]; then
        echo "`date +%c` -- ${PID} not found, restart service" >> $DIR_LOG/$LOG 2>&1
        KILLEMALL
    fi

    # If found tcp ${TCPPORT}
    if netstat -anlpt | grep ":${TCPPORT}" > /dev/null 2>&1;
        then
            # Check port, if port up, go to next check
            if nc -vz ${IPADDR} ${TCPPORT} > /dev/null 2>&1;
                then

                    echo "`date +%c` -- NETCAT | TCP PORT ${TCPPORT} -> OPEN" >> $DIR_LOG/$LOG 2>&1

                    if curl http://${IPADDR}:${TCPPORT}${URL} |& grep 'Connection refused' > /dev/null 2>&1
                       then
                         echo "`date +%c` -- Connection refused -> http://${IPADDR}:${TCPPORT}${URL}" >> $DIR_LOG/$LOG 2>&1
                         KILLEMALL
                    fi

                    if curl http://${IPADDR}:${TCPPORT}${URL} |& grep "Couldn\'t connect to host" > /dev/null 2>&1
                       then
                         echo "`date +%c` -- Couldn\'t connect to host -> http://${IPADDR}:${TCPPORT}${URL}" >> $DIR_LOG/$LOG 2>&1
                         KILLEMALL
                    fi

                    if [ $( curl http://${IPADDR}:${TCPPORT}${URL} |& egrep -c 'totalcount":[0-9]{1,100}' ) -eq 0 ] > /dev/null 2>&1
                       then
                         echo "`date +%c` -- Can\`t get result for query -> Give me totalcont for this request" >> $DIR_LOG/$LOG 2>&1
                         KILLEMALL
                    fi


                        TOTALCOUNT=`curl http://${IPADDR}:${TCPPORT}${URL} |& egrep -o 'totalcount":[0-9]{1,100}' | awk -F \: {'print$2'}`

                        if [ "${TOTALCOUNT:-zero}" = zero ]; then
                              echo "Cant get resule -> result is ${TOTALCOUNT:-zero}" >> $DIR_LOG/$LOG 2>&1
                              TOTALCOUNT=0
                              echo "`date +%c` -- GET query for server ${IPADDR}:${TCPPORT} totalcount - ${TOTALCOUNT}" >> $DIR_LOG/$LOG 2>&1
                        else
                              echo "`date +%c` -- GET query for server ${IPADDR}:${TCPPORT} totalcount - ${TOTALCOUNT}" >> $DIR_LOG/$LOG 2>&1
                        fi

         else

            echo "`date +%c` -- NETCAT | TCP PORT ${TCPPORT} -> UNREACH" >> $DIR_LOG/$LOG 2>&1
            KILLEMALL

      fi

   else
      echo "`date +%c` -- TCP PORT ${TCPPORT} -> NOT LISTEN" >> $DIR_LOG/$LOG 2>&1
      KILLEMALL

fi

sleep 5s
done
fi
