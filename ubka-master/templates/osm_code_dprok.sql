update ui.dom_element
set props =
      '
      {
        "szi": {
          "map": "portal.journalaccounfishinggridzkvs.map",
          "edit": false,
          "view": "portal.journalaccounfishinggridzkvs.view",
          "print": "portal.journalaccounfishinggridzkvs.print",
          "create": false,
          "remove": false
        },
       "where": {
    "args": [
      {
        "args": [
          {
            "path": "mmsi",
            "label": "attribute"
          },
          {
            "ui": true,
            "label": "value",
            "uiTitle": "MMSI",
            "controlElement": {
              "feature": "string"
            }
          }
        ],
        "label": "like"
      },
      {
        "args": [
          {
            "path": "shipFlag",
            "label": "attribute"
          },
          {
            "ui": true,
            "label": "value",
            "controlElement": {
              "feature": "multiSelectGraphNode"
            }
          }
        ],
        "label": "in"
      },
      {
        "args": [
          {
            "args": [
              {
                "path": "shipName",
                "label": "attribute"
              },
              {
                "ui": true,
                "label": "value",
                "controlElement": {
                  "feature": "string"
                }
              }
            ],
            "label": "like"
          },
          {
            "args": [
              {
                "path": "shipNameLat",
                "label": "attribute"
              },
              {
                "path": "shipName",
                "label": "selfContext"
              }
            ],
            "label": "like"
          }
        ],
        "label": "or"
      },
      {
        "args": [
          {
            "path": "ownerShipData.company.name",
            "label": "attribute"
          },
          {
            "ui": true,
            "label": "value",
            "uiTitle": "Собственник",
            "controlElement": {
              "feature": "string"
            }
          }
        ],
        "label": "like"
      },
      {
        "args": [
          {
            "path": "registryPort",
            "label": "attribute"
          },
          {
            "ui": true,
            "label": "value",
            "controlElement": {
              "feature": "multiSelectGraphNode"
            }
          }
        ],
        "label": "in"
      },
      {
        "args": [
          {
            "args": [
              {
                "path": "shipKind.fishingActivityKind",
                "label": "attribute"
              },
              {
                "ui": true,
                "label": "value",
                "controlElement": {
                  "feature": "selectGraphNode"
                }
              }
            ],
            "label": "equal"
          },
          {
            "args": [
              {
                "path": "fishingActivityKind",
                "label": "attribute"
              },
              {
                "path": "fishingActivityKind",
                "label": "selfContext"
              }
            ],
            "label": "equal"
          }
        ],
        "label": "or"
      },
      {
        "args": [
          {
            "args": [
              {
                "path": "shipKind",
                "label": "attribute"
              },
              {
                "label": "value",
                "value": {
                  "id": "27a877c6-fa50-4028-b78f-6809eddb419d",
                  "name": "Типы рыболовных судов ОСМ ФАР",
                  "type": "91d5f27c-a87e-4038-a1d8-da53a2588b5e",
                  "label": "osmShipType"
                },
                "controlElement": {
                  "feature": "treeSetSelect",
                  "properties": {
                    "domElementId": "shipType"
                  }
                }
              },
              {
                "label": "value",
                "value": "withChildren",
                "controlElement": {
                  "feature": "stringSelectRadio",
                  "properties": {
                    "options": [
                      {
                        "name": "Включить подчинённые",
                        "value": "withChildren"
                      },
                      {
                        "name": "Только взаимодействующие",
                        "value": "siblingsWithChildren"
                      }
                    ]
                  }
                }
              }
            ],
            "label": "treeFilter"
          },
          {
            "args": [
              {
                "path": "shipKind",
                "label": "attribute"
              },
              {
                "label": "value",
                "value": {
                  "id": "8f610998-5890-408f-9d88-e92466fbb4fb",
                  "name": "Рыболовные типы судов от других источников",
                  "type": "91d5f27c-a87e-4038-a1d8-da53a2588b5e",
                  "label": "otherFishingShipType"
                },
                "controlElement": {
                  "feature": "treeSetSelect",
                  "properties": {
                    "domElementId": "shipType"
                  }
                }
              },
              {
                "label": "value",
                "value": "withChildren",
                "controlElement": {
                  "feature": "stringSelectRadio",
                  "properties": {
                    "options": [
                      {
                        "name": "Включить подчинённые",
                        "value": "withChildren"
                      },
                      {
                        "name": "Только взаимодействующие",
                        "value": "siblingsWithChildren"
                      }
                    ]
                  }
                }
              }
            ],
            "label": "treeFilter"
          },
          {
            "args": [
              {
                "args": [
                  {
                    "path": "osmShipForeignCode",
                    "label": "attribute"
                  }
                ],
                "label": "isNotNull"
              },
              {
                "args": [
                  {
                    "path": "osmShipForeignCode",
                    "label": "attribute"
                  },
                  {
                    "label": "value",
                    "value": "0"
                  }
                ],
                "label": "notEqual"
              }
            ],
            "label": "and"
          }
        ],
        "label": "or"
      }
    ],
    "label": "and"
  },
        "version": 22,
        "allowRowEdit": false,
        "domElementIds": [
          "shipFormZKVS"
        ]
      }
      '::jsonb
where label = 'journalAccounfishingGridZKVS';

update ui.dom_element
set
  attribute_id = '1caea3b9-4853-407c-b9b5-d238e16bb59c'
where parent_id in (
  select id
  from ui.dom_element
  where parent_id in (
    select id from ui.dom_element where label = 'journalAccounfishingGridZKVS')
)
  and attribute_id = '1a97d73d-0bb2-4c55-bca4-19a939fc4127';

select count(*) from node."dac4c525-cbd4-4942-ad89-5aefb6e8a6f9"
where props->>'1caea3b9-4853-407c-b9b5-d238e16bb59c' = '' or props->>'1caea3b9-4853-407c-b9b5-d238e16bb59c' = '0';

update node."dac4c525-cbd4-4942-ad89-5aefb6e8a6f9"
set props = props - '1caea3b9-4853-407c-b9b5-d238e16bb59c'
where props->>'1caea3b9-4853-407c-b9b5-d238e16bb59c' = '' or props->>'1caea3b9-4853-407c-b9b5-d238e16bb59c' = '0';