do
$$
declare
  threshold_date  TIMESTAMP = '2019-02-06';
  threshold_date_to  TIMESTAMP = '2019-02-15';
  reglament_table RECORD;
  counter         INTEGER = 0;
begin
  for reglament_table in
  select distinct
    r.label,
    table_name
  from sync.sync_reglament reg
    join metamodel.resource r on r.id :: TEXT = reg.table_name
  where r.label not in ('company', 'person')
  order by r.label loop
    execute format('select count(*) from node.%I where dbcreated > %L and dbcreated < %L', reglament_table.table_name, threshold_date, threshold_date_to)
    into counter;
    if (counter > 0)
    then
      raise notice '% - % records', reglament_table.label, counter;
      execute format('update node.%I set props = props where dbcreated > %L and dbcreated < %L', reglament_table.table_name, threshold_date, threshold_date_to);
    end if;
  end loop;
end;
$$;