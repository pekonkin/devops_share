CREATE OR REPLACE FUNCTION service.compute_ship_activity_kind(ship_id uuid, type0 uuid DEFAULT NULL, name0 text DEFAULT NULL, props0 jsonb DEFAULT NULL)
RETURNS text
LANGUAGE plpgsql
AS
$$
DECLARE
  ret text;
BEGIN
-- вернуть текстовый вид деятельности судна по версии ФАР, а если там не указано, то по версии ручного ввода её для судна
  SELECT activ.name INTO ret
   FROM ONLY rel."dac4c525-cbd4-4942-ad89-5aefb6e8a6f9" r_kind -- from ship to kind
   JOIN ONLY rel."91d5f27c-a87e-4038-a1d8-da53a2588b5e" r_activ ON r_activ.attribute_id = '7c0fcabc-ba58-4c65-a360-e70876050d8d' AND r_activ.from_id = r_kind.to_id -- from kind to activ
   JOIN ONLY node."d33d103c-f1d6-4bee-ad5d-1b7dcc2dc8e9" activ ON activ.id = r_activ.to_id -- activ
   WHERE r_kind.attribute_id = 'cccde464-d477-4adf-bf83-5c88d29ea459' AND r_kind.from_id = ship_id;
  IF ret is NOT NULL THEN return ret; END IF;
  RETURN (
    SELECT activ.name
     FROM ONLY rel."dac4c525-cbd4-4942-ad89-5aefb6e8a6f9"  r_activ -- from ship to activ
     JOIN ONLY node."d33d103c-f1d6-4bee-ad5d-1b7dcc2dc8e9" activ ON activ.id = r_activ.to_id -- activ
     WHERE r_activ.attribute_id = '7c0fcabc-ba58-4c65-a360-e70876050d8d' AND r_activ.from_id = ship_id
    limit 1
  );
-- SELECT service.compute_ship_activity_kind('7750b64a-8dbf-4a2c-b367-0a58bc0a19ad') AS far, service.compute_ship_activity_kind('fd3a64c4-2a93-4b85-b5e8-2584b0b1b264') AS ne_far
END; $$;