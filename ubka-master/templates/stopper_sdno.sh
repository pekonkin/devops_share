#!/bin/bash

if [ $( /etc/init.d/crond status | egrep -c '\(pid.*[0-9]\)' ) -eq 1 ]; then
   /etc/init.d/crond stop
fi

if [ -d /etc/sysconfig/starter ]; then

   if [ -e /etc/sysconfig/starter/firestarter_check.sh ]; then
   if [ $( ps aux | grep -c firestarter_check.sh ) -ge 1 ]; then
      ps aux | grep firestarter_check.sh | grep -v grep | awk '{print$2}' | xargs -I {} kill -9 {}
   fi
   fi

fi

if [ $( /etc/init.d/crond status | egrep -c '\(pid.*[0-9]\)' ) -eq 0 ]; then
   /etc/init.d/crond start
fi
