update sync.sync_reglament set out_node_order = '[["0","3"]]'::json where id in (select id from sync.sync_reglament where out_node_mask = '1'::bit(128));

update sync.sync_node set limit_rows = 200 where ip is not null or is_closed = true;

update sync.sync_node set period = 1 where ip is not null;
