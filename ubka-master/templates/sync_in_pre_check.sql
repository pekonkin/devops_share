CREATE OR REPLACE FUNCTION sync_in_pre_check()
  RETURNS trigger AS $pre$
BEGIN
  IF NEW.id is null OR NEW.node_id is null OR NEW.status_id IS NULL
  THEN
    RAISE WARNING 'no id or node_id or status_id given, skipping';
    return null;
  END IF;

  IF NEW.schema_name is null OR NEW.table_name IS NULL
  THEN
    RAISE WARNING 'no schema or table name given, skipping';
    return null;
  END IF;

  IF NEW.json_data IS NULL
  THEN
    RAISE WARNING 'inserting with empty json_data';
    NEW.json_data = '{}' :: json;
  END IF;

  IF NEW.create_date IS NULL
  THEN
    RAISE WARNING 'inserting with empty create date';
    NEW.create_date = now();
  END IF;

  IF NEW.order_num IS NULL
  THEN
    RAISE WARNING 'inserting with empty order num';
    NEW.order_num = 9001;
  END IF;

  RETURN NEW;
END;
$pre$
LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trg_sync_in_pre_check
ON sync.sync_in;

CREATE TRIGGER trg_sync_in_pre_check
  BEFORE INSERT OR UPDATE
  ON sync.sync_in
  FOR EACH ROW EXECUTE PROCEDURE sync_in_pre_check();

