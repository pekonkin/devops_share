select sync.func_set_state_trigger(false);

truncate sync.sync_in, sync.sync_out, sync.sync_out_info, sync.sync_out_resend, sync.sync_log, sync.sync_in_log, sync.sync_tree_aud;

select sync.func_set_state_trigger(true);