select *
from sync.func_set_state_trigger(false);

delete from sync.sync_tree
where reglament_id not in (select reg.id
                           from sync.sync_reglament reg
                             join metamodel.resource r on r.id :: TEXT = reg.table_name
                           where label in
                                 (
                                   --------------------
                                   --- OK
                                   --------------------
                                   'alarm',
                                   'cabotageShipJournal',
                                   'receptionJournalPrip',

                                   'mpsOutcome',
                                   'kpmShip',

                                   'hydrometeorologicalSituation',
                                   'hydrometeoForecast',
                                   'currentUnfavourableGMU',

                                   'actualIceConditions',

                                   'inspectorsDoingList',
                                   'inspectorSquadPosition',
                                   'foreignShipJournal',
                                   'shipMKP',

                                   'event',
                                   'eventInteractionPlan',
                                   'source',
                                   'planInteractionFoiv',

                                   'caseGivenNumberJournal',
                                   'admOffencesCaseJournal',
                                   'illegalCasesJournal',
                                   'admOffencesCaseJournal',

                                   'smallShip',
                                   'industryObject',
                                   'infrastructureTransportObjects',
                                   'districtRegistry',
                                   'machineList',

                                   'activityStartNotice',
                                   'withdrawalNotice',
                                   'applicationRussianExitNpgg',
                                   'noticeToStartActivities',
                                   'noticeChangeMPSData',
                                   'shipLocationNPGG',
                                   'registrMPSAndIcyTSNotice',
                                   'noticeGGRFcrossing',
                                   'otherActivityNotice',
                                   'scientificResearchActivityNotice',
                                   'vbrOverloadNotice',

                                   'npd',
                                   'tracingPaper',

                                   'passBorderZone',
                                   'childBlankBorderPass',
                                   'collectiveBlankBorderPass',

                                   'economicFishActivitySocioPoliticalCulturalEventsPermission',
                                   --'company',
                                   --'person',
                                   --'callHeader',

                                   --------------------
                                   --- ZK
                                   --------------------
                                   'eventFrontier',
                                   'csOnDutyJournal',

                                   'targetLand',
                                   'alarmLand',
                                   'frontierGuard',

                                   'aviaMovingJournal',
                                   'aviaFlightJournal',

                                   'personnelZKVS',
                                   'kks',
                                   'logisticsAvailability',
                                   'automobileChecklist',
                                   'tsogRegistrList',
                                   'tspkCatalog',
                                   'structuresRegistrList',
                                   'meansOfComunicationMain',

                                   'frontierGuardComunications',
                                   'callsignsCommunicationCenterMain',

                                   'pathOfFrontierGuard',
                                   'pdMonthPlanning',

                                   'adminProtokol',
                                   'pdMonthPlanning',
                                   'csInSeaJournal',
                                   'personnelZKVS'
                                 )
                           order by r.label);

delete from sync.sync_reglament
where id not in (select reg.id
                 from sync.sync_reglament reg
                   join metamodel.resource r on r.id :: TEXT = reg.table_name
                 where label in
                       (
                         --------------------
                         --- OK
                         --------------------
                         'alarm',
                         'cabotageShipJournal',
                         'receptionJournalPrip',

                         'mpsOutcome',
                         'kpmShip',

                         'hydrometeorologicalSituation',
                         'hydrometeoForecast',
                         'currentUnfavourableGMU',

                         'actualIceConditions',

                         'inspectorsDoingList',
                         'inspectorSquadPosition',
                         'foreignShipJournal',
                         'shipMKP',

                         'event',
                         'eventInteractionPlan',
                         'source',
                         'planInteractionFoiv',

                         'caseGivenNumberJournal',
                         'admOffencesCaseJournal',
                         'illegalCasesJournal',
                         'admOffencesCaseJournal',

                         'smallShip',
                         'industryObject',
                         'infrastructureTransportObjects',
                         'districtRegistry',
                         'machineList',

                         'activityStartNotice',
                         'withdrawalNotice',
                         'applicationRussianExitNpgg',
                         'noticeToStartActivities',
                         'noticeChangeMPSData',
                         'shipLocationNPGG',
                         'registrMPSAndIcyTSNotice',
                         'noticeGGRFcrossing',
                         'otherActivityNotice',
                         'scientificResearchActivityNotice',
                         'vbrOverloadNotice',

                         'npd',
                         'tracingPaper',

                         'passBorderZone',
                         'childBlankBorderPass',
                         'collectiveBlankBorderPass',

                         'economicFishActivitySocioPoliticalCulturalEventsPermission',
                         --'company',
                         --'person',
                         --'callHeader',

                         --------------------
                         --- ZK
                         --------------------
                         'eventFrontier',
                         'csOnDutyJournal',

                         'targetLand',
                         'alarmLand',
                         'frontierGuard',

                         'aviaMovingJournal',
                         'aviaFlightJournal',

                         'personnelZKVS',
                         'kks',
                         'logisticsAvailability',
                         'automobileChecklist',
                         'tsogRegistrList',
                         'tspkCatalog',
                         'structuresRegistrList',
                         'meansOfComunicationMain',

                         'frontierGuardComunications',
                         'callsignsCommunicationCenterMain',

                         'pathOfFrontierGuard',
                         'pdMonthPlanning',

                         'adminProtokol',
                         'pdMonthPlanning',
                         'csInSeaJournal',
                         'personnelZKVS'

                       )
                 order by r.label);

delete from sync.sync_tree
where from_table_name = '3217e8c2-4ab5-4d09-89e0-8367bd0de08f'
      or to_table_name = '3217e8c2-4ab5-4d09-89e0-8367bd0de08f';
delete from sync.sync_tree
where from_table_name = 'cb672aba-4318-47d3-814c-815930e2da76'
      or to_table_name = 'cb672aba-4318-47d3-814c-815930e2da76';

truncate sync.sync_tree_aud;
truncate sync.sync_out;
truncate sync.sync_log;
truncate sync.sync_in;
truncate sync.sync_in_log;

select *
from sync.func_set_state_trigger(true);
