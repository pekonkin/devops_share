#!/bin/bash

if [ -e /etc/init.d/webgio2 ]; then

   if [ $( ps aux | egrep webgio2 | grep -v grep | wc -l ) -ge 2 ]; then
      ps aux | egrep webgio2 | grep -v grep | awk '{print$2}' | xargs -I {} kill-9 {}
   fi

   if [ ! -e /var/run/webgio/webgio2_qt5.pid ]; then
      if [ $( ps aux | egrep webgio2 | grep -v grep | wc -l ) -ge 1 ]; then
         ps aux | egrep webgio2 | grep -v grep | awk '{print$2}' | xargs -I {} kill-9 {}
      fi
   if [ ! $( netstat -anlpt | grep webgio2 | grep -o LISTEN ) == LISTEN ]; then
      if [ $( ps aux | egrep webgio2 | grep -v grep | wc -l ) -ge 1 ]; then
         ps aux | egrep webgio2 | grep -v grep | awk '{print$2}' | xargs -I {} kill-9 {}
         /etc/init.d/webgio2 start 2>&1 > /dev/null
         fi
      fi
   fi

if [ $( /etc/init.d/webgio2 status | grep -c 'webgio2_qt5 is not running' ) -ge 1 ]; then
   if [ $(netstat -anlpt | grep -c webgio2) -ge 1 ]; then
      netstat -anlpt | grep webgio2 | awk {'print$7'} | awk -F \/ {'print$1'} | xargs kill
   fi

   rm -f /var/run/webgio/webgio2_qt5.pid
   nohup /etc/init.d/webgio2 start

fi
fi