#!/bin/bash

if [ -e /etc/init.d/webgiocalc ]; then

   if [ $( /etc/init.d/webgiocalc status | grep -c 'Webgiocalc is not running' ) -eq 1 ]; then
      if [ $( ps aux | egrep WebGio | grep -v grep | wc -l ) -ge 1 ]; then
         ps aux | egrep WebGio | grep -v grep | awk '{print$2}' | xargs -I {} kill -9 {} 2>&1 > /dev/null
      fi

      rm -f /var/run/webgiocalc/webgiocalc.pid
      nohup /etc/init.d/webgiocalc start

   fi

fi
